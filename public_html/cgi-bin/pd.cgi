#!/usr/bin/perl -w

#################################################################
# Program:	    checkwiki.cgi
# Descrition:	show all errors of Wikipedia-Project "Personendaten"
# Author:	    Stefan Kühn
# Licence:      GPL
#################################################################

our $VERSION = '2015-02-22';

use strict;
use CGI qw(param);				# Handler for Parameters in URL www.xyz.org?view=test
use CGI::Carp qw(fatalsToBrowser);
use CGI::Carp qw(fatalsToBrowser set_message); 	# CGI-Error
use HTML::Entities;
use Encode qw(encode decode);
use URI::Escape;				# Handle of URL
use LWP::UserAgent;				# Get webpages
use DBI;

set_message('There is a problem in the script. 
Send me a mail to (<a href="mailto:kuehn-s@gmx.net">kuehn-s@gmx.net</a>), 
giving this error message and the time and date of the error.');

print "Content-type: text/html\n\n";


our %parameter;	# storage for all parameter
our $dbh;

html_begin();		# Begin of HTML (CSS etc) without title
read_configfile();
open_db();


########################################################################
# Environment-Variables (program_name)
########################################################################

my $script_name = $ENV{'SCRIPT_FILENAME'}; #get scriptname from cgi

if (defined($script_name) != 1) {
	# for test local
	$script_name = $0;	# get scriptname
}
#print $script_name.' ' ;
$script_name =~ s/^.+\///g;
#print $script_name.' ' ;



########################################################################
# Parameter
########################################################################

our %parameter;							# TODO alles umstellen
our $param_view     = param('view');	# list, high, middle, low, only, detail, whitelist, delta
our $param_id 	    = param('id');	    # id of improvment
our $param_pageid   = param('pageid');	
our $param_offset   = param('offset');
our $param_limit    = param('limit');
our $param_orderby  = param('orderby');
our $param_sort     = param('sort');
our $param_from     = param('from');	# only for detail (for back)
$parameter{'whitelist'} = param('whitelist');   # do page with this error on whitelist


$param_view = '' 		unless defined $param_view;
$param_id = '' 			unless defined $param_id;
$param_pageid = '' 		unless defined $param_pageid;
$param_offset = '' 		unless defined $param_offset;
$param_limit  = '' 		unless defined $param_limit;
$param_orderby = '' 	unless defined $param_orderby;
$param_sort = '' 		unless defined $param_sort;
$param_from = '' 		unless defined $param_from;
$parameter{'whitelist'} = '' unless defined $param_from;


if ($param_offset =~ /^[0-9]+$/) {} else {
	$param_offset = 0;
}

if ($param_limit =~ /^[0-9]+$/) {} else {
	$param_limit = 25;
}
$param_limit = 500 if ($param_limit > 500);
our $offset_lower = $param_offset - $param_limit;
our $offset_higher = $param_offset + $param_limit;
	$offset_lower = 0 if ($offset_lower < 0);
our  $offset_end =  $param_offset + $param_limit;



########################################################################
# Order-By-Clausel
########################################################################

our $column_orderby = '';
our $column_sort = '';
if ($param_orderby ne ''){
	if (    $param_orderby ne 'article'
		and $param_orderby ne 'notice'
		and $param_orderby ne 'found'
		and $param_orderby ne 'more'
		and $param_orderby ne 'todo'
		and $param_orderby ne 'ok'
		and $param_orderby ne 'error_id'
		and $param_orderby ne 'whitelist'
	) {
		$param_orderby = '';
	}
}

if ($param_sort ne '') {
	if (   $param_sort ne 'asc'
		and $param_sort ne 'desc') {
		$column_sort = 'asc' 
	} else {
		$column_sort = 'asc'  if ($param_sort eq 'asc');
		$column_sort = 'desc'  if ($param_sort eq 'desc');
	}
}



########################################################################
# Do update and delete in Database
########################################################################

if (    $param_view =~ /^(detail|only|whitelist|delta)$/
	and $param_pageid =~ /^[0-9]+$/
	and $param_id     =~ /^[0-9]+$/ ) {
	# Artikel abgearbeitet
	my $sql_text ='';
	my $sql_text2 ='';
	my $sql_text3 ='';
	if ($param_id != 0) {

		if ($parameter{'whitelist'} eq 'yes'){
			# insert page on whitelist and update pd_error
			$sql_text  = "update whitelist set notice = (select distinct notice from pd_error where page_id=".$param_pageid." and error_id=".$param_id.")
 where page_id=".$param_pageid." and error_id=".$param_id.";";

			# only insert whitelist, when not in whiteliste
			$sql_text2 = "insert into whitelist 
select e.* from pd_error e
left outer join whitelist w
on (w.page_id = e.page_id and w.error_id = e.error_id)
where e.error_id = ".$param_id."
and e.page_id = ".$param_pageid."
and w.page_id is null;";
			$sql_text3 = "update pd_error set whitelist = 1 where page_id=".$param_pageid." and error_id=".$param_id.";";

		} elsif ($parameter{'whitelist'} eq 'no') {
			# delete page from whitelist and update pd_error
			$sql_text = "delete from whitelist where page_id=".$param_pageid." and error_id=".$param_id.";";
			$sql_text2 = "update pd_error set whitelist = NULL where page_id=".$param_pageid." and error_id=".$param_id.";";

		} else {
			# only one error set done
			$sql_text = "update pd_error set done=1 where page_id=".$param_pageid." and error_id=".$param_id.";";
		}


	} else { 
		# all errors (error_id = 0) set done;
		$sql_text = "update pd_error set done=1 where page_id=".$param_pageid.";";
	}		
	
	#print $sql_text.'<br />';
	#print $sql_text2.'<br />';
	#print $sql_text3.'<br />';
	#my $sth = $dbh->prepare( $sql_text )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
	#$sth->execute or die $sth->errstr; # hier geschieht die Anfrage an die DB	
	$dbh->do($sql_text);	
	$dbh->do($sql_text2) if ($sql_text2 ne '');
	$dbh->do($sql_text3) if ($sql_text3 ne '');
}




########################################################################
# Build HTML-Page
########################################################################

my $html_body = '';

#print '<p><span style="background-color:yellow;">Im Moment wird gerade am Skript gearbeitet. Daher kann es zu Ausfällen kommen. - Stefan</span></p>'."\n";


# Startseite
if ($param_view eq ''){
	$html_body = get_html_startpage();
}


# Alle aus einer Priorität
if ($param_view eq 'high' or $param_view eq 'middle' or $param_view eq 'low' or $param_view eq 'all'){
	$html_body = get_html_page_priority();
}


# Alles über eine konkrete Fehlernummer auflisten
if (	$param_view =~ /^(only(done)?|whitelist|delta)$/   
	and $param_id =~ /^[0-9]+$/) {
	$html_body = get_html_page_of_one_error();
}


# Detailseite eines Artikels
if (    $param_view eq 'detail'
	and $param_pageid =~ /^[0-9]+$/ ) {
	$html_body = get_html_page_for_one_article();
}


# Liste aller Vorschläge
if ( $param_view eq 'list') {
	print '<p>→ <a href="'.$script_name.'">Startseite</a> → Liste</p>'."\n";	
	print get_html_list();	
}	


# Title
my $title = 'PD Wartung';

# Subtitle
if ($parameter{'html_subtitle'} ne ''){
	$title .= ' - '.$parameter{'html_subtitle'};
}
print '<title>'.$title.'</title></head>'."\n";


print "<body><h1>PD Wartung</h1>\n";
print $html_body;

# Close datenbase
$dbh->disconnect();

html_foot();

#END






#######################################################################
#######################################################################
# Subroutines
#######################################################################
#######################################################################



################################################################	
# Read configfile 
################################################################	

sub read_configfile{
	# read_configfile for global settings (DB,Schemata,Password,dumppath...)

	# open configfile
	my $config_file = '../../config.txt';
	open(CONFIG, "<$config_file") or die "cannot open < $config_file\n$!";


	# read configfile line by line
	do {
		my $line = <CONFIG>;

		
		if ($line =~ /=/ ) {
			$line =~ s/\n//g;  
			$line =~ s/\t/ /g; #tabulator to space
			my $key = substr($line, 0, index($line,'=')-1);
			my $value = substr($line, index($line,'=')+1);

			$key   =~ s/^\s+|\s+$//g;	# remove space at begin and end
			$value =~ s/^\s+|\s+$//g;

			if ($key ne ''){
				$parameter{$key} = $value;	# save in hash parameter
			}
			
		}
	}
	while (eof(CONFIG) != 1);

	# close configfile
	close(CONFIG);

	# print all parameter
	foreach my $name(sort keys %parameter){
		# printf ("%15s = %s\n", $name ,$parameter{$name}) ; 
	}
	#print "\n";
	#die;
}



##############################################
# Open database
##############################################

sub open_db{
	
	#Connect to database u_sk
	#print "\n".'open database: '.$parameter{'db_database'}."\n";

	$dbh = DBI->connect( "DBI:mysql:$parameter{'db_database'};$parameter{'host'}",  # local
							$parameter{'db_user'},
							$parameter{'db_password'} ,
							{
								RaiseError => 1,
								AutoCommit => 1,
								mysql_enable_utf8 => 1
							}
	  ) or die "Database connection not made: $DBI::errstr" . DBI->errstr;
	
}



##############################################
# HTML_Begin
##############################################

sub html_begin{
print <<HTML_HEAD;
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<head>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<style type="text/css">
body {  
	
	font-family: Verdana, Tahoma, Arial, Helvetica, sans-serif;
	font-size:14px;
	font-style:normal;
	
	/* color:#9A91ff; */
	
	/* background-color:#00077A; */
	/* background-image:url(back.jpg); */
	/* background:url(back_new.jpg) no-repeat fixed top center; */
	/* background:url(../images/back_new.jpg) no-repeat fixed top center; */
	/* background:url(../images/back_schaf2.jpg) no-repeat fixed bottom left; */
	/* background:url(../images/back_schaf3.jpg) no-repeat fixed bottom left; */
	
	background-color:white;
	color:#555555;
	text-decoration:none; 
	line-height:normal; 
	font-weight:normal; 
	font-variant:normal; 
	text-transform:none; 
	margin-left:5%;
	margin-right:5%;
	}
	
h1	{
	/*color:red; */
	font-size:20px;
	}

h2	{
	/*color:red; */
	font-size:16px;
	}
	
a 	{  

	/*nur blau */
	/* color:#80BFBF; */ 
	/* color:#4889c5; */ 
	/* color:#326a9e; */  
	
	color:#4889c5;
	font-weight:bold;
	/*nettes grün */
	/*color:#7CFC00;*/
	
	/* Nette Kombination */
	/*color:#00077A; */
	/*background-color:#eee8fd;*/
	
	
	/* Kein Unterstrich */
	text-decoration:none; 
	
	/*Außenabstand*/
	/*padding:2px;*/
	}

a:hover {  
	background-color:#ffdeff;
	color:red;
	}
	
.nocolor{  
	background-color:white;
	color:white;
	} 
	
a:hover.nocolor{  
	background-color:white;
	color:white;
	}
	
.table{
	font-size:12px; 

	vertical-align:top;

	border-width:thin;
  	border-style:solid;
  	border-color:blue;
  	background-color:#EEEEEE;
  	
  	/*Innenabstand*/
	padding-top:2px;
	padding-bottom:2px;
	padding-left:5px;
	padding-right:5px;
  	
  	/* dünne Rahmen */
  	border-collapse:collapse; 
	
	/*kein Zeilenumbruch
	white-space:nowrap;*/
  	
  	}
	
</style>
HTML_HEAD


}



##############################################################################
# HTML-END
##############################################################################

sub html_foot{
	print '<p><span style="font-size:10px;">Autor: <a href="https://de.wikipedia.org/wiki/Benutzer:Stefan Kühn">Stefan Kühn</a> · 
	<a href="https://de.wikipedia.org/wiki/Wikipedia:Personendaten/Wartung/Fehlerliste">Projektseite</a> · 
	<a href="https://de.wikipedia.org/wiki/Hilfe_Diskussion:Personendaten/Wartung/Fehlerliste">Kommentare und Meldungen</a><br />';
	print 'Version '.$VERSION.' · 
	license: <a href="http://www.gnu.org/copyleft/gpl.html">GPL</a> · 
	Powered by <a href="https://tools.wmflabs.org/">Wikimedia Tool Labs</a></span></p>'."\n";

	print '</body>';
	print '</html>';
}



##############################################################################
# Startpage
##############################################################################

sub get_html_startpage{
	my $html = '';
	
	$parameter{'html_subtitle'} = 'Startseite';
	$html .= '<p>→ Startseite</p>'."\n";
	# Einleitungstext			
	$html .= '<p>Auf dieser Seite werden Verbesserungsvorschläge für die Personendaten in der deutschsprachigen Wikipedia angezeigt.  Für weitere Informationen am besten mal hier reinschauen: <a href="http://de.wikipedia.org/wiki/Wikipedia:Personendaten/Wartung/Fehlerliste">Wikipedia:Personendaten/Wartung/Fehlerliste</a>.</p>';



	# Aktueller Stand
	my $nr_all = get_number_all_pd(); # Select count(*) dauert sehr lange
	my $nr_all_pd_with_error = get_number_all_article_with_pd_error();
	my $prozent = int( ($nr_all_pd_with_error / ($nr_all / 100))*1000 ) / 1000;

	$html .= '<p>';
	$html .= 'Aktuell sind dem Skript ca. <b>'.$nr_all.'</b> Artikel mit Personendaten bekannt.<br/>';
	$html .= 'Davon wurden in <b>'.$nr_all_pd_with_error.'</b> Artikeln (<b>'.$prozent.'%</b>) insgesamt <b>'.get_number_all_errors().'</b> mögliche Verbesserungsvorschläge gefunden.<br/>';
	$html .= '</p>';


	# Tabelle der Startseite
	$html .= '<p>Aktuelle Vorschläge:<br /><table class="table">';
	$html .= '<tr><th class="table">&nbsp;</th><th class="table">Vorschläge</th><th class="table">Erledigt</th></tr>'."\n";
	$html .= get_number_of_prio();
	$html .= '</table></p>';



	# weitere Listen
	$html .= '<p>Weitere Listen:<br />';
	$html .= '<ul>';
	$html .= '<li><a href="'.$script_name.'?view=list" >Alle Artikel mit Vorschlägen</a></li>';
#	$html .= '<li>Aussortieren der Kategorien (TODO)</li>';
#	$html .= '<li>Aussortieren der Vornamen (TODO)</li>';
	$html .= '</ul>';
	$html .= '</p>';

	$html .= '<p>';	
	my $new_timestamp = get_newest_pd_error_timestamp();
	my @new_split = split(/ /, $new_timestamp);
	$html .= 'Am <b>'.$new_split[0].'</b> um <b>'.$new_split[1].'</b> UTC wurde der neuste Vorschlag gefunden.<br />'."\n";

	my $old_timestamp = get_oldest_pd_error_timestamp();
        my @old_split = split(/ /, $old_timestamp);
        $html .= 'Am <b>'.$old_split[0].'</b> um <b>'.$old_split[1].'</b> UTC wurde der älteste Vorschlag gefunden.<br />'."\n";

	$html .= '</p>';
	return ($html);
}



###################################################################
# HTML-Page für Priorität (z.B. Hohe Priorität)
###################################################################

sub get_html_page_priority{
	my $html = '';
	my $prio = 0;
	my $headline = '';
	if ($param_view eq 'high') {
		$prio = 1;
		$headline = 'Hohe Priorität';
	}
	
	if ($param_view eq 'middle') { 
		$prio = 2;
		$headline = 'Mittlere Priorität';
	}
	
	if ($param_view eq 'low') {
		$prio = 3;
		$headline = 'Niedrige Priorität';
	}
	
	if ($param_view eq 'all') {
		$prio = 0;
		$headline = 'Alle Prioritäten';
	}

	$parameter{'html_subtitle'} = $headline;
	#print '<h2>'.$headline.'</h2>'."\n";	
	$html .= '<p>→ <a href="'.$script_name.'">Startseite</a> → '.$headline.'</p>'."\n";	
	
	$html .= '<p>Prioritäten: ';
	$html .= '<a href="'.$script_name.'?view=all">Alle</a> - ';
	$html .= '<a href="'.$script_name.'?view=high">Hoch</a> - ';
	$html .= '<a href="'.$script_name.'?view=middle">Mittel</a> - ';
	$html .= '<a href="'.$script_name.'?view=low">Niedrig</a>';

	
	$html .= get_number_error_and_desc_by_prio($prio);
	
	return($html);
}



########################################################################
# HTML-Page einen Fehler (z.B. Nr.123 only, onlydone, whitelist, delta)
########################################################################

sub get_html_page_of_one_error{	
	# Ein konkrete Fehlernummer auflisten
	my $html = '';
	my $headline = '';
	$headline = get_headline($param_id);
	$parameter{'html_subtitle'} = $headline;

	my $prio = get_prio_of_error($param_id);
	$prio = '<a href="'.$script_name.'?view=high">Höchster Priorität</a>' if ($prio eq '1');
	$prio = '<a href="'.$script_name.'?view=middle">Mittlere Priorität</a>' if ($prio eq '2');
	$prio = '<a href="'.$script_name.'?view=low">Niedrigste Priorität</a>' if ($prio eq '3');
	$html .= '<p>→ <a href="'.$script_name.'">Startseite</a> → '.$prio.' → '.$headline.'</p>'."\n";	

	$html .= '<p><b>Vorschlag Nr. '.$param_id.':</b> '.get_description($param_id).'</p>'."\n";
	#print '<p>'.get_number_of_error($param_id).' mal gefunden (ID '.$param_id .')</p>'."\n"; #TODO Einblenden

	

	# Menu	
	if ($param_view =~ /^(only|onlydone|whitelist|delta)$/ ) {
		$html .= 'Zeige: ';

		# todo
		if ($param_view =~ /^only$/ ) {
		    $html .= '<b>To-Do-Artikel</b> - ';
			$parameter{'html_subtitle'} .= ' - Todo'; 
		} else {
			$html .= '<a href="'.$script_name.'?&view=only&id='.$param_id.'">To-Do-Artikel</a> - ';
		}

		# done
		if ($param_view =~ /^onlydone$/ ) {
			$html .= '<b>Erledigte Artikel</b> - ';
			$parameter{'html_subtitle'} .= ' - Done';
		} else {
			$html .= '<a href="'.$script_name.'?view=onlydone&id='.$param_id.'">Erledigte Artikel</a> - ';
		}

		# whitelist
		if ($param_view =~ /^whitelist$/ ) {
			$html .= '<b>Whitelist</b> - ';
			$parameter{'html_subtitle'} .= ' - Whitelist';
		} else {
			$html .= '<a href="'.$script_name.'?view=whitelist&id='.$param_id.'">Whitelist</a> - ';
		}

		#delta
		if ($param_view =~ /^delta$/ ) {
			$html .= '<b>Delta</b>';
			$parameter{'html_subtitle'} .= ' - Delta';
		} else {
			$html .= '<a href="'.$script_name.'?view=delta&id='.$param_id.'">Delta</a>';
		}	

		# Hinweis
		
		$html .= '<p>';
		$html .= 'In den hier gelisteten Artikeln wurde dieser Verbesserungsvorschlag gefunden.' if ($param_view =~ /^only$/ );
		$html .= 'In den hier gelisteten Artikeln wurde dieser Verbesserungsvorschlag schon erledigt.' if ($param_view =~ /^onlydone$/ );
		$html .= '<span style="background-color:yellow;">Betaversion</span>: Die hier gelisteten Artikel wurden von Menschen aussortiert und auf die Whiteliste gesetzt. Das Skript hatte hier jeweils einen falschen Vorschlag erkannte.' if ($param_view =~ /^whitelist$/ );
		$html .= '<span style="background-color:yellow;">Betaversion</span>: Die hier gelisteten Artikel wurden auf der Whiteliste eingetragene, aber das Skript hat nun im Artikel der Wikipedia eine Änderung entdeckt. Diese Veränderungen kann dazu führen, dass der Artikel nicht mehr auf der Whitlist stehen muss oder das der Eintrag in der Whitlist angepasst werden muss.' if ($param_view =~ /^delta$/ );
		$html .= '</p>';

		# Tabelle
		$html .= get_article_of_error($param_id)           if ($param_view =~ /^only$/ );
		$html .= get_done_article_of_error($param_id)      if ($param_view =~ /^onlydone$/ );
		$html .= get_whitelist_article_of_error($param_id) if ($param_view =~ /^whitelist$/ );
		$html .= get_delta_article_of_error($param_id)     if ($param_view =~ /^delta$/ );
	}
	return($html);
}



########################################################################
# HTML-Page für genau einen Artikel (detail)
########################################################################

sub get_html_page_for_one_article{
	my $html = '';
	
	# Details zu einem Artikel anzeigen
	#print '<h2>Detailansicht</h2>'."\n";
	$html .= '<p>→ <a href="'.$script_name.'">Startseite</a> → ';
	
	my $headline = '';
	if ($param_from =~ /^[0-9]+$/) {
		$headline = get_headline($param_from);
		my $prio = get_prio_of_error($param_from);
		$prio = '<a href="'.$script_name.'?view=high">Höchster Priorität</a>' if ($prio eq '1');
		$prio = '<a href="'.$script_name.'?view=middle">Mittlere Priorität</a>' if ($prio eq '2');
		$prio = '<a href="'.$script_name.'?view=low">Niedrigste Priorität</a>' if ($prio eq '3');
		$html .= $prio.' → <a href="'.$script_name.'?view=only&id='.$param_from.'">'.$headline.'</a>'."\n";
	} else {
		$html .= '<a href="'.$script_name.'?view=list">Liste</a>'."\n";	

	}
	$html .= ' → Detailansicht </p>'."\n";

	my $sql_text = "select distinct title from pd_error where page_id=".$param_pageid.";";
	my $sth = $dbh->prepare( $sql_text )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
	$sth->execute or die $sth->errstr; 	
	while (my @array = $sth->fetchrow_array()) {
	    my ($title) = @array;
		$title = encode_entities($title);
		$title = '' unless defined $title;
		if ($title ne '') {
			$parameter{'html_subtitle'} = $title;
			$html .= '<p>Artikel: <a target="_blank" href="http://de.wikipedia.org/wiki/'.$title.'">'.$title.'</a> - <a target="_blank" href="http://de.wikipedia.org/w/index.php?title='.$title.'&action=edit&summary=[[Wikipedia:Personendaten/Wartung/Fehlerliste|PD-Wartung]]">edit</a>';

			# set all done
        	$html .= ' - <a href="'.$script_name.'?view=detail&id=0&pageid='.$param_pageid.'">Erledigt</a> (alles)';
			$html .= '</p>'."\n";
		}
	}

	# all errors of this article
	$html .= get_all_error_of_article($param_pageid);
	
}


###################################################################
# Anzahl der Artikel mit Verbesserungsvorschlägen 
###################################################################

sub get_number_all_article_with_pd_error{
	my $sql_text = "select count(a.page_id) from (select page_id from pd_error where done=0 and whitelist is null group by page_id) a;";
	my $result = 0;
	my $sth = $dbh->prepare( $sql_text )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
	#print '<p class="smalltext"/>'.$sql_text."</p>\n";					  
	$sth->execute or die $sth->errstr; # hier geschieht die Anfrage an die DB
	while (my $arrayref = $sth->fetchrow_arrayref()) {	
		foreach(@$arrayref) {
			$result = $_;
			$result = '' unless defined $result;
		}
	}
	return ($result);
}



###################################################################
# Anzahl der erledigten Artikel
###################################################################

sub get_number_of_ok{
	my $sql_text = "select count(*) from pd_error where done=1;";
	my $result = 0;
	my $sth = $dbh->prepare( $sql_text )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
	#print '<p class="smalltext"/>'.$sql_text."</p>\n";					  
	$sth->execute or die $sth->errstr; # hier geschieht die Anfrage an die DB
	while (my $arrayref = $sth->fetchrow_arrayref()) {	
		foreach(@$arrayref) {
			$result = $_;
			$result = '' unless defined $result;
		}
	}
	return ($result);
}


###################################################################
# Anzahl der Artikel mit Personendaten
###################################################################

#TODO: Gesamtzahl einmal täglich ermitteln und als Parameter ablegen
sub get_number_all_pd{
#	my $sql_text = "select count(*) from pd;";
#	my $result = 0;
#	my $sth = $dbh->prepare( $sql_text )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
#	#print '<p class="smalltext"/>'.$sql_text."</p>\n";					  
#	$sth->execute or die $sth->errstr; # hier geschieht die Anfrage an die DB
#	while (my $arrayref = $sth->fetchrow_arrayref()) {	
#		foreach(@$arrayref) {
#			$result = $_;
#			$result = '' unless defined $result;
#		}
#	}
#	return ($result);



	my $filename = 'numberOfEntrys.txt';
        my $entryCount = 0;

	open(FH, '<', $filename) or die $!;
	while(<FH>){
	   $entryCount = $_;
	   # print("\n---".$entryCount."---\n");
	}
	close(FH);

	return($entryCount);
}



###################################################################
# Anzahl der Artikel mit Verbesserungsvorschlägen
###################################################################

sub get_number_all_errors{
	my $sql_text = "select count(*) from pd_error where done=0 and whitelist is null ;";
	my $result = 0;
	my $sth = $dbh->prepare( $sql_text )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
	#print '<p class="smalltext"/>'.$sql_text."</p>\n";					  
	$sth->execute or die $sth->errstr; # hier geschieht die Anfrage an die DB
	while (my $arrayref = $sth->fetchrow_arrayref()) {	
		foreach(@$arrayref) {
			$result = $_;
			$result = '' unless defined $result;
		}
	}
	return ($result);
}



###################################################################
# Anzahl der Artikel für einen Vorschlag
###################################################################

sub get_number_of_error{
	# Anzahl gefundenen Vorkommen eines Fehlers
	my $error = shift;
	my $sql_text = "select count(*) from pd_error where done=0 and whitelist is null and error_id = ".$error.";";
	my $result = 0;
	my $sth = $dbh->prepare( $sql_text )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
	#print '<p class="smalltext"/>'.$sql_text."</p>\n";					  
	$sth->execute or die $sth->errstr; # hier geschieht die Anfrage an die DB
	while (my $arrayref = $sth->fetchrow_arrayref()) {	
		foreach(@$arrayref) {
			$result = $_;
			$result = '' unless defined $result;
		}
	}
	return ($result);
}



###################################################################
# ältesten Zeitstempel aller Fehler
###################################################################

sub get_oldest_pd_error_timestamp{
	# gibt den ältesten Zeitstempel aller Fehler zurück
	my $sql_text = "select min(found) from pd_error WHERE whitelist IS NULL AND done=0;";
	my $result = 0;
	my $sth = $dbh->prepare( $sql_text )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
	#print '<p class="smalltext"/>'.$sql_text."</p>\n";					  
	$sth->execute or die $sth->errstr; # hier geschieht die Anfrage an die DB
	while (my $arrayref = $sth->fetchrow_arrayref()) {	
		foreach(@$arrayref) {
			$result = $_;
			$result = '' unless defined $result;
		}
	}
	return ($result);
}



###################################################################
# neuster Zeitstempel aller Fehler
###################################################################

sub get_newest_pd_error_timestamp{
	# gibt den neusten Zeitstempel aller Fehler zurück
	my $sql_text = "select max(found) from pd_error;";
	my $result = 0;
	my $sth = $dbh->prepare( $sql_text )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
	#print '<p class="smalltext"/>'.$sql_text."</p>\n";					  
	$sth->execute or die $sth->errstr; # hier geschieht die Anfrage an die DB
	while (my $arrayref = $sth->fetchrow_arrayref()) {	
		foreach(@$arrayref) {
			$result = $_;
			$result = '' unless defined $result;
		}
	}
	return ($result);
}




###################################################################
# Anzahl der Fehler jeder Priorität für Liste auf Startseite
###################################################################

sub get_number_of_prio{
	# Alle vorrätigen Fehler einer Priorität
	my $sql_text = "
	select a1.anzahl, a1.prio, IFNULL(b1.anzahl,0) erledigt  from (
		select count(*) anzahl, b.prio prio from pd_error a 
        join pd_error_desc b on ( a.error_id = b.error_id) where a.done=0 and whitelist is null group by b.prio
	) a1
	left outer join (
		select count(*) anzahl, b.prio prio from pd_error a 
        join pd_error_desc b on ( a.error_id = b.error_id) where a.done=1 group by b.prio
	) b1
	on (a1.prio = b1.prio) order by a1.prio;";
	my $result = '';
	my $sth = $dbh->prepare( $sql_text )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
	#print '<p class="smalltext"/>'.$sql_text."</p>\n";					  
	$sth->execute or die $sth->errstr; # hier geschieht die Anfrage an die DB
	my $sum_of_all = 0;
	my $sum_of_all_ok = 0;
	while (my $arrayref = $sth->fetchrow_arrayref()) {	
		my @output;
		my $i = 0;
		my $j = 0;
		foreach(@$arrayref) {
			#print $_."\n";
			$output[$i][$j] = $_;
			$output[$i][$j] = '' unless defined $output[$i][$j];
			$j = $j +1;
			if ($j == 3) {
				$j= 0;
				$i ++;
			}
			#print $_."\n";
		}
		my $number_of_error = $i;

		$result .= '<tr><td class="table" align="right"><a href="/cgi-bin/'.$script_name.'?view=';
		$result .= 'high">höchste Priorität' if ($output[0][1] == 1);
		$result .= 'middle">mittlere Priorität' if ($output[0][1] == 2);
		$result .= 'low">niedrigste Priorität' if ($output[0][1] == 3);
		$result .= '</a></td><td class="table" align="right"  valign="middle">'.$output[0][0].'</td><td class="table" align="right"  valign="middle">'.$output[0][2].'</td></tr>'."\n";
		$sum_of_all = $sum_of_all + $output[0][0];
		$sum_of_all_ok = $sum_of_all_ok + $output[0][2];
		if ($output[0][1] == 3) {
			# summe -> all priorities
			my $result2 .= '<tr><td class="table" align="right"><a href="/cgi-bin/'.$script_name.'?view=';
			$result2 .= 'all">alle Prioritäten';
			$result2 .= '</a></td><td class="table" align="right"  valign="middle">'.$sum_of_all.'</td><td class="table" align="right"  valign="middle">'.$sum_of_all_ok.'</td></tr>'."\n";
			#$result2 .= '<tr><td class="table" align="right">&nbsp;</td><td class="table" align="right"  valign="middle">&nbsp;</td><td class="table" align="right"  valign="middle">&nbsp;</td></tr>'."\n";
			$result = $result2.$result;
		}
		
	}
	return ($result);
}



##########################################################################
# Anzahl der erledigten Fehler jeder Priorität für Liste auf Startseite
##########################################################################

sub get_number_of_ok_of_prio{
	# Alle abgarbeiteten fehler einer Priorität
	my $prio = shift;
	my $sql_text = "select count(*) from pd_error a join pd_error_desc b on ( a.error = b.id) where b.prio = ".$prio." and done=1;";
	my $result = 0;
	my $sth = $dbh->prepare( $sql_text )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
	#print '<p class="smalltext"/>'.$sql_text."</p>\n";					  
	$sth->execute or die $sth->errstr; # hier geschieht die Anfrage an die DB
	while (my $arrayref = $sth->fetchrow_arrayref()) {	
		foreach(@$arrayref) {
			$result = $_;
			$result = '' unless defined $result;
		}
	}
	return ($result);
}


##########################################################################
# HTML Tabelle für Prioritäten-Seite
##########################################################################

sub get_number_error_and_desc_by_prio{
	my $prio = shift;

	my $order_column = 'a.name';
	$order_column = 'b.anzahl'      if ($param_orderby eq 'todo');
	$order_column = 'c.anzahl'      if ($param_orderby eq 'ok');
	$order_column = 'a.error_id'    if ($param_orderby eq 'error_id');
	$order_column = 'd.anzahl'      if ($param_orderby eq 'whitelist');
	my $order_asc = 'asc';
	$order_asc    = 'desc'          if ($param_sort eq 'desc');

	my $sql_text = "
  select IFNULL(b.anzahl, '') todo, IFNULL(c.anzahl, '') ok, a.name, a.error_id, IFNULL(d.anzahl, '') whitelist 
  from ( select name, error_id from pd_error_desc where prio = ".$prio." ) a

  left outer join (	
       select count(*) anzahl, todo.error_id 
       from pd_error todo where todo.done=0 and whitelist is null group by todo.error_id ) b 
  on (a.error_id = b.error_id)

  left outer join (	
       select count(*) anzahl, ok.error_id
       from pd_error ok where ok.done=1   group by ok.error_id)    c 
  on (a.error_id = c.error_id)

  left outer join (	
       select count(*) anzahl, wl.error_id   
       from whitelist wl group by wl.error_id) d 
  on (a.error_id = d.error_id)

  order by ".$order_column.' '.$order_asc.', a.name;';
  

	if ($prio == 0) {
		# show all priorities
		  $sql_text = "select IFNULL(b.anzahl, '') todo, IFNULL(c.anzahl, '') ok, a.name, a.error_id, IFNULL(d.anzahl, '') whitelist from ( select name, error_id from pd_error_desc where length( name )>1 ) a
		  left outer join (	select count(*) anzahl, todo.error_id from pd_error todo where todo.done=0 and whitelist is null group by todo.error_id ) b on (a.error_id = b.error_id)
		  left outer join (	select count(*) anzahl, ok.error_id   from pd_error ok   where ok.done=1       group by ok.error_id)    c on (a.error_id = c.error_id)
		  left outer join (	select count(*) anzahl, wl.error_id   from whitelist wl                        group by wl.error_id)    d on (a.error_id = d.error_id)
		  order by ".$order_column.' '.$order_asc.', a.name;';
	}  
  
	my $result = '';
	my $sth = $dbh->prepare( $sql_text )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
	#print '<p class="smalltext"/>'.$sql_text."</p>\n";					  
	$sth->execute or die $sth->errstr; # hier geschieht die Anfrage an die DB
	$result .= '<table class="table">';
	$result .= '<tr>';
	$result .= '<th class="table">Anzahl';
	$result .= '<a href="'.$script_name.'?view='.$param_view.'&orderby=todo&sort=asc">↑</a>';
	$result .= '<a href="'.$script_name.'?view='.$param_view.'&orderby=todo&sort=desc">↓</a>';
	$result .= '</th>';
	$result .= '<th class="table">Erledigt';
	$result .= '<a href="'.$script_name.'?view='.$param_view.'&orderby=ok&sort=asc">↑</a>';
	$result .= '<a href="'.$script_name.'?view='.$param_view.'&orderby=ok&sort=desc">↓</a>';
	$result .= '</th>';
	$result .= '<th class="table">Beschreibung';
	$result .= '<a href="'.$script_name.'?view='.$param_view.'&orderby=name&sort=asc">↑</a>';
	$result .= '<a href="'.$script_name.'?view='.$param_view.'&orderby=name&sort=desc">↓</a>';
	$result .= '</th>';
	$result .= '<th class="table">ID';
	$result .= '<a href="'.$script_name.'?view='.$param_view.'&orderby=error_id&sort=asc">↑</a>';
	$result .= '<a href="'.$script_name.'?view='.$param_view.'&orderby=error_id&sort=desc">↓</a>';
	$result .= '</th>';
	$result .= '<th class="table">Whitelist';
	$result .= '<a href="'.$script_name.'?view='.$param_view.'&orderby=whitelist&sort=asc">↑</a>';
	$result .= '<a href="'.$script_name.'?view='.$param_view.'&orderby=whitelist&sort=desc">↓</a>';
	$result .= '</th>';
	$result .= '</tr>'."\n";
	while (my @array = $sth->fetchrow_array()) {
	    my ($todo, $done, $error_name, $error_id, $whitelist) = @array;
		$error_name  = encode_entities($error_name);
		$result .= '<tr>';
		$result .= '<td class="table" align="right"  valign="middle">'.$todo.'</td>';
		$result .= '<td class="table" align="right"  valign="middle">'.$done.'</td>';
		$result .= '<td class="table"><a href="'.$script_name.'?view=only&id='.$error_id.'">'.$error_name.'</a></td>';
		$result .= '<td class="table" align="right"  valign="middle">'.$error_id.'</td>';
		$result .= '<td class="table" align="middle"  valign="middle">'.$whitelist.'</td>';
		$result .= '</tr>'."\n";	

	}
	$result .= '</table>';
	return ($result);
}



########################################
# Get headline of one eror
########################################

sub get_headline{
	my $error = shift;
	my $sql_text = " select name from pd_error_desc where error_id = ".$error.";";
	my $result = '';
	my $sth = $dbh->prepare( $sql_text )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
	#print '<p class="smalltext"/>'.$sql_text."</p>\n";					  
	$sth->execute or die $sth->errstr; # hier geschieht die Anfrage an die DB
	while (my $arrayref = $sth->fetchrow_arrayref()) {	
		foreach(@$arrayref) {
			$result = encode_entities($_);
			$result = '' unless defined $result;
		}
	}
	return ($result);
}



########################################
# Get description of one eror
########################################

sub get_description{
	my $error = shift;
	my $sql_text = " select notice from pd_error_desc where error_id = ".$error.";";
	my $result = '';
	my $sth = $dbh->prepare( $sql_text )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
	#print '<p class="smalltext"/>'.$sql_text."</p>\n";					  
	$sth->execute or die $sth->errstr; # hier geschieht die Anfrage an die DB
	while (my $arrayref = $sth->fetchrow_arrayref()) {	
		foreach(@$arrayref) {
			$result = $_;
			$result = encode_entities($result);
			$result = '' unless defined $result;

			$result =~ s/&lt;/</g;
            $result =~ s/&gt;/>/g;
            $result =~ s/&quot;/"/g;
		}
	}
	return ($result);
}



########################################
# Get priority of one eror
########################################

sub get_prio_of_error{
	my $error = shift;
	my $sql_text = " select prio from pd_error_desc where error_id = ".$error.";";
	my $result = '';
	my $sth = $dbh->prepare( $sql_text )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
	#print '<p class="smalltext"/>'.$sql_text."</p>\n";					  
	$sth->execute or die $sth->errstr; # hier geschieht die Anfrage an die DB
	while (my $arrayref = $sth->fetchrow_arrayref()) {	
		foreach(@$arrayref) {
			$result = encode_entities($_);
			$result = '' unless defined $result;
		}
	}
	return ($result);
}



########################################
# List with articles of one error
########################################

sub get_article_of_error{
	my $error = shift;
	my $view  = 'only';
	
	$column_orderby = '' if ( $column_orderby eq '');	
	$column_orderby = 'order by a.title' if ($param_orderby eq 'article');
	$column_orderby = 'order by a.notice' if ($param_orderby eq 'notice');
	$column_orderby = 'order by more' if ($param_orderby eq 'more');
	#$column_orderby = 'a.found' if ($param_orderby eq 'found');	
	$column_sort    = 'asc' if ( $column_sort  eq '');	
	$column_orderby = $column_orderby .' '. $column_sort.' , a.title asc' if ($column_orderby ne '');
	
	my $sql_text =	 "
		select a.title, a.notice , a.page_id, count(*) more from (	select title, notice, page_id from pd_error where error_id=".$error." and done=0 and whitelist is null) a
		join pd_error b
		on (a.page_id = b.page_id)
		where b.done = 0
		group by a.page_id
		".$column_orderby."					
		limit ".$param_offset.",".$param_limit.";";
	#-- ".$column_sort." 
	my $result = '';
	my $sth = $dbh->prepare( $sql_text )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
	#print '<p class="smalltext"/>'.$sql_text."</p>\n";					  
	$sth->execute or die $sth->errstr; # hier geschieht die Anfrage an die DB
	
	$result .= '<p>';
	$result .= '<a href="'.$script_name.'?view='.$view.'&id='.$param_id.'&offset='.$offset_lower.'&limit='.$param_limit.'&orderby='.$param_orderby.'&sort='.$param_sort.'">←</a>';
	$result .= ' '.$param_offset.' bis '.$offset_end.' ';
	$result .= '<a href="'.$script_name.'?view='.$view.'&id='.$param_id.'&offset='.$offset_higher.'&limit='.$param_limit.'&orderby='.$param_orderby.'&sort='.$param_sort.'">→</a>';
	$result .= '</p>';
	
	$result .= '<table class="table">';
	$result .= '<tr>';
	$result .= '<th class="table">Artikel';
		$result .= '<a href="'.$script_name.'?view='.$view.'&id='.$param_id.'&offset='.$param_offset.'&limit='.$param_limit.'&orderby=article&sort=asc">↑</a>';
		$result .= '<a href="'.$script_name.'?view='.$view.'&id='.$param_id.'&offset='.$param_offset.'&limit='.$param_limit.'&orderby=article&sort=desc">↓</a>';
		$result .= '</th>';
	$result .= '<th class="table">Edit</th>';
	$result .= '<th class="table">Hinweis';
		$result .= '<a href="'.$script_name.'?view='.$view.'&id='.$param_id.'&offset='.$param_offset.'&limit='.$param_limit.'&orderby=notice&sort=asc">↑</a>';
		$result .= '<a href="'.$script_name.'?view='.$view.'&id='.$param_id.'&offset='.$param_offset.'&limit='.$param_limit.'&orderby=notice&sort=desc">↓</a>';
		$result .= '</th>';
	$result .= '<th class="table">Mehrere';
		$result .= '<a href="'.$script_name.'?view='.$view.'&id='.$param_id.'&offset='.$param_offset.'&limit='.$param_limit.'&orderby=more&sort=asc">↑</a>';
		$result .= '<a href="'.$script_name.'?view='.$view.'&id='.$param_id.'&offset='.$param_offset.'&limit='.$param_limit.'&orderby=more&sort=desc">↓</a>';
		$result .= '</th>';
	$result .= '<th class="table">Abarbeitung</th>';
	$result .= '<th class="table">Whitelist</th>';
	$result .= '</tr>'."\n";


	my $counter = 0;
	# einzelne Zeilen
	while (my @array = $sth->fetchrow_array()) {
	    my ($title, $notice, $page_id, $more) = @array;
		$title = encode_entities($title);
		$notice = encode_entities($notice);
		$counter ++;

		$result .= '<tr>';
		$result .= '<td class="table" '.row_color($counter).'><a target="_blank" href="http://de.wikipedia.org/wiki/'.$title.'">'.$title.'</a></td>';
		$result .= '<td class="table" '.row_color($counter).'><a target="_blank" href="http://de.wikipedia.org/w/index.php?title='.$title.'&action=edit&summary=[[Wikipedia:Personendaten/Wartung/Fehlerliste|PD-Wartung]] '.get_headline($error).' (Nr. '.$error.')">edit</a></td>';
		$result .= '<td class="table" '.row_color($counter).'>'.$notice.'</td>';
		$result .= '<td class="table" '.row_color($counter).' align="center"  valign="middle">';
		#if ($more == 1) {
			# only one error
		#} else {
			# more other errors
			
			$result .= '<a href="'.$script_name.'?view=detail&pageid='.$page_id.'&from='.$param_id.'">'.$more.'</a>';
		#}
		$result .= '</td>';

		# Abarbeitung
		$result .= '<td class="table" '.row_color($counter).'align="center"  valign="middle">';
		$result .= '<a href="'.$script_name.'?view='.$view.'&id='.$error.'&pageid='.$page_id.'&offset='.$param_offset.'&limit='.$param_limit.'&orderby='.$param_orderby.'&sort='.$param_sort.'" rel="nofollow">Erledigt</a>';
		$result .= '</td>';

		# Whitelist
		$result .= '<td class="table" '.row_color($counter).'align="center"  valign="middle">';
		$result .= '<a href="'.$script_name.'?view='.$view.'&id='.$error.'&pageid='.$page_id.'&offset='.$param_offset.'&limit='.$param_limit.'&orderby='.$param_orderby.'&sort='.$param_sort.'&whitelist=yes" rel="nofollow">+</a>';
		$result .= '</td>';
		$result .= '</tr>'."\n";	

	}
	$result .= '</table>';
	return ($result);
}


sub row_color{
	# every second row in other color
	my $i = shift;
	my $return = '';
	$return = 'style="background-color:#D0F5A9;"' if ( $i % 2 == 1);
	return($return);
}

########################################
# List with done articles of one error
########################################

sub get_done_article_of_error{
	my $error = shift;
	my $view  = 'onlydone';
	
	$column_orderby = '' if ( $column_orderby eq '');	
	$column_orderby = 'order by a.title' if ($param_orderby eq 'article');
	$column_orderby = 'order by a.notice' if ($param_orderby eq 'notice');
	$column_orderby = 'order by more' if ($param_orderby eq 'more');
	#$column_orderby = 'a.found' if ($param_orderby eq 'found');	
	$column_sort    = 'asc ' if ( $column_sort  eq '');	
	$column_orderby = $column_orderby.' '.$column_sort.' , a.title asc' if ($column_orderby ne '');
	my $sql_text =	 "
		select a.title, a.notice , a.page_id, count(*) more 
		from (	select title, notice, page_id from pd_error where error_id=".$error." and done=1) a
		join pd_error b
		on (a.page_id = b.page_id)
		group by a.page_id
		".$column_orderby."
		limit ".$param_offset.",".$param_limit.";";
	
	my $result = '';
	my $sth = $dbh->prepare( $sql_text )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
	#print '<p class="smalltext"/>'.$sql_text."</p>\n";					  
	$sth->execute or die $sth->errstr; # hier geschieht die Anfrage an die DB
	
	$result .= '<p>';
	$result .= '<a href="'.$script_name.'?view='.$view.'&id='.$param_id.'&offset='.$offset_lower.'&limit='.$param_limit.'&orderby='.$param_orderby.'&sort='.$param_sort.'">←</a>';
	$result .= ' '.$param_offset.' bis '.$offset_end.' ';
	$result .= '<a href="'.$script_name.'?view='.$view.'&id='.$param_id.'&offset='.$offset_higher.'&limit='.$param_limit.'&orderby='.$param_orderby.'&sort='.$param_sort.'">→</a>';
	$result .= '</p>';
	
	$result .= '<table class="table">';
	$result .= '<tr>';
	$result .= '<th class="table">Artikel';
		$result .= '<a href="'.$script_name.'?view='.$view.'&id='.$param_id.'&offset='.$param_offset.'&limit='.$param_limit.'&orderby=article&sort=asc">↑</a>';
		$result .= '<a href="'.$script_name.'?view='.$view.'&id='.$param_id.'&offset='.$param_offset.'&limit='.$param_limit.'&orderby=article&sort=desc">↓</a>';
		$result .= '</th>';
	$result .= '<th class="table">Version</th>';
	$result .= '<th class="table">Hinweis';
		$result .= '<a href="'.$script_name.'?view='.$view.'&id='.$param_id.'&offset='.$param_offset.'&limit='.$param_limit.'&orderby=notice&sort=asc">↑</a>';
		$result .= '<a href="'.$script_name.'?view='.$view.'&id='.$param_id.'&offset='.$param_offset.'&limit='.$param_limit.'&orderby=notice&sort=desc">↓</a>';
		$result .= '</th>';
	$result .= '<th class="table">Mehrere';
		$result .= '<a href="'.$script_name.'?view='.$view.'&id='.$param_id.'&offset='.$param_offset.'&limit='.$param_limit.'&orderby=more&sort=asc">↑</a>';
		$result .= '<a href="'.$script_name.'?view='.$view.'&id='.$param_id.'&offset='.$param_offset.'&limit='.$param_limit.'&orderby=more&sort=desc">↓</a>';
		$result .= '</th>';
	$result .= '<th class="table">Abarbeitung</th>';
	$result .= '</tr>'."\n";


	# einzelne Zeilen
	while (my @array = $sth->fetchrow_array()) {
	    my ($title, $notice, $page_id, $more) = @array;
		$title = encode_entities($title);
		$notice = encode_entities($notice);

		$result .= '<tr>';
		$result .= '<td class="table"><a target="_blank" href="http://de.wikipedia.org/wiki/'.$title.'">'.$title.'</a></td>';
		$result .= '<td class="table"><a target="_blank" href="http://de.wikipedia.org/w/index.php?title='.$title.'&action=history">Versionen</a></td>';
		$result .= '<td class="table">'.$notice.'</td>';
		$result .= '<td class="table" align="center"  valign="middle">';
		if ($more == 1) {
			# only one error
		} else {
			# more other errors
			$result .= '<a href="'.$script_name.'?view=detail&pageid='.$page_id.'&from='.$param_id.'">'.$more.'</a>'
		}
		$result .= '</td>';
		$result .= '<td class="table" align="center"  valign="middle">';
		#$result .= '<a href="'.$script_name.'?view=only&id='.$error.'&pageid='.$output[0][2].'&offset='.$param_offset.'&limit='.$param_limit.'&orderby='.$param_orderby.'&sort='.$param_sort.'">Erledigt</a>';
		$result .= 'ok';
		$result .= '</td></tr>'."\n";	

	}
	$result .= '</table>';
	return ($result);
}



###############################################################

sub get_whitelist_article_of_error{
	my $error = shift;
	my $view = 'whitelist';
	
	# order by clausel
	$column_orderby = '' if ( $column_orderby eq '');	
	$column_orderby = 'order by a.title' if ($param_orderby eq 'article');
	$column_orderby = 'order by a.notice' if ($param_orderby eq 'notice');
	$column_orderby = 'order by more' if ($param_orderby eq 'more');
	#$column_orderby = 'a.found' if ($param_orderby eq 'found');	
	$column_sort    = 'asc'   if ( $column_sort  eq '');	
	$column_orderby = $column_orderby.' '.$column_sort if ($column_orderby ne '');


	# SQL-Statment
	my $sql_text =	 "
		select a.title, a.notice , a.page_id, count(*) more 
		from (	select title, notice, page_id from whitelist where error_id=".$error.") a
		join pd_error b
		on (a.page_id = b.page_id and a.notice = b.notice)
		group by a.page_id
		".$column_orderby."
		limit ".$param_offset.",".$param_limit.";";
	
	my $result = '';
	my $sth = $dbh->prepare( $sql_text )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
	#print '<p class="smalltext"/>'.$sql_text."</p>\n";	
	$sth->execute or die $sth->errstr; # hier geschieht die Anfrage an die DB


	# next page "1 bis 25"	
	$result .= '<p>';
	$result .= '<a href="'.$script_name.'?view='.$view.'&id='.$param_id.'&offset='.$offset_lower.'&limit='.$param_limit.'&orderby='.$param_orderby.'&sort='.$param_sort.'">←</a>';
	$result .= ' '.$param_offset.' bis '.$offset_end.' ';
	$result .= '<a href="'.$script_name.'?view='.$view.'&id='.$param_id.'&offset='.$offset_higher.'&limit='.$param_limit.'&orderby='.$param_orderby.'&sort='.$param_sort.'">→</a>';
	$result .= '</p>';

	
	# Tabellehead
	$result .= '<table class="table">';
	$result .= '<tr>';

	$result .= '<th class="table">Artikel';
	$result .= '<a href="'.$script_name.'?view='.$view.'&id='.$param_id.'&offset='.$param_offset.'&limit='.$param_limit.'&orderby=article&sort=asc">↑</a>';
	$result .= '<a href="'.$script_name.'?view='.$view.'&id='.$param_id.'&offset='.$param_offset.'&limit='.$param_limit.'&orderby=article&sort=desc">↓</a>';
	$result .= '</th>';

	$result .= '<th class="table">Version</th>';

	$result .= '<th class="table">Hinweis';
	$result .= '<a href="'.$script_name.'?view='.$view.'&id='.$param_id.'&offset='.$param_offset.'&limit='.$param_limit.'&orderby=notice&sort=asc">↑</a>';
	$result .= '<a href="'.$script_name.'?view='.$view.'&id='.$param_id.'&offset='.$param_offset.'&limit='.$param_limit.'&orderby=notice&sort=desc">↓</a>';
	$result .= '</th>';

	$result .= '<th class="table">Mehrere';
	$result .= '<a href="'.$script_name.'?view='.$view.'&id='.$param_id.'&offset='.$param_offset.'&limit='.$param_limit.'&orderby=more&sort=asc">↑</a>';
	$result .= '<a href="'.$script_name.'?view='.$view.'&id='.$param_id.'&offset='.$param_offset.'&limit='.$param_limit.'&orderby=more&sort=desc">↓</a>';
	$result .= '</th>';
	$result .= '<th class="table">Whitelist</th>';
	$result .= '</tr>'."\n";


	# einzelne Zeilen
	while (my @array = $sth->fetchrow_array()) {
	    my ($title, $notice, $page_id, $more) = @array;
		$title = encode_entities($title);
		$notice = encode_entities($notice);

		$result .= '<tr>';
		# Title
		$result .= '<td class="table"><a target="_blank" href="http://de.wikipedia.org/wiki/'.$title.'">'.$title.'</a></td>';
		$result .= '<td class="table"><a target="_blank" href="http://de.wikipedia.org/w/index.php?title='.$title.'&action=history">Versionen</a></td>';
		#$result .= '<td class="table"><a target="_blank" href="http://de.wikipedia.org/w/index.php?title='.$output[0][0].'&action=edit">edit</a></td>';

		$result .= '<td class="table">'.$notice.'</td>';

		# more
		$result .= '<td class="table" align="center"  valign="middle">';
		if ($more == 1) {
			# only one error
		} else {
			# more other errors
			$result .= '<a href="'.$script_name.'?view=detail&pageid='.$page_id.'&from='.$param_id.'">'.$more.'</a>'
		}
		$result .= '</td>';

		
		#whitelist
		$result .= '<td class="table" align="center"  valign="middle">';
		$result .= '<a href="'.$script_name.'?view='.$view.'&id='.$error.'&pageid='.$page_id.'&offset='.$param_offset.'&limit='.$param_limit.'&orderby='.$param_orderby.'&sort='.$param_sort.'&whitelist=no">Entfernen</a>';
		$result .= '</td></tr>'."\n";	

	}
	$result .= '</table>';
	return ($result);
}


###############################################################

sub get_delta_article_of_error{
	my $error = shift;
	my $view = 'delta';

	# Order by Clausel
	$column_orderby = '' if ( $column_orderby eq '');	
	$column_orderby = 'order by title' if ($param_orderby eq 'article');
	$column_orderby = 'order by e.notice' if ($param_orderby eq 'notice');
	$column_orderby = 'order by more' if ($param_orderby eq 'more');
	#$column_orderby = 'a.found' if ($param_orderby eq 'found');	
	$column_sort    = 'asc'   if ( $column_sort  eq '');	
	$column_orderby = $column_orderby.' '.$column_sort if ($column_orderby ne '');

	# SQL-Statment
	# type 1 = unterschiedliche Werte
	# type 2 = kein Fehler mehr

	my $sql_text =	 "

select w.title, 
concat('in Artikel: ',IFNULL(e.notice, ' kein Fehler gefunden')),
concat('in Whitelist: ',w.notice), w.page_id 
, 1 type
from whitelist w
join pd_error e 
on (w.page_id = e.page_id and w.error_id = e.error_id  and w.notice <> e.notice)
where w.error_id =  $error

union

select w.title, 
concat('in Artikel: ',IFNULL(e.notice, ' kein Fehler mehr gefunden')),
concat('in Whitelist: ',w.notice), w.page_id 
, 2 type
from whitelist w
left outer join pd_error e 
on (w.page_id = e.page_id and w.error_id = e.error_id)
where w.error_id = $error
and e.page_id is null

		".$column_orderby."
		limit ".$param_offset.",".$param_limit.";";
	
	my $result = '';
	my $sth = $dbh->prepare( $sql_text )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
	#print '<p class="smalltext"/>'.$sql_text."</p>\n";					  
	$sth->execute or die $sth->errstr; # hier geschieht die Anfrage an die DB
	
	# 
	$result .= '<p>';
	$result .= '<a href="'.$script_name.'?view='.$view.'&id='.$param_id.'&offset='.$offset_lower.'&limit='.$param_limit.'&orderby='.$param_orderby.'&sort='.$param_sort.'">←</a>';
	$result .= ' '.$param_offset.' bis '.$offset_end.' ';
	$result .= '<a href="'.$script_name.'?view='.$view.'&id='.$param_id.'&offset='.$offset_higher.'&limit='.$param_limit.'&orderby='.$param_orderby.'&sort='.$param_sort.'">→</a>';
	$result .= '</p>';
	

	# Tabellenkopf
	$result .= '<table class="table">';
	$result .= '<tr>';
	$result .= '<th class="table">Artikel';
		$result .= '<a href="'.$script_name.'?view='.$view.'&id='.$param_id.'&offset='.$param_offset.'&limit='.$param_limit.'&orderby=article&sort=asc">↑</a>';
		$result .= '<a href="'.$script_name.'?view='.$view.'&id='.$param_id.'&offset='.$param_offset.'&limit='.$param_limit.'&orderby=article&sort=desc">↓</a>';
		$result .= '</th>';
	$result .= '<th class="table">Version</th>';
	$result .= '<th class="table">Hinweis';
		$result .= '<a href="'.$script_name.'?view='.$view.'&id='.$param_id.'&offset='.$param_offset.'&limit='.$param_limit.'&orderby=notice&sort=asc">↑</a>';
		$result .= '<a href="'.$script_name.'?view='.$view.'&id='.$param_id.'&offset='.$param_offset.'&limit='.$param_limit.'&orderby=notice&sort=desc">↓</a>';
		$result .= '</th>';

	$result .= '<th class="table">Whitelist</th>';
	$result .= '<th class="table">Aktualisierung</th>';
	$result .= '</tr>'."\n";


	# einzelne Zeilen
	while (my @array = $sth->fetchrow_array()) {
	    my ($title, $notice_pd, $notice_wl, $page_id, $type) = @array;
		$title = encode_entities($title);
		$notice_pd = encode_entities($notice_pd);
		$notice_wl = encode_entities($notice_wl);

		$result .= '<tr>';
		$result .= '<td class="table"><a target="_blank" href="http://de.wikipedia.org/wiki/'.$title.'">'.$title.'</a></td>';
		$result .= '<td class="table"><a target="_blank" href="http://de.wikipedia.org/w/index.php?title='.$title.'&action=history">Versionen</a></td>';
		$result .= '<td class="table">'.$notice_pd.'<br />'.$notice_wl.'</td>';
		
		# whitelist
		$result .= '<td class="table" align="center"  valign="middle">';
		$result .= '<a href="'.$script_name.'?view=delta&id='.$error.'&pageid='.$page_id.'&offset='.$param_offset.'&limit='.$param_limit.'&orderby='.$param_orderby.'&sort='.$param_sort.'&whitelist=no">Entfernen</a>';
		$result .= '</td>';

		# whitelist update
		$result .= '<td class="table" align="center"  valign="middle">';
		if ($type == '1') {
			$result .= '<a href="'.$script_name.'?view=delta&id='.$error.'&pageid='.$page_id.'&offset='.$param_offset.'&limit='.$param_limit.'&orderby='.$param_orderby.'&sort='.$param_sort.'&whitelist=yes">Aktualisierung</a>';
		}
		$result .= '</td>';


		$result .= '</tr>'."\n";	

	}
	$result .= '</table>';
	return ($result);
}

########################################
# List
########################################

sub get_html_list{
	$column_orderby = 'more' if ( $column_orderby eq '');	
	$column_orderby = 'title'    if ($param_orderby eq 'article');
	$column_orderby = 'more' if ($param_orderby eq 'more');
	$column_sort    = 'desc'     if ( $column_sort  eq '');
	
	
	my $sql_text = "select title, count(*)more, page_id from pd_error where done=0 
	group by page_id order by ".$column_orderby." ".$column_sort.", title 
	limit ".$param_offset.",".$param_limit.";";
	
	my $sth = $dbh->prepare( $sql_text )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
	#print '<p class="smalltext"/>'.$sql_text."</p>\n";					  
	$sth->execute or die $sth->errstr; # hier geschieht die Anfrage an die DB
	my $result = '';
	
	$result .= '<p>';
	$result .= '<a href="'.$script_name.'?view=list&offset='.$offset_lower.'&limit='.$param_limit.'&orderby='.$param_orderby.'&sort='.$param_sort.'">←</a>';
	$result .= ' '.$param_offset.' bis '.$offset_end.' ';
	$result .= '<a href="'.$script_name.'?view=list&offset='.$offset_higher.'&limit='.$param_limit.'&orderby='.$param_orderby.'&sort='.$param_sort.'">→</a>';
	$result .= '</p>';
	
	$result .= '<table class="table">';
	$result .= '<tr>';
	$result .= '<th class="table">Vorschläge';
		$result .= '<a href="'.$script_name.'?view=list&offset='.$param_offset.'&limit='.$param_limit.'&orderby=more&sort=asc">↑</a>';
		$result .= '<a href="'.$script_name.'?view=list&offset='.$param_offset.'&limit='.$param_limit.'&orderby=more&sort=desc">↓</a>';
		$result .= '</th>';

	$result .= '<th class="table">Artikel';
		$result .= '<a href="'.$script_name.'?view=list&offset='.$param_offset.'&limit='.$param_limit.'&orderby=article&sort=asc">↑</a>';
		$result .= '<a href="'.$script_name.'?view=list&offset='.$param_offset.'&limit='.$param_limit.'&orderby=article&sort=desc">↓</a>';
		$result .= '</th>';
	$result .= '<th class="table">Details</th>';
	$result .= '</tr>'."\n";
	
	# jede Zeile
	while (my @array = $sth->fetchrow_array()) {
	    my ($title, $more, $page_id) = @array;
		$title = encode_entities($title);

		$result .= '<tr><td class="table">'.$more.'</td>
<td class="table"><a target="_blank" href="http://de.wikipedia.org/wiki/'.$title.'">'.$title.'</a></td>';
		$result .= '<td class="table" align="middle"  valign="middle"><a href="'.$script_name.'?view=detail&pageid='.$page_id.'">Details</a></td>';
		$result .= '</tr>'."\n";	

	}
	$result .= '</table>';
	return ($result);
}



########################################
# Detailansicht
########################################

sub get_all_error_of_article{
	my $page_id = shift;
	my $sql_text = "select a.error_id, b.name, a.notice, a.page_id, a.done, a.whitelist
					from pd_error a join pd_error_desc b 
					on ( a.error_id = b.error_id) 
					where a.page_id = ".$page_id." 
					order by b.name;";
	my $sth = $dbh->prepare( $sql_text )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
	#print '<p class="smalltext"/>'.$sql_text."</p>\n";					  
	$sth->execute or die $sth->errstr; # hier geschieht die Anfrage an die DB
	my $result = '';
	$result .= '<table class="table">';
	$result .= '<tr>';
	$result .= '<th class="table">Fehler</th>';
	$result .= '<th class="table">Hinweis</th>';
	$result .= '<th class="table">Abarbeitung</th>';
	$result .= '</tr>'."\n";

	while (my @array = $sth->fetchrow_array()) {
	    my ($error_id, $error_name, $notice, $page_id, $done, $whitelist) = @array;
		$error_name = encode_entities($error_name);
		$notice     = encode_entities($notice);

		$result .= '<tr>';
		$result .= '<td class="table"><a href="'.$script_name.'?view=only&id='.$error_id.'">'.$error_name.'</a></td>';
		$result .= '<td class="table">'.$notice.'</td>';
		$result .= '<td class="table" align="center"  valign="middle">';
		if ($whitelist eq '1') {
			$result .= 'steht auf der <a href="'.$script_name.'?view=whitelist&id='.$error_id.'&pageid='.$page_id.'" rel="nofollow">Whitelist</a></td>';
		} elsif ($done eq '0') {
			$result .= '<a href="'.$script_name.'?view=detail&id='.$error_id.'&pageid='.$page_id.'" rel="nofollow">Erledigt</a></td>';
		} else {
			$result .= 'ok';
		}
		
		$result .= '</tr>'."\n";		

	}
	$result .= '</table>';
	return ($result);
}


