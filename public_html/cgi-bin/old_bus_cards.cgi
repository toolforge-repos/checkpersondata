#!/usr/bin/perl -w

#################################################################
# Program:	bus_cards.cgi
# Descrition:	Helps to manage the postcards
# Author:	Stefan Kühn
# Licence:      GPL
#################################################################

our $VERSION = '2015-09-12';

use strict;
use CGI qw(param);				# Handler for Parameters in URL www.xyz.org?view=test
use CGI::Carp qw(fatalsToBrowser);
use CGI::Carp qw(fatalsToBrowser set_message); 	# CGI-Error
use HTML::Entities;
use Encode qw(encode decode);
use URI::Escape;				# Handle of URL
use LWP::UserAgent;				# Get webpages
use DBI;

set_message('There is a problem in the script. 
Send me a mail to (<a href="mailto:kuehn-s@gmx.net">kuehn-s@gmx.net</a>), 
giving this error message and the time and date of the error.');

print "Content-type: text/html\n\n";


our %parameter;	# storage for all parameter
our $dbh;
our @all_images;

html_begin();		# Begin of HTML (CSS etc) without title
#print '<p>lokal</p>';
read_configfile();
open_db();


########################################################################
# Environment-Variables (program_name)
########################################################################

my $script_name = $ENV{'SCRIPT_FILENAME'}; #get scriptname from cgi

if (defined($script_name) != 1) {
	# for test local
	$script_name = $0;	# get scriptname
}
#print $script_name.' ' ;
$script_name =~ s/^.+\///g;
#print $script_name.' ' ;



########################################################################
# Parameter
########################################################################

our %parameter;							    

my @keys=(
'view',            # view (list, card)
'nr',              # karten nummer
'modus',           # modus (update)
'motiv',           # card update motiv
'notiz',           # card update notice
'von',             # Liste einschränken
'bis',             # Liste einschränken
'offset',          # SQL Offset
'limit',           # SQL-Limit
'orderby',         # SQL Order by
'sort',            # SQL asc/desc
'search_nr',       # Filter
'search_ort',
'search_land',
'search_motiv',
'search_jahr',
'search_notiz',

'search_artikel',
'search_region',
'search_scan',
'search_id',
'search_cat',
'search_word',
'search_done',


'artikel',
'land',
'region',
'categorie',       # Name of Categorie
'add_cat',         # Add Cat to Card
'del_cat',
'word',				# Stichwort
'cat_id',
'word_id',
'done'
);

foreach my $key (@keys){
	$parameter{$key}  = param($key);	# get parameter from html
	$parameter{$key}  = ''   unless defined $parameter{$key};  # if not defined, set null
}



if ($parameter{'offset'} =~ /^[0-9]+$/) {} else {
	$parameter{'offset'} = 0;
}

if ($parameter{'limit'} =~ /^[0-9]+$/) {} else {
	$parameter{'limit'} = 500;
}

our $offset_lower = $parameter{'offset'} - $parameter{'limit'};
our $offset_higher = $parameter{'offset'} + $parameter{'limit'};
	$offset_lower = 0 if ($offset_lower < 0);
our  $offset_end =  $parameter{'offset'} + $parameter{'limit'};



########################################################################
# Order-By-Clausel
########################################################################

our $column_orderby = '';
our $column_sort = '';

if ($parameter{'orderby'} ne ''){
	if (    $parameter{'orderby'} ne 'nummer'
		and $parameter{'orderby'} ne 'ort'
		and $parameter{'orderby'} ne 'land'
		and $parameter{'orderby'} ne 'motiv'
		and $parameter{'orderby'} ne 'jahr'
		and $parameter{'orderby'} ne 'notiz'

		and $parameter{'orderby'} ne 'artikel'
		and $parameter{'orderby'} ne 'region'
		and $parameter{'orderby'} ne 'scan'
		and $parameter{'orderby'} ne 'id'
		and $parameter{'orderby'} ne 'cat'
		and $parameter{'orderby'} ne 'done'
	) {
		$parameter{'orderby'} = '';
	}
}

if ($parameter{'sort'} ne '') {
	if (   $parameter{'sort'} ne 'asc'
		and $parameter{'sort'} ne 'desc') {
		$column_sort = 'asc' 
	} else {
		$column_sort = 'asc'  if ($parameter{'sort'} eq 'asc');
		$column_sort = 'desc'  if ($parameter{'sort'} eq 'desc');
	}
}

my $html_body = '';

########################################################################
# Do update and delete in Database
########################################################################

#print $parameter{'view'}.'<br />';
#print 'nr:'.$parameter{'nr'}.'<br />';
#print $parameter{'modus'}.'<br />';
#print 'categorie:'.$parameter{'categorie'}.'<br />';
#print $parameter{'add_cat'}.'<br />';
#print $parameter{'word'}.'<br />';
#print 'word_id:'.$parameter{'word_id'}.'<br />';
#print $parameter{'cat_id'}.'<br />';
#print 'notiz:'.$parameter{'notiz'}.'<br />';
#print 'done:'.$parameter{'done'}.'<br />';


if (    $parameter{'view'} =~ /^(card|ort|cat|word|oneword)$/
	and $parameter{'modus'} eq 'update' ) {
	# Artikel abgearbeitet
	my $sql_text ='';
	if ($parameter{'nr'} =~ /^[0-9]+$/ and $parameter{'nr'} != 0
		and $parameter{'add_cat'} eq '' and  $parameter{'del_cat'} eq '') {

		#print $parameter{'view'} .' '.$parameter{'nr'}.' ';
		if ($parameter{'view'} eq 'card') {
			$sql_text = "update karten set 
			motiv= ".nulltest($parameter{'motiv'}).", 
			notiz= ".nulltest($parameter{'notiz'})."
			where nummer=".$parameter{'nr'}.";";
		}

		if ($parameter{'view'} eq 'ort') {
			$sql_text = "update karten_ort set 
			artikel= ".nulltest($parameter{'artikel'}).", 
			land= ".nulltest($parameter{'land'}).", 
			region= ".nulltest($parameter{'region'}).", 
			notiz= ".nulltest($parameter{'notiz'}).",
			cat= ".nulltest($parameter{'categorie'})."
			where id=".$parameter{'nr'}.";";
		}
	}	

	# neue Kategorie	
	if ($parameter{'view'} =~ /^(cat|card)$/ and $parameter{'categorie'} ne '') {


		$html_body .= 'Neue Kategorie: ';
		$html_body .= $parameter{'categorie'};
		$html_body .= ' hinzugefügt';

		$sql_text = "select id, cat from karten_commons_cat where cat = '".$parameter{'categorie'}."';";
		#$html_body .= '<p>'.$sql_text.'</p>';
		#print $sql_text;

		my $sth = $dbh->prepare( $sql_text )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
		$sth->execute or die $sth->errstr; 	
		if ($sth->rows() > 0) {
			$html_body .= 'Kategorie schon vorhanden';
		} else {
			$parameter{'categorie'} =~ s/^\s+|\s+$//g;  #Leerzeichen vorne hinten
			$parameter{'categorie'} =~ s/^Category://g;
			$parameter{'categorie'} =~ s/_/ /g;
			$sql_text = "insert into karten_commons_cat (cat) values ('".$parameter{'categorie'}."');";
			#$html_body .=  $sql_text;
		}

	}

	# neue Kategorie zu Karte hinzufügen (manuell)
	if ($parameter{'view'} =~ /^card$/ 
		and $parameter{'nr'} =~ /^[0-9]+$/ and $parameter{'nr'} != 0
		and $parameter{'add_cat'} =~ /^[0-9]+$/ and $parameter{'add_cat'} != 0) {
		$sql_text = "insert into karten_cat (nr, cat_id, type) values ('".$parameter{'nr'}."', '".$parameter{'add_cat'}."', 'manual');";
	}

	# Kategorie von Karte entfernen
	if ($parameter{'view'} =~ /^card$/ 
		and $parameter{'nr'} =~ /^[0-9]+$/ and $parameter{'nr'} != 0
		and $parameter{'del_cat'} =~ /^[0-9]+$/ and $parameter{'del_cat'} != 0) {
		$sql_text = "delete from karten_cat where nr = '".$parameter{'nr'}."' and cat_id = '".$parameter{'del_cat'}."';";
	}


	# neues Stichwort	
	if ($parameter{'view'} =~ /^word$/ and $parameter{'word'} ne ''
		and $parameter{'cat_id'} =~ /^[0-9]+$/ and $parameter{'cat_id'} != 0) {


		$html_body .= 'Neues Stichwort: ';
		$html_body .= $parameter{'word'};
		#$html_body .= $parameter{'cat_id'};
		$html_body .= ' hinzugefügt';

		$parameter{'word'} =~ s/^\s+|\s+$//g;       #Leerzeichen vorne hinten
		$parameter{'word'} =~ s/_/ /g;

		$sql_text = "insert into karten_word (word, cat_id) values ('".$parameter{'word'}."', '".$parameter{'cat_id'}."');";

	}


	# Stichwort mit Kategorie, Notiz and Done versehen
	if ($parameter{'view'} =~ /^oneword$/ 
		and $parameter{'word_id'} =~ /^[0-9]+$/ and $parameter{'word_id'} != 0
		and $parameter{'add_cat'} eq ''
		and $parameter{'del_cat'} eq ''
	) {

		$parameter{'categorie'} =~ s/^\s+|\s+$//g;  #Leerzeichen vorne hinten
		$parameter{'categorie'} =~ s/^Category://gi;
		$parameter{'categorie'} =~ s/_/ /g;
		
		$sql_text = "update karten_word set 
		cat= ".nulltest($parameter{'categorie'}).", 
		notiz= ".nulltest($parameter{'notiz'}).",
		done= ".nulltest($parameter{'done'})."
		where id=".$parameter{'word_id'}.";";
	}

	# Ein Stichwort - Karte Kategorie hinzufügen
	if ($parameter{'view'} =~ /^oneword$/ 
		and $parameter{'word_id'} =~ /^[0-9]+$/ and $parameter{'word_id'} != 0
		and $parameter{'nr'} =~ /^[0-9]+$/ and $parameter{'word_id'} != 0
		and $parameter{'add_cat'} =~ /^[0-9]+$/ and $parameter{'add_cat'} != 0
	) {
		$sql_text = "insert into karten_cat (nr, cat_id, type) values (
		".nulltest($parameter{'nr'}).", ".nulltest($parameter{'add_cat'}).", 'word');";
	}

	# Ein Stichwort - Karte Kategorie hinzufügen (Multi)
	if ($parameter{'view'} =~ /^oneword$/ 
		and $parameter{'word_id'} =~ /^[0-9]+$/ and $parameter{'word_id'} != 0
		and $parameter{'nr'} =~ /^[0-9]+(_[0-9]+)+$/ and $parameter{'word_id'} != 0
		and $parameter{'add_cat'} =~ /^[0-9]+$/ and $parameter{'add_cat'} != 0
	) {
		my @nr = split(/_/, $parameter{'nr'});
		$sql_text = '';
		foreach my $nr (@nr) {
			#print $sql_text.'<br />';
			$dbh->do($sql_text) if $sql_text ne '';
			$sql_text = "insert into karten_cat (nr, cat_id, type) values (
			".nulltest($nr).", ".nulltest($parameter{'add_cat'}).", 'word');";
		}

	}



	# Ein Stichwort - Karte Kategorie entfernen
	if ($parameter{'view'} =~ /^oneword$/ 
		and $parameter{'word_id'} =~ /^[0-9]+$/ and $parameter{'word_id'} != 0
		and $parameter{'nr'} =~ /^[0-9]+$/ and $parameter{'word_id'} != 0
		and $parameter{'del_cat'} =~ /^[0-9]+$/ and $parameter{'del_cat'} != 0
	) {
		$sql_text = "delete from karten_cat where nr =
		".nulltest($parameter{'nr'})." and cat_id = ".nulltest($parameter{'del_cat'}).";";
	}

	#print $sql_text.'<br />';
	#my $sth = $dbh->prepare( $sql_text )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
	#$sth->execute or die $sth->errstr; # hier geschieht die Anfrage an die DB	
	$dbh->do($sql_text) if $sql_text ne '';	

}




########################################################################
# Build HTML-Page
########################################################################



#print '<p><span style="background-color:yellow;">Im Moment wird gerade am Skript gearbeitet. Daher kann es zu Ausfällen kommen. - Stefan</span></p>'."\n";


# Startseite
if ($parameter{'view'} eq ''){
	$html_body .= get_html_startpage();
}

# Detailseite einer Karte
if (    $parameter{'view'} eq 'card'
	and $parameter{'nr'} =~ /^[0-9]+$/ ) {
	$parameter{'html_subtitle'} = 'Karte';
	$html_body .= get_html_page_for_one_card();
}


# Liste aller Karten
if ( $parameter{'view'} eq 'list') {
	$parameter{'html_subtitle'} = 'Liste aller Karten';
	$html_body .= get_html_list();	
}	

# Liste aller Orte
if ( $parameter{'view'} eq 'location') {
	$parameter{'html_subtitle'} = 'Liste aller Orte';	
	$html_body .= get_html_list_location();	
}

# Liste aller Kategorien
if ( $parameter{'view'} eq 'cat') {
	$parameter{'html_subtitle'} = 'Liste aller Kategorien';
	$html_body .= get_html_list_categories();	
}

# Detailseite einer Kategorie
if (    $parameter{'view'} eq 'onecat'
	and $parameter{'cat_id'} =~ /^[0-9]+$/ ) {
	$parameter{'html_subtitle'} = 'Kategorie';
	$html_body .= get_html_page_for_one_category();
}	



# Liste aller Wörter
if ( $parameter{'view'} eq 'word') {
	$parameter{'html_subtitle'} = 'Liste aller Stichwörter';
	$html_body .= get_html_list_word();	
}

# Neues Stichwort
if ( $parameter{'view'} eq 'new_word') {
	$parameter{'html_subtitle'} = 'Stichwort hinzufügen';
	$html_body .= get_html_new_word();	
}

# Detailseite einer Karte
if (    $parameter{'view'} eq 'ort'
	and $parameter{'nr'} =~ /^[0-9]+$/ ) {
	$parameter{'html_subtitle'} = 'Ort';
	$html_body .= get_html_page_for_one_location();
}	

# Detailseite eines Stichworts
if (    $parameter{'view'} eq 'oneword'
	and $parameter{'word_id'} =~ /^[0-9]+$/ ) {
	$parameter{'html_subtitle'} = 'Stichwort';
	$html_body .= get_html_page_for_one_word();
}	



# Neue Kategorie
if ( $parameter{'view'} eq 'new_cat') {
	$parameter{'html_subtitle'} = 'Kategorie hinzufügen';
	$html_body .= get_html_new_cat();	
}

# Statistic
if ( $parameter{'view'} eq 'statistic') {
	$parameter{'html_subtitle'} = 'Liste aller Statistiken';
	$html_body .= get_html_list_statistic();	
}


# Bilder
if ( $parameter{'view'} eq 'images') {
	$parameter{'html_subtitle'} = 'Bilder lokal';
	$html_body .= get_html_images();	
}

# QS: Karten ohne Kategorien
if ( $parameter{'view'} eq 'no_cat') {
	$parameter{'html_subtitle'} = 'Karten ohne Kategorien';
	$html_body .= get_html_no_cat();	
}

if ( $parameter{'view'} eq 'no_cat_group') {
	$parameter{'html_subtitle'} = 'Karten ohne Kategorien';
	$html_body .= get_html_no_cat_group();	
}





# Title
my $title = 'BuS Cards';

# Subtitle
if ($parameter{'html_subtitle'} ne ''){
	$title .= ' - '.$parameter{'html_subtitle'};
}
print '<title>'.$title.'</title></head>'."\n";

# begin body
print '<body>'."\n";
print '<h1>BuS Cards - '.$parameter{'html_subtitle'}.'</h1>'."\n";
print $html_body;

# Close datenbase
$dbh->disconnect();


html_foot();

#END






#######################################################################
#######################################################################
# Subroutines
#######################################################################
#######################################################################



################################################################	
# Read configfile 
################################################################	

sub read_configfile{
	# read_configfile for global settings (DB,Schemata,Password,dumppath...)

	# open configfile
	my $config_file = '../../config.txt';
	open(CONFIG, "<$config_file") or die "cannot open < $config_file\n$!";


	# read configfile line by line
	do {
		my $line = <CONFIG>;

		
		if ($line =~ /=/ ) {
			$line =~ s/\n//g;  
			$line =~ s/\t/ /g; #tabulator to space
			my $key = substr($line, 0, index($line,'=')-1);
			my $value = substr($line, index($line,'=')+1);

			$key   =~ s/^\s+|\s+$//g;	# remove space at begin and end
			$value =~ s/^\s+|\s+$//g;

			if ($key ne ''){
				$parameter{$key} = $value;	# save in hash parameter
			}
			
		}
	}
	while (eof(CONFIG) != 1);

	# close configfile
	close(CONFIG);

	# print all parameter
	foreach my $name(sort keys %parameter){
		# printf ("%15s = %s\n", $name ,$parameter{$name}) ; 
	}
	#print "\n";
	#die;
}



##############################################
# Open database
##############################################

sub open_db{
	
	#Connect to database u_sk
	#print "\n".'open database: '.$parameter{'db_database'}."\n";

	$dbh = DBI->connect( "DBI:mysql:$parameter{'db_database'};$parameter{'host'}",  # local
							$parameter{'db_user'},
							$parameter{'db_password'} ,
							{
								RaiseError => 1,
								AutoCommit => 1,
								mysql_enable_utf8 => 1
							}
	  ) or die "Database connection not made: $DBI::errstr" . DBI->errstr;
	
}



##############################################
# HTML_Begin
##############################################

sub html_begin{
print <<HTML_HEAD;
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<head>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<style type="text/css">
body {  
	
	font-family: Verdana, Tahoma, Arial, Helvetica, sans-serif;
	font-size:14px;
	font-style:normal;
	
	/* color:#9A91ff; */
	
	/* background-color:#00077A; */
	/* background-image:url(back.jpg); */
	/* background:url(back_new.jpg) no-repeat fixed top center; */
	/* background:url(../images/back_new.jpg) no-repeat fixed top center; */
	/* background:url(../images/back_schaf2.jpg) no-repeat fixed bottom left; */
	/* background:url(../images/back_schaf3.jpg) no-repeat fixed bottom left; */
	
	background-color:white;
	color:#555555;
	text-decoration:none; 
	line-height:normal; 
	font-weight:normal; 
	font-variant:normal; 
	text-transform:none; 
	margin-left:5%;
	margin-right:5%;
	}
	
h1	{
	/*color:red; */
	font-size:20px;
	}

h2	{
	/*color:red; */
	font-size:16px;
	}
	
a 	{  

	/*nur blau */
	/* color:#80BFBF; */ 
	/* color:#4889c5; */ 
	/* color:#326a9e; */  
	
	color:#4889c5;
	font-weight:bold;
	/*nettes grün */
	/*color:#7CFC00;*/
	
	/* Nette Kombination */
	/*color:#00077A; */
	/*background-color:#eee8fd;*/
	
	
	/* Kein Unterstrich */
	text-decoration:none; 
	
	/*Außenabstand*/
	/*padding:2px;*/
	}

a:hover {  
	background-color:#ffdeff;
	color:red;
	}
	
.nocolor{  
	background-color:white;
	color:white;
	} 
	
a:hover.nocolor{  
	background-color:white;
	color:white;
	}
	
.table{
	font-size:12px; 

	vertical-align:top;

	border-width:thin;
  	border-style:solid;
  	border-color:blue;
  	background-color:#EEEEEE;
  	
  	/*Innenabstand*/
	padding-top:2px;
	padding-bottom:2px;
	padding-left:5px;
	padding-right:5px;
  	
  	/* dünne Rahmen */
  	border-collapse:collapse; 
	
	/*kein Zeilenumbruch
	white-space:nowrap;*/
  	
  	}
	
</style>
HTML_HEAD


}



##############################################################################
# HTML-END
##############################################################################

sub html_foot{
	print '<p><span style="font-size:10px;">Autor: <a href="https://de.wikipedia.org/wiki/Benutzer:Stefan Kühn">Stefan Kühn</a> '; 
	print 'Version '.$VERSION.'<br /> 
	license: <a href="http://www.gnu.org/copyleft/gpl.html">GPL</a>
	Powered by <a href="https://tools.wmflabs.org/">Wikimedia Tool Labs</a></span></p>'."\n";

	print '</body>';
	print '</html>';
}



##############################################################################
# Startpage
##############################################################################

sub get_html_startpage{
	my $html = '';
	
	$parameter{'html_subtitle'} = 'Startseite';
	$html .= '<p>→ Startseite</p>'."\n";
	# Einleitungstext			
	$html .= '<p>Temporäre Hilfsseite für das Projet "BuS Cards".</p>';

	# Liste der Startseite
	$html .= '<h2>Listen</h2>';
	$html .= '<p><ul>';
	$html .= '<li><a href="'.$script_name.'?view=list" >Alle Karten</a></li>';
	$html .= '<li><a href="'.$script_name.'?view=location" >Alle Orte</a></li>';
	$html .= '<li><a href="'.$script_name.'?view=cat" >Alle Kategorien</a></li>';
	$html .= '<li><a href="'.$script_name.'?view=word" >Alle Stichwörter</a></li>';
	$html .= '<li><a href="'.$script_name.'?view=statistic" >Alle Statistiken</a></li>';
	$html .= '<li><a href="'.$script_name.'?view=no_cat" >QS: Karten ohne Kategorien</a></li>';
	# $html .= '<li><a href="'.$script_name.'?view=images" >Bilder lokal</a></li>';
	$html .= '</ul>';
	$html .= '</p>';


	# Liste der Startseite
	$html .= '<h2>Schrittfolge</h2>';
	$html .= '<p>grün=fertig; gelb=in Arbeit; rot=in Zukunft';
	$html .= '<ol>';
	$html .= '<li><span style="background:lightgreen;">Daten aus Excel in Datenbank überführen</span></li>';	
	$html .= '<li><span style="background:lightgreen;">Eindeutige ID vergeben, Doppelte Nummerierung beseitigen</span></li>';	
	$html .= '<li><span style="background:lightgreen;">Abkürzungen in Motiv soweit wie möglich beseitigen</span></li>';
	$html .= '<li><span style="background:lightgreen;">Tippfehler in Motiv soweit wie möglich beseitigen</span></li>';
	$html .= '<li><span style="background:lightgreen;">Möglichst alle Orte mit Artikel in Wikipedia verküpfen</span></li>';
	$html .= '<li><span style="background:lightgreen;">Möglichst alle Orte mit Land versehen (ISO 3166-1)</span></li>';
	$html .= '<li><span style="background:lightgreen;">Möglichst alle Orte mit Region versehen  (ISO 3166-2)</span></li>';
	$html .= '<li><span style="background:lightgreen;">Bilddateien beim Verlag kopieren</span></li>';
	$html .= '<li><span style="background:lightgreen;">Orten automatisch per Skript eine Commons-Kategorien zuordnen</span></li>';
	$html .= '<li><span style="background:lightgreen;">Restliche Orten manuell eine Commons-Kategorien zuordnen</span></li>';
	$html .= '<li><span style="background:lightgreen;">Automatisierte Erstellung einer Stichwort-List aus den Motivbeschreibungen</span></li>';	
	$html .= '<li><span style="background:yellow">Manuelles Hinzufügen der Commons-Kategorien zu den Karten</span></li>';
	$html .= '<li><span style="background:#FF9966;">Mit Fehlerlisten Problemfälle finden (Karten ohne Kategorien etc.)</span></li>';	
	$html .= '<li><span style="background:yellow;">Automatisiert allen Karten der jeweiligen Orte Commons-Kategorien zuordnen</span></li>';
	$html .= '<li><span style="background:yellow;">Automatisiert allen Karten über die Jahresangabe Commons-Kategorien zuordnen</span></li>';
	$html .= '<li><span style="background:yellow;">Visuelle Kontrolle der Kategorienzuordnung über Bild-Gallerien</span></li>';
	$html .= '<li><span style="background:yellow;">Alle Orte in Sachsen feiner kategorisieren (HASC)</span></li>';
	$html .= '<li><span style="background:#FF9966;">Automatisches Finden von Subkategorien in Commons</span></li>';
	$html .= '<li><span style="background:#FF9966;">Automatisches Zuordnen der Subkategorien über Regionalangabe bei den Karten</span></li>';
	$html .= '<li><span style="background:#FF9966;">Dateinamen für Commons generieren und überprüfen</span></li>';
	$html .= '<li><span style="background:#FF9966;">Endkontrolle</span></li>';
	$html .= '<li><span style="background:#FF9966;">Automatisiertes Hochladen nach Commons</span></li>';
	$html .= '<li><span style="background:#FF9966;">Feedback an den Verlag</span></li>';

	$html .= '</ol>';
	$html .= '</p>';

	return ($html);
}









########################################################################
# HTML-Page für genau einen Karte (card)
########################################################################

sub get_html_page_for_one_card{
	my $html = '';
	
	$html .= '<p>→ <a href="'.$script_name.'">Startseite</a> → ';
	$html .= '<a href="'.$script_name.'?view=list">Liste aller Karten</a> → Karte Nr. ';
	
	my $headline = '';
	if ($parameter{'nr'} =~ /^[0-9]+$/) {
		$headline = 'Karte Nr. '.$parameter{'nr'};
		$html .= $parameter{'nr'}."\n";
	} else {
		$html .= '?'."\n";	
	}
	$html .= '</p>';
	#$html .= '<h2>'.$headline.'</h2>';


	#$html .=  $parameter{'nr'}.'<br />'."\n";

	$html .= '<table><tr><td>';

	$html .= '<form action="'.$script_name.'?view=card&nr='.$parameter{'nr'}.'" accept-charset="utf-8" method="post" >'."\n";


	my $sql_text = "select nummer, ort,region,land,motiv,jahr,notiz 
from karten where nummer=".$parameter{'nr'}.";";
$sql_text = "select k.nummer, o.ort,o.land, o.region, k.motiv,k.jahr,k.notiz 
from karten k
left outer join karten_ort o
on o.ort=k.ort
where k.nummer=".$parameter{'nr'}.";";

	#$html .= '<p>'.$sql_text.'</p>';
	my $sth = $dbh->prepare( $sql_text )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
	$sth->execute or die $sth->errstr; 	
	my %card;
	while (my @array = $sth->fetchrow_array()) {
	    ($card{'nummer'},
			$card{'ort'},
			$card{'land'},
			$card{'region'},
			$card{'motiv'},
			$card{'jahr'},
			$card{'notiz'}
			) = @array;

		$card{'ort'} = encode_entities($card{'ort'});
		$card{'region'} = encode_entities($card{'region'});
		$card{'land'} = encode_entities($card{'land'});
		$card{'motiv'} = encode_entities($card{'motiv'});
		$card{'notiz'} = encode_entities($card{'notiz'});
	
		#print $card{'motiv'}.'<br />';

		$html .= '<table class="table">';
		$html .= '<tr><th class="table">Feld</th><th class="table">Inhalt</th></tr>';
		$html .= '<tr><td class="table">Nummer</td><td class="table">'.$card{'nummer'}.'</td></tr>';
		$html .= '<tr><td class="table">Ort   </td><td class="table">'.$card{'ort'}.'   </td></tr>';
		$html .= '<tr><td class="table">Region   </td><td class="table">'.get_regionname($card{'land'}.'-'.$card{'region'}).'   </td></tr>';
		$html .= '<tr><td class="table">Land  </td><td class="table">'.get_landname($card{'land'}).'  </td></tr>';

		# motiv
		$html .= '<tr><td class="table">Motiv </td>';
		$html .= '<td class="table">';
		$html .= '<input name="motiv" type="text" size="30" maxlength="400" value="'.$card{'motiv'}.'" >';
		$html .= ' </td></tr>';

		$html .= '<tr><td class="table">Jahr  </td><td class="table">'.$card{'jahr'}.'  </td></tr>';

		# notiz
		$html .= '<tr><td class="table">Notiz </td>';
		$html .= '<td class="table">';
		$html .= '<input name="notiz" type="text" size="30" maxlength="400" value="'.$card{'notiz'}.'" >';
		$html .= ' </td></tr>';
		$html .= '</table>';
	}

	# hidden parameter: nr, modus
	$html .= '<input name="view" type="hidden" value="'.$parameter{'view'}.'">'."\n";	
	$html .= '<input name="nr" type="hidden" value="'.$parameter{'nr'}.'">'."\n";
	$html .= '<input name="modus" type="hidden" value="update">'."\n";
	$html .= '<input type="submit" value="Speichern">'."\n";


	$html .= '</td>';


	###################################################	
	# Suche 
	###################################################

	$html .= '<td valign="top">';

	#$html .= '<h2>Suche</h2>';
	$html .= '<a href="https://www.google.de/search?q='.$card{'ort'}.'+'.$card{'motiv'}.'&tbm=isch" target="_blank">Google Bildsuche Ort+Motiv</a></li>';




   




	###################################################	
	# Kategorien für diese Karte
	###################################################

	
	$html .= '<h2>Kategorien der Karte</h2><p>';
	
	my $sql_text = "
select a.id, a.cat_id, b.cat 
from karten_cat a 
join karten_commons_cat b 
on a.cat_id = b.id
where nr = '".$parameter{'nr'}."' order by b.cat ;";

	# search in database
	my $sth = $dbh->prepare( $sql_text )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
	#$html .= '<p class="smalltext"/>'.$sql_text."</p>\n";					  
	$sth->execute or die $sth->errstr; # hier geschieht die Anfrage an die DB

	$html .= '<table class="table">';
	$html .= '<tr><th class="table">ID</th><th class="table">Kategorie</th>';
	$html .= '</tr>';


	my %row;
	my $counter = 0;

	# jede Zeile jeder Karte
	while (my @array = $sth->fetchrow_array()) {
	    ($row{'id'},
			$row{'cat_id'},
			$row{'cat'},
			) = @array;


		$counter ++;

		$html .= '<tr>';
		$html .= '<td class="table" '.row_color($counter).'><a href="'.$script_name.'?view=onecat&cat_id='.$row{'cat_id'}.'">'.$row{'cat_id'}.'</a>'.'</td>';
		$html .= '<td class="table" '.row_color($counter).'>'.encode_entities($row{'cat'}).'</td>';
		$html .= '<td class="table" '.row_color($counter).'><a href="'.$script_name.'?view=card&nr='.$parameter{'nr'}.'&del_cat='.$row{'cat_id'}.'&modus=update">Entfernen</a>'.'</td>';
		$html .= '</tr>'."\n";	

	}
	$html .= '</table></p>';


   	$html .= '</td></tr>';







	###################################################	
	# Neue Kategorien für diese Karte
	###################################################

	$html .= '<tr><td valign="top">';
	$html .= '<h2>Neue Kategorie erstellen</h2>';
	$html .= '<form action="'.$script_name.'?view=card&nr='.$parameter{'nr'}.'" accept-charset="utf-8" method="post" >'."\n";
	$html .= '<input name="categorie" type="text" size="20" maxlength="400" value="'.'" >';

	# hidden parameter: view, modus
	$html .= '<input name="view" type="hidden" value="cat">'."\n";
	$html .= '<input name="modus" type="hidden" value="update">'."\n";
	$html .= '<input type="submit" value="Erstellen">'."\n";
	$html .= '</form>'."\n";



	###################################################	
	# Liste aller möglichen Kategorien 
	###################################################


	$html .= '<h2>Alle vorhandenen Kategorien</h2>';

	# Katfilter	
	$html .= '<form action="'.$script_name.'?view=card&nr='.$parameter{'nr'}.'" accept-charset="utf-8" method="post" >'."\n";
	$html .= '<input name="search_cat" type="text" size="20" maxlength="100" value="'.$parameter{'search_cat'}.'" >';
	# hidden parameter: nr, modus
	$html .= '<input name="view"  type="hidden" value="'.$parameter{'view'}.'">'."\n";	
	#$html .= '<input name="modus" type="hidden" value="update">'."\n";
	$html .= '<input name="nr" type="hidden" value="'.$parameter{'nr'}.'">'."\n";
	$html .= '<input type="submit" value="Filtern"></p>'."\n";

	my $sql_text = "select id, cat from karten_commons_cat
where cat like '%".$parameter{'search_cat'}."%' and id not in (
select cat_id from karten_cat a where nr = '".$parameter{'nr'}."') order by cat;";

#$sql_text = "
#select kcc.id, kcc.cat, kc.anzahl from karten_commons_cat kcc
#join (
#select count(*) anzahl, cat_id 
#from karten_cat
#group by cat_id
#) kc
#on kcc.id = kc.cat_id
#where cat like '%".$parameter{'search_cat'}."%' and id not in (
#select cat_id from karten_cat a where nr = '".$parameter{'nr'}."')
#order by anzahl desc;";


	# search in database
	my $sth = $dbh->prepare( $sql_text )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
	#$html .= '<p class="smalltext"/>'.$sql_text."</p>\n";					  
	$sth->execute or die $sth->errstr; # hier geschieht die Anfrage an die DB

	$html .= '<table class="table">';
	$html .= '<tr><th class="table">Kategorie</th>';
	$html .= '</tr>';


	my %row;
	my $counter = 0;

	# jede Zeile jeder Karte
	while (my @array = $sth->fetchrow_array()) {
	    ($row{'id'},
		 $row{'cat'},
        # $row{'anzahl'},
			) = @array;


		$counter ++;

		$html .= '<tr>';
		#$html .= '<td class="table" '.row_color($counter).' align=right>'.$row{'anzahl'}.'*</td>';
		$html .= '<td class="table" '.row_color($counter).'>'.encode_entities($row{'cat'}).'</td>';
		$html .= '<td class="table" '.row_color($counter).'><a href="'.$script_name.'?view=card&nr='.$parameter{'nr'}.'&add_cat='.$row{'id'}.'&modus=update">Hinzufügen</a>'.'</td>';

		$html .= '</tr>'."\n";	

	}
	$html .= '</table>';
	$html .= '</td>';



	###################################################	
	# Kategorien für den Ort dieser Karte
	###################################################

	$html .= '<td valign="top">';
	$html .= '<h2>Kategorien im Ort</h2><p>';
	my $sql_text = "
select kcc.id, kcc.cat, kc.anzahl from karten_commons_cat kcc
join 
(select count(*) anzahl, kc1.cat_id from karten_cat kc1
  join 
  (select nummer from karten k1 where ort in 
   (select ort from karten_ort where artikel =
    (select artikel from karten_ort where ort =
     (select ort from karten where nummer = '".$parameter{'nr'}."'
     )
    )
   )
  ) k2
 on (kc1.nr = k2.nummer)
 group by kc1.cat_id
) kc
on kcc.id = kc.cat_id
where kcc.id not in (
 select cat_id from karten_cat a where nr = '".$parameter{'nr'}."')
order by anzahl desc, kcc.cat;
";


	# search in database
	my $sth = $dbh->prepare( $sql_text )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
	#$html .= '<p class="smalltext"/>'.$sql_text."</p>\n";					  
	$sth->execute or die $sth->errstr; # hier geschieht die Anfrage an die DB

	$html .= '<table class="table">';
	$html .= '<tr><th class="table">Anzahl</th><th class="table">Kategorie</th>';
	$html .= '</tr>';


	my %row;
	my $counter = 0;

	# jede Zeile jeder Karte
	while (my @array = $sth->fetchrow_array()) {
	    ($row{'id'},
		 $row{'cat'},
         $row{'anzahl'},
			) = @array;


		$counter ++;

		$html .= '<tr>';
		$html .= '<td class="table" '.row_color($counter).' align=right>'.$row{'anzahl'}.'*</td>';
		$html .= '<td class="table" '.row_color($counter).'>'.encode_entities($row{'cat'}).'</td>';
		$html .= '<td class="table" '.row_color($counter).'><a href="'.$script_name.'?view=card&nr='.$parameter{'nr'}.'&add_cat='.$row{'id'}.'&modus=update">Hinzufügen</a>'.'</td>';

		$html .= '</tr>'."\n";	

	}
	$html .= '</table></p>';
	$html .= '</td></tr></table>';


	return ($html);
}


########################################
# Liste aller Karten
########################################

sub get_html_list{
	$column_orderby = 'nummer'    if ($column_orderby eq '');	
	$column_orderby = 'nummer'    if ($parameter{'orderby'} eq 'nummer');
	$column_orderby = 'ort'       if ($parameter{'orderby'} eq 'ort');
	$column_orderby = 'land'      if ($parameter{'orderby'} eq 'land');
	$column_orderby = 'motiv'     if ($parameter{'orderby'} eq 'motiv');
	$column_orderby = 'jahr'      if ($parameter{'orderby'} eq 'jahr');
	$column_orderby = 'notiz'     if ($parameter{'orderby'} eq 'notiz');

	$column_sort    = 'asc'       if ( $column_sort  eq '');
	
	my $html = '';

	$html .= '<p>→ <a href="'.$script_name.'">Startseite</a> → Liste aller Karten</p>'."\n";	


	# formular
	$html .= '<form action="'.$script_name.'?view=list" accept-charset="utf-8" method="post" >'."\n";
	$html .= '<p>Von Nr. <input name="von" type="text" size="10" maxlength="8" value="'.$parameter{'von'}.'" /> bis ';
	$html .= '<input name="bis" type="text" size="10" maxlength="8" value="'.$parameter{'bis'}.'" />';
	$html .= ' Maximalanzahl <input name="limit" type="text" size="5" maxlength="8" value="'.$parameter{'limit'}.'" />'.'</p>';

	$html .= '<p>Suche Nr<input name="search_nr" type="text" size="8" maxlength="50" value="'.$parameter{'search_nr'}.'" />';
	$html .= ' Ort<input name="search_ort" type="text" size="8" maxlength="50" value="'.$parameter{'search_ort'}.'" />';
	$html .= ' Land<input name="search_land" type="text" size="8" maxlength="50" value="'.$parameter{'search_land'}.'" />';
	$html .= ' Motiv<input name="search_motiv" type="text" size="8" maxlength="50" value="'.$parameter{'search_motiv'}.'" />';
	$html .= ' Jahr<input name="search_jahr" type="text" size="8" maxlength="50" value="'.$parameter{'search_jahr'}.'" />';
	$html .= ' Notiz<input name="search_notiz" type="text" size="8" maxlength="50" value="'.$parameter{'search_notiz'}.'" />';

	
	$html .= '<input name="view" type="hidden" value="'.$parameter{'view'}.'">'."\n";	
	#$html .= '<input name="von"  type="hidden" value="'.$parameter{'von'}.'">'."\n";
	#$html .= '<input name="bis"  type="hidden" value="'.$parameter{'bis'}.'">'."\n";		


	$html .= '<input type="submit" value="Filtern"></p>'."\n";


	# where klausel
	$parameter{'where'} = 'where 1=1';
	if ($parameter{'von'} ne ''){
		$parameter{'where'} .= ' and nummer >= '.$parameter{'von'};
	}
	if ($parameter{'bis'} ne ''){
		$parameter{'where'} .= ' and nummer <= '.$parameter{'bis'};
	}

	if ($parameter{'search_nr'} ne ''){
		$parameter{'where'} .= " and nummer like '%".$parameter{'search_nr'}."%'";
	}

	if ($parameter{'search_ort'} ne ''){
		$parameter{'where'} .= " and ort like '%".$parameter{'search_ort'}."%'";
	}

	if ($parameter{'search_land'} ne ''){
		$parameter{'where'} .= " and land like '%".$parameter{'search_land'}."%'";
	}

	if ($parameter{'search_motiv'} ne ''){
		$parameter{'where'} .= " and motiv like '%".$parameter{'search_motiv'}."%'";
	}

	if ($parameter{'search_jahr'} ne ''){
		$parameter{'where'} .= " and jahr like '%".$parameter{'search_jahr'}."%'";
	}

	if ($parameter{'search_notiz'} ne ''){
		$parameter{'where'} .= " and notiz like '%".$parameter{'search_notiz'}."%'";
	}
	
	# SQL
	my $sql_text = "select nummer, ort, land, motiv, jahr, notiz from karten ".$parameter{'where'}." order by ".$column_orderby." ".$column_sort." 
	limit ".$parameter{'offset'}.",".$parameter{'limit'}.";";



	# search in database
	my $sth = $dbh->prepare( $sql_text )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
	$html .= '<p class="smalltext"/>'.$sql_text."</p>\n";					  
	$sth->execute or die $sth->errstr; # hier geschieht die Anfrage an die DB
	
	
	$html .= '<p>';
	$html .= '<a href="'.$script_name.'?view=list&offset='.$offset_lower.html_link_parameter().'&orderby='.$parameter{'orderby'}.'&sort='.$parameter{'sort'}.'">←</a>';
	$html .= ' '.$parameter{'offset'}.' bis '.$offset_end.' ';
	$html .= '<a href="'.$script_name.'?view=list&offset='.$offset_higher.html_link_parameter().'&orderby='.$parameter{'orderby'}.'&sort='.$parameter{'sort'}.'">→</a>';
	$html .= '</p>';
	
	$html .= '<table class="table">';
	$html .= '<tr>';
	$html .= '<th class="table">Nummer';
	$html .= '<a href="'.$script_name.'?view=list'.html_link_parameter().'&orderby=nummersort=asc">↑</a>';
	$html .= '<a href="'.$script_name.'?view=list'.html_link_parameter().'&orderby=nummer&sort=desc">↓</a>';
		$html .= '</th>';

	$html .= '<th class="table">Ort';
$html .= '<a href="'.$script_name.'?view=list'.html_link_parameter().'&orderby=ort&sort=asc">↑</a>';
$html .= '<a href="'.$script_name.'?view=list'.html_link_parameter().'&orderby=ort&sort=desc">↓</a>';
$html .= '</th>';

	$html .= '<th class="table">Land';
$html .= '<a href="'.$script_name.'?view=list'.html_link_parameter().'&orderby=land&sort=asc">↑</a>';
$html .= '<a href="'.$script_name.'?view=list'.html_link_parameter().'&orderby=land&sort=desc">↓</a>';
$html .= '</th>';

	$html .= '<th class="table">Motiv';
$html .= '<a href="'.$script_name.'?view=list'.html_link_parameter().'&orderby=motiv&sort=asc">↑</a>';
$html .= '<a href="'.$script_name.'?view=list'.html_link_parameter().'&orderby=motiv&sort=desc">↓</a>';
$html .= '</th>';

	$html .= '<th class="table">Jahr';
$html .= '<a href="'.$script_name.'?view=list'.html_link_parameter().'&orderby=jahr&sort=asc">↑</a>';
$html .= '<a href="'.$script_name.'?view=list'.html_link_parameter().'&orderby=jahr&sort=desc">↓</a>';
$html .= '</th>';

	$html .= '<th class="table">Notiz';
$html .= '<a href="'.$script_name.'?view=list'.html_link_parameter().'&orderby=notiz&sort=asc">↑</a>';
$html .= '<a href="'.$script_name.'?view=list'.html_link_parameter().'&orderby=notiz&sort=desc">↓</a>';
$html .= '</th>';


	$html .= '</tr>'."\n";
	

	my %card;
	my $counter = 0;

	# jede Zeile
	while (my @array = $sth->fetchrow_array()) {
	    ($card{'nummer'},
			$card{'ort'},
			$card{'land'},
			$card{'motiv'},
			$card{'jahr'},
			$card{'notiz'}
			) = @array;

		$card{'ort'} = encode_entities($card{'ort'});
		$card{'land'} = encode_entities($card{'land'});
		$card{'motiv'} = encode_entities($card{'motiv'});
		$card{'notiz'} = encode_entities($card{'notiz'});

		$counter ++;

		$html .= '<tr>';
		$html .= '<td class="table" '.row_color($counter).'><a href="'.$script_name.'?view=card&nr='.$card{'nummer'}.'">'.$card{'nummer'}.'</a>'.'</td>';
		$html .= '<td class="table" '.row_color($counter).'>'.$card{'ort'}.'</td>';
		$html .= '<td class="table" '.row_color($counter).'>'.$card{'land'}.'</td>';
		$html .= '<td class="table" '.row_color($counter).'><a href="'.$script_name.'?view=card&nr='.$card{'nummer'}.'">'.$card{'motiv'}.'</a>'.'</td>';
		$html .= '<td class="table" '.row_color($counter).'>'.$card{'jahr'}.'</td>';
		$html .= '<td class="table" '.row_color($counter).'>'.$card{'notiz'}.'</td>';
		$html .= '</tr>'."\n";	

	}
	$html .= '</table>';
	return ($html);
}







########################################
# Liste aller Orte
########################################

sub get_html_list_location{
	$column_orderby = 'ort';
	$column_orderby = 'ort'       if ($parameter{'orderby'} eq 'ort');
	$column_orderby = 'artikel'   if ($parameter{'orderby'} eq 'artikel');
	$column_orderby = 'land'      if ($parameter{'orderby'} eq 'land');
	$column_orderby = 'region'    if ($parameter{'orderby'} eq 'region');
	$column_orderby = 'notiz'     if ($parameter{'orderby'} eq 'notiz');
	$column_orderby = 'scan'      if ($parameter{'orderby'} eq 'scan');
	$column_orderby = 'cat'       if ($parameter{'orderby'} eq 'cat');
	
	$column_sort    = 'asc'       if ( $column_sort  eq '');	# default
	
	my $html = '';

	$html .= '<p>→ <a href="'.$script_name.'">Startseite</a> → Liste aller Orte</p>'."\n";	


	# formular
	$html .= '<form action="'.$script_name.'?view=list" accept-charset="utf-8" method="post" >'."\n";
	$html .= '<p>Von (A-Z) <input name="von" type="text" size="5" maxlength="8" value="'.$parameter{'von'}.'" /> bis ';
	$html .= '<input name="bis" type="text" size="5" maxlength="8" value="'.$parameter{'bis'}.'" />';
	$html .= ' Maximalanzahl <input name="limit" type="text" size="5" maxlength="8" value="'.$parameter{'limit'}.'" />'.'</p>';
	$html .= ' Ort<input name="search_ort" type="text" size="5" maxlength="50" value="'.$parameter{'search_ort'}.'" />';
	$html .= ' Artikel<input name="search_artikel" type="text" size="5" maxlength="50" value="'.$parameter{'search_artikel'}.'" />';
	$html .= ' Land<input name="search_land" type="text" size="5" maxlength="50" value="'.$parameter{'search_land'}.'" />';
	$html .= ' Region<input name="search_region" type="text" size="5" maxlength="50" value="'.$parameter{'search_region'}.'" />';
	$html .= ' Notiz<input name="search_notiz" type="text" size="5" maxlength="50" value="'.$parameter{'search_notiz'}.'" />';
	$html .= ' Scan<input name="search_scan" type="text" size="5" maxlength="50" value="'.$parameter{'search_scan'}.'" />';
	$html .= ' Kategorie<input name="search_cat" type="text" size="5" maxlength="50" value="'.$parameter{'search_cat'}.'" />';
	
	$html .= '<input name="view" type="hidden" value="'.$parameter{'view'}.'">'."\n";	
	#$html .= '<input name="von"  type="hidden" value="'.$parameter{'von'}.'">'."\n";
	#$html .= '<input name="bis"  type="hidden" value="'.$parameter{'bis'}.'">'."\n";		


	$html .= '<input type="submit" value="Filtern"></p>'."\n";


	# where klausel
	$parameter{'where'} = 'where 1=1';
	if ($parameter{'von'} ne ''){
		$parameter{'where'} .= " and ort >= '".$parameter{'von'}."'";
	}
	if ($parameter{'bis'} ne ''){
		$parameter{'where'} .= " and ort <= '".$parameter{'bis'}."'";
	}

	if ($parameter{'search_ort'} ne ''){
		$parameter{'where'} .= " and ort like '%".$parameter{'search_ort'}."%'";
	}

	if ($parameter{'search_artikel'} ne ''){
		$parameter{'where'} .= " and artikel like '%".$parameter{'search_artikel'}."%'";
	}

	if ($parameter{'search_land'} ne ''){
		$parameter{'where'} .= " and land like '%".$parameter{'search_land'}."%'";
	}

	if ($parameter{'search_region'} ne ''){
		$parameter{'where'} .= " and region like '%".$parameter{'search_region'}."%'";
	}

	if ($parameter{'search_notiz'} ne ''){
		$parameter{'where'} .= " and notiz like '%".$parameter{'search_notiz'}."%'";
	}

	if ($parameter{'search_scan'} ne ''){
		$parameter{'where'} .= " and scan like '%".$parameter{'search_scan'}."%'";
	}

	if ($parameter{'search_cat'} ne ''){
		$parameter{'where'} .= " and cat like '%".$parameter{'search_cat'}."%'";
	}
	
	# SQL
	my $sql_text = "select id, ort, artikel, land, region, notiz, scan, cat from karten_ort ".$parameter{'where'}." order by ".$column_orderby." ".$column_sort." 
	limit ".$parameter{'offset'}.",".$parameter{'limit'}.";";



	# search in database
	my $sth = $dbh->prepare( $sql_text )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
	#$html .= '<p class="smalltext"/>'.$sql_text."</p>\n";					  
	$sth->execute or die $sth->errstr; # hier geschieht die Anfrage an die DB
	
	
	$html .= '<p>';
	$html .= '<a href="'.$script_name.'?view=location&offset='.$offset_lower.html_link_parameter().'&orderby='.$parameter{'orderby'}.'&sort='.$parameter{'sort'}.'">←</a>';
	$html .= ' '.$parameter{'offset'}.' bis '.$offset_end.' ';
	$html .= '<a href="'.$script_name.'?view=location&offset='.$offset_higher.html_link_parameter().'&orderby='.$parameter{'orderby'}.'&sort='.$parameter{'sort'}.'">→</a>';
	$html .= '</p>';
	
	$html .= '<table class="table">';
	$html .= '<tr>';
	$html .= '<th class="table">ID';
	$html .= '</th>';

	$html .= '<th class="table">Ort';
$html .= '<a href="'.$script_name.'?view=location'.html_link_parameter().'&orderby=ort&sort=asc">↑</a>';
$html .= '<a href="'.$script_name.'?view=location'.html_link_parameter().'&orderby=ort&sort=desc">↓</a>';
$html .= '</th>';

	$html .= '<th class="table">Artikel';
$html .= '<a href="'.$script_name.'?view=location'.html_link_parameter().'&orderby=artikel&sort=asc">↑</a>';
$html .= '<a href="'.$script_name.'?view=location'.html_link_parameter().'&orderby=artikel&sort=desc">↓</a>';
$html .= '</th>';


	$html .= '<th class="table">Land';
$html .= '<a href="'.$script_name.'?view=location'.html_link_parameter().'&orderby=land&sort=asc">↑</a>';
$html .= '<a href="'.$script_name.'?view=location'.html_link_parameter().'&orderby=land&sort=desc">↓</a>';
$html .= '</th>';

	$html .= '<th class="table">Region';
$html .= '<a href="'.$script_name.'?view=location'.html_link_parameter().'&orderby=region&sort=asc">↑</a>';
$html .= '<a href="'.$script_name.'?view=location'.html_link_parameter().'&orderby=region&sort=desc">↓</a>';
$html .= '</th>';

	$html .= '<th class="table">Notiz';
$html .= '<a href="'.$script_name.'?view=location'.html_link_parameter().'&orderby=notiz&sort=asc">↑</a>';
$html .= '<a href="'.$script_name.'?view=location'.html_link_parameter().'&orderby=notiz&sort=desc">↓</a>';
$html .= '</th>';

	$html .= '<th class="table">Scan';
$html .= '<a href="'.$script_name.'?view=location'.html_link_parameter().'&orderby=scan&sort=asc">↑</a>';
$html .= '<a href="'.$script_name.'?view=location'.html_link_parameter().'&orderby=scan&sort=desc">↓</a>';
$html .= '</th>';

	$html .= '<th class="table">Kategorie';
$html .= '<a href="'.$script_name.'?view=location'.html_link_parameter().'&orderby=cat&sort=asc">↑</a>';
$html .= '<a href="'.$script_name.'?view=location'.html_link_parameter().'&orderby=cat&sort=desc">↓</a>';
$html .= '</th>';


	$html .= '</tr>'."\n";
	

	my %row;
	my $counter = 0;

	# jede Zeile
	while (my @array = $sth->fetchrow_array()) {
	    ($row{'id'},
			$row{'ort'},
			$row{'artikel'},
			$row{'land'},
			$row{'region'},
			$row{'notiz'},
			$row{'scan'},
			$row{'cat'}
			) = @array;

		$row{'ort'} = encode_entities($row{'ort'});
		$row{'artikel'} = encode_entities($row{'artikel'});
		$row{'scan'} = encode_entities($row{'scan'});
		#$row{'land'} = encode_entities($row{'land'});
		$row{'notiz'} = encode_entities($row{'notiz'});
		#$row{'scan'} = encode_entities($row{'scan'});

		$counter ++;

		$html .= '<tr>';
		$html .= '<td class="table" '.row_color($counter).'>'.$row{'id'}.'</td>';
		$html .= '<td class="table" '.row_color($counter).'><a href="'.$script_name.'?view=ort&nr='.$row{'id'}.'">'.$row{'ort'}.'</a>'.'</td>';
		$html .= '<td class="table" '.row_color($counter).'>'.$row{'artikel'}.'</td>';
		$html .= '<td class="table" '.row_color($counter).'>'.$row{'land'}.'</td>';
		$html .= '<td class="table" '.row_color($counter).'>'.$row{'region'}.'</td>';
		$html .= '<td class="table" '.row_color($counter).'>'.$row{'notiz'}.'</td>';
		$html .= '<td class="table" '.row_color($counter).'>'.$row{'scan'}.'</td>';
		$html .= '<td class="table" '.row_color($counter).'>'.encode_entities($row{'cat'}).'</td>';
		$html .= '</tr>'."\n";	

	}
	$html .= '</table>';
	return ($html);
}


########################################################################
# HTML-Page für genau einen Ort
########################################################################

sub get_html_page_for_one_location{
	my $html = '';
	
	$html .= '<p>→ <a href="'.$script_name.'">Startseite</a> → ';
	$html .= '<a href="'.$script_name.'?view=location">Liste aller Orte</a> → Ort Nr. ';
	
	my $headline = '';
	if ($parameter{'nr'} =~ /^[0-9]+$/) {
		$headline = 'Ort Nr. '.$parameter{'nr'};
		$html .= $parameter{'nr'}."\n";
	} else {
		$html .= '?'."\n";	
	}
	$html .= '</p>'."\n"."\n";
	$html .= '<h2>'.$headline.'</h2>'."\n";


	#$html .=  $parameter{'nr'}.'<br />'."\n";

	$html .= '<form action="'.$script_name.'?view=location&nr='.$parameter{'nr'}.'" accept-charset="utf-8" method="post" >'."\n";


	my $sql_text = "select id, ort, artikel, land, region, notiz, scan, cat from karten_ort where id=".$parameter{'nr'}.";";
	$html .= '<p>'.$sql_text.'</p>';
	my $sth = $dbh->prepare( $sql_text )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
	$sth->execute or die $sth->errstr; 	
	my %ort;
	while (my @array = $sth->fetchrow_array()) {
	    ($ort{'id'},
			$ort{'ort'},
			$ort{'artikel'},
			$ort{'land'},
			$ort{'region'},
			$ort{'notiz'},
			$ort{'scan'},
			$ort{'cat'}
			) = @array;

		$ort{'ort'} = encode_entities($ort{'ort'});
		$ort{'artikel'} = encode_entities($ort{'artikel'});
		$ort{'land'} = encode_entities($ort{'land'});
		$ort{'notiz'} = encode_entities($ort{'notiz'});
		$ort{'scan'} = encode_entities($ort{'scan'});
	
		#print $ort{'motiv'}.'<br />';

		$html .= '<table class="table">'."\n";
		$html .= '<tr><th class="table">Feld</th><th class="table">Inhalt</th></tr>'."\n";
		$html .= '<tr><td class="table">Nummer</td><td class="table">'.$ort{'id'}.'</td></tr>'."\n";
		$html .= '<tr><td class="table">Ort   </td><td class="table">'.$ort{'ort'}.'   </td></tr>'."\n";

		# artikel
		$html .= '<tr><td class="table">Artikel </td>';
		$html .= '<td class="table">';
		$html .= '<input name="artikel" type="text" size="60" maxlength="400" value="'.$ort{'artikel'}.'" >';
		$html .= ' </td></tr>'."\n";

		# land
		$html .= '<tr><td class="table">Land </td>';
		$html .= '<td class="table">';
		$html .= '<input name="land" type="text" size="60" maxlength="400" value="'.$ort{'land'}.'" >';
		$html .= ' </td></tr>'."\n";

		# region
		$html .= '<tr><td class="table">Region</td>';
		$html .= '<td class="table">';
		$html .= '<input name="region" type="text" size="60" maxlength="400" value="'.$ort{'region'}.'" >';
		$html .= ' </td></tr>'."\n";


		# notiz
		$html .= '<tr><td class="table">Notiz </td>';
		$html .= '<td class="table">';
		$html .= '<input name="notiz" type="text" size="60" maxlength="400" value="'.$ort{'notiz'}.'" >';
		$html .= ' </td></tr>'."\n";

		# cat
		$html .= '<tr><td class="table">Commons-Kategorie</td>';
		$html .= '<td class="table">';
		$html .= '<input name="categorie" type="text" size="60" maxlength="400" value="'.encode_entities($ort{'cat'}).'" >';
		$html .= ' </td></tr>'."\n";

		$html .= '<tr><td class="table">Scan</td><td class="table">'.$ort{'scan'}.'   </td></tr>'."\n";
		$html .= '</table>'."\n";

	}

	# hidden parameter: nr, modus
	$html .= '<input name="view" type="hidden" value="'.$parameter{'view'}.'">'."\n";	
	$html .= '<input name="nr" type="hidden" value="'.$parameter{'nr'}.'">'."\n";
	$html .= '<input name="modus" type="hidden" value="update">'."\n";
	$html .= '<p><ul>
<li>Artikel: Artikel mit Commonscat; alternativ bei REDIRECT Ziel des REDIRECT; alternativ bei Ortsteil übergeordnete Gemeinde</li>
<li>Land: Land wo der Ort heute liegt (2 Buchstaben)</li>
<li>Region: nach ISO 3166-2 (Bayern=BY, Sachsen=SN, ...)</li>
</ul></p>'."\n";

	$html .= '<p><input type="submit" value="Speichern"></p>'."\n";


	$html .= '<h2>Suche</h2>';
	$html .= '<p><ul>';

	$html .= '<li>Suche nach Ort "'.$ort{'ort'}.'" in ';
	$html .= '<a href="https://www.google.de/search?q='.$ort{'ort'}.'" target="_blank">Google</a>, ';
	$html .= '<a href="https://de.wikipedia.org/w/index.php?search='.$ort{'ort'}.'" target="_blank">Wikipedia</a>';
	$html .= '</li>';

	if ($ort{'artikel'} ne '') {
		$html .= '<li>Suche nach Artikel "'.$ort{'artikel'}.'" in ';
		$html .= '<a href="https://de.wikipedia.org/w/index.php?search='.$ort{'artikel'}.'" target="_blank">Wikipedia</a>, ';
		$html .= '<a href="https://commons.wikimedia.org/w/index.php?search=Category:'.$ort{'artikel'}.'" target="_blank">Commons</a>';
		$html .= '</li>';
	}

	if ($ort{'cat'} ne '') {
		$html .= '<li>Suche nach "Category:'.encode_entities($ort{'cat'}).'" in ';
		$html .= '<a href="https://commons.wikimedia.org/w/index.php?search=Category:'.encode_entities($ort{'cat'}).'" target="_blank">Commons</a>';
		$html .= '</li>';
	}
	$html .= '</ul>';
	$html .= '</p>'."\n"."\n";


	###################################################	
	# Karten aus dem ort
	###################################################

	$html .= '<h2>Karten aus dem Ort</h2>';
	
	$ort{'ort'} = decode_entities($ort{'ort'});
	my $sql_text = "select nummer, ort, land, motiv, jahr, notiz from karten where ort ='".$ort{'ort'}."' order by motiv ;";

	# search in database
	my $sth = $dbh->prepare( $sql_text )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
	#$html .= '<p class="smalltext"/>'.$sql_text."</p>\n";					  
	$sth->execute or die $sth->errstr; # hier geschieht die Anfrage an die DB

	$html .= '<table class="table">';
	$html .= '<tr><th class="table">Nr</th><th class="table">Ort</th>';
	$html .= '<th class="table">Land</th><th class="table">Motiv</th>';
	$html .= '<th class="table">Jahr</th><th class="table">Notiz</th></tr>';


	my %card;
	my $counter = 0;

	# jede Zeile jeder Karte
	while (my @array = $sth->fetchrow_array()) {
	    ($card{'nummer'},
			$card{'ort'},
			$card{'land'},
			$card{'motiv'},
			$card{'jahr'},
			$card{'notiz'}
			) = @array;

		$card{'ort'} = encode_entities($card{'ort'});
		$card{'land'} = encode_entities($card{'land'});
		$card{'motiv'} = encode_entities($card{'motiv'});
		$card{'notiz'} = encode_entities($card{'notiz'});

		$counter ++;

		$html .= '<tr>';
		$html .= '<td class="table" '.row_color($counter).'><a href="'.$script_name.'?view=card&nr='.$card{'nummer'}.'">'.$card{'nummer'}.'</a>'.'</td>';
		$html .= '<td class="table" '.row_color($counter).'>'.$card{'ort'}.'</td>';
		$html .= '<td class="table" '.row_color($counter).'>'.$card{'land'}.'</td>';
		$html .= '<td class="table" '.row_color($counter).'><a href="'.$script_name.'?view=card&nr='.$card{'nummer'}.'">'.$card{'motiv'}.'</a>'.'</td>';
		$html .= '<td class="table" '.row_color($counter).'>'.$card{'jahr'}.'</td>';
		$html .= '<td class="table" '.row_color($counter).'>'.$card{'notiz'}.'</td>';
		$html .= '</tr>'."\n";	

	}
	$html .= '</table>';


		

	return ($html);
}








########################################
# Liste aller Kategorien
########################################

sub get_html_list_categories{
	$column_orderby = 'cat';
	$column_orderby = 'id'       if ($parameter{'orderby'} eq 'id');
	$column_orderby = 'cat'      if ($parameter{'orderby'} eq 'cat');
	
	$column_sort    = 'asc'       if ( $column_sort  eq '');	# default
	
	my $html = '';

	$html .= '<p>→ <a href="'.$script_name.'">Startseite</a> → Liste aller Kategorien</p>'."\n";	


	$html .= '<p><a href="'.$script_name.'?view=new_cat">Neue Kategorie hinzufügen</a></p>'."\n";	


	# formular
	$html .= '<form action="'.$script_name.'?view=list" accept-charset="utf-8" method="post" >'."\n";
	$html .= '<p>Von (A-Z) <input name="von" type="text" size="5" maxlength="8" value="'.$parameter{'von'}.'" /> bis ';
	$html .= '<input name="bis" type="text" size="5" maxlength="8" value="'.$parameter{'bis'}.'" />';
	$html .= ' Maximalanzahl <input name="limit" type="text" size="5" maxlength="8" value="'.$parameter{'limit'}.'" />'.'</p>';
	$html .= ' ID <input name="search_id" type="text" size="5" maxlength="50" value="'.$parameter{'search_id'}.'" />';
	$html .= ' Kategorie <input name="search_cat" type="text" size="5" maxlength="50" value="'.$parameter{'search_cat'}.'" />';
	
	$html .= '<input name="view" type="hidden" value="'.$parameter{'view'}.'">'."\n";	
	#$html .= '<input name="von"  type="hidden" value="'.$parameter{'von'}.'">'."\n";
	#$html .= '<input name="bis"  type="hidden" value="'.$parameter{'bis'}.'">'."\n";		


	$html .= '<input type="submit" value="Filtern"></p>'."\n";


	# where klausel
	$parameter{'where'} = 'where 1=1';
	if ($parameter{'von'} ne ''){
		$parameter{'where'} .= " and cat >= '".$parameter{'von'}."'";
	}
	if ($parameter{'bis'} ne ''){
		$parameter{'where'} .= " and cat <= '".$parameter{'bis'}."'";
	}

	if ($parameter{'search_id'} ne ''){
		$parameter{'where'} .= " and id like '%".$parameter{'search_id'}."%'";
	}

	if ($parameter{'search_cat'} ne ''){
		$parameter{'where'} .= " and cat like '%".$parameter{'search_cat'}."%'";
	}

	
	# SQL
	my $sql_text = "select id, cat  from karten_commons_cat ".$parameter{'where'}." order by ".$column_orderby." ".$column_sort." 
	limit ".$parameter{'offset'}.",".$parameter{'limit'}.";";



	# search in database
	my $sth = $dbh->prepare( $sql_text )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
	$html .= '<p class="smalltext"/>'.$sql_text."</p>\n";					  
	#print $sql_text;
	$sth->execute or die $sth->errstr; # hier geschieht die Anfrage an die DB
	
	
	$html .= '<p>';
	$html .= '<a href="'.$script_name.'?view=cat&offset='.$offset_lower.html_link_parameter().'&orderby='.$parameter{'orderby'}.'&sort='.$parameter{'sort'}.'">←</a>';
	$html .= ' '.$parameter{'offset'}.' bis '.$offset_end.' ';
	$html .= '<a href="'.$script_name.'?view=cat&offset='.$offset_higher.html_link_parameter().'&orderby='.$parameter{'orderby'}.'&sort='.$parameter{'sort'}.'">→</a>';
	$html .= '</p>';
	
	$html .= '<table class="table">';
	$html .= '<tr>';

	$html .= '<th class="table">ID';
$html .= '<a href="'.$script_name.'?view=cat'.html_link_parameter().'&orderby=id&sort=asc">↑</a>';
$html .= '<a href="'.$script_name.'?view=cat'.html_link_parameter().'&orderby=id&sort=desc">↓</a>';
$html .= '</th>';

	$html .= '<th class="table">Kategorie';
$html .= '<a href="'.$script_name.'?view=cat'.html_link_parameter().'&orderby=cat&sort=asc">↑</a>';
$html .= '<a href="'.$script_name.'?view=cat'.html_link_parameter().'&orderby=cat&sort=desc">↓</a>';
$html .= '</th>';

	

	$html .= '</tr>'."\n";
	

	my %row;
	my $counter = 0;

	# jede Zeile
	while (my @array = $sth->fetchrow_array()) {
	    ($row{'id'},
			$row{'cat'},
			) = @array;

		$row{'cat'} = encode_entities($row{'cat'});
		$counter ++;

		$html .= '<tr>';
		$html .= '<td class="table" '.row_color($counter).'>'.$row{'id'}.'</td>';
		$html .= '<td class="table" '.row_color($counter).'><a href="'.$script_name.'?view=onecat&cat_id='.$row{'id'}.'">'.$row{'cat'}.'</a>'.'</td>';
		$html .= '<td class="table" '.row_color($counter).'><a href="https://commons.wikimedia.org/wiki/Category:'.$row{'cat'}.'" target="_blank">Commonscat</a>'.'</td>';		$html .= '</tr>'."\n";	

	}
	$html .= '</table>';
	return ($html);
}






########################################
# Neue Kategorien
########################################



sub get_html_new_cat{
	my $html = '';
	
	$html .= '<p>→ <a href="'.$script_name.'">Startseite</a> → ';
	$html .= '<a href="'.$script_name.'?view=cat">Liste aller Kategorien</a> → Neue Kategorie hinzufügen';
	$html .= '<h2>Neue Kategorie hinzufügen</h2>';

	$html .= '<form action="'.$script_name.'?view=cat" accept-charset="utf-8" method="post" >'."\n";
	$html .= 'Neue Kategorie: <input name="categorie" type="text" size="60" maxlength="400" value="'.'" >';

	# hidden parameter: view, modus
	$html .= '<input name="view" type="hidden" value="cat">'."\n";
	$html .= '<input name="modus" type="hidden" value="update">'."\n";
	$html .= '<p><input type="submit" value="Speichern"></p>'."\n";


	return ($html);
}



########################################
# Eine Kategorien im Detail anzeigen
########################################


sub get_html_page_for_one_category{
	my $html = '';
	
	$html .= '<p>→ <a href="'.$script_name.'">Startseite</a> → ';
	$html .= '<a href="'.$script_name.'?view=cat">Liste aller Kategorien</a> → Kategorien ';
	
	my $headline = '';
	my $id_left ;
	my $id_right;
	my $navi = '';
	if ($parameter{'cat_id'} =~ /^[0-9]+$/) {
		$headline = 'Nr. '.$parameter{'cat_id'};
		$html .= $parameter{'cat_id'}."\n";
		$id_left = $parameter{'cat_id'} -1;
		$id_right = $parameter{'cat_id'} +1;
		$navi .= '<a href="'.$script_name.'?view=onecat&cat_id='.$id_left.'">Vorherige Kategorie</a> - ';
		$navi .= '<a href="'.$script_name.'?view=onecat&cat_id='.$id_right.'">Nächste Kategorie</a>';

	} else {
		$html .= '?'."\n";	
	}
	$html .= '</p>'."\n"."\n";
	$html .= '<h2>'.$headline.'</h2>'."\n";
	$html .= '<p>'.$navi.'</p>'."\n";


	#$html .=  $parameter{'cat_id'}.'<br />'."\n";

	$html .= '<form action="'.$script_name.'?view=word&word_id='.$parameter{'word_id'}.'" accept-charset="utf-8" method="post" >'."\n";


	my $sql_text = "select id, cat from karten_commons_cat where id=".$parameter{'cat_id'}.";";
	$html .= '<p>'.$sql_text.'</p>';
	my $sth = $dbh->prepare( $sql_text )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
	$sth->execute or die $sth->errstr; 	
	my %row;
	while (my @array = $sth->fetchrow_array()) {
	    ($row{'cat_id'},
			$row{'categorie'},
			) = @array;
	
		#print $row{'motiv'}.'<br />';

		$html .= '<table class="table">'."\n";
		$html .= '<tr><th class="table">Feld</th><th class="table">Inhalt</th></tr>'."\n";
		$html .= '<tr><td class="table">Cat-ID</td><td class="table">'.$row{'cat_id'}.'</td></tr>'."\n";

		# Kategorie
		$html .= '<tr><td class="table">Kategorie </td>';
		$html .= '<td class="table">';
		$html .= '<input name="categorie" type="text" size="60" maxlength="400" value="'.encode_entities($row{'categorie'}).'" >';
		$html .= ' </td></tr>'."\n";


		$html .= ' </td></tr>'."\n";


		$html .= '</table>'."\n";

	}

	# hidden parameter: nr, modus
	$html .= '<input name="view" type="hidden" value="'.$parameter{'view'}.'">'."\n";	
	$html .= '<input name="cat_id" type="hidden" value="'.$parameter{'cat_id'}.'">'."\n";
	$html .= '<input name="modus" type="hidden" value="update">'."\n";
	$html .= '<p><input type="submit" value="Speichern"></p>'."\n";

	$parameter{'cat'} = $row{'categorie'};


	# get_cat_id
	my $cat=$parameter{'cat'};
	$cat =~ s/'/''/g;

	my $sql_text = "select id from karten_commons_cat where lower(cat) = lower('".$cat."')";
	#print $sql_text."<br />";

	my $sth = $dbh->prepare( $sql_text )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
	$sth->execute or die $sth->errstr; # hier geschieht die Anfrage an die DB
	
	while (my @array = $sth->fetchrow_array()) {
	   	($parameter{'cat_id'}) =@array;
	}
	
	if ($parameter{'cat_id'} eq '' and $parameter{'cat'} ne '') {
		$parameter{'cat'} =~ s/^\s+|\s+$//g;  #Leerzeichen vorne hinten
		$parameter{'cat'} =~ s/^Category://gi;
		$parameter{'cat'} =~ s/_/ /g;

		$sql_text = "insert into karten_commons_cat (cat) values ('".$cat."')";
		$sth = $dbh->prepare( $sql_text )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
		$sth->execute or die $sth->errstr; # hier geschieht die Anfrage an die DB

		$sql_text = "select id from karten_commons_cat where lower(cat) = lower('".$cat."')";
		$sth = $dbh->prepare( $sql_text )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
		$sth->execute or die $sth->errstr; # hier geschieht die Anfrage an die DB
	
		while (my @array = $sth->fetchrow_array()) {
		   	($parameter{'cat_id'}) =@array;
		}
	}	

	


	#$html .= 'Cat:'.$parameter{'cat'}.'<br />';
	#$html .= 'Cat_ID:'.$parameter{'cat_id'}.'<br />';



	#$html .= '<p><ul>
#<li>Die letzte benutzte Kategorie wird beim Stichwort mit gespeichert.</li>
#</ul></p>'."\n";




	$html .= '<ul>';
	$html .= '<li>Suche nach Stichwort "'.encode_entities($parameter{'cat'}).'" bei <a href="https://www.google.de/search?q='.encode_entities($parameter{'cat'}).'" target="_blank">Google</a>, ';
	$html .= '<a href="https://de.wikipedia.org/w/index.php?search='.encode_entities($parameter{'cat'}).'" target="_blank">Wikipedia</a>, ';
	$html .= '<a href="https://commons.wikimedia.org/w/index.php?search='.encode_entities($parameter{'cat'}).'" target="_blank">Commons</a></li>';

	$html .= '<li>Gehe zur Commons <a href="https://commons.wikimedia.org/wiki/Category:'.$parameter{'cat'}.'?uselang=de" target="_blank"> "Category:'.encode_entities($parameter{'cat'}).'"</a></li>' if ($parameter{'cat'} ne '');

	$html .= '</ul>';


	###################################################	
	# Karten einer Kategorie
	###################################################

	$html .= '<h2>Karten der Kategorie mit der Kategorie</h2>';
	
	$sql_text = "select k.nummer, k.ort, k.land, k.motiv, k.jahr, k.notiz
from karten k 
join karten_cat kc 
on (k.nummer = kc.nr)
where kc.cat_id = '".$parameter{'cat_id'}."'
order by k.motiv ;";

	# search in database
	my $sth = $dbh->prepare( $sql_text )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
	#$html .= '<p class="smalltext"/>'.$sql_text."</p>\n";					  
	$sth->execute or die $sth->errstr; # hier geschieht die Anfrage an die DB

	$html .= '<table class="table">';
	$html .= '<tr><th class="table">Nr</th><th class="table">Ort</th>';
	$html .= '<th class="table">Land</th><th class="table">Motiv</th>';
	$html .= '<th class="table">Jahr</th><th class="table">Notiz</th>';
	$html .= '<th class="table">Kategorie</th></tr>';


	my %row;
	my $counter = 0;

	# jede Zeile jeder Karte
	while (my @array = $sth->fetchrow_array()) {
	    ($row{'nummer'},
			$row{'ort'},
			$row{'land'},
			$row{'motiv'},
			$row{'jahr'},
			$row{'notiz'}
			) = @array;

		$row{'ort'} = encode_entities($row{'ort'});
		$row{'land'} = encode_entities($row{'land'});
		$row{'motiv'} = encode_entities($row{'motiv'});
		$row{'notiz'} = encode_entities($row{'notiz'});

		$counter ++;

		$html .= '<tr>';
		$html .= '<td class="table" '.row_color($counter).'><a href="'.$script_name.'?view=card&nr='.$row{'nummer'}.'">'.$row{'nummer'}.'</a>'.'</td>';
		$html .= '<td class="table" '.row_color($counter).'>'.$row{'ort'}.'</td>';
		$html .= '<td class="table" '.row_color($counter).'>'.$row{'land'}.'</td>';
		$html .= '<td class="table" '.row_color($counter).'><a href="'.$script_name.'?view=card&nr='.$row{'nummer'}.'">'.$row{'motiv'}.'</a>'.'</td>';
		$html .= '<td class="table" '.row_color($counter).'>'.$row{'jahr'}.'</td>';
		$html .= '<td class="table" '.row_color($counter).'>'.$row{'notiz'}.'</td>';
		$html .= '<td class="table" '.row_color($counter).'><a href="'.$script_name.'?view=oneword&word_id='.$parameter{'word_id'}.'&nr='.$row{'nummer'}.'&del_cat='.$parameter{'cat_id'}.'&modus=update">Entfernen</a></td>';
		$html .= '</tr>'."\n";	

	}
	$html .= '</table>';

	return ($html);
}




########################################
# Liste aller Stichworte
########################################

sub get_html_list_word{
	$column_orderby = 'word';
	$column_orderby = 'id'       if ($parameter{'orderby'} eq 'id');
	$column_orderby = 'word'      if ($parameter{'orderby'} eq 'word');
	$column_orderby = 'cat'      if ($parameter{'orderby'} eq 'cat');
	$column_orderby = 'done'      if ($parameter{'orderby'} eq 'done');
	$column_orderby = 'notiz'      if ($parameter{'orderby'} eq 'notiz');
	
	$column_sort    = 'asc'       if ( $column_sort  eq '');	# default
	
	my $html = '';

	$html .= '<p>→ <a href="'.$script_name.'">Startseite</a> → Liste aller Stichwörter</p>'."\n";	


	$html .= '<p><a href="'.$script_name.'?view=new_word">Neues Stichwort hinzufügen</a></p>'."\n";	


	# formular
	$html .= '<form action="'.$script_name.'?view=word" accept-charset="utf-8" method="post" >'."\n";
	$html .= '<p>Von (A-Z) <input name="von" type="text" size="5" maxlength="8" value="'.$parameter{'von'}.'" /> bis ';
	$html .= '<input name="bis" type="text" size="5" maxlength="8" value="'.$parameter{'bis'}.'" />';
	$html .= ' Maximalanzahl <input name="limit" type="text" size="5" maxlength="8" value="'.$parameter{'limit'}.'" />'.'</p>';
	$html .= ' ID <input name="search_id" type="text" size="5" maxlength="50" value="'.$parameter{'search_id'}.'" />';
	$html .= ' Stichwort <input name="search_word" type="text" size="5" maxlength="50" value="'.$parameter{'search_word'}.'" />';
	$html .= ' Kategorie <input name="search_cat" type="text" size="5" maxlength="50" value="'.$parameter{'search_cat'}.'" />';
	$html .= ' Erledigt <input name="search_done" type="text" size="5" maxlength="50" value="'.$parameter{'search_done'}.'" />';
	$html .= ' Notiz <input name="search_notiz" type="text" size="5" maxlength="50" value="'.$parameter{'search_notiz'}.'" />';

	
	$html .= '<input name="view" type="hidden" value="'.$parameter{'view'}.'">'."\n";	
	#$html .= '<input name="von"  type="hidden" value="'.$parameter{'von'}.'">'."\n";
	#$html .= '<input name="bis"  type="hidden" value="'.$parameter{'bis'}.'">'."\n";		


	$html .= '<input type="submit" value="Filtern"></p>'."\n";


	# where klausel
	$parameter{'where'} = 'where 1=1';
	if ($parameter{'von'} ne ''){
		$parameter{'where'} .= " and word >= '".$parameter{'von'}."'";
	}
	if ($parameter{'bis'} ne ''){
		$parameter{'where'} .= " and word <= '".$parameter{'bis'}."'";
	}

	if ($parameter{'search_id'} ne ''){
		$parameter{'where'} .= " and id like '%".$parameter{'search_id'}."%'";
	}

	if ($parameter{'search_word'} ne ''){
		$parameter{'where'} .= " and word like '%".$parameter{'search_word'}."%'";
	}

	if ($parameter{'search_cat'} ne ''){
		$parameter{'where'} .= " and cat like '%".$parameter{'search_cat'}."%'";
	}

	if ($parameter{'search_done'} ne ''){
		$parameter{'where'} .= " and done like '%".$parameter{'search_done'}."%'";
	}

	if ($parameter{'search_notiz'} ne ''){
		$parameter{'where'} .= " and notiz like '%".$parameter{'search_notiz'}."%'";
	}


	
	# SQL
	my $sql_text = "select id, word, cat, notiz, done  from karten_word ".$parameter{'where'}." order by ".$column_orderby." ".$column_sort." 
	limit ".$parameter{'offset'}.",".$parameter{'limit'}.";";



	# search in database
	my $sth = $dbh->prepare( $sql_text )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
	$html .= '<p class="smalltext"/>'.$sql_text."</p>\n";					  
	#print $sql_text;
	$sth->execute or die $sth->errstr; # hier geschieht die Anfrage an die DB
	
	
	$html .= '<p>';
	$html .= '<a href="'.$script_name.'?view=word&offset='.$offset_lower.html_link_parameter().'&orderby='.$parameter{'orderby'}.'&sort='.$parameter{'sort'}.'">←</a>';
	$html .= ' '.$parameter{'offset'}.' bis '.$offset_end.' ';
	$html .= '<a href="'.$script_name.'?view=word&offset='.$offset_higher.html_link_parameter().'&orderby='.$parameter{'orderby'}.'&sort='.$parameter{'sort'}.'">→</a>';
	$html .= '</p>';
	
	$html .= '<table class="table">';
	$html .= '<tr>';

	$html .= '<th class="table">ID';
	$html .= '<a href="'.$script_name.'?view=word'.html_link_parameter().'&orderby=id&sort=asc">↑</a>';
	$html .= '<a href="'.$script_name.'?view=word'.html_link_parameter().'&orderby=id&sort=desc">↓</a>';
	$html .= '</th>';

	$html .= '<th class="table">Stichwort';
	$html .= '<a href="'.$script_name.'?view=word'.html_link_parameter().'&orderby=word&sort=asc">↑</a>';
	$html .= '<a href="'.$script_name.'?view=word'.html_link_parameter().'&orderby=word&sort=desc">↓</a>';
	$html .= '</th>';

	$html .= '<th class="table">Kategorie';
	$html .= '<a href="'.$script_name.'?view=word'.html_link_parameter().'&orderby=cat&sort=asc">↑</a>';
	$html .= '<a href="'.$script_name.'?view=word'.html_link_parameter().'&orderby=cat&sort=desc">↓</a>';
	$html .= '</th>';

	$html .= '<th class="table">Erledigt';
	$html .= '<a href="'.$script_name.'?view=word'.html_link_parameter().'&orderby=done&sort=asc">↑</a>';
	$html .= '<a href="'.$script_name.'?view=word'.html_link_parameter().'&orderby=done&sort=desc">↓</a>';
	$html .= '</th>';	

	$html .= '<th class="table">Notiz';
	$html .= '<a href="'.$script_name.'?view=word'.html_link_parameter().'&orderby=notiz&sort=asc">↑</a>';
	$html .= '<a href="'.$script_name.'?view=word'.html_link_parameter().'&orderby=notiz&sort=desc">↓</a>';
	$html .= '</th>';	


	$html .= '</tr>'."\n";
	

	my %row;
	my $counter = 0;

	# jede Zeile
	while (my @array = $sth->fetchrow_array()) {
	    ($row{'word_id'},
		 $row{'word'},
			$row{'cat'},
			$row{'notiz'},
			$row{'done'},
			) = @array;

		$counter ++;

		$html .= '<tr>';
		$html .= '<td class="table" '.row_color($counter).'>'.$row{'word_id'}.'</td>';
		$html .= '<td class="table" '.row_color($counter).'><a href="'.$script_name.'?view=oneword&word_id='.$row{'word_id'}.'">'.encode_entities($row{'word'}).'</a>'.'</td>';
		$html .= '<td class="table" '.row_color($counter).'>'.encode_entities($row{'cat'}).'</td>';

		
		$html .= '<td class="table" '.row_color($counter).'>';
		if ($row{'done'} eq 'yes') {
			$html .= '<span style="background-color:lightgreen;">';
		} else {
			$html .= '<span style="background-color:yellow">'; #FF9966;
		}
		$html .= encode_entities($row{'done'}).'</span></td>';

		$html .= '<td class="table" '.row_color($counter).'>'.encode_entities($row{'notiz'}).'</td>';


	}
	$html .= '</table>';
	return ($html);
}



########################################################################
# HTML-Page für genau ein Stichwort
########################################################################

sub get_html_page_for_one_word{
	my $html = '';
	
	$html .= '<p>→ <a href="'.$script_name.'">Startseite</a> → ';
	$html .= '<a href="'.$script_name.'?view=word">Liste aller Stichworte</a> → Stichwort ';
	
	my $headline = '';
	my $id_left ;
	my $id_right;
	my $navi = '';
	if ($parameter{'word_id'} =~ /^[0-9]+$/) {
		$headline = 'Nr. '.$parameter{'word_id'};
		$html .= $parameter{'word_id'}."\n";
		$id_left = $parameter{'word_id'} -1;
		$id_right = $parameter{'word_id'} +1;
		$navi .= '<a href="'.$script_name.'?view=oneword&word_id='.$id_left.'">Vorheriges Wort</a> - ';
		$navi .= '<a href="'.$script_name.'?view=oneword&word_id='.$id_right.'">Nächstes Wort</a>';

	} else {
		$html .= '?'."\n";	
	}
	$html .= '</p>'."\n"."\n";
	$html .= '<h2>'.$headline.'</h2>'."\n";
	$html .= '<p>'.$navi.'</p>'."\n";


	#$html .=  $parameter{'nr'}.'<br />'."\n";

	$html .= '<form action="'.$script_name.'?view=word&word_id='.$parameter{'word_id'}.'" accept-charset="utf-8" method="post" >'."\n";


	my $sql_text = "select id, word, cat, notiz, done from karten_word where id=".$parameter{'word_id'}.";";
	#$html .= '<p>'.$sql_text.'</p>';
	my $sth = $dbh->prepare( $sql_text )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
	$sth->execute or die $sth->errstr; 	
	my %row;
	while (my @array = $sth->fetchrow_array()) {
	    ($row{'word_id'},
			$row{'word'},
			$row{'categorie'},
			$row{'notiz'},
			$row{'done'},
			) = @array;
	
		#print $row{'motiv'}.'<br />';

		$html .= '<table class="table">'."\n";
		$html .= '<tr><th class="table">Feld</th><th class="table">Inhalt</th></tr>'."\n";
		$html .= '<tr><td class="table">Nummer</td><td class="table">'.$row{'word_id'}.'</td></tr>'."\n";

		# word
		$html .= '<tr><td class="table">Word</td><td class="table">'.encode_entities($row{'word'}).'</td></tr>'."\n";


		# Kategorie
		$html .= '<tr><td class="table">Kategorie </td>';
		$html .= '<td class="table">';
		$html .= '<input name="categorie" type="text" size="60" maxlength="400" value="'.encode_entities($row{'categorie'}).'" >';
		$html .= ' </td></tr>'."\n";


		# notiz
		$html .= '<tr><td class="table">Notiz </td>';
		$html .= '<td class="table">';
		$html .= '<input name="notiz" type="text" size="60" maxlength="400" value="'.encode_entities($row{'notiz'}).'" >';
		$html .= ' </td></tr>'."\n";

		# erledigt
		$html .= '<tr><td class="table">Erledigt?</td>'."\n";
		$html .= '<td class="table"><input type="radio" name="done" value="no" ';
		$html .= 'checked' if ($row{'done'} eq 'no' or $row{'done'} eq '');
		$html .= '> noch abzuarbeiten<br />'."\n";
		$html .= '<input type="radio" name="done" value="yes" ';
		$html .= 'checked' if $row{'done'} eq 'yes';
		$html .= '> Fertig (Alle, zu diesem Stichwort passende, Kategorien wurden den Karten hinzugefügt.)</td></tr>'."\n";

		$html .= ' </td></tr>'."\n";


		$html .= '</table>'."\n";

	}

	# hidden parameter: nr, modus
	$html .= '<input name="view" type="hidden" value="'.$parameter{'view'}.'">'."\n";	
	$html .= '<input name="word_id" type="hidden" value="'.$parameter{'word_id'}.'">'."\n";
	$html .= '<input name="modus" type="hidden" value="update">'."\n";
	$html .= '<p><input type="submit" value="Speichern"></p>'."\n";

	$parameter{'cat'} = $row{'categorie'};
	$parameter{'word'} = $row{'word'};



	# get_cat_id
	my $cat=$parameter{'cat'};
	$cat =~ s/'/''/g;

	my $sql_text = "select id from karten_commons_cat where lower(cat) = lower('".$cat."')";
	#print $sql_text."<br />";

	my $sth = $dbh->prepare( $sql_text )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
	$sth->execute or die $sth->errstr; # hier geschieht die Anfrage an die DB
	
	while (my @array = $sth->fetchrow_array()) {
	   	($parameter{'cat_id'}) =@array;
	}
	
	if ($parameter{'cat_id'} eq '' and $parameter{'cat'} ne '') {
		$parameter{'cat'} =~ s/^\s+|\s+$//g;  #Leerzeichen vorne hinten
		$parameter{'cat'} =~ s/^Category://gi;
		$parameter{'cat'} =~ s/_/ /g;

		$sql_text = "insert into karten_commons_cat (cat) values ('".$cat."')";
		$sth = $dbh->prepare( $sql_text )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
		$sth->execute or die $sth->errstr; # hier geschieht die Anfrage an die DB

		$sql_text = "select id from karten_commons_cat where lower(cat) = lower('".$cat."')";
		$sth = $dbh->prepare( $sql_text )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
		$sth->execute or die $sth->errstr; # hier geschieht die Anfrage an die DB
	
		while (my @array = $sth->fetchrow_array()) {
		   	($parameter{'cat_id'}) =@array;
		}
	}	

	


	#$html .= 'Cat:'.$parameter{'cat'}.'<br />';
	#$html .= 'Cat_ID:'.$parameter{'cat_id'}.'<br />';



	#$html .= '<p><ul>
#<li>Die letzte benutzte Kategorie wird beim Stichwort mit gespeichert.</li>
#</ul></p>'."\n";




	$html .= '<ul>';
	$html .= '<li>Suche nach Stichwort "'.encode_entities($parameter{'word'}).'" bei <a href="https://www.google.de/search?q='.encode_entities($parameter{'word'}).'" target="_blank">Google</a>, ';
	$html .= '<a href="https://de.wikipedia.org/w/index.php?search='.encode_entities($parameter{'word'}).'" target="_blank">Wikipedia</a>, ';
	$html .= '<a href="https://commons.wikimedia.org/w/index.php?search='.encode_entities($parameter{'word'}).'" target="_blank">Commons</a></li>';

	$html .= '<li>Gehe zur Commons <a href="https://commons.wikimedia.org/wiki/Category:'.$parameter{'cat'}.'?uselang=de" target="_blank"> "Category:'.encode_entities($parameter{'cat'}).'"</a></li>' if ($parameter{'cat'} ne '');

	$html .= '</ul>';


	###################################################	
	# Karten mit dem Stichwort ohne die Kategorie
	###################################################

	$html .= '<h2>Karten mit dem Stichwort <span style="background:yellow;">aber ohne der Kategorie</span></h2>';
	
	$sql_text = "select k.nummer, k.ort, k.land, k.motiv, k.jahr, k.notiz
from karten k 
left outer join karten_cat kc 
on (k.nummer = kc.nr)
where k.motiv like '%".$parameter{'word'}."%' 
and k.nummer not in (select kc.nr from karten_cat kc where kc.cat_id = '".$parameter{'cat_id'}."' )
order by k.ort, k.motiv ;";


	# search in database
	my $sth = $dbh->prepare( $sql_text )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
	#$html .= '<p class="smalltext"/>'.$sql_text."</p>\n";					  
	$sth->execute or die $sth->errstr; # hier geschieht die Anfrage an die DB

	$html .= '<table class="table">';
	$html .= '<tr><th class="table">Nr</th><th class="table">Ort</th>';
	$html .= '<th class="table">Land</th><th class="table">Motiv</th>';
	$html .= '<th class="table">Jahr</th><th class="table">Notiz</th>';
	$html .= '<th class="table">Kategorie</th><th class="table">Multi 5</th>';
	$html .= '</tr>';


	my %row;
	my $counter = 0;
	my $insert_count_5 = 0;
	my $insert_count_5_nr = '';

	# jede Zeile jeder Karte
	while (my @array = $sth->fetchrow_array()) {
	    ($row{'nummer'},
			$row{'ort'},
			$row{'land'},
			$row{'motiv'},
			$row{'jahr'},
			$row{'notiz'}
			) = @array;

		$row{'ort'} = encode_entities($row{'ort'});
		$row{'land'} = encode_entities($row{'land'});
		$row{'motiv'} = encode_entities($row{'motiv'});
		$row{'notiz'} = encode_entities($row{'notiz'});

		$counter ++;

		$insert_count_5 ++;
		if ($insert_count_5 > 10) {
			$insert_count_5 = 1;
			$insert_count_5_nr = '';
		}

		$insert_count_5_nr .= '_'.$row{'nummer'};
		$insert_count_5_nr =~ s/^_//g;



		$html .= '<tr>';
		$html .= '<td class="table" '.row_color($counter).'><a href="'.$script_name.'?view=card&nr='.$row{'nummer'}.'">'.$row{'nummer'}.'</a>'.'</td>';
		$html .= '<td class="table" '.row_color($counter).'>'.$row{'ort'}.'</td>';
		$html .= '<td class="table" '.row_color($counter).'>'.$row{'land'}.'</td>';
		$html .= '<td class="table" '.row_color($counter).'><a href="'.$script_name.'?view=card&nr='.$row{'nummer'}.'">'.$row{'motiv'}.'</a>'.'</td>';
		$html .= '<td class="table" '.row_color($counter).'>'.$row{'jahr'}.'</td>';
		$html .= '<td class="table" '.row_color($counter).'>'.$row{'notiz'}.'</td>';
		$html .= '<td class="table" '.row_color($counter).'><a href="'.$script_name.'?view=oneword&word_id='.$parameter{'word_id'}.'&nr='.$row{'nummer'}.'&add_cat='.$parameter{'cat_id'}.'&modus=update">Hinzuf&uuml;gen</a></td>';
		$html .= '<td class="table" '.row_color($counter).'><a href="'.$script_name.'?view=oneword&word_id='.$parameter{'word_id'}.'&nr='.$insert_count_5_nr.'&add_cat='.$parameter{'cat_id'}.'&modus=update">'.$insert_count_5.'x Hinzuf&uuml;gen</a></td>';
		$html .= '</tr>'."\n";	

	}
	$html .= '</table>';

	###################################################	
	# Karten mit dem Stichwort mit der Kategorie
	###################################################

	$html .= '<h2>Karten mit dem Stichwort <span style="background:lightgreen;">mit der Kategorie</span></h2>';
	
	$sql_text = "select k.nummer, k.ort, k.land, k.motiv, k.jahr, k.notiz
from karten k 
left outer join karten_cat kc 
on (k.nummer = kc.nr)
where k.motiv like '%".$parameter{'word'}."%' 
and k.nummer in (select kc.nr from karten_cat kc where kc.cat_id = '".$parameter{'cat_id'}."' )
order by k.motiv ;";

	# search in database
	my $sth = $dbh->prepare( $sql_text )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
	#$html .= '<p class="smalltext"/>'.$sql_text."</p>\n";					  
	$sth->execute or die $sth->errstr; # hier geschieht die Anfrage an die DB

	$html .= '<table class="table">';
	$html .= '<tr><th class="table">Nr</th><th class="table">Ort</th>';
	$html .= '<th class="table">Land</th><th class="table">Motiv</th>';
	$html .= '<th class="table">Jahr</th><th class="table">Notiz</th>';
	$html .= '<th class="table">Kategorie</th></tr>';


	my %row;
	my $counter = 0;

	# jede Zeile jeder Karte
	while (my @array = $sth->fetchrow_array()) {
	    ($row{'nummer'},
			$row{'ort'},
			$row{'land'},
			$row{'motiv'},
			$row{'jahr'},
			$row{'notiz'}
			) = @array;

		$row{'ort'} = encode_entities($row{'ort'});
		$row{'land'} = encode_entities($row{'land'});
		$row{'motiv'} = encode_entities($row{'motiv'});
		$row{'notiz'} = encode_entities($row{'notiz'});

		$counter ++;

		$html .= '<tr>';
		$html .= '<td class="table" '.row_color($counter).'><a href="'.$script_name.'?view=card&nr='.$row{'nummer'}.'">'.$row{'nummer'}.'</a>'.'</td>';
		$html .= '<td class="table" '.row_color($counter).'>'.$row{'ort'}.'</td>';
		$html .= '<td class="table" '.row_color($counter).'>'.$row{'land'}.'</td>';
		$html .= '<td class="table" '.row_color($counter).'><a href="'.$script_name.'?view=card&nr='.$row{'nummer'}.'">'.$row{'motiv'}.'</a>'.'</td>';
		$html .= '<td class="table" '.row_color($counter).'>'.$row{'jahr'}.'</td>';
		$html .= '<td class="table" '.row_color($counter).'>'.$row{'notiz'}.'</td>';
		$html .= '<td class="table" '.row_color($counter).'><a href="'.$script_name.'?view=oneword&word_id='.$parameter{'word_id'}.'&nr='.$row{'nummer'}.'&del_cat='.$parameter{'cat_id'}.'&modus=update">Entfernen</a></td>';
		$html .= '</tr>'."\n";	

	}
	$html .= '</table>';
		

	return ($html);
}




########################################
# Neues Stichwort
########################################



sub get_html_new_word{
	my $html = '';
	
	$html .= '<p>→ <a href="'.$script_name.'">Startseite</a> → ';
	$html .= '<a href="'.$script_name.'?view=word">Liste aller Stichwörter</a> → Neues Stichwort hinzufügen';
	$html .= '<h2>Neues Stichwort hinzufügen</h2>';

	$html .= '<p>Wenn im Motiv der Karte das Stichwort (z.B. "Dampfer", "Automobil", "Deutsche Eisenbahntruppe") gefunden wird,  soll die hier angegebene Kategorie vorgeschlagen werden. Ist Land oder Region mit ausgewählt, dann ist eine Unterteilung auf Commons sinnvoll (z.B. "Dampfer in Deutschland" bzw. "Dampfer in Sachsen"). Diese regionale Ergänzung wird automatisch eingebaut, auch wenn sie heute evt. noch nicht auf Commons existiert. Manche Kategorien (z.B. "Augustusburg", "Dresdener Zwinger") haben keine weiter Unterteilung auf Commons.</p>';

	$html .= '<form action="'.$script_name.'?view=word" accept-charset="utf-8" method="post" >'."\n";
	$html .= 'Neues Stichwort: <input name="word" type="text" size="60" maxlength="400" value="'.'" ><br />';

	# Kategorie Option
	my %cat;
	my	$sql_text = "select id, cat from karten_commons_cat order by cat";
	# search in database
	my $sth = $dbh->prepare( $sql_text )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
	#$html .= '<p class="smalltext"/>'.$sql_text."</p>\n";					  
	#print $sql_text;
	$sth->execute or die $sth->errstr; # hier geschieht die Anfrage an die DB
	
	
	while (my ($id, $cat_name) = $sth->fetchrow_array()) {
		%cat = (%cat, ( $cat_name => $id)); 
	}

	$html .= 'Passende Kategorie: <select name="cat_id" size="1">';
	foreach my $cat_name(sort keys %cat){
		#my $id_2 = $cat{$cat_name} ; 
		$html .= '<option'; 
		#$html .= ' selected' if $cat_name eq $id_2; 
		$html .= ' value='.$cat{$cat_name}.'>'.encode_entities($cat_name).'</option>'."\n";
	}
	$html .= '</select><br />';

	#$html .= '<input name="categorie" type="text" size="60" maxlength="400" value="'.'" ><br />';

    $html .= '<input type="checkbox" name="country" value="country"> Länderunterteilung (Deutschland, USA, ...)<br />';
    $html .= '<input type="checkbox" name="region" value="region"> Regionalunterteilung (Bayern, Sachsen, ... )<br />';

	# hidden parameter: view, modus
	$html .= '<input name="view" type="hidden" value="word">'."\n";
	$html .= '<input name="modus" type="hidden" value="update">'."\n";
	$html .= '<p><input type="submit" value="Speichern"></p>'."\n";




	return ($html);
}






















########################################
# Statistien
########################################


sub get_html_list_statistic{
	my $html = '';
	$html .= '<p>→ <a href="'.$script_name.'">Startseite</a> → ';

	my $sql_text = '';

	############################
	# Allgemeine Zahlen
	#########################
	my %row;
	my $counter = 0;
	my %anzahl;
	$anzahl{'gesamt'} = 0;
	$anzahl{'de'} = 0;
	$anzahl{'ort'} = 0;
	$anzahl{'land'} = 0;
	my $format = '%.2f';
	
	$html .= '<table class="table">';
	$html .= '<tr>';	
	$html .= '<th class="table">Wert</th>';
	$html .= '<th class="table">Bemerkung</th>';
	$html .= '</tr>';

	######################
	# anzahl Karten
	$sql_text = "select count(*) from karten;";	

	my $sth = $dbh->prepare( $sql_text )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
	$sth->execute or die $sth->errstr; # hier geschieht die Anfrage an die DB

	# jede Zeile
	while (my @array = $sth->fetchrow_array()) {
	    ($row{'value'}
			) = @array;
		$anzahl{'gesamt'} = $row{'value'};
		$counter ++;
		$html .= '<tr>';
		$html .= '<td class="table" '.row_color($counter).' align="right">'.$row{'value'}.'</td>';
		$html .= '<td class="table" '.row_color($counter).'>Karten insgesamt</td>';
		$html .= '<td class="table" '.row_color($counter).' align="right">'.(sprintf $format, $row{'value'}*100/$anzahl{'gesamt'}).'%</td>';
		$html .= '</tr>'."\n";	

	}

	######################
	# anzahl Orte
	$sql_text = "select count(*) from karten_ort;";	

	my $sth = $dbh->prepare( $sql_text )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
	$sth->execute or die $sth->errstr; # hier geschieht die Anfrage an die DB

	# jede Zeile
	while (my @array = $sth->fetchrow_array()) {
	    ($row{'value'}
			) = @array;
		$anzahl{'ort'} = $row{'value'};
		$counter ++;
		$html .= '<tr>';
		$html .= '<td class="table" '.row_color($counter).' align="right">'.$row{'value'}.'</td>';
		$html .= '<td class="table" '.row_color($counter).'>verschiedener Orte</td>';
		$html .= '</tr>'."\n";	
	}

	######################
	# anzahl Länder
	$sql_text = "select count(*) from (
select distinct(land) from karten_ort where land <> '')a;";	

	my $sth = $dbh->prepare( $sql_text )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
	$sth->execute or die $sth->errstr; # hier geschieht die Anfrage an die DB

	# jede Zeile
	while (my @array = $sth->fetchrow_array()) {
	    ($row{'value'}
			) = @array;
		$anzahl{'land'} = $row{'value'};
		$counter ++;
		$html .= '<tr>';
		$html .= '<td class="table" '.row_color($counter).' align="right">'.$row{'value'}.'</td>';
		$html .= '<td class="table" '.row_color($counter).'>verschiedener Länder </td>';
		$html .= '</tr>'."\n";	
	}

	######################
	# anzahl Regionen
	$sql_text = "select count(*) from (
select distinct land, region from karten_ort where region <> '' and region is not NULL) a;";	

	my $sth = $dbh->prepare( $sql_text )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
	$sth->execute or die $sth->errstr; # hier geschieht die Anfrage an die DB

	# jede Zeile
	while (my @array = $sth->fetchrow_array()) {
	    ($row{'value'}
			) = @array;
		$counter ++;
		$html .= '<tr>';
		$html .= '<td class="table" '.row_color($counter).' align="right">'.$row{'value'}.'</td>';
		$html .= '<td class="table" '.row_color($counter).'>verschiedener Regionen</td>';
		$html .= '</tr>'."\n";	
	}

	######################
	# Karten ohne Ort
	$sql_text = "select count(*) from karten where ort is null or ort = '';";	

	my $sth = $dbh->prepare( $sql_text )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
	$sth->execute or die $sth->errstr; # hier geschieht die Anfrage an die DB

	# jede Zeile
	while (my @array = $sth->fetchrow_array()) {
	    ($row{'value'}
			) = @array;
		$counter ++;
		$html .= '<tr>';
		$html .= '<td class="table" '.row_color($counter).' align="right">'.$row{'value'}.'</td>';
		$html .= '<td class="table" '.row_color($counter).'>Karten ohne Ort</td>';
		$html .= '<td class="table" '.row_color($counter).' align="right">'.(sprintf $format, $row{'value'}*100/$anzahl{'gesamt'}).'%</td>';
		$html .= '</tr>'."\n";	
	}

	######################
	# Karten ohne Motiv
	$sql_text = "select count(*) from karten where motiv is null or motiv = '';";	

	my $sth = $dbh->prepare( $sql_text )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
	$sth->execute or die $sth->errstr; # hier geschieht die Anfrage an die DB

	# jede Zeile
	while (my @array = $sth->fetchrow_array()) {
	    ($row{'value'}
			) = @array;
		$counter ++;
		$html .= '<tr>';
		$html .= '<td class="table" '.row_color($counter).' align="right">'.$row{'value'}.'</td>';
		$html .= '<td class="table" '.row_color($counter).'>Karten ohne Motiv</td>';
		$html .= '<td class="table" '.row_color($counter).' align="right">'.(sprintf $format, $row{'value'}*100/$anzahl{'gesamt'}).'%</td>';
		$html .= '</tr>'."\n";	
	}

	######################
	# Orte ohne Land
	$sql_text = "select count(*) from karten_ort where land is null or land = '';";	

	my $sth = $dbh->prepare( $sql_text )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
	$sth->execute or die $sth->errstr; # hier geschieht die Anfrage an die DB

	# jede Zeile
	while (my @array = $sth->fetchrow_array()) {
	    ($row{'value'}
			) = @array;
		$counter ++;
		$html .= '<tr>';
		$html .= '<td class="table" '.row_color($counter).' align="right">'.$row{'value'}.'</td>';
		$html .= '<td class="table" '.row_color($counter).'>Orte ohne Land</td>';
		$html .= '<td class="table" '.row_color($counter).' align="right">'.(sprintf $format, $row{'value'}*100/$anzahl{'ort'}).'%</td>';
		$html .= '</tr>'."\n";	
	}
	######################
	# Orte ohne Region
	$sql_text = "select count(*) from karten_ort where region is null or region = '';";	

	my $sth = $dbh->prepare( $sql_text )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
	$sth->execute or die $sth->errstr; # hier geschieht die Anfrage an die DB

	# jede Zeile
	while (my @array = $sth->fetchrow_array()) {
	    ($row{'value'}
			) = @array;
		$counter ++;
		$html .= '<tr>';
		$html .= '<td class="table" '.row_color($counter).' align="right">'.$row{'value'}.'</td>';
		$html .= '<td class="table" '.row_color($counter).'>Orte ohne Region</td>';
		$html .= '<td class="table" '.row_color($counter).' align="right">'.(sprintf $format, $row{'value'}*100/$anzahl{'ort'}).'%</td>';
		$html .= '</tr>'."\n";	
	}


	######################
	# Karten mit Kategorien
	$sql_text = "select count(distinct nr) from karten_cat";	

	my $sth = $dbh->prepare( $sql_text )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
	$sth->execute or die $sth->errstr; # hier geschieht die Anfrage an die DB

	# jede Zeile
	while (my @array = $sth->fetchrow_array()) {
	    ($row{'value'}
			) = @array;
		$counter ++;
		$html .= '<tr>';
		$html .= '<td class="table" '.row_color($counter).' align="right">'.$row{'value'}.'</td>';
		$html .= '<td class="table" '.row_color($counter).'>Karten mit Kategorien</td>';
		$html .= '<td class="table" '.row_color($counter).' align="right">'.(sprintf $format, $row{'value'}*100/$anzahl{'gesamt'}).'%</td>';
		$html .= '</tr>'."\n";	
	}



	$html .= '</table>';
	####################################################
	# ENDE allgemeine Zahlen
	####################################################



	######################
	# Verteilung in Weltweit
	$sql_text = "select o.land, count(*) from karten_ort o
join karten k
on o.ort = k.ort
where o.land is not null and o.land <> ''
group by o.land
order by count(*) desc, o.land;";	

	$html .= '<h2>Weltweit Verteilung </h2>';	
	$html .= '<table class="table">';
	$html .= '<tr>';	
	$html .= '<th class="table" >Anzahl Karten</th>';
	$html .= '<th class="table">ISO</th>';
	$html .= '<th class="table">Land</th>';
	$html .= '</tr>';

	my $sth = $dbh->prepare( $sql_text )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
	$sth->execute or die $sth->errstr; # hier geschieht die Anfrage an die DB

	# jede Zeile
	while (my @array = $sth->fetchrow_array()) {
	    ($row{'key'},
		 $row{'value'}
			) = @array;
		$anzahl{'de'} = $row{'value'} if ($row{'key'} eq 'DE');
		$counter ++;
		$html .= '<tr>';
		$html .= '<td class="table" '.row_color($counter).' align="right">'.$row{'value'}.'</td>';
		$html .= '<td class="table" '.row_color($counter).'>'.$row{'key'}.'</td>';
		$html .= '<td class="table" '.row_color($counter).'><a href="'.$script_name.'?view=location&search_land='.$row{'key'}.'">'.get_landname($row{'key'}).'</a></td>';
		$html .= '<td class="table" '.row_color($counter).' align="right">'.(sprintf $format, $row{'value'}*100/$anzahl{'gesamt'}).'%</td>';
		$html .= '</tr>'."\n";	
	}
	$html .= '</table>';


	######################
	# Verteilung in Deutschland
	$sql_text = "select o.region, count(*) from karten_ort o
join karten k
on o.ort = k.ort
where o.land ='DE'  
and o.region is not null and o.region <> ''
group by o.region
order by count(*) desc, o.region;";	

	$html .= '<h2>Verteilung in Deutschland</h2>';	
	#$html .= '<p>Insgesamt '.$anzahl{'de'}.' Karten von Deutschland</p>';
	$html .= '<table class="table">';
	$html .= '<tr>';	
	$html .= '<th class="table" >Anzahl Karten</th>';
	$html .= '<th class="table">ISO</th>';
	$html .= '<th class="table">Bundesland</th>';
	$html .= '<th class="table">Anteil DE</th>';
	$html .= '<th class="table">Anteil Gesamt</th>';
	$html .= '</tr>';

	my $sth = $dbh->prepare( $sql_text )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
	$sth->execute or die $sth->errstr; # hier geschieht die Anfrage an die DB

	# jede Zeile
	while (my @array = $sth->fetchrow_array()) {
	    ($row{'key'},
		 $row{'value'}
			) = @array;
		$counter ++;
		$html .= '<tr>';
		$html .= '<td class="table" '.row_color($counter).' align="right">'.$row{'value'}.'</td>';
		$html .= '<td class="table" '.row_color($counter).'>'.$row{'key'}.'</td>';
		$html .= '<td class="table" '.row_color($counter).'><a href="'.$script_name.'?view=location&search_region='.$row{'key'}.'">'.get_regionname('DE-'.$row{'key'}).'</a></td>';
		$html .= '<td class="table" '.row_color($counter).' align="right">'.(sprintf $format, $row{'value'}*100/$anzahl{'de'}).'%</td>';
		$html .= '<td class="table" '.row_color($counter).' align="right">'.(sprintf $format, $row{'value'}*100/$anzahl{'gesamt'}).'%</td>';
		$html .= '</tr>'."\n";	
	}
	$html .= '</table>';
	
	return ($html);
}



#######################################
# ISO 3166-2
#######################################
sub get_regionname{
	my $code = shift;
	my %regionname;
	#print $code.' ';
	$regionname{'DE-HH'} = 'Hamburg';
	$regionname{'DE-HB'} = 'Bremen';
	$regionname{'DE-SN'} = 'Sachsen';
	$regionname{'DE-ST'} = 'Sachsen-Anhalt';
	$regionname{'DE-BB'} = 'Brandenburg';
	$regionname{'DE-BE'} = 'Berlin';
	$regionname{'DE-MV'} = 'Mecklenburg-Vorpommern';
	$regionname{'DE-RP'} = 'Rheinland-Pfalz';
	$regionname{'DE-BY'} = 'Bayern';
	$regionname{'DE-BW'} = 'Baden-Württemberg';
	$regionname{'DE-NI'} = 'Niedersachsen';
	$regionname{'DE-SH'} = 'Schleswig-Holstein';
	$regionname{'DE-SR'} = 'Saarland';
	$regionname{'DE-HE'} = 'Hessen';
	$regionname{'DE-TH'} = 'Thüringen';
	$regionname{'DE-NW'} = 'Nordrhein-Westfalen';

	#print $regionname{$code}.' ';
	my $return = '';
	if (exists $regionname{$code}) {
		#print 'HOHU '.$regionname{$code}.' ';
		$return = $regionname{$code};
	}
	return ($return)
}

#######################################
# ISO 3166-2
#######################################
sub get_landname{
	my $code = shift;
	my %name;
	#print $code.' ';
	$name{'DE'} = 'Deutschland';
	$name{'CZ'} = 'Tschechien';
	$name{'US'} = 'USA';
	$name{'HU'} = 'Ungarn';
	$name{'PL'} = 'Polen';
	$name{'LU'} = 'Luxemburg';
	$name{'AT'} = 'Österreich';
	$name{'FR'} = 'Frankreich';
	$name{'ES'} = 'Spanien';
	$name{'IT'} = 'Italien';
	$name{'EE'} = 'Estland';
	$name{'DK'} = 'Dänemark';
	$name{'CN'} = 'China';
	$name{'MA'} = 'Marokko';
	$name{'CA'} = 'Kanada';
	$name{'MX'} = 'Mexiko';
	$name{'PH'} = 'Philippinen';
	$name{'RU'} = 'Russland';
	$name{'SE'} = 'Schweden';
	$name{'LT'} = 'Litauen';
	$name{'NA'} = 'Nanibia';
	$name{'TG'} = 'Togo';
	$name{'RO'} = 'Rumänien';
	$name{'HR'} = 'Kroatien';
	$name{'SI'} = 'Slowenien';
	$name{'UA'} = 'Ukraine';
	$name{'AU'} = 'Australien';
	$name{'SK'} = 'Slowakei';
	$name{'CS'} = 'Australien';
	$name{'CH'} = 'Schweiz';
	$name{'NO'} = 'Norwegen';

	#print $name{$code}.' ';
	my $return = '';
	if (exists $name{$code}) {
		#print 'HOHU '.$name{$code}.' ';
		$return = $name{$code};
	}
	return ($return)
}




###############################
# Kartengruppen ohne Kategorien
###############################


sub get_html_no_cat{
	my $html = '';
	
	$html .= '<p>→ <a href="'.$script_name.'">Startseite</a> → '.$parameter{'html_subtitle'}.'</p>';


	my $sql_text = "select k1.g1 , ifnull(k2.anzahl,'') Anzahl from (select distinct (nummer - nummer%100) g1 from karten) k1
left outer join 
(
 select nummer - nummer%100 g1, count(*) anzahl from karten where nummer not in (
  select nr from karten_cat where cat_id in (
   select id from karten_commons_cat
   where cat not like 'Meta:%'
  )
 )
 and nummer not in 
 (-- Karten ohne Bild
  select nr from karten_cat where cat_id in (
   select id from karten_commons_cat
   where cat = 'Meta: Ohne Bild'
  )
 )
 group by (nummer - nummer%100)
) k2

on k1.g1 = k2.g1
;";	

	$html .= '<h2>Karten ohne Kategorien</h2>';	
	$html .= '<table class="table">';
	$html .= '<tr>';	
	$html .= '<th class="table" >Nummer</th>';
	$html .= '<th class="table" >Anzahl Karten</th>';
	$html .= '</tr>';

	my $sth = $dbh->prepare( $sql_text )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
	$sth->execute or die $sth->errstr; # hier geschieht die Anfrage an die DB

	my %row;
	my $counter = 0;
	# jede Zeile
	while (my @array = $sth->fetchrow_array()) {
	    ($row{'group'},
		 $row{'value'}
			) = @array;
		$counter ++;
		$html .= '<tr>';
		$html .= '<td class="table" '.row_color($counter).' align="right"><a href="bus_cards.cgi?view=no_cat_group&von='.$row{'group'}.'">'.$row{'group'}.' - '.($row{'group'}+99).'</a></td>';
		$html .= '<td class="table" '.row_color($counter).'>'.$row{'value'}.'</td>';
		$html .= '</tr>'."\n";	
	}
	$html .= '</table>';
	



	return $html;	
}


###############################
# Kartengruppen ohne Kategorien group
###############################


sub get_html_no_cat_group{
	my $html = '';
	
	$html .= '<p>→ <a href="'.$script_name.'">Startseite</a> → '.$parameter{'html_subtitle'}.'</p>';


	my $sql_text = "select nummer from karten where nummer not in (
 select nr from karten_cat where cat_id in (
  select id from karten_commons_cat
  where cat not like 'Meta:%'
 )
)
and nummer not in 
(-- Karten ohne Bild
 select nr from karten_cat where cat_id in (
  select id from karten_commons_cat
  where cat = 'Meta: Ohne Bild'
 )
)
and nummer >=".$parameter{'von'}." 
and nummer <".($parameter{'von'}+100)." 
;
;";	

	$html .= '<h2>Karten ohne Kategorien</h2>';	
	$html .= '<table class="table">';
	$html .= '<tr>';	
	$html .= '<th class="table" >Nummer</th>';
	$html .= '</tr>';

	my $sth = $dbh->prepare( $sql_text )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
	$sth->execute or die $sth->errstr; # hier geschieht die Anfrage an die DB

	my %row;
	my $counter = 0;
	# jede Zeile
	while (my @array = $sth->fetchrow_array()) {
	    ($row{'nummer'},
			) = @array;
		$counter ++;
		$html .= '<tr>';
		$html .= '<td class="table" '.row_color($counter).' align="right"><a href="bus_cards.cgi?view=card&nr='.$row{'nummer'}.'">'.$row{'nummer'}.'</a></td>';
		$html .= '</tr>'."\n";	
	}
	$html .= '</table>';
	



	return $html;	
}

#######################################
# Bilder lokal anzeigen
#######################################


sub get_html_images{
	my $html = '';
	
	$html .= '<p>→ <a href="'.$script_name.'">Startseite</a> → '.$parameter{'html_subtitle'}.'</p>';
	
	my @path;
	push (@path, '/home/sk/daten/project/bus/images');
	push (@path, '/opt/test');
	push (@path, 'C:/bus/images');

	$parameter{'image_path'} = '';
	foreach my $path (@path) {
		if (-d $path) {
			# directory called cgi-bin exists
			$parameter{'image_path'} = $path;
		}
		elsif (-e "cgi-bin") {
			# cgi-bin exists but is not a directory
		}
		else {
			# nothing called cgi-bin exists
		}		
	}

	$html .= 'Verzeichnis: '.$parameter{'image_path'}."\n";

	# Verzeichnis gefunden und anzeigen
	if ($parameter{'image_path'} ne ''){

		#my $Totalbytes = 0;

		$html .= "<hr noshade size=\"1\"><pre>\n";

		ermitteln($parameter{'image_path'});
		@all_images = sort(@all_images);
		foreach my $image (@all_images) {
			$html .= "$image\n";
			$html .= '<img src="file://'.$image.'" border="0" height="200px" />';
			$html .= '<br />';

		}
		$html .= "</pre><hr noshade size=\"1\">\n";
	}

	



	return $html;	
}



sub ermitteln {
  my $Verzeichnis = shift;
  my $Eintrag;
  my $Pfadname;
  my $HTML_Eintrag;
  my $Bytes;
  local *DH;

  unless (opendir(DH, $Verzeichnis)) {
   return;
  }
  while (defined ($Eintrag = readdir(DH))) {
   next if($Eintrag eq "." or $Eintrag eq "..");
   $Pfadname = $Verzeichnis."/".$Eintrag;
   #print $Pfadname.'<br />';
   if( -d $Pfadname) {
    $HTML_Eintrag = $Verzeichnis."/".$Eintrag." [VERZEICHNIS]";
	#print $HTML_Eintrag.'<br />';
   }
   else {
    $Bytes = -s $Pfadname;		# Dateigröße
    #$Totalbytes += $Bytes;
    $HTML_Eintrag = $Verzeichnis."/".$Eintrag;
    push(@all_images, $HTML_Eintrag);	
   }

   ermitteln($Pfadname) if(-d $Pfadname);
  }
 closedir(DH);
}



















#######################################
# Zeilen in Tabelle unterschiedlich färben
#######################################

sub row_color{
	# every second row in other color
	my $i = shift;
	my $return = '';
	$return = 'style="background-color:#D0F5A9;"' if ( $i % 2 == 1);
	return($return);
}


#######################################
# Update testen auf null
#######################################

sub nulltest {
	# wenn Wert leer, dann null zurückgeben für Insert/Update
	my $value = shift;
	if ((length($value) == 0) or ($value =~ /^[ ]+$/)) {
		$value = 'null';
	} else { 
		$value =~ s/'/''/g;
		$value = "'$value'";
	
	}
	return ($value)	
}

#######################################
# link in html search
#######################################
sub html_link_parameter{

	my $return = '';

	my @list=('offset', 'limit', 'von', 'bis', 'limit', 'search_nr', 'search_ort', 'search_land', 'search_motiv',
    'search_jahr', 'search_notiz', 'search_artikel', 'search_region', 'search_scan','search_cat','search_word');

	foreach my $value (@list){
		$return .= '&'.$value.'='.$parameter{$value} if ($parameter{$value} ne '');
	}
	return ($return);
}

