#!/usr/bin/perl -w

#################################################################
# Program:	    checkwiki.cgi
# Descrition:	show all errors of Wikipedia-Project "Personendaten"
# Author:	    Stefan Kühn
# Licence:      GPL
#################################################################

our $VERSION = '2015-01-23';

use strict;
use CGI qw(param);				# Handler for Parameters in URL www.xyz.org?view=test
use CGI::Carp qw(fatalsToBrowser);
use CGI::Carp qw(fatalsToBrowser set_message); 	# CGI-Error
use HTML::Entities;
use Encode qw(encode decode);
use URI::Escape;				# Handle of URL
use LWP::UserAgent;				# Get webpages
use DBI;

set_message('There is a problem in the script. 
Send me a mail to (<a href="mailto:kuehn-s@gmx.net">kuehn-s@gmx.net</a>), 
giving this error message and the time and date of the error.');

print "Content-type: text/html\n\n";


our %parameter;	# storage for all parameter
our $dbh;

read_configfile();
open_db();


########################################################################
# program_name
########################################################################

# Environment-Variables

#foreach my $p (sort keys %ENV) { 
#	print $p=$ENV{$p}."<br />\n"; 
#}
#our $script_name = 'pd.cgi';
#print "<br />\n"; 
#$script_name = $ENV{'SCRIPT_FILENAME'};
#$script_name = '' unless defined $script_name;
#$script_name =~ s/^.+\///g;
#print $script_name."<br />\n"; 


my $script_name = $ENV{'SCRIPT_FILENAME'}; #get scriptname from cgi

if (defined($script_name) != 1) {
	# for test local
	$script_name = $0;	# get scriptname
}
#print $script_name.' ' ;
$script_name =~ s/^.+\///g;
#print $script_name.' ' ;



##############
our $param_view     = param('view');	# list, high, middle, low, only, detail
our $param_id 	    = param('id');	    # id of improvment
our $param_pageid   = param('pageid');	
our $param_offset   = param('offset');
our $param_limit    = param('limit');
our $param_orderby  = param('orderby');
our $param_sort     = param('sort');
our $param_from     = param('from');	# only for detail (for back)

$param_view = '' 		unless defined $param_view;
$param_id = '' 			unless defined $param_id;
$param_pageid = '' 		unless defined $param_pageid;
$param_offset = '' 		unless defined $param_offset;
$param_limit  = '' 		unless defined $param_limit;
$param_orderby = '' 	unless defined $param_orderby;
$param_sort = '' 		unless defined $param_sort;
$param_from = '' 		unless defined $param_from;


if ($param_offset =~ /^[0-9]+$/) {} else {
	$param_offset = 0;
}

if ($param_limit =~ /^[0-9]+$/) {} else {
	$param_limit = 25;
}
$param_limit = 500 if ($param_limit > 500);
our $offset_lower = $param_offset - $param_limit;
our $offset_higher = $param_offset + $param_limit;
	$offset_lower = 0 if ($offset_lower < 0);
our  $offset_end =  $param_offset + $param_limit;

# Sorting
our $column_orderby = '';
our $column_sort = '';
if ($param_orderby ne ''){
	if (    $param_orderby ne 'article'
		and $param_orderby ne 'notice'
		and $param_orderby ne 'found'
		and $param_orderby ne 'more'
		and $param_orderby ne 'todo'
		and $param_orderby ne 'ok'
		and $param_orderby ne 'error_id'
	) {
		$param_orderby = '';
	}
}

if ($param_sort ne '') {
	if (   $param_sort ne 'asc'
		and $param_sort ne 'desc') {
		$column_sort = 'asc' 
	} else {
		$column_sort = 'asc'  if ($param_sort eq 'asc');
		$column_sort = 'desc'  if ($param_sort eq 'desc');
	}
}




print <<HTML_HEAD;
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<head>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<style type="text/css">
body {  
	
	font-family: Verdana, Tahoma, Arial, Helvetica, sans-serif;
	font-size:14px;
	font-style:normal;
	
	/* color:#9A91ff; */
	
	/* background-color:#00077A; */
	/* background-image:url(back.jpg); */
	/* background:url(back_new.jpg) no-repeat fixed top center; */
	/* background:url(../images/back_new.jpg) no-repeat fixed top center; */
	/* background:url(../images/back_schaf2.jpg) no-repeat fixed bottom left; */
	/* background:url(../images/back_schaf3.jpg) no-repeat fixed bottom left; */
	
	background-color:white;
	color:#555555;
	text-decoration:none; 
	line-height:normal; 
	font-weight:normal; 
	font-variant:normal; 
	text-transform:none; 
	margin-left:5%;
	margin-right:5%;
	}
	
h1	{
	/*color:red; */
	font-size:20px;
	}

h2	{
	/*color:red; */
	font-size:16px;
	}
	
a 	{  

	/*nur blau */
	/* color:#80BFBF; */ 
	/* color:#4889c5; */ 
	/* color:#326a9e; */  
	
	color:#4889c5;
	font-weight:bold;
	/*nettes grün */
	/*color:#7CFC00;*/
	
	/* Nette Kombination */
	/*color:#00077A; */
	/*background-color:#eee8fd;*/
	
	
	/* Kein Unterstrich */
	text-decoration:none; 
	
	/*Außenabstand*/
	/*padding:2px;*/
	}

a:hover {  
	background-color:#ffdeff;
	color:red;
	}
	
.nocolor{  
	background-color:white;
	color:white;
	} 
	
a:hover.nocolor{  
	background-color:white;
	color:white;
	}
	
.table{
	font-size:12px; 

	vertical-align:top;

	border-width:thin;
  	border-style:solid;
  	border-color:blue;
  	background-color:#EEEEEE;
  	
  	/*Innenabstand*/
	padding-top:2px;
	padding-bottom:2px;
	padding-left:5px;
	padding-right:5px;
  	
  	/* dünne Rahmen */
  	border-collapse:collapse; 
	
	/*kein Zeilenumbruch
	white-space:nowrap;*/
  	
  	}
	
</style>

<title>Personendaten Verbesserungsvorschläge</title></head>
HTML_HEAD


print "<body><h1>PD Verbesserungsvorschläge</h1>\n";

#print '<p><span style="background-color:yellow;">Im Moment wird gerade am Skript gearbeitet. Daher kann es zu Ausfällen kommen. - Stefan</span></p>'."\n";


if (     $param_view eq '') {
	 
print '<p>→ Startseite</p>'."\n";		
print <<EINFUEHRUNGSTEXT;

<p>Auf dieser Seite werden Verbesserungsvorschläge für die Personendaten in der deutschsprachigen Wikipedia angezeigt.  Für weitere Informationen am besten mal hier reinschauen: <a href="http://de.wikipedia.org/wiki/Hilfe:Personendaten/Wartung/Fehlerliste">Hilfe:Personendaten/Wartung/Fehlerliste</a>.</p>

EINFUEHRUNGSTEXT

my $nr_all = 541800; #get_number_all_pd(); # Select count(*) dauert sehr lange
my $nr_all_pd_with_error = get_number_all_article_with_pd_error();
my $prozent = int( ($nr_all_pd_with_error / ($nr_all / 100))*1000 ) / 1000;



print '<p>Aktuell sind dem Skript ca. <b>'.$nr_all.'</b> Artikel mit Personendaten bekannt.<br />Davon wurden in <b>'.$nr_all_pd_with_error.'</b> Artikeln (<b>'.$prozent.'%</b>) insgesamt <b>'.get_number_all_errors().'</b> mögliche Verbesserungsvorschläge gefunden. </p>'."\n";

print '<p><a href="'.$script_name.'?view=list" >Alle Artikel mit Vorschlägen</a></p>';

print '<p><table class="table">';
print '<tr><th class="table">&nbsp;</th><th class="table">Vorschläge</th><th class="table">Erledigt</th></tr>'."\n";
print get_number_of_prio();
print '</table></p>';

print '<p>';
my $new_timestamp = get_newest_pd_error_timestamp();
my $old_timestamp = get_oldest_pd_error_timestamp();
my @new_split = split(/ /, $new_timestamp);
my @old_split = split(/ /, $old_timestamp);


#get_article_by_timestamp($old_timestamp);
#get_article_by_timestamp($new_timestamp);

print 'Am <b>'.$new_split[0].'</b> um <b>'.$new_split[1].'</b> UTC wurde der neuste Vorschlag gefunden.<br />'."\n";
#print 'Am <b>'.$old_split[0].'</b> um <b>'.$old_split[1].'</b> UTC wurde der älteste Vorschlag gefunden.<br />'."\n";
print '</p>';

}

###########################################################
if (   $param_view eq 'high'
	or $param_view eq 'middle'
	or $param_view eq 'low'
	or $param_view eq 'all') {


	my $prio = 0;
	my $headline = '';
	if ($param_view eq 'high') {
		$prio = 1;
		$headline = 'Hohe Priorität';
	}
	
	if ($param_view eq 'middle') { 
		$prio = 2;
		$headline = 'Mittlere Priorität';
	}
	
	if ($param_view eq 'low') {
		$prio = 3;
		$headline = 'Niedrige Priorität';
	}
	
	if ($param_view eq 'all') {
		$prio = 0;
		$headline = 'Alle Prioritäten';
	}
	
	
	#print '<h2>'.$headline.'</h2>'."\n";	
	print '<p>→ <a href="'.$script_name.'">Startseite</a> → '.$headline.'</p>'."\n";	
	
	print '<p>Prioritäten: ';
	print '<a href="'.$script_name.'?view=all">Alle</a> - ';
	print '<a href="'.$script_name.'?view=high">Hoch</a> - ';
	print '<a href="'.$script_name.'?view=middle">Mittel</a> - ';
	print '<a href="'.$script_name.'?view=low">Niedrig</a>';

	
	print get_number_error_and_desc_by_prio($prio);
	
	
}

##############################################################

if (   $param_view eq 'list') {
	print '<p>→ <a href="'.$script_name.'">Startseite</a> → Liste</p>'."\n";	
	#print '<h2>Vorschlagsliste</h2>'."\n";
	print get_list();	

}	

################################################################
if (    $param_view =~ /^(detail|only)$/
	and $param_pageid =~ /^[0-9]+$/
	and $param_id     =~ /^[0-9]+$/ ) {
	# Artikel abgearbeitet
	my $sql_text ='';
	if ($param_id != 0) {
		# only one error
		$sql_text = "update pd_error set done=1 where page_id=".$param_pageid." and error_id=".$param_id.";";
	} else { 
		# all errors (error_id = 0);
		$sql_text = "update pd_error set done=1 where page_id=".$param_pageid.";";
	}		
	
	my $sth = $dbh->prepare( $sql_text )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
	$sth->execute or die $sth->errstr; # hier geschieht die Anfrage an die DB	
}

#################################################################
if (	$param_view =~ /^only(done)?$/   
	and $param_id =~ /^[0-9]+$/) {
	# Ein konkrete Fehlernummer auflisten
	my $headline = '';
	$headline = get_headline($param_id);
	
	my $prio = get_prio_of_error($param_id);
	$prio = '<a href="'.$script_name.'?view=high">Höchster Priorität</a>' if ($prio eq '1');
	$prio = '<a href="'.$script_name.'?view=middle">Mittlere Priorität</a>' if ($prio eq '2');
	$prio = '<a href="'.$script_name.'?view=low">Niedrigste Priorität</a>' if ($prio eq '3');
	print '<p>→ <a href="'.$script_name.'">Startseite</a> → '.$prio.' → '.$headline.'</p>'."\n";	

	print '<p>'.get_description($param_id).'</p>'."\n";
	#print '<p>'.get_number_of_error($param_id).' mal gefunden (ID '.$param_id .')</p>'."\n";
	
	if ($param_view =~ /^only$/ ) {
	    print '<a href="'.$script_name.'?view=onlydone&id='.$param_id.'">Zeige alle erledigten Artikel</a>';
		print get_article_of_error($param_id);
	}
	if ($param_view =~ /^onlydone$/ ) {
	    print '<a href="'.$script_name.'?&view=only&id='.$param_id.'">Zeige die To-do-Liste</a>';
		print get_done_article_of_error($param_id);
	}

	#print get_article_of_error($param_id);
	


}

################################################################
if (    $param_view eq 'detail'
	and $param_pageid =~ /^[0-9]+$/ ) {
	# Details zu einem Artikel anzeigen
	#print '<h2>Detailansicht</h2>'."\n";


	print '<p>→ <a href="'.$script_name.'">Startseite</a> → ';
	
	my $headline = '';
	if ($param_from =~ /^[0-9]+$/) {
		$headline = get_headline($param_from);
		my $prio = get_prio_of_error($param_from);
		$prio = '<a href="'.$script_name.'?view=high">Höchster Priorität</a>' if ($prio eq '1');
		$prio = '<a href="'.$script_name.'?view=middle">Mittlere Priorität</a>' if ($prio eq '2');
		$prio = '<a href="'.$script_name.'?view=low">Niedrigste Priorität</a>' if ($prio eq '3');
		print $prio.' → <a href="'.$script_name.'?view=only&id='.$param_from.'">'.$headline.'</a>'."\n";
	} else {
		print '<a href="'.$script_name.'?view=list">Liste</a>'."\n";	

	}
	print ' → Detailansicht </p>'."\n";

	my $sql_text = "select distinct title from pd_error where page_id=".$param_pageid.";";
	my $sth = $dbh->prepare( $sql_text )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
	$sth->execute or die $sth->errstr; 	
		while (my $arrayref = $sth->fetchrow_arrayref()) {	
		foreach(@$arrayref) {
			my $result = $_;
			$result = encode_entities($result);
			$result = '' unless defined $result;
			if ($result ne '') {
				print '<p>Artikel: <a target="_blank" href="http://de.wikipedia.org/wiki/'.$result.'">'.$result.'</a> - 
								<a target="_blank" href="http://de.wikipedia.org/w/index.php?title='.$result.'&action=edit">edit</a>';

	# set all done
        print ' - <a href="'.$script_name.'?view=detail&id=0&pageid='.$param_pageid.'">Erledigt</a> (alles)';
print '</p>'."\n";
			}
		}
	}

	# all errors of this article
	print get_all_error_of_article($param_pageid);
	



	
}


print '<p><span style="font-size:10px;">Autor: <a href="http://de.wikipedia.org/wiki/Benutzer:Stefan Kühn">Stefan Kühn</a> · 
<a href="http://de.wikipedia.org/wiki/Hilfe:Personendaten/Wartung/Fehlerliste">projectpage</a> · 
<a href="http://de.wikipedia.org/w/index.php?title=Hilfe_Diskussion:Personendaten/Wartung/Fehlerliste&action=edit&section=new">comments and bugs</a><br />';
print 'Version '.$VERSION.' · 
license: <a href="http://www.gnu.org/copyleft/gpl.html">GPL</a> · 
Powered by <a href="https://tools.wmflabs.org/">Wikimedia Tool Labs</a></span></p>'."\n";

print '</body>';
print '</html>';



##############################################
# Close datenbase
##############################################

$dbh->disconnect();


#######################################################################
#######################################################################
# Subroutines
#######################################################################
#######################################################################



################################################################	
# Read configfile 
################################################################	

sub read_configfile{
	# read_configfile for global settings (DB,Schemata,Password,dumppath...)

	# open configfile
	my $config_file = '../../config.txt';
	open(CONFIG, "<$config_file") or die "cannot open < $config_file\n$!";


	# read configfile line by line
	do {
		my $line = <CONFIG>;

		
		if ($line =~ /=/ ) {
			$line =~ s/\n//g;  
			$line =~ s/\t/ /g; #tabulator to space
			my $key = substr($line, 0, index($line,'=')-1);
			my $value = substr($line, index($line,'=')+1);

			$key   =~ s/^\s+|\s+$//g;	# remove space at begin and end
			$value =~ s/^\s+|\s+$//g;

			if ($key ne ''){
				$parameter{$key} = $value;	# save in hash parameter
			}
			
		}
	}
	while (eof(CONFIG) != 1);

	# close configfile
	close(CONFIG);

	# print all parameter
	foreach my $name(sort keys %parameter){
		# printf ("%15s = %s\n", $name ,$parameter{$name}) ; 
	}
	#print "\n";
	#die;
}



##############################################
# Open database
##############################################

sub open_db{
	
	#Connect to database u_sk
	#print "\n".'open database: '.$parameter{'db_database'}."\n";

	$dbh = DBI->connect( "DBI:mysql:$parameter{'db_database'};$parameter{'host'}",  # local
							$parameter{'db_user'},
							$parameter{'db_password'} ,
							{
								RaiseError => 1,
								AutoCommit => 1,
								mysql_enable_utf8 => 1
							}
	  ) or die "Database connection not made: $DBI::errstr" . DBI->errstr;
	
}

################################################

sub get_number_all_article_with_pd_error{
	my $sql_text = "select count(a.page_id) from (select page_id from pd_error where done=0 group by page_id) a;";
	my $result = 0;
	my $sth = $dbh->prepare( $sql_text )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
	#print '<p class="smalltext"/>'.$sql_text."</p>\n";					  
	$sth->execute or die $sth->errstr; # hier geschieht die Anfrage an die DB
	while (my $arrayref = $sth->fetchrow_arrayref()) {	
		foreach(@$arrayref) {
			$result = $_;
			$result = '' unless defined $result;
		}
	}
	return ($result);
}



sub get_number_of_ok{
	my $sql_text = "select count(*) from pd_error where done=1;";
	my $result = 0;
	my $sth = $dbh->prepare( $sql_text )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
	#print '<p class="smalltext"/>'.$sql_text."</p>\n";					  
	$sth->execute or die $sth->errstr; # hier geschieht die Anfrage an die DB
	while (my $arrayref = $sth->fetchrow_arrayref()) {	
		foreach(@$arrayref) {
			$result = $_;
			$result = '' unless defined $result;
		}
	}
	return ($result);
}

sub get_daytime_last_article_from_change{
	my $sql_text = "select value from parameter where name='daytime_last_article_from_change' and dbname ='dewiki';";
	my $result = 0;
	my $sth = $dbh->prepare( $sql_text )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
	#print '<p class="smalltext"/>'.$sql_text."</p>\n";					  
	$sth->execute or die $sth->errstr; # hier geschieht die Anfrage an die DB
	while (my $arrayref = $sth->fetchrow_arrayref()) {	
		foreach(@$arrayref) {
			$result = $_;
			$result = '' unless defined $result;
		}
	}
	return ($result);
}

sub get_daytime_last_article_from_new{
	my $sql_text = "select value from parameter where name='daytime_last_article_from_new' and dbname ='dewiki';";
	my $result = 0;
	my $sth = $dbh->prepare( $sql_text )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
	#print '<p class="smalltext"/>'.$sql_text."</p>\n";					  
	$sth->execute or die $sth->errstr; # hier geschieht die Anfrage an die DB
	while (my $arrayref = $sth->fetchrow_arrayref()) {	
		foreach(@$arrayref) {
			$result = $_;
			$result = '' unless defined $result;
		}
	}
	return ($result);
}

sub get_number_all_pd{
	my $sql_text = "select count(*) from pd;";
	my $result = 0;
	my $sth = $dbh->prepare( $sql_text )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
	#print '<p class="smalltext"/>'.$sql_text."</p>\n";					  
	$sth->execute or die $sth->errstr; # hier geschieht die Anfrage an die DB
	while (my $arrayref = $sth->fetchrow_arrayref()) {	
		foreach(@$arrayref) {
			$result = $_;
			$result = '' unless defined $result;
		}
	}
	return ($result);
}


sub get_number_all_errors{
	my $sql_text = "select count(*) from pd_error where done=0 ;";
	my $result = 0;
	my $sth = $dbh->prepare( $sql_text )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
	#print '<p class="smalltext"/>'.$sql_text."</p>\n";					  
	$sth->execute or die $sth->errstr; # hier geschieht die Anfrage an die DB
	while (my $arrayref = $sth->fetchrow_arrayref()) {	
		foreach(@$arrayref) {
			$result = $_;
			$result = '' unless defined $result;
		}
	}
	return ($result);
}



sub get_number_of_error{
	# Anzahl gefundenen Vorkommen eines Fehlers
	my $error = shift;
	my $sql_text = "select count(*) from pd_error where done=0 and error_id = ".$error.";";
	my $result = 0;
	my $sth = $dbh->prepare( $sql_text )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
	#print '<p class="smalltext"/>'.$sql_text."</p>\n";					  
	$sth->execute or die $sth->errstr; # hier geschieht die Anfrage an die DB
	while (my $arrayref = $sth->fetchrow_arrayref()) {	
		foreach(@$arrayref) {
			$result = $_;
			$result = '' unless defined $result;
		}
	}
	return ($result);
}

sub get_oldest_pd_error_timestamp{
	# gibt den ältesten Zeitstempel aller Fehler zurück
	my $sql_text = "select min(found) from pd_error;";
	my $result = 0;
	my $sth = $dbh->prepare( $sql_text )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
	#print '<p class="smalltext"/>'.$sql_text."</p>\n";					  
	$sth->execute or die $sth->errstr; # hier geschieht die Anfrage an die DB
	while (my $arrayref = $sth->fetchrow_arrayref()) {	
		foreach(@$arrayref) {
			$result = $_;
			$result = '' unless defined $result;
		}
	}
	return ($result);
}

sub get_newest_pd_error_timestamp{
	# gibt den neusten Zeitstempel aller Fehler zurück
	my $sql_text = "select max(found) from pd_error;";
	my $result = 0;
	my $sth = $dbh->prepare( $sql_text )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
	#print '<p class="smalltext"/>'.$sql_text."</p>\n";					  
	$sth->execute or die $sth->errstr; # hier geschieht die Anfrage an die DB
	while (my $arrayref = $sth->fetchrow_arrayref()) {	
		foreach(@$arrayref) {
			$result = $_;
			$result = '' unless defined $result;
		}
	}
	return ($result);
}


sub get_article_by_timestamp{
	my $timestamp = shift;
	my $sql_text = "select page_id, title from pd_error where found = '".$timestamp."' limit 1;";
	my $result = 0;
	my $sth = $dbh->prepare( $sql_text )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
	#print '<p class="smalltext"/>'.$sql_text."</p>\n";					  
	$sth->execute or die $sth->errstr; # hier geschieht die Anfrage an die DB
	while (my $arrayref = $sth->fetchrow_arrayref()) {	
		foreach(@$arrayref) {
			$result = $_;
			$result = '' unless defined $result;
		}
	}
	
	return ($result);
};





sub get_number_of_prio{
	# Alle vorrätigen Fehler einer Priorität
	my $sql_text = "
	select a1.anzahl, a1.prio, IFNULL(b1.anzahl,0) erledigt  from (
		select count(*) anzahl, b.prio prio from pd_error a join pd_error_desc b on ( a.error_id = b.error_id) where a.done=0 group by b.prio
	) a1
	left outer join (
		select count(*) anzahl, b.prio prio from pd_error a join pd_error_desc b on ( a.error_id = b.error_id) where a.done=1 group by b.prio
	) b1
	on (a1.prio = b1.prio) order by a1.prio;";
	my $result = '';
	my $sth = $dbh->prepare( $sql_text )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
	#print '<p class="smalltext"/>'.$sql_text."</p>\n";					  
	$sth->execute or die $sth->errstr; # hier geschieht die Anfrage an die DB
	my $sum_of_all = 0;
	my $sum_of_all_ok = 0;
	while (my $arrayref = $sth->fetchrow_arrayref()) {	
		my @output;
		my $i = 0;
		my $j = 0;
		foreach(@$arrayref) {
			#print $_."\n";
			$output[$i][$j] = $_;
			$output[$i][$j] = '' unless defined $output[$i][$j];
			$j = $j +1;
			if ($j == 3) {
				$j= 0;
				$i ++;
			}
			#print $_."\n";
		}
		my $number_of_error = $i;

		$result .= '<tr><td class="table" align="right"><a href="'.$script_name.'?view=';
		$result .= 'high">höchste Priorität' if ($output[0][1] == 1);
		$result .= 'middle">mittlere Priorität' if ($output[0][1] == 2);
		$result .= 'low">niedrigste Priorität' if ($output[0][1] == 3);
		$result .= '</a></td><td class="table" align="right"  valign="middle">'.$output[0][0].'</td><td class="table" align="right"  valign="middle">'.$output[0][2].'</td></tr>'."\n";
		$sum_of_all = $sum_of_all + $output[0][0];
		$sum_of_all_ok = $sum_of_all_ok + $output[0][2];
		if ($output[0][1] == 3) {
			# summe -> all priorities
			my $result2 .= '<tr><td class="table" align="right"><a href="'.$script_name.'?view=';
			$result2 .= 'all">alle Prioritäten';
			$result2 .= '</a></td><td class="table" align="right"  valign="middle">'.$sum_of_all.'</td><td class="table" align="right"  valign="middle">'.$sum_of_all_ok.'</td></tr>'."\n";
			#$result2 .= '<tr><td class="table" align="right">&nbsp;</td><td class="table" align="right"  valign="middle">&nbsp;</td><td class="table" align="right"  valign="middle">&nbsp;</td></tr>'."\n";
			$result = $result2.$result;
		}
		
	}
	return ($result);
}



sub get_number_of_ok_of_prio{
	# Alle abgarbeiteten fehler einer Priorität
	my $prio = $_[0];
	my $sql_text = "select count(*) from pd_error a join pd_error_desc b on ( a.error = b.id) where b.prio = ".$prio." and done=1;";
	my $result = 0;
	my $sth = $dbh->prepare( $sql_text )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
	#print '<p class="smalltext"/>'.$sql_text."</p>\n";					  
	$sth->execute or die $sth->errstr; # hier geschieht die Anfrage an die DB
	while (my $arrayref = $sth->fetchrow_arrayref()) {	
		foreach(@$arrayref) {
			$result = $_;
			$result = '' unless defined $result;
		}
	}
	return ($result);
}



sub get_number_error_and_desc_by_prio{
	my $prio = $_[0];

	my $order_column = 'a.name';
	$order_column = 'b.anzahl'      if ($param_orderby eq 'todo');
	$order_column = 'c.anzahl'        if ($param_orderby eq 'ok');
	$order_column = 'a.error_id'   if ($param_orderby eq 'error_id');
	my $order_asc = 'asc';
	$order_asc    = 'desc'   if ($param_sort eq 'desc');

	my $sql_text = "
  select IFNULL(b.anzahl, '') todo, IFNULL(c.anzahl, '') ok, a.name, a.error_id from ( select name, error_id from pd_error_desc where prio = ".$prio." ) a
  left outer join (	select count(*) anzahl, todo.error_id from pd_error todo where todo.done=0 group by todo.error_id ) b on (a.error_id = b.error_id)
  left outer join (	select count(*) anzahl, ok.error_id   from pd_error ok   where ok.done=1	  group by ok.error_id)   c on  (a.error_id = c.error_id)
  order by ".$order_column.' '.$order_asc.', a.name;';
  

	if ($prio == 0) {
		# show all priorities
		  $sql_text = "select IFNULL(b.anzahl, '') todo, IFNULL(c.anzahl, '') ok, a.name, a.error_id from ( select name, error_id from pd_error_desc where length( name )>1 ) a
		  left outer join (	select count(*) anzahl, todo.error_id from pd_error todo where todo.done=0 group by todo.error_id ) b on (a.error_id = b.error_id)
		  left outer join (	select count(*) anzahl, ok.error_id   from pd_error ok   where ok.done=1	  group by ok.error_id)   c on  (a.error_id = c.error_id)
		  order by ".$order_column.' '.$order_asc.', a.name;';
	}  
  
	my $result = '';
	my $sth = $dbh->prepare( $sql_text )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
	#print '<p class="smalltext"/>'.$sql_text."</p>\n";					  
	$sth->execute or die $sth->errstr; # hier geschieht die Anfrage an die DB
	$result .= '<table class="table">';
	$result .= '<tr>';
	$result .= '<th class="table">Anzahl';
	$result .= '<a href="'.$script_name.'?view='.$param_view.'&orderby=todo&sort=asc">↑</a>';
	$result .= '<a href="'.$script_name.'?view='.$param_view.'&orderby=todo&sort=desc">↓</a>';
	$result .= '</th>';
	$result .= '<th class="table">Erledigt';
	$result .= '<a href="'.$script_name.'?view='.$param_view.'&orderby=ok&sort=asc">↑</a>';
	$result .= '<a href="'.$script_name.'?view='.$param_view.'&orderby=ok&sort=desc">↓</a>';
	$result .= '</th>';
	$result .= '<th class="table">Beschreibung';
	$result .= '<a href="'.$script_name.'?view='.$param_view.'&orderby=name&sort=asc">↑</a>';
	$result .= '<a href="'.$script_name.'?view='.$param_view.'&orderby=name&sort=desc">↓</a>';
	$result .= '</th>';
	$result .= '<th class="table">ID';
	$result .= '<a href="'.$script_name.'?view='.$param_view.'&orderby=error_id&sort=asc">↑</a>';
	$result .= '<a href="'.$script_name.'?view='.$param_view.'&orderby=error_id&sort=desc">↓</a>';
	$result .= '</th>';
	$result .= '</tr>'."\n";
	while (my $arrayref = $sth->fetchrow_arrayref()) {	
		my @output;
		my $i = 0;
		my $j = 0;
		foreach(@$arrayref) {
			#print $_."\n";
			$output[$i][$j] = encode_entities($_);
			$output[$i][$j] = '' unless defined $output[$i][$j];
			$j = $j +1;
			if ($j == 4) {
				$j= 0;
				$i ++;
			}
			#print $_."\n";
		}

		$result .= '<tr>';
		$result .= '<td class="table" align="right"  valign="middle">'.$output[0][0].'</td>';
		$result .= '<td class="table" align="right"  valign="middle">'.$output[0][1].'</td>';
		$result .= '<td class="table"><a href="'.$script_name.'?view=only&id='.$output[0][3].'">'.$output[0][2].'</a></td>';
		$result .= '<td class="table" align="right"  valign="middle">'.$output[0][3].'</td>';
		$result .= '</tr>'."\n";	

	}
	$result .= '</table>';
	return ($result);
}



########################################
# Get headline of one eror
########################################

sub get_headline{
	my $error = $_[0];
	my $sql_text = " select name from pd_error_desc where error_id = ".$error.";";
	my $result = '';
	my $sth = $dbh->prepare( $sql_text )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
	#print '<p class="smalltext"/>'.$sql_text."</p>\n";					  
	$sth->execute or die $sth->errstr; # hier geschieht die Anfrage an die DB
	while (my $arrayref = $sth->fetchrow_arrayref()) {	
		foreach(@$arrayref) {
			$result = encode_entities($_);
			$result = '' unless defined $result;
		}
	}
	return ($result);
}



########################################
# Get description of one eror
########################################

sub get_description{
	my $error = $_[0];
	my $sql_text = " select notice from pd_error_desc where error_id = ".$error.";";
	my $result = '';
	my $sth = $dbh->prepare( $sql_text )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
	#print '<p class="smalltext"/>'.$sql_text."</p>\n";					  
	$sth->execute or die $sth->errstr; # hier geschieht die Anfrage an die DB
	while (my $arrayref = $sth->fetchrow_arrayref()) {	
		foreach(@$arrayref) {
			$result = $_;
			$result = encode_entities($result);
			$result = '' unless defined $result;
		}
	}
	return ($result);
}



########################################
# Get priority of one eror
########################################

sub get_prio_of_error{
	my $error = $_[0];
	my $sql_text = " select prio from pd_error_desc where error_id = ".$error.";";
	my $result = '';
	my $sth = $dbh->prepare( $sql_text )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
	#print '<p class="smalltext"/>'.$sql_text."</p>\n";					  
	$sth->execute or die $sth->errstr; # hier geschieht die Anfrage an die DB
	while (my $arrayref = $sth->fetchrow_arrayref()) {	
		foreach(@$arrayref) {
			$result = encode_entities($_);
			$result = '' unless defined $result;
		}
	}
	return ($result);
}



########################################
# List with articles of one error
########################################

sub get_article_of_error{
	my $error = $_[0];
	
	$column_orderby = '' if ( $column_orderby eq '');	
	$column_orderby = 'order by a.title' if ($param_orderby eq 'article');
	$column_orderby = 'order by a.notice' if ($param_orderby eq 'notice');
	$column_orderby = 'order by more' if ($param_orderby eq 'more');
	#$column_orderby = 'a.found' if ($param_orderby eq 'found');	
	$column_sort    = 'asc'   if ( $column_sort  eq '');	
	$column_orderby = $column_orderby .' '. $column_sort if ($column_orderby ne '');
	
	my $sql_text =	 "
		select a.title, a.notice , a.page_id, count(*) more from (	select title, notice, page_id from pd_error where error_id=".$error." and done=0) a
		join pd_error b
		on (a.page_id = b.page_id)
		where b.done = 0
		group by a.page_id
		".$column_orderby."					
		limit ".$param_offset.",".$param_limit.";";
	#-- ".$column_sort." 
	my $result = '';
	my $sth = $dbh->prepare( $sql_text )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
	#print '<p class="smalltext"/>'.$sql_text."</p>\n";					  
	$sth->execute or die $sth->errstr; # hier geschieht die Anfrage an die DB
	
	$result .= '<p>';
	$result .= '<a href="'.$script_name.'?view=only&id='.$param_id.'&offset='.$offset_lower.'&limit='.$param_limit.'&orderby='.$param_orderby.'&sort='.$param_sort.'">←</a>';
	$result .= ' '.$param_offset.' bis '.$offset_end.' ';
	$result .= '<a href="'.$script_name.'?view=only&id='.$param_id.'&offset='.$offset_higher.'&limit='.$param_limit.'&orderby='.$param_orderby.'&sort='.$param_sort.'">→</a>';
	$result .= '</p>';
	
	$result .= '<table class="table">';
	$result .= '<tr>';
	$result .= '<th class="table">Artikel';
		$result .= '<a href="'.$script_name.'?view=only&id='.$param_id.'&offset='.$param_offset.'&limit='.$param_limit.'&orderby=article&sort=asc">↑</a>';
		$result .= '<a href="'.$script_name.'?view=only&id='.$param_id.'&offset='.$param_offset.'&limit='.$param_limit.'&orderby=article&sort=desc">↓</a>';
		$result .= '</th>';
	$result .= '<th class="table">Edit</th>';
	$result .= '<th class="table">Hinweis';
		$result .= '<a href="'.$script_name.'?view=only&id='.$param_id.'&offset='.$param_offset.'&limit='.$param_limit.'&orderby=notice&sort=asc">↑</a>';
		$result .= '<a href="'.$script_name.'?view=only&id='.$param_id.'&offset='.$param_offset.'&limit='.$param_limit.'&orderby=notice&sort=desc">↓</a>';
		$result .= '</th>';
	$result .= '<th class="table">Mehrere';
		$result .= '<a href="'.$script_name.'?view=only&id='.$param_id.'&offset='.$param_offset.'&limit='.$param_limit.'&orderby=more&sort=asc">↑</a>';
		$result .= '<a href="'.$script_name.'?view=only&id='.$param_id.'&offset='.$param_offset.'&limit='.$param_limit.'&orderby=more&sort=desc">↓</a>';
		$result .= '</th>';
	$result .= '<th class="table">Abarbeitung</th>';
	$result .= '</tr>'."\n";
	while (my $arrayref = $sth->fetchrow_arrayref()) {	
		my @output;
		my $i = 0;
		my $j = 0;
		foreach(@$arrayref) {
			#print $_."\n";
			# Ausgabe der einzelnen Fehler eines Artikels
			$output[$i][$j] = encode_entities($_);
			$output[$i][$j] =~ s/\n/<br \/>/g;
			$output[$i][$j] = '' unless defined $output[$i][$j];
			$j = $j +1;
			if ($j == 4) {
				$j= 0;
				$i ++;
			}
			#print $_."\n";
		}

		$result .= '<tr>';
		$result .= '<td class="table"><a target="_blank" href="http://de.wikipedia.org/wiki/'.$output[0][0].'">'.$output[0][0].'</a></td>';
		$result .= '<td class="table"><a target="_blank" href="http://de.wikipedia.org/w/index.php?title='.$output[0][0].'&action=edit">edit</a></td>';

		$result .= '<td class="table">'.$output[0][1].'</td>';
		$result .= '<td class="table" align="center"  valign="middle">';
		#if ($output[0][3] == 1) {
			# only one error
		#} else {
			# more other errors
			
			$result .= '<a href="'.$script_name.'?view=detail&pageid='.$output[0][2].'&from='.$param_id.'">'.$output[0][3].'</a>';
		#}
		$result .= '</td>';
		$result .= '<td class="table" align="center"  valign="middle">';
		$result .= '<a href="'.$script_name.'?view=only&id='.$error.'&pageid='.$output[0][2].'&offset='.$param_offset.'&limit='.$param_limit.'&orderby='.$param_orderby.'&sort='.$param_sort.'" rel="nofollow">Erledigt</a>';
		$result .= '</td></tr>'."\n";	

	}
	$result .= '</table>';
	return ($result);
}



########################################
# List with done articles of one error
########################################

sub get_done_article_of_error{
	my $error = $_[0];
	
	$column_orderby = '' if ( $column_orderby eq '');	
	$column_orderby = 'order by a.title' if ($param_orderby eq 'article');
	$column_orderby = 'order by a.notice' if ($param_orderby eq 'notice');
	$column_orderby = 'order by more' if ($param_orderby eq 'more');
	#$column_orderby = 'a.found' if ($param_orderby eq 'found');	
	$column_sort    = 'asc'   if ( $column_sort  eq '');	
	$column_orderby = $column_orderby.' '.$column_sort if ($column_orderby ne '');
	my $sql_text =	 "
		select a.title, a.notice , a.page_id, count(*) more 
		from (	select title, notice, page_id from pd_error where error_id=".$error." and done=1) a
		join pd_error b
		on (a.page_id = b.page_id)
		group by a.page_id
		".$column_orderby."
		limit ".$param_offset.",".$param_limit.";";
	
	my $result = '';
	my $sth = $dbh->prepare( $sql_text )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
	#print '<p class="smalltext"/>'.$sql_text."</p>\n";					  
	$sth->execute or die $sth->errstr; # hier geschieht die Anfrage an die DB
	
	$result .= '<p>';
	$result .= '<a href="'.$script_name.'?view=onlydone&id='.$param_id.'&offset='.$offset_lower.'&limit='.$param_limit.'&orderby='.$param_orderby.'&sort='.$param_sort.'">←</a>';
	$result .= ' '.$param_offset.' bis '.$offset_end.' ';
	$result .= '<a href="'.$script_name.'?view=onlydone&id='.$param_id.'&offset='.$offset_higher.'&limit='.$param_limit.'&orderby='.$param_orderby.'&sort='.$param_sort.'">→</a>';
	$result .= '</p>';
	
	$result .= '<table class="table">';
	$result .= '<tr>';
	$result .= '<th class="table">Artikel';
		$result .= '<a href="'.$script_name.'?view=onlydone&id='.$param_id.'&offset='.$param_offset.'&limit='.$param_limit.'&orderby=article&sort=asc">↑</a>';
		$result .= '<a href="'.$script_name.'?view=onlydone&id='.$param_id.'&offset='.$param_offset.'&limit='.$param_limit.'&orderby=article&sort=desc">↓</a>';
		$result .= '</th>';
	$result .= '<th class="table">Version</th>';
	$result .= '<th class="table">Hinweis';
		$result .= '<a href="'.$script_name.'?view=onlydone&id='.$param_id.'&offset='.$param_offset.'&limit='.$param_limit.'&orderby=notice&sort=asc">↑</a>';
		$result .= '<a href="'.$script_name.'?view=onlydone&id='.$param_id.'&offset='.$param_offset.'&limit='.$param_limit.'&orderby=notice&sort=desc">↓</a>';
		$result .= '</th>';
	$result .= '<th class="table">Mehrere';
		$result .= '<a href="'.$script_name.'?view=onlydone&id='.$param_id.'&offset='.$param_offset.'&limit='.$param_limit.'&orderby=more&sort=asc">↑</a>';
		$result .= '<a href="'.$script_name.'?view=onlydone&id='.$param_id.'&offset='.$param_offset.'&limit='.$param_limit.'&orderby=more&sort=desc">↓</a>';
		$result .= '</th>';
	$result .= '<th class="table">Abarbeitung</th>';
	$result .= '</tr>'."\n";
	while (my $arrayref = $sth->fetchrow_arrayref()) {	
		my @output;
		my $i = 0;
		my $j = 0;
		foreach(@$arrayref) {
			#print $_."\n";
			$output[$i][$j] = encode_entities($_);
			$output[$i][$j] = '' unless defined $output[$i][$j];
			$j = $j +1;
			if ($j == 4) {
				$j= 0;
				$i ++;
			}
			#print $_."\n";
		}

		$result .= '<tr>';
		$result .= '<td class="table"><a target="_blank" href="http://de.wikipedia.org/wiki/'.$output[0][0].'">'.$output[0][0].'</a></td>';
		$result .= '<td class="table"><a target="_blank" href="http://de.wikipedia.org/w/index.php?title='.$output[0][0].'&action=history">Versionen</a></td>';
		#$result .= '<td class="table"><a target="_blank" href="http://de.wikipedia.org/w/index.php?title='.$output[0][0].'&action=edit">edit</a></td>';

		$result .= '<td class="table">'.$output[0][1].'</td>';
		$result .= '<td class="table" align="center"  valign="middle">';
		if ($output[0][3] == 1) {
			# only one error
		} else {
			# more other errors
			
			$result .= '<a href="'.$script_name.'?view=detail&pageid='.$output[0][2].'&from='.$param_id.'">'.$output[0][3].'</a>'
		}
		$result .= '</td>';
		$result .= '<td class="table" align="center"  valign="middle">';
		#$result .= '<a href="'.$script_name.'?view=only&id='.$error.'&pageid='.$output[0][2].'&offset='.$param_offset.'&limit='.$param_limit.'&orderby='.$param_orderby.'&sort='.$param_sort.'">Erledigt</a>';
		$result .= 'ok';
		$result .= '</td></tr>'."\n";	

	}
	$result .= '</table>';
	return ($result);
}



########################################
# List
########################################

sub get_list{
	$column_orderby = 'more' if ( $column_orderby eq '');	
	$column_orderby = 'title'    if ($param_orderby eq 'article');
	$column_orderby = 'more' if ($param_orderby eq 'more');
	$column_sort    = 'desc'     if ( $column_sort  eq '');
	
	
	my $sql_text = "select title, count(*)more, page_id from pd_error where done=0 
	group by page_id order by ".$column_orderby." ".$column_sort.", title 
	limit ".$param_offset.",".$param_limit.";";
	
	my $sth = $dbh->prepare( $sql_text )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
	#print '<p class="smalltext"/>'.$sql_text."</p>\n";					  
	$sth->execute or die $sth->errstr; # hier geschieht die Anfrage an die DB
	my $result = '';
	
	$result .= '<p>';
	$result .= '<a href="'.$script_name.'?view=list&offset='.$offset_lower.'&limit='.$param_limit.'&orderby='.$param_orderby.'&sort='.$param_sort.'">←</a>';
	$result .= ' '.$param_offset.' bis '.$offset_end.' ';
	$result .= '<a href="'.$script_name.'?view=list&offset='.$offset_higher.'&limit='.$param_limit.'&orderby='.$param_orderby.'&sort='.$param_sort.'">→</a>';
	$result .= '</p>';
	
	$result .= '<table class="table">';
	$result .= '<tr>';
	$result .= '<th class="table">Vorschläge';
		$result .= '<a href="'.$script_name.'?view=list&offset='.$param_offset.'&limit='.$param_limit.'&orderby=more&sort=asc">↑</a>';
		$result .= '<a href="'.$script_name.'?view=list&offset='.$param_offset.'&limit='.$param_limit.'&orderby=more&sort=desc">↓</a>';
		$result .= '</th>';

	$result .= '<th class="table">Artikel';
		$result .= '<a href="'.$script_name.'?view=list&offset='.$param_offset.'&limit='.$param_limit.'&orderby=article&sort=asc">↑</a>';
		$result .= '<a href="'.$script_name.'?view=list&offset='.$param_offset.'&limit='.$param_limit.'&orderby=article&sort=desc">↓</a>';
		$result .= '</th>';
	$result .= '<th class="table">Details</th>';
	$result .= '</tr>'."\n";
	
	while (my $arrayref = $sth->fetchrow_arrayref()) {	
		my @output;
		my $i = 0;
		my $j = 0;
		foreach(@$arrayref) {
			#print $_."\n";
			$output[$i][$j] = encode_entities($_);
			$output[$i][$j] = '' unless defined $output[$i][$j];
			$j = $j +1;
			if ($j == 3) {
				$j= 0;
				$i ++;
			}
			#print $_."\n";
		}

		$result .= '<tr><td class="table">'.$output[0][1].'</td><td class="table"><a target="_blank" href="http://de.wikipedia.org/wiki/'.$output[0][0].'">'.$output[0][0].'</a></td>';
		$result .= '<td class="table" align="middle"  valign="middle"><a href="'.$script_name.'?view=detail&pageid='.$output[0][2].'">Details</a></td>';
		$result .= '</tr>'."\n";	

	}
	$result .= '</table>';
	return ($result);
}



########################################
# Detailansicht
########################################

sub get_all_error_of_article{
	my $page_id = $_[0];
	my $sql_text = "select a.error_id, b.name, a.notice, a.page_id, a.done
					from pd_error a join pd_error_desc b 
					on ( a.error_id = b.error_id) 
					where a.page_id = ".$page_id." 
					order by b.name;";
	my $sth = $dbh->prepare( $sql_text )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
	#print '<p class="smalltext"/>'.$sql_text."</p>\n";					  
	$sth->execute or die $sth->errstr; # hier geschieht die Anfrage an die DB
	my $result = '';
	$result .= '<table class="table">';
	$result .= '<tr>';
	$result .= '<th class="table">Fehler</th>';
	$result .= '<th class="table">Hinweis</th>';
	$result .= '<th class="table">Abarbeitung</th>';
	$result .= '</tr>'."\n";
	
	while (my $arrayref = $sth->fetchrow_arrayref()) {	
		my @output;
		my $i = 0;
		my $j = 0;
		foreach(@$arrayref) {
			#print $_."\n";
			$output[$i][$j] = encode_entities($_);
			$output[$i][$j] = '' unless defined $output[$i][$j];
			$j = $j +1;
			if ($j == 5) {
				$j= 0;
				$i ++;
			}
			#print $_."\n";
		}

	
		$result .= '<tr>';
		$result .= '<td class="table"><a href="'.$script_name.'?view=only&id='.$output[0][0].'">'.$output[0][1].'</a></td>';
		$result .= '<td class="table">'.$output[0][2].'</td>';
		$result .= '<td class="table" align="center"  valign="middle">';
		if ($output[0][4] eq '0') {
			$result .= '<a href="'.$script_name.'?view=detail&id='.$output[0][0].'&pageid='.$output[0][3].'" rel="nofollow">Erledigt</a></td>';
		} else {
			$result .= 'ok';
		}
		
		$result .= '</tr>'."\n";		

	}
	$result .= '</table>';
	return ($result);
}


