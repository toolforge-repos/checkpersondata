#################################################################
#								#
#	Updates the numberOfEntrys.txt file in 			#
#	public_html/cgi-bin/ every night			#
#								#
#################################################################
#/bin/bash

echo "select count(*) from pd;" > tmpSQL

mysql --host=tools.db.svc.wikimedia.cloud < tmpSQL > tmpTitles

rm tmpSQL

tail -n +2 tmpTitles > noe.txt

rm tmpTitles

mv noe.txt public_html/cgi-bin/numberOfEntrys.txt

