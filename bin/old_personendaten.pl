﻿#!/usr/local/bin/perl

#################################################################
# Program:	personendaten.pl
# Descrition:	Scan for PD from Wikipedia (dump or live)
# Autor:	Stefan Kühn
#################################################################


use strict;				# only nice code 
use warnings;			# show all warnings
use DBI;				# DB-Connection 
use Encode;				# decode / encode
use URI::Escape;		# Handle of URL
use LWP::UserAgent;		# Get webpages

use lib 'bin/';
use wikipedia;			# for procedures like: scan_article_for_categorys()



#################################################################
# Declaration of parameters (extern)
#################################################################

our %parameter;	# storage for all parameter

$parameter{'code_version'} 		= '2014-02-02';

$parameter{'path_dump'} 	  	= ''; # now configfile
$parameter{'path_output'} 	 	= 'var';



our $input_directory_new 	       		= '../data/new_article/';
our $input_directory_last_changes      = '../data/last_changes/';
	


#################################################################
# Declaration of variables (intern and global)
#################################################################
$parameter{'modus_scan'}    = 'dump';		# scan modus (dump, live)
$parameter{'modus_silent'}  = '';	        # not so much output
$parameter{'modus_nodb'}    = '';	        # for tests with no impact of the db

our $project = 'dewiki';

our $dump_date_for_output 					= '';

#our $parameter{'dump_filename'}  						= '';
$parameter{'file_page_to_scan'}				= 'var/input_page_to_scan.txt';
$parameter{'file_error_wiki'}				= 'output_pd_error_for_wikipedia.html';

#our $first_name_filename 					= 'input_first_name.txt';
our $family_name_filename 					= 'input_family_name.txt';
our $category_for_person_filename 			= 'input_category_de_person.txt';
our $category_for_noperson_filename 		= 'input_category_de_noperson.txt';
our $output_new_first_names_filename 		= 'output_new_first_names.txt';
our $output_pd_export_filename				= 'output_pd_export.txt';

our $time_start				= time();	# start timer in secound
our $line_number			= 0;		# number of line in dump
our $article_number			= 0;		# number of article
our $language				= '';		# language of dump "de", "en"; 
our $date					= 0;		# date of dump "20060324"


our %page;
our $title					= '';		# title of the current article
our $page_id				= -1;		# page id of the current article
our $revision_id			= -1;		# revision id of the current article
our $revision_time			= -1;		# revision time of the current article
our $text					= '';		# text of the current article

our $name 					= "";
our $aname		 			= "";
our $kurz		 			= "";
our $kurz_plain   			= "";
our $geburtsdatum 			= "";
our $geburtsdatum_plain 	= "";
our $geburtstag 			= "";
our $geburtsmonat			= "";
our $geburtsjahr 			= "";
our $geburtsort				= "";
our $geburtsort_plain		= "";
our $sterbedatum			= "";
our $sterbedatum_plain		= "";
our $sterbetag				= "";
our $sterbemonat			= "";
our $sterbejahr				= "";
our $sterbeort				= "";
our $sterbeort_plain		= "";	
our $is_redirect = 0;

our @live_article;									# to-do-list for live (all articles to scan)
our $current_live_article				= -1;		# line_number_of_current_live_article
our $end_of_dump 						= 0;		# when last article from dump scan then 1, else 0
our $end_of_live 						= 0;		# when last article from live scan then 1, else 0


our $all_categories 					= '';
our $all_category_for_person 			= '';		# all known categories for person
our $all_interwikis 					= '';


our @error_description;								# Error Description
our $this_article_is_with_errors 		= -1;
our $this_article_all_errors 			= '';
our $fehler_anzahl 						= -1;		# number of found errors
our $number_of_error_description		= -1;		# number of error descriptions

our @fehlerarticle ;
our $zaehler_fehler_998_nur_jahr 		= 0;
our $double_error 						= 0;

our $number_of_live_tests 				= -1;		# Number of articles for live test

our $has_pd 							= 0;
our $is_a_person 						= 0;
our $is_a_noperson 						= 0;
our $has_defaultsort 					= 0;
our $defaultsort 						= '';

our $new_article_for_output 			= 0;
our $change_article_for_output 			= 0;

our $last_article_new_daytime 			= '';		# time of the last scanned article from new
our $last_article_change_daytime 		= '';		# time of the last scanned article from change

# aktuelle Zeit
our ($akSekunden, $akMinuten, $akStunden, $akMonatstag, $akMonat,
	$akJahr, $akWochentag, $akJahrestag, $akSommerzeit) = localtime(time);
our $CTIME_String = localtime(time);
$akMonat = $akMonat + 1;
$akJahr = $akJahr + 1900;	
$akMonat   = "0".$akMonat if ($akMonat<10);
$akMonatstag = "0".$akMonatstag if ($akMonatstag<10);
$akStunden = "0".$akStunden if ($akStunden<10);
$akMinuten = "0".$akMinuten if ($akMinuten<10);
#print $akJahr.'-'.$akMonat.'-'.$akMonatstag.' '.$akStunden.':'.$akMinuten."\n";

our $xml_text_from_api 					='';		#get more then on article from api


our @first_name;		# all known first names
our @family_name;		# all known family names
our $new_first_names 	   = '';
our $vorname_anzahl 	   = 0;
our $familiennamen_anzahl  = 0;

our @cat_person;
our @cat_no_person;

our $dbh;			# for database handler
our $password;








print "\npersonendaten - version ".$parameter{'code_version'}."\n";
print "\n";	


# Error exception
$SIG{__DIE__} = \&die_error;			# error handler for the subroutine die
$SIG{__WARN__} = \&warn_error;			# warning handler



check_input_arguments();

read_configfile();
open_db();							
get_dump_file_name();
	
load_first_name();
load_family_name();
load_category_for_person();
load_category_for_no_person();


#truncate_error_desc();				# should replaced by "get_error_description" see F001
get_error_description();
output_errors_desc_in_db() ;

# before scan
open_dump_file();
load_article_for_live_scan();
truncate_table_pd_error();

# scan
scan_pages();


# after scan
close_dump_file();
output_input_for_live();
output_new_first_names();
delete_article_from_table_pd_new();
delete_article_from_table_change();


close_db();
print_end_time();

print 'Finish'. "\n\n";











#################################################################
#################################################################
#################################################################
#################################################################
# Subroutines
#################################################################
#################################################################


sub check_input_arguments{


	# input agruments
	# Syntax:  perl personendaten.pl MODUS
	# Example: perl personendaten.pl -dump 
	# Example: perl personendaten.pl -live

	# all possible input agrument
	my %input_agrument;
	$input_agrument{'a'} = 'all';		
	$input_agrument{'c'} = 'change';	
	$input_agrument{'d'} = 'dump';		
	$input_agrument{'e'} = 'error';		
	$input_agrument{'f'} = 'file';		
	$input_agrument{'p'} = 'pd';		
	$input_agrument{'t'} = 'table';		

	$input_agrument{'s'} = 'silent';	# TODO
	$input_agrument{'o'} = 'only';		# TODO
	$input_agrument{'n'} = 'nodb';		# TODO  

	# description for all parameter
	my %input_agru_desc;
	$input_agru_desc{'a'} = 'scan change & file & table';
	$input_agru_desc{'c'} = 'scan from table change';
	$input_agru_desc{'d'} = 'scan from dump';
	$input_agru_desc{'e'} = 'scan all articles from table pd with errors';
	$input_agru_desc{'f'} = 'scan from file input_page_to_scan.txt';
	$input_agru_desc{'t'} = 'scan from table page_to_scan';
	$input_agru_desc{'p'} = 'scan all articles from table pd';

	$input_agru_desc{'s'} = 'silent (minimal output)';
	$input_agru_desc{'o'} = 'scan only some errors (like -o=51,23,108)';
	$input_agru_desc{'n'} = 'no impact at database';

	# no parameters
	if ( @ARGV < 1) {
		print 'Found no parameters'."\n\n";
		foreach my $argu (sort keys %input_agrument){
			printf ("\t -%1s -%-8s %s\n", $argu, $input_agrument{$argu}, $input_agru_desc{$argu});
		}
		print "\nExamples:\n";
		print "\tperl personendaten.pl -c\n";
		print "\tperl personendaten.pl -d\n";
		print "\tperl personendaten.pl -c -t -f\n";
		print "\n";
		exit;
	}	
	
	# unkown parameter
	foreach my $argv (@ARGV){
		$argv =~ s/=.*//;	# only first part like "-o" at "-o=51,23,108"
		my $ok = 'no';
		foreach my $argu (sort keys %input_agrument){
			$ok = 'yes' if $argv eq '-'.$argu;
			$ok = 'yes' if $argv eq '-'.$input_agrument{$argu};
		}
		if ($ok ne 'yes') {
			print 'Found unkown parameter: '.$argv."\n";
			die 'End of program';
		}
	}


	# wrong parameter for scanmodus
#	if ( $ARGV[0] ne '-dump' and 
#		 $ARGV[0] ne '-live' and 
#		 $ARGV[0] ne '-only'){
#		die 'Die, wrong modus, -dump or -live or -only' 
#	}


	# save scan_modus in table
	$parameter{'scan_change'} = 'no';
	$parameter{'scan_dump'}   = 'no';
	$parameter{'scan_file'}   = 'no';
	$parameter{'scan_table'}  = 'no'; 
	$parameter{'scan_pd'}     = 'no';
	$parameter{'scan_error'}  = 'no';
	foreach my $argv (@ARGV){
		if ($argv eq '-a' or $argv eq '-'.$input_agrument{'a'}){
			$parameter{'scan_change'} = 'yes';
			$parameter{'scan_file'}   = 'yes';
			$parameter{'scan_table'}  = 'yes'; 
		}
		if ($argv eq '-c' or $argv eq '-'.$input_agrument{'c'}){
			$parameter{'scan_change'} = 'yes'; 
		}
		if ($argv eq '-d' or $argv eq '-'.$input_agrument{'d'}){
			$parameter{'scan_dump'}   = 'yes'; 
		}
		if ($argv eq '-e' or $argv eq '-'.$input_agrument{'e'}){
			$parameter{'scan_error'}  = 'yes'; 
		}
		if ($argv eq '-f' or $argv eq '-'.$input_agrument{'f'}){
			$parameter{'scan_file'}   = 'yes'; 
		}
		if ($argv eq '-p' or $argv eq '-'.$input_agrument{'p'}){
			$parameter{'scan_pd'}     = 'yes'; 
		}
		if ($argv eq '-t' or $argv eq '-'.$input_agrument{'t'}){
			$parameter{'scan_table'}  = 'yes'; 
		}

	}	

	# output
	foreach my $key(sort keys %parameter){
		# printf ("\t%-30s = %s\n", $key ,$parameter{$key}) ; 
	}
	print "\n";


	# scan_dump only without table, file, change
	if ($parameter{'scan_dump'} eq 'yes'){
		if ($parameter{'scan_change'} eq 'yes' or
			$parameter{'scan_file'} eq 'yes' or
			$parameter{'scan_table'} eq 'yes') {
			print 'Scan_dump is not allowed with option: change, file, table'."\n";
			die 'End of program';
		}
		$parameter{'modus_scan'} = 'dump';
	} else {
		$parameter{'modus_scan'} = 'live';
	}	
	
	if ( @ARGV > 1) {
		foreach ( @ARGV) {
			if ( $_ eq 'silent') {
				$parameter{'modus_silent'}  = 'silent';
			}
		}
	}
	
	if ( @ARGV > 1) {
		foreach ( @ARGV) {
			if ( $_ eq 'nodb') {
				$parameter{'modus_nodb'}  = 'nodb';
			}
		}
	}
	
	print 'Modus: ';
	print 'scan a dump of wikipedia' if ($parameter{'scan_dump'} eq 'yes');
	print 'scan live in wikipedia'   if ($parameter{'scan_dump'} eq 'no');
	print 'scan a dump of wikipedia only some errors'   if ($parameter{'modus_scan'}  eq 'only');
	print "\n";
	print 'silent = activated' if ($parameter{'modus_silent'}  eq 'silent');
	print "\n";


	
}



##############################################################################

sub read_configfile{
	# read_configfile for global settings (DB,Schemata,Password,dumppath...)

	print 'read config.txt'."\n";

	# open configfile
	my $config_file = 'config.txt';
	open(CONFIG, "<$config_file") or die "cannot open < $config_file\n$!";


	# read configfile line by line
	do {
		my $line = <CONFIG>;

		
		if ($line =~ /=/ ) {
			$line =~ s/\n//g;  
			$line =~ s/\t/ /g; #tabulator to space
			my $key = substr($line, 0, index($line,'=')-1);
			my $value = substr($line, index($line,'=')+1);

			$key   =~ s/^\s+|\s+$//g;	# remove space at begin and end
			$value =~ s/^\s+|\s+$//g;

			if ($key ne ''){
				$parameter{$key} = $value;	# save in hash parameter
			}
			
		}
	}
	while (eof(CONFIG) != 1);

	# close configfile
	close(CONFIG);

	# print all parameter
	foreach my $key(sort keys %parameter){
		printf ("\t%-30s = %s\n", $key ,$parameter{$key}) ; 
	}
	print "\n";
	
}



##############################################################################



sub get_dump_file_name{

	# check path_dump from configfile
	if ($parameter{'path_dump'} eq ''){
		die 'path_dump has no value';
	}


	# search the newest dump
	$parameter{'dump_filename_new'} = '';
	$parameter{'dump_filename_new'} = search_for_last_dump();


	# load dump_filename from database table parameter # TODO
	$parameter{'dump_filename'} = ''; 
	my $sql_text ="select value from parameter where dbname='dewiki' and name='dump_filename' limit 1;";
	my $sth = $dbh->prepare($sql_text) or die ($dbh->errstr);
	$sth->execute or die $sth->errstr;
	if ($sth->rows() > 0) {
		while (my ($value) = $sth->fetchrow_array()) {
		   $parameter{'dump_filename'} = $value;
		}
	} else {
		# no parameter in database -> insert empty parameter
		my $sql_text ="insert into parameter (dbname,name,value)
        values ('dewiki', 'dump_filename', '".$parameter{'dump_filename_new'}."');";
		$dbh->do($sql_text);		
		$parameter{'dump_filename'} = ''
	}


	# found newer dumpfile and start dumpscan
	if ($parameter{'dump_filename_new'} ne $parameter{'dump_filename'}) {
		print 'Last:    '. $parameter{'dump_filename'}."\n";
		print 'Current: '. $parameter{'dump_filename_new'}."\n";
		
		# write the new dump in Database table parameter 
		my $sql_text ="update parameter set value = '".$parameter{'dump_filename_new'}."' where 
		dbname ='dewiki' and name ='dump_filename';";
		$dbh->do($sql_text);

		$parameter{'dump_filename'} = $parameter{'dump_filename_new'};
	}

 
	
	
	#get date from dumpfile
	if ($parameter{'dump_filename'} ne '') {
		$dump_date_for_output = $parameter{'dump_filename'};
		$dump_date_for_output =~ s/^[^0-9]+//g;
		$dump_date_for_output =~ s/[^0-9]+$//g;
		$dump_date_for_output = substr($dump_date_for_output,6,2).'.'.substr($dump_date_for_output,4,2).'.'.substr($dump_date_for_output,0,4);
	}
	

}

sub open_dump_file{

	if ($parameter{'modus_scan'}  eq 'dump' or $parameter{'modus_scan'}  eq 'only') {

		my $dump_file = $parameter{'path_dump'}.'/'.$parameter{'dump_filename'};

		# check for existens
		if (-e "$dump_file") {
			print "Data: $dump_file\n";
		} else {
			die "$dump_file don't exist!\n";
		}

		#open dump 
		open(DUMP, "bzip2 -d -q <$dump_file |");

		if ($parameter{'modus_scan'}  eq 'dump') {
			open(OUTPUT, ">$output_pd_export_filename");
			close(OUTPUT);
		}
		
	}
}

sub close_dump_file{
	if ($parameter{'modus_scan'}  eq 'dump' or $parameter{'modus_scan'}  eq 'only') {
		close(DUMP);
	}
}


##############################################################################
# Subroutines DB

sub open_db{
	if ($parameter{'modus_nodb'}  ne 'nodb') {
		#Connect to database u_sk
		print "\n".'open database: '.$parameter{'db_database'}."\n";

		$dbh = DBI->connect( "DBI:mysql:database=$parameter{'db_database'};host=$parameter{'host'}",  # local
								$parameter{'db_user'},
								$parameter{'db_password'} ,
								{
									RaiseError => 1,
									AutoCommit => 1,
									mysql_enable_utf8 => 1
								}
		  ) or die "Database connection not made: $DBI::errstr" . DBI->errstr;
	}
	
}




sub close_db{
	if ($parameter{'modus_nodb'}  ne 'nodb') {
		$dbh->disconnect();
	}

}

sub truncate_table_pd_error{
	# TODO Trennen leeren pd_errors und create tables
	# TODO Wozu create tables procedure noch notwendig?

	if ($parameter{'modus_nodb'}  ne 'nodb') {

		# check for existing of table pd_error
		my $sql_text = "show table status like 'pd_error';";
		my $result = '';
		my $sth = $dbh->prepare( $sql_text );
		print $sql_text."\n";					  
		$sth->execute;
		my $arrayref = $sth->fetchrow_arrayref();
		if ( defined($arrayref)) {
		} else {
				print 'create table pd_error'."\n";
				$sql_text = "
					create table pd_overview_errors
					(project varchar(100),
					error_id int(8),
					error_sum bigint(20),
					done_sum bigint(20),
					name varchar(4000),
					prio int(8)
					);";
				$sth = $dbh->prepare( $sql_text )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
				$sth->execute or die $sth->errstr; # hier geschieht die Anfrage an die DB

				print 'create table pd_errors'."\n";
				$sql_text = "
					create table pd_error
					(project varchar(100),
					page_id bigint(20),
					title varchar(4000),
					error_id int(8),
					notice varchar(4000),
					done int(1),
					found datetime
					);";
				$sth = $dbh->prepare( $sql_text )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
				$sth->execute or die $sth->errstr; # hier geschieht die Anfrage an die DB

				print 'create table pd_error_desc'."\n";
				$sql_text = "
					create table pd_error_desc
					(error_id int(8),
					prio int(8),
					name varchar(4000),
					notice varchar(4000)
					);";
				$sth = $dbh->prepare( $sql_text )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
				$sth->execute or die $sth->errstr; # hier geschieht die Anfrage an die DB

				print 'create table parameter'."\n";
				$sql_text = "
					create table parameter
					(
					project varchar(200),
					name varchar(200),
					value varchar(200)
					);";
				$sth = $dbh->prepare( $sql_text )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
				$sth->execute or die $sth->errstr; # hier geschieht die Anfrage an die DB

				print 'create table pd_new'."\n";
				$sql_text = "
					create table pd_new
					(
					project varchar (100),
					title varchar(4000),
					daytime datetime,
					scan_live tinyint(1)
					);";
				$sth = $dbh->prepare( $sql_text )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
				$sth->execute or die $sth->errstr; # hier geschieht die Anfrage an die DB

				print 'create table pd_change'."\n";
				$sql_text = "
					create table pd_change
					(
					project varchar (100),
					title varchar(4000),
					daytime datetime,
					scan_live tinyint(1)
					);";
				$sth = $dbh->prepare( $sql_text )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
				$sth->execute or die $sth->errstr; # hier geschieht die Anfrage an die DB

				print 'create index'."\n";
				$sql_text = "create index pd_error_done on pd_error (done);";
				$sth = $dbh->prepare( $sql_text )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
				$sth->execute or die $sth->errstr; # hier geschieht die Anfrage an die DB

				$sql_text = "create index pd_error_error_id on pd_error (error_id);";
				$sth = $dbh->prepare( $sql_text )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
				$sth->execute or die $sth->errstr; # hier geschieht die Anfrage an die DB

				$sql_text = "create index pd_error_page_id on pd_error (page_id);";
				$sth = $dbh->prepare( $sql_text )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
				$sth->execute or die $sth->errstr; # hier geschieht die Anfrage an die DB

				$sql_text = "create index pd_error_error_id_done on pd_error (error_id, done);";
				$sth = $dbh->prepare( $sql_text )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
				$sth->execute or die $sth->errstr; # hier geschieht die Anfrage an die DB

				print 'insert parameter'."\n";
				$sql_text = "insert into parameter value ('dewiki', 'daytime_last_article_from_new',    '2008-04-02 05:59:32');";
				$sth = $dbh->prepare( $sql_text )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
				$sth->execute or die $sth->errstr; # hier geschieht die Anfrage an die DB

				$sql_text = "insert into parameter value ('dewiki', 'daytime_last_article_from_change', '2008-04-02 05:59:32');";
				$sth = $dbh->prepare( $sql_text )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
				$sth->execute or die $sth->errstr; # hier geschieht die Anfrage an die DB

				get_error_description();
		}

		# 
#		print 'truncate table pd_error'."\n";
#		$sql_text = "truncate table pd_error; ";
#		$sth = $dbh->prepare( $sql_text )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
#		$sth->execute or die $sth->errstr; # hier geschieht die Anfrage an die DB
	}
}

sub truncate_error_desc{
	
	if ($parameter{'modus_nodb'}  ne 'nodb') {

		my $sql_text = "truncate table pd_error_desc;";
		my $sth = $dbh->prepare( $sql_text )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
		$sth->execute or die $sth->errstr; # hier geschieht die Anfrage an die DB


		for(my $i = 0; $i <= 2000; $i ++) {
			my $sql_text2 = "insert into pd_error_desc values (" .$i. ", 0, '' ,'');";
			$sth = $dbh->prepare( $sql_text2 )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
			$sth->execute or die $sth->errstr;
			$error_description[$i][0] = '';				# set in error
			$error_description[$i][1] = '';				# set in error
			$error_description[$i][2] = -1;				# set in error
			$error_description[$i][3] = 0;
			$error_description[$i][4] = -1;
			$error_description[$i][5] = '';
			$error_description[$i][6] = '';
			$error_description[$i][7] = 0;
			$error_description[$i][8] = 0;
			$error_description[$i][9] = '';
			$error_description[$i][10] = '';
		}
	}
}

sub get_error_description{
	# this subroutine check out the error description of all possible errors
	print 'Load all error description'."\n" ;
	error_list('get_description');

	# count the number of error description
	
	$number_of_error_description = 1;		# first error is error with number 1
	while  (defined($error_description[$number_of_error_description][1]) ) {
		#print $number_of_error_description.' '. $error_description[$number_of_error_description][1]."\n";
		$number_of_error_description = $number_of_error_description + 1;
	}
	
	
	# set all known error description to a basic level
	for (my $i = 1; $i <= $number_of_error_description; $i++) {
		#$error_description[$i][0] = -1;				# set in error
		#$error_description[$i][1] = '';				# set in error
		#$error_description[$i][2] = '';				# set in error
		$error_description[$i][3] = 0;
		$error_description[$i][4] = -1;
		$error_description[$i][5] = '';
		$error_description[$i][6] = '';
		$error_description[$i][7] = 0;
		$error_description[$i][8] = 0;
		$error_description[$i][9] = '';
		$error_description[$i][10] = '';
	
	}
	my $output_number = $number_of_error_description -1;
	#print $output_number .' error description in script'."\n" ;
	

}


sub output_errors_desc_in_db{
	if ($parameter{'modus_scan'}  eq 'live'
		and $parameter{'modus_nodb'}  ne 'nodb') {
		print 'insert new and update old description in the database'."\n";
		#print $parameter{'modus_nodb'} ."\n";

	# mysql> desc pd_error_desc;
	# +----------+---------------+------+-----+---------+-------+
	# | Field    | Type          | Null | Key | Default | Extra |
	# +----------+---------------+------+-----+---------+-------+
	# | error_id | int(8)        | YES  |     | NULL    |       |
	# | prio     | int(8)        | YES  |     | NULL    |       |
	# | name     | varchar(4000) | YES  |     | NULL    |       |
	# | notice   | varchar(4000) | YES  |     | NULL    |       |
	# +----------+---------------+------+-----+---------+-------+


	


		for (my $i = 1; $i < $number_of_error_description; $i++) {
			my $sql_headline = $error_description[$i][0];
			$sql_headline =~ s/'/\\'/g;
			my $sql_desc = $error_description[$i][1];
			$sql_desc =~ s/'/\\'/g;
			$sql_desc = substr( $sql_desc, 0, 3999);				# max 4000
			
			# insert or update error
			my $sql_text2 = "update pd_error_desc 
			set prio=".$error_description[$i][2].", 
			name='".$sql_headline."' ,
			notice='".$sql_desc."'
			where error_id = ". $i." 
			;";
			#print $sql_text2."\n" ;
			my $sth = $dbh->prepare( $sql_text2 );
			my $x = $sth->execute;
			if ( $x eq '1')  {
				#print 'Update '.$x.' rows'."\n";
			} else {
				print 'new error - description insert into db'."\n";
				$sql_text2 = "insert into pd_error_desc (error_id, prio, name, notice) 
							values ( ". $i.", ".$error_description[$i][3].", '".$sql_headline."' ,'".$sql_desc."');";	
				print $sql_text2."\n"; 
				$sth = $dbh->prepare( $sql_text2 );
				$sth->execute;
			}
		}
	}
}


###############################################################################################
# Subroutines Loading

sub load_first_name{
	#first_name
	print 'Load data:'."\n";

	# read all names from table firstname in database
	# TODO: error_handling - table don't exist --> don't die
	my $sth = $dbh->prepare("SELECT firstname FROM firstname")  or die ($dbh->errstr);
	$sth->execute or die $sth->errstr;
	while (my ($firstname) = $sth->fetchrow_array()) {
	   push(@first_name,  $firstname);
	   
	}
	$vorname_anzahl = @first_name;
	$vorname_anzahl = $vorname_anzahl -1;
	printf "\t%-25s %10d\n", 'load firstname', scalar @first_name."\n";
	
}



sub load_family_name{	
	# read all names from table lastname in database
	my $sth = $dbh->prepare("SELECT lastname FROM lastname") or die ($dbh->errstr);
	$sth->execute or die $sth->errstr;
	while (my ($firstname) = $sth->fetchrow_array()) {
	   push(@family_name,  $firstname);
	}

	
	$familiennamen_anzahl = @family_name;
	$familiennamen_anzahl = $familiennamen_anzahl -1;
	printf "\t%-25s %10d\n", 'load lastname', scalar @family_name."\n";
}



sub load_category_for_person{
	# category for person
	$all_category_for_person = '';
	
	# read all names from table lastname in database
	my $sth = $dbh->prepare("SELECT categorie FROM cat_person") or die ($dbh->errstr);
	$sth->execute or die $sth->errstr;
	while (my ($catname) = $sth->fetchrow_array()) {
	   push(@cat_person,  $catname);
	   $all_category_for_person = $all_category_for_person.lc($catname)."\n";
	}	
	$all_category_for_person =~ s/^\n//;
	printf "\t%-25s %10d\n", 'load cat_person', scalar @cat_person."\n";	
}	



sub load_category_for_no_person{
	# category for no person
	
	# read all names from table lastname in database
	my $sth = $dbh->prepare("SELECT categorie FROM cat_no_person") or die ($dbh->errstr);
	$sth->execute or die $sth->errstr;
	while (my ($catname) = $sth->fetchrow_array()) {
	   push(@cat_no_person,  $catname);
	}	
	printf "\t%-25s %10d\n", 'load cat no person', scalar @cat_no_person."\n";	

}



sub print_end_time{
	my $time_end = time();		# start timer in secound
	my $second_duration = $time_end - $time_start;
	my $minute = int(($second_duration/60)*100)/100;
	print 'Durration '.$second_duration.' seconds or '.$minute.' minutes'."\n";
}

###############################################################################################



sub output_input_for_live{	
#################################################################
#  write Error-file
#################################################################

# Artikel soll nur einmal im OUTPUT auftauchen
print 'Write output files'."\n";

	### Artikeldoppelungen eliminieren
	my @output; 	
	my $output_anzahl = -1;
	for (my $i = 0; $i <= $fehler_anzahl; $i++) {
		print_article_title_every_x($language, '', $i, 5000) ;
		my $insert = 1;
		for (my $j = 0; $j <= $output_anzahl; $j++) {
			if ($fehlerarticle[$i][1] eq $output[$j][0]) {
				# Artikel schon vorhanden
				$output[$j][1] = $output[$j][1].', '.$fehlerarticle[$i][0];
				$insert = 0;
				#print "Doppel ".$fehlerarticle[$i][0]."\n";
			}
		} 
		if ($insert == 1) {
			#neuen Fehler einfügen
			#print $output_number."\n";
			$output_anzahl = $output_anzahl +1;
			$output[$output_anzahl][0] = $fehlerarticle[$i][1];
			$output[$output_anzahl][1] = $fehlerarticle[$i][0];
			#die;
		}
	}
	
	print 'Sort Output'."\n";
	my @ausgabe;
	my @ausgabe_sort;	
	for (my $j = 0; $j <= $output_anzahl; $j++) {
		$ausgabe[$j] = $output[$j][0]."\t".$output[$j][1];
	}
	@ausgabe_sort = sort(@ausgabe);
	
	# Ausgabe der sortierten Artikelliste für nächsten Scan # TODO in DB
#	print "Write output for next scan\n";
#	$parameter{'file_page_to_scan'}= 'output_only.txt' if ($parameter{'modus_scan'}  eq 'only');
#	open(OUTPUT, ">$parameter{'file_page_to_scan'}");
#	for (my $j = 0; $j <= $output_anzahl; $j++) {
#		print OUTPUT "\n" if ($j >0);
#		print OUTPUT $ausgabe_sort[$j];
#	}	
#	close(OUTPUT);
	
	#######################################
	# Finde alle auftretenden Fehler
	my @list_of_errors;
	for (my $i = 0; $i <= $fehler_anzahl; $i++) {
		my $test_counter = @list_of_errors;
		#$test_counter = $test_counter -1;
		#print 'Test_counter:'.$test_counter."\n";
		my $found_error = 0;
		if ($test_counter > 0 ) {
			for (my $j = 0; $j < $test_counter; $j++) {
				if ($list_of_errors[$j] == $fehlerarticle[$i][0]) {
					$found_error = 1;
				}
			}
		}
		push(@list_of_errors, $fehlerarticle[$i][0] ) if ( $found_error == 0);
		#foreach(@list_of_errors) {
		#	print "$_ ";
		#}
		#print "\n##########\n";
	}
	#die;
	
	
	#############################################
	# sort error_list by headline of error

	my @sort_helper;
	my $number_of_different_errors = @list_of_errors;
	print "Number of different errors: ".$number_of_different_errors."\n";
	$number_of_different_errors = $number_of_different_errors -1;

	for (my $i = 0; $i <=$number_of_different_errors; $i++) {
			$sort_helper[$i] = $error_description[$list_of_errors[$i]][0]."\t".$list_of_errors[$i];
			#print $sort_helper[$i]."\n";
	}

	my @sort_list = sort(@sort_helper);
	foreach (@sort_list){
		#print $_."\n";
		$_ = substr($_ , index($_,"\t")+1);
		#print $_."\n";
	}
	
	@list_of_errors = @sort_list;
	
	#Ausgabe der sortierten Fehlerliste
	foreach(@list_of_errors) {
		print "$_ ";
	}
	
	#die;
	#######################################
	
	
	if ($parameter{'modus_scan'}  eq 'live') {
		#Ausgabe des WikiTextest
		print "Write output for Wikipedia\n";
		

		my $html_head = '';
		$html_head = $html_head.'<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">'."\n";
		$html_head = $html_head.'<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en" dir="ltr">'."\n";
		$html_head = $html_head.'<head>'."\n";
		$html_head = $html_head.'<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />'."\n";
		$html_head = $html_head.'</head>'."\n";
		$html_head = $html_head.'<body>'."\n";
		$html_head = $html_head.'<pre> <code>'."\n";
		
		my $output_text = '';
		my $starttext = '';
		$starttext = $starttext ."{{/Einleitungstext}}"."\n";
#		$starttext = $starttext ."Die hier in der '''Fehlerliste''' eingetragenen Artikel wurden von Wikipedianern mit Personendaten versehen, dabei haben sich aber eventuell unterschiedliche Fehler eingeschlichen.";
#		$starttext = $starttext ."Die hier aufgelisteten Artikel wurden vollautomatisch zusammengestellt. Dafür wurden alle Artikeln, in denen die Personendaten eingebaut sind, gefiltert und analysiert. Aufgrund der automatischen Zusammenstellung kann es zu Fehlzuordnungen kommen, sodass jeder eingetragene Artikel individuell geprüft werden muss, um Verschlimmbesserungen bei der Abarbeitung der Listen zu vermeiden."."\n\n";
#		$starttext = $starttext ."Es sind durchaus auch Mehrfachzuordnungen möglich. Wenn also ein Personendatenfeld mehrere Fehler besitzt, wird es auch mehrmals in den entsprechenden Rubriken aufgelistet."."\n\n";		
		
#		$starttext = $starttext ."*'''Was ist zu tun:'''"."\n";
#		$starttext = $starttext ."**Alle 24 Stunden läuft das Skript einmal auf dem Toolserver. Das Ergebnis kann von [http://toolserver.org/~sk/pd/de_output_for_wikipedia.html hier] auf diese Wikipedia-Seite kopiert werden."."\n" ;
#		$starttext = $starttext ."**In jedem dieser Artikeln sollten die Personendaten korrigiert und verbessert werden."."\n" ;
#		$starttext = $starttext ."**Abgearbeitete Artikel können aus der Liste entfernt werden. Ansonsten erfolgt die Entfernung bei der nächsten vollautomatischen Überarbeitung."."\n";
#		$starttext = $starttext ."**Abgearbeitete Rubriken sollten für die Übersichtlichkeit komplett entfernt werden."."\n";
#		$starttext = $starttext ."*'''Was solltet ihr nicht tun:'''"."\n";
#		$starttext = $starttext ."**Artikel hinzufügen, da bei der nächsten vollautomatischen Aktualisierung diese Artikel verschwinden. "."\n\n";
#		$starttext = $starttext ."*Weitere Infos:'''"."\n";
#		$starttext = $starttext ."**[[Wikipedia:Personendaten]]"."\n";
		#$starttext = $starttext ."**[[Wikipedia:Personendaten/Wartung/Fehlerliste3]] "."\n";
#		$starttext = $starttext ."**Aktualisierungswünsche, Kommentar, Hinweise, Verbesserungsvorschläge hier auf die Diskussionseite"."\n";
#		$starttext = $starttext ."**Skript-Code erhältlich bei: [[Benutzer:Stefan Kühn|sk]] "."\n\n\n";

		#$starttext = $starttext ."* [[Wikipedia:Personendaten/Wartung/Fehlerliste2]] "."\n";
		$output_text = $output_text. $starttext;
		
		

		##Statistik
		my $statistik = '';
		use CGI::Carp qw(fatalsToBrowser);

		my $fehler_ausgabe = $fehler_anzahl +1;
		$statistik = "*Statistik: "."\n";
		$statistik = $statistik ."** Der letzte Suchlauf lief am '''".$akMonatstag.".".$akMonat.".".$akJahr."''' um '''".$akStunden.":".$akMinuten." Uhr''' (GMT, Toolserverzeit)."."\n";
		$statistik = $statistik ."** Es wurden dabei zusätzlich '''".$new_article_for_output. "''' neue Artikel gescannt.\n"; 
		$statistik = $statistik ."** Weiterhin wurden '''".$change_article_for_output. "''' Artikel aus der Liste der letzten Änderungen gescannt.\n"; 
		$statistik = $statistik ."** Beim letzten vollständigen Scan aller Artikel wurde der Dump vom '''".$dump_date_for_output. "''' genutzt.\n"; 
		$statistik = $statistik ."** Aktuell sind '''".$fehler_ausgabe."''' Verbesserungsvorschläge in '''". $output_anzahl."''' Artikeln bekannt."."\n";
		$statistik = $statistik .'*<span style="background-color:yellow;">Neu: [http://toolserver.org/~sk/cgi-bin/pd/pd.cgi Dynamische Liste] </span>'."\n";
		$statistik = $statistik ."\n";
		$output_text = $output_text. $statistik;
		
		my $ueberschrift = '';
		my $beschreibung = '';
		
		print "Build wiki-structure\n";
		for (my $k = 1; $k <= 3; $k ++) {	
			
			$output_text = $output_text. "\n".'== Hohe Priorität =='."\n\n"  if ($k == 1);
			$output_text = $output_text. "\n".'== Mittlere Priorität =='."\n\n"  if ($k == 2);
			$output_text = $output_text. "\n".'== Niedrige Priorität =='."\n\n"  if ($k == 3); 
			
			
			#for (my $j = 0; $j < 2000; $j++) {
			foreach(@list_of_errors) {
				my $j = $_;
				
				if ($error_description[$j][0] and $error_description[$j][2] == $k) {
					#Nur wenn Fehler vorhanden
					print 'Error '.$j." - "; 
					my $titel = '';
					my $fehlerfeld = '';
					
					
					$output_text = $output_text. "\n".'=== '.$error_description[$j][0].' ==='."\n";
					$output_text = $output_text. '<!-- Error-Nr. '. $j .'-->'."\n";
					$output_text = $output_text. $error_description[ $j ][1]."\n\n";
					
					
					
					my $tabelle = "true";
					$tabelle = "false" if ($j eq '1');
					$tabelle = "false" if ($j eq '2');
					$tabelle = "false" if ($j eq '6');
					$tabelle = "false" if ($j eq '7');
					$tabelle = "false" if ($j eq '100');
					$tabelle = "false" if ($j eq '104');
					$tabelle = "false" if ($j eq '111');
					$tabelle = "false" if ($j eq '301');
					$tabelle = "false" if ($j eq '302');
					$tabelle = "false" if ($j eq '505');
					$tabelle = "false" if ($j eq '700');
					$tabelle = "false" if ($j eq '900');
					$tabelle = "false" if ($j eq '991');
					$tabelle = "false" if ($j eq '992');
					$tabelle = "false" if ($j eq '993');
					$tabelle = "false" if ($j eq '997');
					$tabelle = "false" if ($j eq '985');
					$tabelle = "false" if ($j eq '986');
					$tabelle = "false" if ($j eq '1011');
					$tabelle = "false" if ($j eq '1034');
					$tabelle = "false" if ($j eq '1048');
					
					
				
					
					my $error_counter = 0;
					# Fehleranzahl ermitteln
					for (my $i = 0; $i <= $fehler_anzahl; $i++) {
						#durchsuche alle Fehler
						if ($j eq $fehlerarticle[$i][0]) {
							$error_counter = $error_counter + 1;
						}
					}
				
				
					
					# Auswahl der X.ten Artikel 
					my $schrittweise = 1;
					my $max_error_count = 100;	# maximale Anzahl der angezeigten Artikel
					$schrittweise = int($error_counter/ $max_error_count) if ($error_counter > $max_error_count);
					my $schrittweise_1 = $schrittweise;
					my $schrittweise_2 = $schrittweise + 1;
					my $current_schrittweise = $schrittweise_1;
					
					my $error_counter_2 = 0;
					my $error_counter_3 = 100;
					for (my $i = 0; $i <= $fehler_anzahl; $i++) {
						#durchsuche alle Fehler
						if ($j eq $fehlerarticle[$i][0]) {
							$error_counter_2 = $error_counter_2 +1;
							if ($error_counter_2 ==  $current_schrittweise) {	#muss hier rein   
								$error_counter_2 = 0;
								$error_counter_3 = $error_counter_3 - 1; # Hochzählen bis 200
								
								#my $edit_weblink = $fehlerarticle[$i][1];
								#$edit_weblink =~ s/ /_/g;
								#$edit_weblink = '[http://de.wikipedia.org/w/index.php?title='.$edit_weblink.'&redirect=no&action=edit&redlink=1 Edit]';
								
								if ( $tabelle eq "false"	) {
									#zusammenfassen
									$fehlerfeld =~ s/\n//;
									$fehlerfeld = $fehlerfeld." - [[".$fehlerarticle[$i][1]."]] "."\n";		# .$edit_weblink
									$fehlerfeld =~ s/^ - //;
									
								} else {
									#einzelne Zeilen
									$fehlerfeld = $fehlerfeld.'| [['. $fehlerarticle[$i][1]."]] ";
									$fehlerfeld = $fehlerfeld."|| ".$fehlerarticle[$i][2];
									#$fehlerfeld = $fehlerfeld."|| ".$edit_weblink;
									$fehlerfeld = $fehlerfeld."\n|-\n";
									
									

									
								}
							}
							if ( (($max_error_count - $error_counter_3) * $schrittweise_1) + $error_counter_3 * $schrittweise_2 < $error_counter) {
								# wegen Rundungsproblematik auf höhere Schrittweite gehen, sobald Produkt kleiner als die Fehlerzahl ist
								$current_schrittweise = $schrittweise_2;
								#print $error_counter_3."\n";
								#print $schrittweise_1."\n";
								#print $schrittweise_2."\n";
							}
						}
					}
							

					
					if ($tabelle eq 'true') {
						$fehlerfeld =~ s/\|-\n$//;
						$fehlerfeld = '{| class="wikitable sortable"'."\n".'! Artikel '."\n".'! Hinweis '."\n".'|-'."\n".$fehlerfeld.'|}';
					}
					
					$output_text = $output_text. "Dieser Fehler wurde '''". $error_counter ."''' mal gefunden.";
					$output_text = $output_text. ' Die Ausgabe wurde aber auf '.$max_error_count.' Artikel begrenzt.' if ($error_counter > $max_error_count);
					$output_text = $output_text. "\n\n";
					
					$output_text = $output_text. $fehlerfeld;
				}
			}	
		}
		my $html_foot = '';
		$html_foot = $html_foot.'<pre> <code>'."\n";
		$html_foot = $html_foot.'</body></html>'."\n";
	
		my $file = 	$parameter{'path_output'}.'/'.$parameter{'file_error_wiki'};	
		open(OUTPUT, ">$file");
		print OUTPUT $html_head;
		my $output_wikipedia_text_html = $output_text;
		$output_wikipedia_text_html =~ s/</&lt;/g;
		$output_wikipedia_text_html =~ s/>/&gt;/g;
		print OUTPUT $output_wikipedia_text_html;
		print OUTPUT $html_foot;
		close (OUTPUT);
	}

}






sub output_new_first_names{
	###################
	# output new found firstnames

	print 'Output neue Vornamen'."\n";
	open(OUTPUT, ">>$output_new_first_names_filename");

	close (OUTPUT);
}

##########################################################################
# Load titles from different sources
##########################################################################

sub	load_article_for_live_scan{
	if ($parameter{'modus_scan'}  eq 'live') {
		print 'load title to scan'."\n";

		my $format = "\t%-25s %10d\n";

		# load articles from textfile
		if ($parameter{'scan_file'} eq 'yes') {
			# article form file "input_for_live.txt", only for additonal scans
			my $count = load_from_textfile();
			printf $format, 'from file', $count ."\n";
		}
		

		# load articles from table page_to_scan
		if ($parameter{'scan_table'} eq 'yes') {
			my $count = load_from_table_page_to_scan();
			printf $format, 'from table page_to_scan', $count ."\n";
		}


		# load articles from table changes
		if ($parameter{'scan_change'} eq 'yes') {
			my $count = load_from_table_changes();
			printf $format, 'from table change', $count ."\n";
		}

		# load_from_table_pd_with_error();
		if ($parameter{'scan_error'} eq 'yes') {
			my $count = load_from_table_pd_with_error();
			printf $format, 'from table pd with error', $count ."\n";
		}



		# TODO Load from table pd all articles
#		load_from_db_article_new();		# new_articles check for first name and family name


		printf $format, 'Summe', scalar @live_article ."\n";



		# delete in input list all double titles
		delete_all_double_input_articles();
		printf $format, 'Summe ohne Doppeleinträge', scalar @live_article ."\n";
		
		
		#print '###############################'."\n";		
		#foreach( @live_article[80..96]) {print $_;};
		#print '###############################'."\n";
		#die;
	}
}



#################################################################
# Loop for all articles
#################################################################
sub scan_pages{
	#scan article text
	print 'begin scan of articles'."\n";

	do {
		$article_number	++ ;
		
		my @article; 
		if ($parameter{'modus_scan'}  eq 'dump' or $parameter{'modus_scan'}  eq 'only') {
			@article = scan_dump_for_next_article();
		} else {
			@article = scan_live_for_next_article();
		}
		
		if ($end_of_dump == 0 and $end_of_live == 0) {
			# if an article was found
			$title 			=  $article[0];
			$page_id		=  $article[1];
			$revision_id 	=  $article[2];
			$revision_time  =  $article[3];
			$text 			=  $article[4];

			$name 				= '';
			$aname		 		= '';
			$kurz		 		= '';
			$kurz_plain     	= '';
			$geburtsdatum 		= '';
			$geburtsdatum_plain = '';
			$geburtsjahr 		= '';
			$geburtsmonat		= '';
			$geburtstag 		= '';
			$geburtsort			= '';
			$geburtsort_plain	= '';
			$sterbedatum		= '';
			$sterbedatum_plain	= '';
			$sterbejahr			= '';
			$sterbemonat		= '';
			$sterbetag			= '';
			$sterbeort			= '';
			$sterbeort_plain	= '';		

			$all_categories 	= '';
			$all_interwikis 	= '';	
			$is_redirect 		= 0;
			$this_article_is_with_errors = 0;
			$this_article_all_errors = '';
			$has_defaultsort 	= 0;
			$defaultsort 		= '';

			if ( 
				index ($title, 'Benutzer:') == -1 and
				index ($title, 'Bild:') == -1 and
				index ($title, 'Datei:') == -1 and
				index ($title, 'Diskussion:') == -1 and
				index ($title, 'File:') == -1 and
				index ($title, 'Hilfe:') == -1 and
				index ($title, 'Kategorie:') == -1 and
				index ($title, 'MediaWiki:') == -1 and
				index ($title, 'Mediawiki:') == -1 and
				index ($title, 'Portal:') == -1 and
				index ($title, 'Vorlage:') == -1 and
				index ($title, 'Wikipedia:') == -1 and	
				index ($title, 'Nekrolog') == -1
				) {
				
				check_article();				#Main check routine
				
				
			}
		}
		
		# if limit on dump scan set		
		if ($parameter{'dump_limit'} and $parameter{'modus_scan'}  eq 'dump' ){
			$end_of_dump = 1 if ($article_number > $parameter{'dump_limit'});
		}		

		#$end_of_dump = 1 if ($page_id >13000); 
		#$end_of_live = 1 if ($current_live_article >120); 
		#$end_of_live = 1 if ($current_live_article > $parameter{'limit_page_to_scan'}); 
	

	}
	until ($end_of_dump == 1 or $end_of_live == 1);
}


###############################################################################################

sub check_article{


	# Example (only for testing)
	# $text = '{{Personendaten .... ';


	if ($parameter{'modus_scan'}  eq 'live') {
		if ($parameter{'modus_silent'}  ne 'silent') {
			print $article_number, ' ', scalar @live_article,' ', $title."\n"; #$page_id
		} else {
			print_article_title_every_x($language, $title, $article_number, 5000) ;
		}
	} else {
		my $counter = 5000;
		$counter = $parameter{'dump_counter'} if ($parameter{'dump_counter'});
		print_article_title_every_x($language, $title, $article_number, $counter ) ;
	}
	
	$has_pd = 0;
	
	$is_a_person = 0;
	$is_a_noperson = 0;


# is redirect
	if (   index(lc($text), '#redirect') > -1
		or index(lc($text), '#weiterleitung') > -1)	 {
		$is_redirect = 1;
	}

###################################################
# get categories
###################################################
	$all_categories = '';	
	
	my @kategorien = scan_article_for_categorys(0, $title, "","",$text, "Kategorie","de");
	my $anzahl_kat = @kategorien;
	$all_categories = '';
	if ($anzahl_kat >0 ){
		for (my $i = 0; $i < $anzahl_kat; $i++) {
			$all_categories = $all_categories. "\t". $kategorien[$i];
		}
	}
	$all_categories =~ s/^\t//g;
	
	
# get interwikilinks
	$all_interwikis = '';
	$all_interwikis = get_interwikis();
	

#get defaultsort
	get_defaultsort();
	
		
# has persondata
	if ( index( lc($text), '{{personendaten') > -1) {
		$is_a_person = 1;
		$has_pd = 1;
		if (index($text, '{{Personendaten') == -1) {
			F002_Personendaten_fehlen('check','yes');
		}
	
		get_pd();
	}

	if ( $is_a_person == 0 and 
		 index(lc($text), 'personendaten') > -1 and  
		 index(lc($text), 'kurzbeschreibung') > -1) {
		 F003_Personendaten_fehlerhaft('check', 'yes');
	}	
	
	
	if 	( $has_pd == 1 ) {
	
		#if (substr($title,1,1) eq 'A') {
			
			if 	($parameter{'modus_scan'}  ne 'only') {
			# Nur zum auskommentieren
				
				error_list('check');
				
			}
			
		if 	($parameter{'modus_scan'}  eq 'only') {
			#F982_Sterbezeichen_im_Text();
				F1103_Zeitlos_mit_Jahrhundert('check');
				F1104_Zeitlos_mit_Jahr('check');
		}
		#}
	}


	
	



	
	if 	( $has_pd == 1 and $parameter{'modus_scan'}  ne 'only') {
		#######################
		# alle Personenkategorien in Tabelle maybe_cat_person speichern
		# später damit filtern und suchen nach neuen Kategorien #TODO filter.cgi erstellen
		my $number_of_cat = @kategorien;
		$number_of_cat = $number_of_cat -1;
		my $sql_cat = '';
		for (my $i = 0; $i <= $number_of_cat; $i ++) {
				#print $kategorien[$i]."\n";
				my $pos1 = index($kategorien[$i], ':');
				my $pos2 = index($kategorien[$i], '|');
				my $pos3 = index($kategorien[$i], ']');
				$pos2 = $pos3 if ($pos2 == -1);
				if ($pos1 > -1 and $pos2 >-1) {
					# cat ok
					my $test = substr($kategorien[$i], $pos1+1, $pos2 - $pos1 -1);
					$test =~ s/^ //g;
					$test =~ s/ $//g;
					$test =~ s/  / /g;
					$test =~ s/_/ /g;
					#print "\t\t".$test."\n"; # print categries for this article

					$test =~ s/'/\\'/g;  # for Insert into MySQL
					if ($sql_cat eq '') {
						$sql_cat = "('".$test."')";
					} else {
						$sql_cat .=",('".$test."')";
					}
				}
		}
		
		#print scalar @kategorien."\n";
		if (scalar @kategorien > 0) {
			# there is minimum one category
			# insert into db all categories as maybe_pd
			my $sql_text = "INSERT ignore INTO maybe_cat_person (cat) VALUES $sql_cat;";
			#print $sql_text."\n";
			my $sth = $dbh->do( $sql_text );
		}

		
		####################
		# alle Vornamen suchen #TODO in DB
		if (index ($title, ' ') > -1) {
			# Artikel mit PD hat Leerzeichen im Titel -> vermutlich Vorname enthalten
	
			my @split_new_first = split (/\n/, $title);
			foreach ( @split_new_first) {
				my $current_name = $_;
				#print $current_name. "\n";
		
				if (index( $current_name, ' (') > -1) {
					# Emil Mustermann (Schriftsteller)
					$current_name = substr( $current_name, 0, index ($current_name, ' ('));
				}
		
				# römische Zahlen und Abkürzungen entfernen
				# Müller III.
				# A. F. Müller
				$current_name =~ s/[A-Z]+\./\./g ;
				#print "\t".$current_name. "\n";
				$current_name =~ s/ \.//g ;
				#print "\t".$current_name. "\n";
				$current_name =~ s/^\. //g ;
				#print "\t".$current_name. "\n";
		

				#nur ein Wort (oft nur der Nachname oder Künstlername, dann alles Weg
				# Hadrian - Hadrian (Kaiser)
				# Tolkien - J. R. R. Tolkien
				if ( index ($current_name, ' ') == -1){
					#printf "\t\t\t%-30s %-30s\n",$current_name, $title 
					$current_name = '';
				}
				#print "\t".$current_name. "\n";

				# entferne Komma und dahinter (von und zu)
				# Arthur Wellesley, 1. Duke
				$current_name =~ s/,.*$//g ;

				# entferne Namenszusätze (von und zu)
				# Jakob der Ältere
				$current_name =~ s/ (aus|bin|da|de|der|di|du|duc|of|van|von|ze|zer|zu|Al|El|De|Le).*$//g ;

				# entferne Bindestrich
				# Pierre-Simon 
				$current_name =~ s/-.*$//g ;				

				# nur Großbuchstaben #TODO		
				# DJ


				#Wenn doch Nachnamen vorhanden, dann abschneiden
				$current_name =~ s/ [^ ]+$// ;
				#print "\t".$current_name. "\n";
		
		
				if ($current_name ne '') {
					#printf "\t\t\t%-30s %-30s\n",$current_name, $title;

					# Vornamen zerlegenen (Anna Maria Otto)					
					my @split_name;
					if (index($current_name, ' ')>1) {
						# mehrere Vornamen übrig
					 	@split_name=split(/ /,$current_name);
					} else {
						# nur ein Vorname übrig
						push(@split_name , $current_name);
					}

					foreach my $firstname (@split_name){
						
						# Einzelbuchstaben-Vornamen vermeiden
						if (length($firstname)>=2) {
							$firstname =~ s/'/\\'/g;  # for Insert into MySQL no '
							# insert into db all categories as maybe_pd
							my $sql_text = "INSERT ignore INTO maybe_firstname (firstname) VALUES ('".$firstname."');";
							my $sth = $dbh->do( $sql_text );
						}
					}
				}	
			}


		}
		
		####################
		# nach neuen Nachnamen suchen	#TODO in DB	
		# die;
	}
	
	if 	( $has_pd == 1 and $parameter{'modus_scan'}  eq 'dump'){
		######################
		# Exportieren der Personendaten in FILE	#TODO als Webseite
#		export_pd();
	}
	
	#Save PD in database
	if 	( $has_pd == 1) {
		save_pd_in_database();
	}


# is not a persone !!!
	if 	( $is_a_person == 0 ) {
		my $low_cat = lc($all_categories);
		foreach (@cat_no_person) {
			$is_a_noperson = 1 if (index ( $low_cat , lc( $_ )) > -1 );
			#print 'Noperson: '.$title."\n" if ($is_a_noperson == 1);
		}
	}	 
		 
# is article about a person 
	if ($has_pd == 0 and $is_a_noperson == 0 ) {
		if ( index($all_categories, 'Kategorie:Mann|')>-1
		  or index($all_categories, 'Kategorie:Mann]')>-1
		  or index($all_categories, 'Kategorie:Frau|')>-1
		  or index($all_categories, 'Kategorie:Frau]')>-1
		  or index($all_categories, 'Kategorie:Intersexueller|')>-1
		  or index($all_categories, 'Kategorie:Intersexueller]')>-1
		  or index($all_categories, 'Kategorie:Geschlecht unbekannt|')>-1
		  or index($all_categories, 'Kategorie:Geschlecht unbekannt]')>-1
		  or index($all_categories, 'Kategorie:Geboren')>-1
		  or index($all_categories, 'Kategorie:Gestorben')>-1
		  
		  or index($text, 'Infobox Badmintonspieler')>-1
		  or index($text, 'Infobox Beachvolleyballspieler')>-1
		  or index($text, 'Infobox Boxer')>-1
		  or index($text, 'Infobox DTM- und Formel-1-Fahrer')>-1
		  or index($text, 'Infobox DTM-Fahrer')>-1
		  or index($text, 'Infobox Feldhockeyspieler')>-1
		  or index($text, 'Infobox Formel-1-Fahrer')>-1
		  or index($text, 'Infobox Fußballspieler')>-1
		  or index($text, 'Infobox Golfer')>-1
		  or index($text, 'Infobox Handballer')>-1
		  or index($text, 'Infobox Kaiser von China')>-1
		  or index($text, 'Infobox NASCAR-Rennfahrer')>-1
		  or index($text, 'Infobox NFL-Spieler')>-1
		  or index($text, 'Infobox Nguyễn Dynastie')>-1
		  or index($text, 'Infobox Pharao')>-1
		  or index($text, '{Infobox Pokerprofi')>-1
		  or index($text, 'Infobox Poolbillardspieler')>-1
		  or index($text, 'Infobox Raumfahrer')>-1
		  or index($text, 'Infobox Rugbyspieler')>-1
		  or index($text, 'Infobox Schachspieler')>-1
		  or index($text, 'Infobox Snookerspieler')>-1
		  or index($text, 'Infobox Sumōringer')>-1
		  or index($text, 'Infobox Tennisspieler')>-1
		  or index($text, 'Infobox Volleyballspieler')>-1
		  or index($text, 'Infobox Wrestler')>-1
		  or index($text, '{{MMAKämpfer')>-1  
		  
		) {
			$is_a_person = 1;
			F002_Personendaten_fehlen('check','yes');
			#fehlermeldung(2, $title, 'has no PD');
		}		
	}

	
# maybe person, search for pdcat	
	if 	( #$is_redirect == 0 and
		  $is_a_person == 0 and 
		  $is_a_noperson == 0 and
		  $has_pd == 0 and
		  index ($title, 'Liste') == -1
		  ) {
		# test of pd_categories, when no person
		my $number_of_cat = @kategorien;
		$number_of_cat = $number_of_cat -1;
		for (my $i = 0; $i <= $number_of_cat; $i ++) {
			my $pos1 = index($kategorien[$i], ':');
			my $pos2 = index($kategorien[$i], '|');
			my $pos3 = index($kategorien[$i], ']');
			$pos2 = $pos3 if ($pos2 == -1);
			if ($pos1 > -1 and $pos2 >-1) {
				my $test = substr($kategorien[$i], $pos1+1, $pos2 - $pos1 -1);
				$test =~ s/^ //g;
				$test =~ s/ $//g;	
				if ( index($all_category_for_person, "\n".lc($test)."\n") > -1) {
						if ($title ne $test  							# Filmregisseur
							#and index($kategorien[$i], '|!]') == -1		# 
							and index($kategorien[$i], '|!') == -1		# Weinkönigen 
							and index($kategorien[$i], '| ') == -1
							) {	
							F004_article_with_pd_cat('check', $test);
							#print "\t\t".$title,"\t", $test."\n";
							
						}
				}
			}
		}		
		
	}
	

	if ($text ne '') {
		# no category and no pd,  maybe a person
		F008_check_for_first_name('check');
		F009_check_for_last_name('check');
		
		# no category , aufheben für nächsten Scan
		F007_Keine_Kategorie('check');
	}


# End check_article	
}



sub error_list {

	my $attribut = $_[0];	# check / get_description
								
				F001_Personendaten_nicht_komplett($attribut, '');
				F002_Personendaten_fehlen($attribut, '');
				F003_Personendaten_fehlerhaft($attribut, '');
				F004_article_with_pd_cat($attribut, '');
				F005_Parameter_mehrfach($attribut, '');
				F006_Personendaten_fehlen_und_Loeschkandidat($attribut, '');
				F007_Keine_Kategorie($attribut);
				F008_check_for_first_name($attribut);
				F009_check_for_last_name($attribut);
				
				F010_Reihenfolge_fehlerhaft($attribut,'');
				F011_Zuviele_Leerzeichen($attribut);
				F012_Leerzeichen_vor_Komma($attribut);
				F013_Verlinkung_schlecht($attribut);
				F014_Leerzeichen_nach_Komma($attribut);
				F015_Name_mit_eckiger_Klammer($attribut);
				F016_Name_mit_roemischer_Zahl($attribut);
				F017_Name_ist_kuerzer_als_Artikelname($attribut);				# deaktiviert
				F018_Name_hat_Komma_am_Ende($attribut);
				F019_Name_erster_Buchstabe_klein($attribut);					# deaktiviert
				
				F020_Name_ein_Zeichen_kuerzer_als_Title($attribut);				# deaktiviert
				F021_Name_Leerzeichen_nach_Punkt($attribut);
				F022_Name_mit_akademischem_Grad($attribut);
				F023_Name_ohne_Umlaut($attribut);
				F024_Name_Leerzeichen_nach_Komma($attribut);
				F025_Name_ist_leer($attribut);
				F026_Name_ohne_Leerzeichen($attribut);
				F027_Name_Anfang_mit_zwei_Grossbuchstaben($attribut);
				F028_Name_mit_Text($attribut);
				F029_Name_und_Apostroph($attribut);

				F030_Name_ohne_Komma($attribut);
				F031_Name_vertauscht($attribut);
				F032_Alternativnamen_ohne_Semikolon($attribut);
				F033_Alternativnamen_nur_ein_Zeichen($attribut);
				F034_Alternativnamen_mit_fehlerhaften_Klammern($attribut);
				F035_Alternativnamen_ohne_Leerzeichen_nach_Komma($attribut);
				F036_Alternativnamen_mit_eckiger_Klammer($attribut);
				F037_Alternativnamen_ohne_Leerzeichen_nach_Semikolon($attribut);
				F038_Alternativnamen_identisch_mit_Titel($attribut);
				F039_Alternativnamen_identisch_mit_Name($attribut);
				
				F040_Alternativnamen_mit_Doppelpunkt($attribut);
				F041_Alternativnamen_mit_Slash($attribut);
				F042_Alternativnamen_mit_Pseudonym($attribut);
				F043_Alternativnamen_mit_Hochkomma($attribut);
				F044_Alternativnamen_mit_eckigen_Klammern($attribut);
				F045_Alternativnamen_mit_schlechtem_Anfang($attribut);
				F046_Kurzbeschreibung_schlechter_Anfang($attribut);
				F047_Kurzbeschreibung_mit_Prozentzeichen($attribut);
				F048_Kurzbeschreibung_fehlt($attribut);
				F049_Kurzbeschreibung_falsch_verlinkt($attribut);

				F050_Kurzbeschreibung_falsch_erstes_Zeichen($attribut);
				F051_Kurzbeschreibung_mit_Name($attribut);
				F052_Kurzbeschreibung_mit_fehlerhaften_Klammern($attribut);
				F053_Kurzbeschreibung_mit_Abkuerzungen($attribut);
				F054_Kurzbeschreibung_ist_Datum($attribut);
				F055_Kurzbeschreibung_fehlerhaftes_und($attribut);
				F056_Kurzbeschreibung_kein_Leerzeichen_vor_und($attribut);
				F057_Kurzbeschreibung_gleich_erstes_Wort_vom_Titel($attribut);
				F058_Kurzbeschreibung_mit_Punkt_am_Ende($attribut);
				F059_Kurzbeschreibung_ohne_Leerzeichen_nach_Adjektiv($attribut);
				
				F060_Kurzbeschreibung_ohne_Leerzeichen_nach_Komma($attribut);
				F061_Kurzbeschreibung_ohne_Leerzeichen_nach_Artikel($attribut);
				F062_Kurzbeschreibung_mit_Schraegstrich_zwischen_Nationen($attribut);
				F063_Kurzbeschreibung_nur_ein_kleingeschriebenes_Wort($attribut);
				F064_Kurzbeschreibung_einzelner_kleine_Buchstabe_am_Ende($attribut);
				F065_Kurzbeschreibung_schlechtem_Ende($attribut);
				F066_Kurzbeschreibung_mit_geschweifter_Klammer($attribut);
				F067_Kurzbeschreibung_mit_HTML($attribut);
				F068_Kurzbeschreibung_mit_von($attribut);
				F069_Kurzbeschreibung_mit_Fett($attribut);
				
				F070_Kurzbeschreibung_mit_Artikel_am_Anfang($attribut);
				F071_Kurzbeschreibung_mit_genealogischen_Zeichen($attribut);
				F072_Kurzbeschreibung_kein_Leerzeichen_vor_Klammer($attribut);
				F073_Kurzbeschreibung_beginnt_mit_Jahreszahl($attribut);
				F074_Kurzbeschreibung_Leerzeichen_vor_Komma($attribut);
				F075_Kurzbeschreibung_mit_Professor($attribut);
				F076_Kurzbeschreibung_aus_Land($attribut);
				F077_Kurzbeschreibung_mit_unnoetigem_Wort($attribut);
				F078_Geburtsjahr_in_Zunkunft($attribut);
				F079_Geburtsjahr_contra_Sterbedatum($attribut);
				
				F080_Ort_mit_Sonderzeichen($attribut);
				F081_Ort_mit_eckigen_Klammern($attribut);
				F082_Ort_mit_Geburtsname($attribut);
				F083_Ort_mit_fehlerhaften_Klammern($attribut);
				F084_Ort_falsch_verlinkt($attribut);
				F085_Ort_ohne_Leerzeichen_nach_Komma($attribut);
				F086_Ort_falschem_Anfang($attribut);
				F087_Ort_falschem_Ende($attribut);
				F088_Ort_erstes_Wort_nicht_im_Text($attribut);
				F089_Sterbejahr_in_Zunkunft($attribut);

				F090_Sterbejahr_fehlt_viele_Jahre($attribut);
				F091_Sterbeort_ebenda($attribut);
				F092_Datum_ohne_Zahl($attribut);
				F093_Datum_mit_Sonderzeichen($attribut);
				F094_Datum_nicht_korrekt_angegeben($attribut);
				F095_Datum_nur_einer_eckigen_Klammer($attribut);
				F096_Datum_Alter_zu_hoch($attribut);
				F097_Datum_Punkt_nach_Tag_fehlt($attribut);
				F098_Datum_nach_Punkt_fehlt_Leerzeichen($attribut);
				F099_Datum_Punkt_nach_Buchstabe_vor_Zahl($attribut);

				F100_Datum_mit_Punkt_und_falschem_Format($attribut);
				F101_Datum_mit_Punkt_und_Zahl_in_falschem_Format($attribut);
				F102_Datum_auf_Zahl_folgt_Leerzeichen_und_Buchstabe($attribut);
				F103_Datum_ohne_zwischen($attribut);
				F104_Datum_mit_leeren_Klammern($attribut);
				F105_Datum_mit_Punkt_am_Anfang($attribut);
				F106_Datum_mit_ueberfluessigem_Wort($attribut);
				F107_Datum_mit_fehlerhaften_Klammern($attribut);
				F108_Datum_mit_mehr_als_31_Tagen($attribut);
				F109_Datum_Alter_zu_niedrig($attribut);
				
				F110_Datum_fehlt_aber_Kurzbeschreibung_mit_Jahr($attribut);
				F111_Datum_Geburtsjahr_nicht_vor_Chr($attribut);
				F112_Datum_ohne_Leerzeichen_nach_Komma($attribut);
				F113_Datum_ohne_Leerzeichen_vor_und_nach_Monat($attribut);
				F114_Datum_fehlerhaften_Text($attribut);
				F115_Datum_falsches_Ende($attribut);
				F116_Datum_falsches_zwischen($attribut);
				F117_Datum_ebenda($attribut);
				F118_Datum_mit_Null($attribut);
				F119_Datum_mit_geboren_Jahrhundert($attribut);

				F120_Datum_mit_gestorben_Jahrhundert($attribut);
				F121_Datum_Jahr_nicht_im_Text($attribut);
				F122_Datum_Tag_Monat_nicht_im_Text($attribut);
				F123_Datum_Restfehler($attribut);											#sollte eigentlich nach dem letzten Fehler zu Datum eingebaut werden
				F124_Artikelname_im_Datenfeld($attribut);
				F125_Kategorie_im_Datenfeld($attribut);
				F126_Interwiki_im_Datenfeld($attribut);
				F127_Verlinkung_fehlerhaft($attribut);	
				F128_Kategorie_Mann_Frau_fehlt($attribut);
				F129_Kategorie_geboren_doppelt($attribut);
				
				F130_Kategorie_gestorben_doppelt($attribut);
				F131_Kategorie_geboren_Geburtsjahr_Differenz($attribut);
				F132_Kategorie_gestorben_Sterbejahr_Differenz($attribut);
				F133_Kategorie_geboren_vorhanden($attribut);
				F134_Kategorie_gestorben_vorhanden($attribut);
				F135_Kategorie_geboren_und_gestorben_fehlt($attribut);
				F136_Kategorie_geboren_fehlt($attribut);
				F137_Kategorie_gestorben_fehlt($attribut);
				F138_Geburtsdatum_fehlt($attribut);											# deaktiviert, scheinbar zuviele (später aktivieren)
				F139_Geburtsdatum_im_Text($attribut);
				
				F140_Sterbedatum_im_Text($attribut);
				F141_Sterbezeichen_im_Text($attribut);
				F142_Geburtsort_und_Todesort($attribut);				
				F143_Geburtsort_und_Todesort3($attribut); #double 142
				F144_Komplettes_Datum_im_Text($attribut);
				F145_Komplettes_Datum_im_Text4($attribut); #double 144
				F146_Rechtschreibung($attribut);
				F147_Monat_vor_oder($attribut);
				F148_Tag_vor_oder($attribut);
				F149_kein_zwischen_am_Anfang($attribut);
				
				F150_keine_Jahreszahl_vor_und($attribut);
				F151_Mehrere_Kommas_im_Aname($attribut);
				F152_Tageszahl_ohne_Punkt($attribut);
				F153_Tageszahl_dreistellig($attribut);
				F154_Datum_mit_Komma_vor_oder($attribut);
				F155_Text_ohne_Geburts_oder_Sterbedatum($attribut);
										#old		F155_Zweibuchstabige_Abkuerzungen_in_kurz($attribut);	# deaktiviert da mit 53 zusammengelegt
				F156_Kurzbeschreibung_mit_unnoetigem_Jahrhundert($attribut);
				F157_Kurzbeschreibung_mit_falsch_formatierten_Zeitraeumen($attribut);
				F158_Personendaten_vor_Kategorien($attribut);
				F159_Ort_mit_Slash($attribut);
				
				F160_Datenfeld_mit_fehlerhafter_Klammer($attribut);
				F161_Kurzbeschreibung_Rest_kleingeschrieben($attribut);
				F162_Name_kleingeschrieben($attribut);
				F163_Alternativname_kleingeschrieben($attribut);
				F164_Name_mit_Klammer_vor_Komma($attribut);
				F165_Alternativname_mit_Klammer_vor_Komma($attribut);
				F166_Alternativname_mit_Verlinkung($attribut);
				F167_Alternativname_ohne_Klammer_am_Ende($attribut);
				F168_Name_mit_Namensvariationen($attribut);
				F169_Name_mit_geboren($attribut);
				
				F170_Alternativname_mit_und_oder($attribut);
				F171_Alternativname_ohne_Geburtsname($attribut);
				F172_Datenfeld_mit_Unterstrich($attribut);
				F173_Alternativname_mit_Abkuerzung($attribut);
				F174_Datenfeld_mit_Semikolon_am_Ende($attribut);
				F175_Datenfeld_ohne_Leerzeichen_vor_Klammer($attribut);
				F176_Ort_mit_lokaler_Praeposition($attribut);								# war vor Umstellung nicht eingetragen,überprüfen				
				F177_Personendaten_mit_Minimaleintrag($attribut);
				F178_Personen_ohne_Taetigkeit($attribut);
				F179_Kategorie_mit_Jahrhundert($attribut);
				
				F180_Personendaten_nach_Interwiki($attribut);
				F181_Personendaten_vor_letzter_Ueberschrift($attribut);
				F182_Genealogisches_Zeichen_ohne_Leerzeichen_danach($attribut);
				F183_Genealogisches_Zeichen_mit_Ort_danach($attribut);
				F184_Genealogisches_Zeichen_mit_Zeitraum_danach($attribut);
				F185_Alternativnamen_mit_Semikolon_in_Klammer($attribut);
				F186_Name_versus_Defaultsort($attribut);
				F187_Alternativnamen_ohne_Semikolon_nach_Klammer($attribut);
				F188_Ort_unsicher($attribut);
				F189_Ort_beginnt_mit_Artikel($attribut);
				
				F190_Ort_bei_Ort($attribut);
				F191_Kurzbeschreibung_mit_Nationalitaet_nicht_klein($attribut);
				F192_Name_nicht_im_Text($attribut);
				F193_Kategorie_geboren_fehlt_total($attribut);
				F194_Kategorie_gestorben_fehlt_total($attribut);
				F195_Zeitlos($attribut);
				F196_Kurzbeschreibung_mit_falschem_schweizer($attribut);
				F197_Kurzbeschreibung_mit_falschem_US_amerikanisch($attribut);
				F198_Kurzbeschreibung_mit_Star($attribut);
				F199_Kurzbeschreibung_mit_falschem_Artikel_fuer_Frau($attribut);
				
				F200_Kurzbeschreibung_mit_falschem_Artikel_fuer_Mann($attribut);
				F201_Kurzbeschreibung_mit_zwei_Grossbuchstaben_am_Anfang($attribut);
				F202_Name_ohne_erstes_Wort_aus_dem_Titel($attribut);
				F203_Geburtsjahr_gleich_Sterbejahr($attribut);
				F204_Ergaenzungsstrich_mit_Leerzeichen($attribut);
				F205_Kurzbeschreibung_mit_Schweitzer($attribut);
				F206_Kurzbeschreibung_mit_Politiker($attribut);
				F207_Kurzbeschreibung_mit_Politiker_ohne_Partei($attribut);
				F208_Datum_mit_falschem_Wort_am_Anfang($attribut);
				F209_Datum_mit_J($attribut);
				
				F210_Datenfeld_mit_HTML($attribut);
				F211_Datenfeld_falsche_Klammeranzahl($attribut);
				F212_Kurzbeschreibung_mit_Herkunft($attribut);
				F213_Kurzbeschreibung_ohne_Nationalitaet($attribut);
				F214_Datenfeld_kleingeschrieben($attribut);
				F215_Datum_mit_Artikel($attribut);
				F216_Datum_mit_doppeltem_Monat($attribut);
				F217_Datum_mit_fehlerhaften_Jahrhundert($attribut);
				F218_Datum_mit_bis($attribut);
				F219_Datum_mit_Punkt_vor_vor($attribut);
				
				F220_Datum_mit_zwischen_ohne_und($attribut);
				F221_Kurzbeschreibung_mit_Name_identisch($attribut);
				F222_Kurzbeschreibung_mit_Alternativnamen_identisch($attribut);
				F223_Name_fehlt_im_Titel($attribut);
				F224_Kategoriensortierung_mit_Kleinbuchstaben_am_Wortanfang($attribut);
				F225_Kategoriensortierung_mit_Buchstaben_nach_Komma($attribut);
				F226_Kategorie_geboren_doppelt_Jahr_und_Jahrhundert($attribut);
				F227_Kategorie_gestorben_doppelt_Jahr_und_Jahrhundert($attribut);
				F228_Ort_mit_Verb($attribut);
				F229_Ort_nur_Zahl($attribut);
				
				F230_Ort_mit_Monat($attribut);
				F231_Ort_mit_Zeit($attribut);
				F232_Ort_mit_Kreuz($attribut);
				F233_Sterbeort_mit_zwei_Zeilen($attribut);
				F234_Name_mit_Punkt_aber_Titel_ohne_Punkt($attribut);
				F235_Kurzbeschreibung_mit_Zeitraum($attribut);
				F236_Kurzbeschreibung_mit_Amerikanern($attribut);										# deaktiviert nach heftigem Widerstand
				F237_Kurzbeschreibung_mit_Bindestrich_nach_und($attribut);
				F238_Kurzbeschreibung_mit_Bindestrich_zwischen_Zahlen($attribut);
				F239_Kurzbeschreibung_mit_Bis_Strich_ohne_Klammer($attribut);
				
				F240_Datenfeld_fett_oder_schraeg($attribut);
				F241_Kurzbeschreibung_mit_Zeit_ohne_Klammer($attribut);
				F242_Kategoriensortierung_fehlt($attribut);
				F243_Geburtsdatum_mit_falschem_Jahrhundert($attribut);
				F244_Geburtsdatum_und_Sterbejahr_ohne_v_Chr($attribut);
				F245_Datum_mit_Komma_ohne_oder($attribut);
				F246_Kurzbeschreibung_mit_fehlerhaften_Chr($attribut);
				F247_Ort_mit_Link_vor_Wort($attribut);
				F248_Alternativname_falsch_sortiert($attribut);
				F249_Zeitlos_mit_Jahrhundert($attribut);
				
				F250_Zeitlos_mit_Jahr($attribut);
				F251_Kurzbeschreibung_ohne_Nationalitaet_aber_im_Text($attribut);
				F252_Name_mit_fehlendem_Buchstaben($attribut);
				F253_Kurzbeschreibung_ohne_Nationalitaet_aber_mit_Kategorie($attribut);
				F254_Kurzbeschreibung_mit_Abkuerzung_am_Anfang($attribut);								# deaktiviert da mit 53 zusammengelegt
				F255_Alternativnamen_besser_mit_wirklicher_Name($attribut);
				F256_Alternativnamen_besser_mit_vollstaendiger_Name($attribut);
				F257_Kategorie_Geboren_im_falschen_Jahrhundert($attribut);
				F258_Kategorie_Gestorben_im_falschen_Jahrhundert($attribut);
				F259_Name_und_Titel_abweichend($attribut);
	
				F260_Ort_mit_Zahl($attribut);   														# deaktiviert, Nur ab un zu mal anzeigen
				F261_Kategorie_geboren_nicht_in_Geburtsdatum($attribut);  
				F262_Kategorie_gestorben_nicht_in_Sterbedatum($attribut);  
				F263_Kategorie_geboren_im_Jahrhundert_fehlerhaft($attribut); 
				F264_Kategorie_gestorben_im_Jahrhundert_fehlerhaft($attribut); 	
				F265_Titelwort_mehrfach_im_Alternativnamen($attribut); 					

				F266_Geburtsdatum_nicht_leer($attribut); 
				F267_Alternativname_leer_Titel_nicht_im_Text($attribut);
				F268_Kategorie_mit_Nationalitaet_fehlt($attribut);
				F269_Kategorienanzahl_zu_niedrig($attribut);
				F270_Kategorie_mit_Leerzeichen($attribut);
				F271_Kategorie_geboren_oder_gestorben_ungenau($attribut);

				# ideas
				# 309 Kurzbeschreibung ist nur Beruf ohne Nationalität
				# ### Vorlage_im_Datenfeld();
				
				
}


#########################################################################





sub scan_dump_for_next_article{
	#this function scan line after line from dump, 
	#the result is the text from the next article
	
	my $line 		= "";		#one line in dump
	my $page_id 		= -1;		#titel of article
	my $title		= "";		#titel of article
	my $text 		= "";		#text of one article
	my $article_complete 	= 0;	
	my $start_recording 	= 0;	
	my $revision_start 	= 0;
	my $revision_id 	= -1;
	my $revision_time	= 0;
	
	my $number_of_scan_line = 0;
	
	#loop for every line
	do {
		$line = <DUMP>;
		$line_number = $line_number +1;
		$number_of_scan_line = $number_of_scan_line +1;		#Security, maybe the finish is not correct
		print "$line";
		
		if ($line =~ /<page>/) {
			$start_recording = 1;
		}
		
		if ($start_recording == 1) {
			$text = $text.$line;
		}

		if ($line =~ /<\/page>/) {
			$start_recording = 0;
			$article_complete = 1;
		}
		
		if ($line =~ /<title>/) {
			#extract title
			$title ="$line";
			my @content= split(/>/,$title);
			@content= split(/</,$content[1]);
			$title=$content[0];
			#print "$title\n";
		}

		if ($line =~ /<id>/ and $page_id == -1 ) {
			#extract id
			$page_id ="$line";
			my @content= split(/>/,$page_id);
			@content= split(/</,$content[1]);
			$page_id=$content[0];
			#print "$page_id\t$title\n";
			#die;
		}		

		if ($line =~ /<revision>/) {
			$revision_start = 1;
		}
		if ($revision_start == 1 and $revision_id == -1 and $line =~ /<id>/) {
			#read revision_id
			$revision_id ="$line";
			my @content= split(/>/,$revision_id);
			@content= split(/</,$content[1]);
			$revision_id=$content[0];
			#print $revision_id,"\n";
		}

		if ($revision_start == 1 and $line =~ /<timestamp>/) {
			#read revision_id
			$revision_time ="$line";
			my @content= split(/>/,$revision_time);
			@content= split(/</,$content[1]);
			$revision_time=$content[0];
			#print $revision_time,"\n";
		}		
		
		$end_of_dump = 1 if ($line =~ /<\/mediawiki>/);
		$end_of_dump = 1 if (eof(DUMP) == 1);
		
	}
	until ( $article_complete == 1 or $end_of_dump == 1);
	#Extract only edit-text
	my $test = index ($text, '<text xml:space="preserve">');
	$text = substr($text, $test);
	$text =~ s/<text xml:space="preserve">//g;
	$test = index($text,	'</text>');
	$text = substr($text,0,$test);

	# only in Dump
	#$title =~ s/&amp;/&/g;
	$title = replace_special_letter_from_DUMP($title);
	$text = replace_special_letter_from_DUMP($text);
	
	return($title, $page_id, $revision_id, $revision_time, $text);
}





############################################################################

sub scan_live_for_next_article {
	#this function scan live the wikipedia, 
	#the result is the text from the next article
	my $title		= '';		#titel of article
	my $text 		= '';		#text of one article
	my $article_complete 	= 0;	
	my $start_recording 	= 0;	
	my $revision_start 		= 0;
	my $revision_id 		= -1;
	my $revision_time		= 0;
	my $page_id				= 0;
		
	$current_live_article ++;	#next article
	my $number_of_article = @live_article;
	if ( $current_live_article < $number_of_article ) {	
	


		if ($xml_text_from_api eq '') {
			# if list of xml_text_from_api is empty, then load next articles
			#print 'Load next texts from API'."\n"; 

			# ask for 25 row text from many titles
			my $many_titles = '';
			my $i = $current_live_article; 
			my $end_many_title = 'false';
			do {

				my $line = $live_article[$i]; 
				my @line_split = split( /\t/, $line);
				my $next_title 		= $line_split[0];
				$many_titles = $many_titles.'|'.$next_title;
				$many_titles =~ s/^\|//;
				$i++;
				$end_many_title = 'true' if ($i == $number_of_article);
				$end_many_title = 'true' if ($i == $current_live_article + 25);   # max 25
			} 
			until ($end_many_title eq 'true');
			#print "\n".'Many titles ='.$many_titles."\n\n";
			$xml_text_from_api = raw_text_more_articles( $many_titles );
			#print $xml_text_from_api."\n";


			# remove head of xml
			$xml_text_from_api =~ s/^<\?xml version="1\.0"\?>//;
			$xml_text_from_api =~ s/^<api>//;

			# remove warnings
			$xml_text_from_api =~ s/^<warnings>.*<\/warnings>//;
#			$xml_text_from_api =~ s/^<query xml:space="preserve">.*<\/query>//;
#			$xml_text_from_api =~ s/<\/query>$//;
#			$xml_text_from_api =~ s/<\/warnings>$//; 

			# remove begin
			$xml_text_from_api =~ s/^<query>//;
			$xml_text_from_api =~ s/^<pages>//;

			# remove end
			$xml_text_from_api =~ s/<\/api>$//;
			$xml_text_from_api =~ s/<\/query>$//;
			$xml_text_from_api =~ s/<\/pages>$//; 

			#print "\n\n\n".$xml_text_from_api."\n\n\n";
		}


		# get next title and  text from xml_text_from_api
		if ($xml_text_from_api ne '') {

			# maybe normalized page_titles
			if (index ($xml_text_from_api, '<normalized>') > -1 ) {
				# found <normalized>
				# <normalized><n from="Abd al-Mu&amp;#039;min" to="Abd al-Mu&#039;min" /><n from="Hendrik &amp;#039;ecb&amp;#039; Beikirch" to="Hendrik &#039;ecb&#039; Beikirch" /><n from="Yohl Ik&amp;#039;nal" to="Yohl Ik&#039;nal" /></normalized>

				my $pos_begin = index ($xml_text_from_api, '<normalized'); 
				my $pos_end = index ($xml_text_from_api, '</normalized>') + length('</normalized>'); 
				my $text_norm = substr($xml_text_from_api,$pos_begin, $pos_end - $pos_begin);
				$xml_text_from_api = substr($xml_text_from_api, $pos_end+1);

				#print $text_norm."\n";
				# no need to handle -> later get title from api-page

			}


			# https://www.mediawiki.org/wiki/API:Query#Missing_and_invalid_titles
			# <page ns="0" title= ... </page>	            normal page
			# <page ns="0" title="XYZ123" missing="" />     missing page

			
			# get next text from xml_text_from_api	
			my $pos_begin = index ($xml_text_from_api, '<page'); # first page		
			my $pos_end = index ($xml_text_from_api, '<page', $pos_begin + 1 ); # next page

			if ($pos_end > -1 ) {
				# normal page
				$text		       = substr ( $xml_text_from_api, 0, $pos_end - 1);
				$xml_text_from_api = substr ( $xml_text_from_api, $pos_end);
			} else {
				# last page
				$text		       = $xml_text_from_api;
				$xml_text_from_api = '';
			}


			my $line = $live_article[$current_live_article];
			my @line_split = split( /\t/, $line);
			$title 		= $line_split[0];
			
			#print "\t".'TITLE:'.$title ."\n";
			#print "\t".substr (  $text, 0, 150)."\n";
			#print "\t".substr (  $xml_text_from_api, 0, 150)."\n\n";
			
			if (index ( $text, 'title='.'"'.$title.'"') == -1 ) {
				# the result from the api is in a other sort 
				# know get the current title
				# for example <page pageid="2065519" ns="0" title=".380 ACP">
				#print "Old title:".$title ."\n";
				my $pos_title = index ($text, 'title="');
				my $title_text = $text;
				$title_text = substr ( $title_text, $pos_title + length ('title="') );
				$pos_title = index ($title_text, '"');
				$title = substr ( $title_text, 0, $pos_title );
				$title = replace_special_letter_from_API($title);
				#print "\tNew title:".$title;
				#print "\n\n";
				#print substr (  $text, 0, 150)."\n";
				#print "\n\n";

			}
				

			# title
			# print $title."\n" ;
			#push(@article_was_scanned, $title);
			


			##############################
			# check for missing pages 
			##############################

			if (index ($text, 'missing=""') > -1) {
				# found missing page, delete in database

				# <page ns="0" title="ZBlu-ray Disc" missing="" />
				#print 'Missing Page: '.$title."\n";
				printf "\t%30s %30s %s\n",'Missing Page:',$title, ' delete in pd, pd_error';
				
				# 1.) get old page_id from table pd
				# 2.) delete from pd
				# 3.) delete from pd_error
				# 4.) delete from pd_error_history

				# 1.)
				my $sql_text = "SELECT page_id, title FROM pd where title = '".$title."';";
				my $sth = $dbh->prepare($sql_text);
				$sth->execute or die $sth->errstr;
				while (my ($id, $title) = $sth->fetchrow_array()) {
					$page_id = $id;
				}

				# 2.)
				$sql_text = "delete from pd where page_id = ".$page_id;
				#scan_change_limit $parameter{'modus_scan'};
				$sth = $dbh->do( $sql_text );

				# 3.)		
				$sql_text = "delete from pd_error where page_id = ".$page_id;
				#scan_change_limit $parameter{'modus_scan'};
				$sth = $dbh->do( $sql_text );

				
			}



			#####################
			# get page_id
			#####################
			# <page pageid="5819116" ns="0" title="Željko Čajkovski">

			my $test_id_pos  = index ($text, 'pageid="');
			if ($test_id_pos > -1) {	
				#print $text."\n";	
				$page_id =  substr($text, $test_id_pos + length( 'pageid="') );
				$test_id_pos = index ($page_id , '"');
				$page_id = substr($page_id, 0, $test_id_pos);
				#print $page_id.' - '.$title."\n";
			}
			#print "\tNach API:".$title."\n" if ($page_id == 212635);			
			

			########################	
			# get revison, get text
			#########################
			# <rev revid="138846409" parentid="138362442" timestamp="2015-02-14T20:27:08Z"
			
			#print $page_id.' - '.$title."\n";
			#print substr($text,0,150)."\n";
			
			my $test = index ($text, '<rev ');
			if ($test > -1) {
				my $pos = index ($text,'">', $test );
				my $rev_text  = substr($text, $test, $pos + 2 -$test);

				# get  text
				$text = substr($text, $pos + 2);			# text kürzen bis nach >
				$test = index($text,'</rev>');			    # ende der rev suchen
				$text = substr($text,0,$test);				# inhalt ist content
				$page{'text'} = $text;
				
				# get revision_id
				if ( $rev_text =~ / revid="(.*?)"/) {
					$page{'revid'} = $1;
				}

				# get revision_timestamp
				if ( $rev_text =~ / timestamp="(.*?)"/) {
					$page{'rev_timestamp'} = $1;
				}



			}


			#print $text."\n";
			#print substr($text, 0, 60)."\n";

		}
		


	} else {
		$end_of_live = 1;		# end of live
	}
	return($title, $page_id, $revision_id, $revision_time, $text);
}



################################################################################
#
################################################################################

sub replace_special_letters_for_URL {	
	my $content = shift;
	# only in dump must replace not in live
	# http://de.wikipedia.org/w/index.php?title=Benutzer_Diskussion:Stefan_K%C3%BChn&oldid=48573921#Dump

	my $result = utf8::is_utf8( $content )
		? URI::Escape::uri_escape_utf8( $content )
		: URI::Escape::uri_escape( $content );

#	$content =~ s/&lt;/</g;
#	$content =~ s/&gt;/>/g;
#	$content =~ s/&quot;/"/g;
#	$content =~ s/&#039;/'/g;
#	$content =~ s/&amp;/&/g;
	# &lt; -> <
	# &gt; -> >
	# &quot;  -> "
	# &#039; -> '
	# &amp; -> &
	return ( $result);
}



################################################################################

sub replace_special_letter_from_API{
	my $input = shift;
	my $output = $input;

	$output = utf8::is_utf8( $input )
		? URI::Escape::uri_unescape_utf8( $input )
		: URI::Escape::uri_unescape( $input );

	$output =~ s/&#039;/'/g;		# for example in:  Abu l-'Ala al-Ma'arri
	$output =~ s/&amp;/&/g;			# for example in:  Alois Stiebitz & Co.
	$output =~ s/&lt;/</g;			
	$output =~ s/&gt;/>/g;
	$output =~ s/&quot;/"/g;
	return ( $output);
}



#################################################################################

sub replace_special_letter_from_DUMP{
	my $input = shift;
	my $output = $input;

	$output = utf8::is_utf8( $input )
		? URI::Escape::uri_unescape_utf8( $input )
		: URI::Escape::uri_unescape( $input );

	$output =~ s/&#039;/'/g;		# for example in:  Abu l-'Ala al-Ma'arri
	$output =~ s/&amp;/&/g;			# for example in:  Alois Stiebitz & Co.
	$output =~ s/&lt;/</g;			
	$output =~ s/&gt;/>/g;
	$output =~ s/&quot;/"/g;
	return ( $output);
}



#####################################################################################


sub raw_text_more_articles {
	my $title = $_[0];
	
	#$title =~ s/&amp;/%26/g;		# Problem with & in title
	#$title =~ s/&#039;/'/g;			# Problem with apostroph in title
	#$title =~ s/&lt;/</g;
	#$title =~ s/&gt;/>/g;
	#$title =~ s/&quot;/"/g;
	#$title =~ s/&#039;/'/g;
	#print $title."\n\n";
	$title = replace_special_letters_for_URL($title);
	#print $title."\n\n";
	
	my $url2 = '';
	my $home = 'http://de.wikipedia.org/wiki/';
	$url2 = $home;
	$url2 =~ s/\/wiki\//\/w\//;
	$url2 = $url2.'api.php?action=query&prop=revisions&titles='.$title.'&rvprop=timestamp|ids|content&format=xml';
	#print $url2."\n\n";

	my $response2 ;
	my $ua2 = LWP::UserAgent->new;
	$response2 = $ua2->get( $url2 );
	my $content2 = $response2->content;
	my  $result2  = '';
	$result2 = $content2 if ($content2) ;
	return($result2);
}



########################################################################################

sub get_interwikis{
	
	my $interwikis = '';
	$interwikis = $interwikis."\t".get_interwikis2('af');
	$interwikis = $interwikis."\t".get_interwikis2('ar');
	$interwikis = $interwikis."\t".get_interwikis2('be');
	$interwikis = $interwikis."\t".get_interwikis2('bg');
	$interwikis = $interwikis."\t".get_interwikis2('bs');
	$interwikis = $interwikis."\t".get_interwikis2('ca');
	$interwikis = $interwikis."\t".get_interwikis2('cs');
	$interwikis = $interwikis."\t".get_interwikis2('cy');
	$interwikis = $interwikis."\t".get_interwikis2('da');
	$interwikis = $interwikis."\t".get_interwikis2('de');
	$interwikis = $interwikis."\t".get_interwikis2('el');
	$interwikis = $interwikis."\t".get_interwikis2('en');
	$interwikis = $interwikis."\t".get_interwikis2('eo');
	$interwikis = $interwikis."\t".get_interwikis2('es');
	$interwikis = $interwikis."\t".get_interwikis2('et');
	$interwikis = $interwikis."\t".get_interwikis2('eu');
	$interwikis = $interwikis."\t".get_interwikis2('fa');
	$interwikis = $interwikis."\t".get_interwikis2('fi');
	$interwikis = $interwikis."\t".get_interwikis2('fr');
	$interwikis = $interwikis."\t".get_interwikis2('fy');
	$interwikis = $interwikis."\t".get_interwikis2('gl');
	$interwikis = $interwikis."\t".get_interwikis2('gn');
	$interwikis = $interwikis."\t".get_interwikis2('gr');
	$interwikis = $interwikis."\t".get_interwikis2('he');
	$interwikis = $interwikis."\t".get_interwikis2('hr');
	$interwikis = $interwikis."\t".get_interwikis2('hu');
	$interwikis = $interwikis."\t".get_interwikis2('id');
	$interwikis = $interwikis."\t".get_interwikis2('is');
	$interwikis = $interwikis."\t".get_interwikis2('it');
	$interwikis = $interwikis."\t".get_interwikis2('ja');
	$interwikis = $interwikis."\t".get_interwikis2('ko');
	$interwikis = $interwikis."\t".get_interwikis2('la');
	$interwikis = $interwikis."\t".get_interwikis2('lt');
	$interwikis = $interwikis."\t".get_interwikis2('ms');
	$interwikis = $interwikis."\t".get_interwikis2('nds');
	$interwikis = $interwikis."\t".get_interwikis2('nds-nl');
	$interwikis = $interwikis."\t".get_interwikis2('nl');
	$interwikis = $interwikis."\t".get_interwikis2('nn');
	$interwikis = $interwikis."\t".get_interwikis2('no');
	$interwikis = $interwikis."\t".get_interwikis2('pl');
	$interwikis = $interwikis."\t".get_interwikis2('pt');
	$interwikis = $interwikis."\t".get_interwikis2('ro');
	$interwikis = $interwikis."\t".get_interwikis2('ru');
	$interwikis = $interwikis."\t".get_interwikis2('simple');
	$interwikis = $interwikis."\t".get_interwikis2('sk');
	$interwikis = $interwikis."\t".get_interwikis2('sl');
	$interwikis = $interwikis."\t".get_interwikis2('sr');
	$interwikis = $interwikis."\t".get_interwikis2('sv');
	$interwikis = $interwikis."\t".get_interwikis2('th');
	$interwikis = $interwikis."\t".get_interwikis2('tr');
	$interwikis = $interwikis."\t".get_interwikis2('uk');
	$interwikis = $interwikis."\t".get_interwikis2('vi');
	$interwikis = $interwikis."\t".get_interwikis2('zh');
	$interwikis =~ s/^\t//g;
	
	my @inter_split = split( /\t/ ,$interwikis);
	$interwikis = '';
	foreach (@inter_split) {
		my $current_interwiki = $_;
		if ( $current_interwiki ne ''){
			$interwikis = $interwikis ."\t". $current_interwiki;
		}
	}
	$interwikis =~ s/^\t//g;
	#print $interwikis."\n";
	return ($interwikis );
}

sub get_interwikis2{
	my $inter_lang = $_[0];
	my $found = '';
	if (index( lc($text), '[['.$inter_lang.':') > -1 ){
		#print $title."\n";
		my $pos = index( lc($text), '[['.$inter_lang.':');
		my $resttext = substr ( $text, $pos);
		my $pos2 = index( $resttext, ']]');
		$found = substr ( $resttext, 0, $pos2+2);
		#print $found;
	}
	return ($found)
}


sub get_defaultsort {

    #      <alias>SORTIERUNG:</alias>
    #      <alias>DEFAULTSORT:</alias>
    #      <alias>DEFAULTSORTKEY:</alias>
    #      <alias>DEFAULTCATEGORYSORT:</alias>

	$defaultsort = '';
	my @sort_array = ( 'SORTIERUNG:', 'DEFAULTSORT:', 'DEFAULTSORTKEY:', 'DEFAULTCATEGORYSORT:');
	my $lc_text = lc($text);
	foreach (@sort_array) {
		my $searchword = lc($_);
		if (index ($lc_text, '{{'.$searchword) > -1 ){
			$has_defaultsort = 1;
			#defaulsort enthalten
			my $pos = index ($lc_text, '{{'.$searchword);
			$defaultsort = substr ( $text, $pos + 2);
			$defaultsort =~ s/$searchword//i;
			
			my $pos2 = index ($defaultsort, '}');
			$defaultsort = substr ($defaultsort, 0, $pos2);
			#print $title."\n";
			#print "\t\t".$defaultsort."\n";
		}
	}
	
}



##############################################################################
# Load from file articles 
###############################################################################

sub load_from_textfile{

	my $file = $parameter{'file_page_to_scan'};
	#check for existens
	if (-e "$file") {
		#print "Data: $file\n";
	} else {
		die "$file don't exist!\n";
	}
	
	# TODO eliminate Tabulator etc after title
	
	#open live articlelist
	open(LIVE, "<".$file);
	@live_article = <LIVE>;
	close (LIVE);

	#remove return 
	foreach (@live_article){
		$_ =~ s/\n//g;

		# normalized
		$_ =~ s/\t/ /g;
		$_ =~ s/_/ /g;
		$_ =~ s/  / /g;
		$_ =~ s/\s$//g;
		$_ =~ s/^\s//g;

	}

	return (scalar @live_article);	
}



##############################################################################
# Load from table page_to_scan
###############################################################################

sub load_from_table_page_to_scan{
	# read all articles from table page_to_scan from database
	my $sth = $dbh->prepare("SELECT page_id,title FROM page_to_scan") or die ($dbh->errstr);
	$sth->execute or die $sth->errstr;
	while (my ($id, $title) = $sth->fetchrow_array()) {
		push(@live_article,  $title);
	}
	my $count = $sth->rows();
	#$number_of_live_tests = @live_article;
	#printf '%24s = %s', 'page to scan', scalar @live_article."\n";	

	return ($count);
}



##############################################################################
# Load from db articles 
###############################################################################

sub load_from_table_pd_with_error{

	# TODO now from table pd columne has_error
	my $counter = 0;

	if ($parameter{'modus_nodb'}  ne 'nodb') {
		my $sql_text = "select title from pd where has_error > 0;";
		my $result = '';
		my $sth = $dbh->prepare( $sql_text );
		#print '<p class="smalltext"/>'.$sql_text."</p>\n";					  
		$sth->execute;
		while (my $arrayref = $sth->fetchrow_arrayref()) {	
			foreach(@$arrayref) {
				$result = $_;
			}
			#print $result."\n";
			push(@live_article, $result);
		}
		$counter = $sth->rows();
	}
	#print "\t".$counter."\t".'article to scan from db'."\n";
	return ($counter);
}



##############################################################################
# Load new articles 
###############################################################################

sub load_from_db_article_new{

	my $new_counter = 0;

	if ($parameter{'modus_nodb'}  ne 'nodb') {

		##############################################
		# get the daytime of last new article
		my $sql_text = "select max(daytime) from pd_new;";
		$last_article_new_daytime = '';
		my $sth = $dbh->prepare( $sql_text );
		$sth->execute;
		while (my $arrayref = $sth->fetchrow_arrayref()) {	
			foreach(@$arrayref) {
				$last_article_new_daytime = $_;
			}
		}
		if ( defined($last_article_new_daytime) ){
			# all ok;
		}else{
			# table pd_new is empty
			$last_article_new_daytime = '';
		};
		
		#print 'Last new article from '. $last_article_new_daytime."\n";
		
		##############################################
		# get new articles
		# $sql_text = "select distinct title from pd_new where scan_live = 0;";		# old
		$sql_text = "select title from pd_new where daytime >	
		(select value from parameter where project='dewiki' and name='daytime_last_article_from_new' limit 1);";

		my $result = '';
		$sth = $dbh->prepare( $sql_text );
		#print '<p class="smalltext"/>'.$sql_text."</p>\n";					  
		$sth->execute;
		while (my $arrayref = $sth->fetchrow_arrayref()) {	
			foreach(@$arrayref) {
				$result = $_;
			}
			#print $result."\n";
			push(@live_article, $result);
			$new_counter ++;
		}
		

		
		
		
	}
	print "\t".$new_counter."\t".'article to scan from db new'."\n";
	$new_article_for_output = $new_counter;
	#print 'Ready with new articles'."\n\n";
	
}



#################################################################
# Load article from table change (recentchange of wikipedia)
################################################################

sub load_from_table_changes{
	# load article from recentchange to page_to_scan
	my $counter = 0;


	# 1. Step: Mark rows in table recentchange
	my $sql_text = "update recentchange set move_now = 'Y'";
	#scan_change_limit $parameter{'modus_scan'};

	my $sth = $dbh->do( $sql_text );

	my $result = '';
	$sql_text = "select distinct title from recentchange where move_now = 'Y';";
	$sth = $dbh->prepare( $sql_text );
	#print '<p class="smalltext"/>'.$sql_text."</p>\n";					  
	$sth->execute or die $sth->errstr;
	while (my $arrayref = $sth->fetchrow_arrayref()) {	
		foreach(@$arrayref) {
			$result = $_;
		}
		#print $result."\n";
		push(@live_article, $result);
		
	}
	$counter = $sth->rows();
	#print "\t".$change_counter."\t".' article to scan from db change'."\n";
	return($counter);
}


################################################################
# Lade letzen Aenderungen from file
################################################################

sub last_changes_old{
	
	##########################################
	print 'Lade letzen Aenderungen'."\n\n";
	#http://de.wikipedia.org/w/index.php?title=Spezial:Neue_Seiten&offset=20080418181733&limit=500
	my $test = '';
	
	# Load new articles
	my $file_input_new = $input_directory_last_changes.'dewiki/dewiki_last_changes.txt';
	#print $file_input_new."\n";
	if (-e $file_input_new) {
		#if existing
		my $new_counter = 0;
		#print 'file exist'."\n";
		open(INPUT_NEW, "<$file_input_new");
		do {
			my $line = <INPUT_NEW>;
			$line =~ s/\n$//g;
			my @split_line = split ( /\t/, $line);
			push(@live_article, $split_line[1]."\t".'0' );
			$new_counter ++;
		}
		until (eof(INPUT_NEW) == 1);	
		close (INPUT_NEW);
		print $new_counter.' last changes'."\n";
		$change_article_for_output = $new_counter;
	}
	print 'Ready with last changes'."\n\n";
	
}



################################################################
# delete all double article in array for scan
################################################################

sub delete_all_double_input_articles{
	# sort_all_live_articles
	@live_article = sort(@live_article);
	#$number_of_live_tests = @live_article;
	#print $number_of_live_tests.' article to scan (file + db + new + change)'."\n";	

	# delet all double input article
	my @new_live_article;
	my @split_line;
	my $old_title = '';
	my $i = -1;
	foreach my $current_title (@live_article) {

		if ($old_title eq $current_title 
		     and $current_title ne '') {
			#double
			$new_live_article[$i] = $new_live_article[$i];
		} else {
			#new_article
			if (
				index ($current_title, 'Benutzer:') == -1 and
				index ($current_title, 'Bild:') == -1 and
				index ($current_title, 'Datei:') == -1 and
				index ($current_title, 'Diskussion:') == -1 and
				index ($current_title, 'File:') == -1 and
				index ($current_title, 'Hilfe:') == -1 and
				index ($current_title, 'Kategorie:') == -1 and
				index ($current_title, 'MediaWiki:') == -1 and
				index ($current_title, 'Mediawiki:') == -1 and
				index ($current_title, 'Portal:') == -1 and
				index ($current_title, 'Vorlage:') == -1 and
				index ($current_title, 'Wikipedia:') == -1 and					
				index ($current_title, 'Nekrolog') == -1 and
				$current_title ne 'Mann' and 
				$current_title ne 'Frau' and
				$current_title ne ''

			) {
				$i = $i+1;
				$new_live_article[$i] = $current_title;
			}
		}
		$old_title = $current_title;
		#die;
	}
	@live_article = @new_live_article;

			
	#$number_of_live_tests = @live_article;
	#print $number_of_live_tests.' article to scan without double'."\n\n";
	return(scalar @live_article);		
}



########################################
# set_article_as_scan_live_in_db
########################################

sub set_article_as_scan_live_in_db{
	# if an article was scan live, than set this in the table cw_dumpscan as true
	my $article = $_[0];
	my $id = $_[1];
	
	# problem: title of an article is "  Ali's Bar   "
	$article =~ s/'/\\'/g;
	$article =~ s/[^\\]\\\\'/\\\\\\'/g;

	#update in the table cw_dumpscan
	#my $sql_text = "update cw_dumpscan set scan_live = true where project = '".$project."' and (title = '".$article."' or id = ".$id.");";
	#my $sth = $dbh->prepare( $sql_text );
	#$sth->execute;
	
	#update in the table cw_new
	my $sql_text = "update pd_new set scan_live = true where title = '".$article."';";
	my $sth = $dbh->prepare( $sql_text );
	$sth->execute;

	#update in the table cw_change
	$sql_text = "update pd_change set scan_live = true where title = '".$article."';";
	$sth = $dbh->prepare( $sql_text );
	$sth->execute;	

}

sub delete_article_from_table_pd_new 	{
	if ($parameter{'modus_scan'}  eq 'live' and $parameter{'modus_nodb'}  ne 'nodb') {
		if ($last_article_new_daytime ne '') {
			print 'delete all new articles from this project older then '. $last_article_new_daytime."\n";
			
			# update parameter
			my $sql_text = "update parameter set value = '".$last_article_new_daytime."' where project = 'dewiki' and name ='daytime_last_article_from_new';"."\n";
			print $sql_text."\n";
			my $sth = $dbh->prepare( $sql_text );
			$sth->execute;

			# delete from pd_new
			$sql_text = "delete from pd_new where daytime <= 
			(select str_to_date(value, '%Y-%m-%d %h:%i:%s') from parameter where project='dewiki' and name='daytime_last_article_from_new' limit 1);";
			$sth = $dbh->prepare( $sql_text );
			$sth->execute;
		}
	
	
		###################
		# old with scan_live
		# print 'delete all scanned or older then 7 days from this project'."\n";
		# my $sql_text2 = "delete from pd_new where (scan_live = 1 or DATEDIFF(now(),daytime) > 7);";
		# #print $sql_text2."\n";
		# my $sth = $dbh->prepare( $sql_text2 );
		# $sth->execute;
		
		# print 'delete all articles from don´t scan projects '."\n";
		# my $sql_text3 = "delete from pd_new where DATEDIFF(now(),daytime) > 8;";
		# #print $sql_text2."\n";
		# $sth = $dbh->prepare( $sql_text3 );
		# $sth->execute;
	}
}

sub delete_article_from_table_change 	{

	if ($parameter{'modus_scan'}  eq 'live' and $parameter{'modus_nodb'}  ne 'nodb') {
		print 'delete scanned articles from table change'. $last_article_change_daytime."\n";

		my $sql_text = "delete from recentchange where move_now = 'Y';"; 

		my $sth = $dbh->prepare( $sql_text );
		$sth->execute;
	}
}





#################################################

sub search_for_last_dump {
	# search in dump_directory for the last XML-file of a project
	print 'search_for_last_dump'."\n";
	my $last_file ='';
	
	# get all xml.bz2 files
	my @xml_files  = glob($parameter{'path_dump'}.'/*.xml.bz2');
	my $count_xml_files = @xml_files;
	
	print 'Found Dumps in path : '..$parameter{'path_dump'}."\n";
	for (my $i = 0; $i < $count_xml_files; $i++) {
		#print $xml_files[$i]."\n";
		$xml_files[$i] =~ s/(.)+\///g;	# remove path
		# get xml-file from project, but not the "-latest"
		if (index($xml_files[$i], $project.'-') == 0	
		    and index($xml_files[$i], $project.'-latest') == -1) {
			print "\t".$xml_files[$i]."\n";
			$last_file = $xml_files[$i];
		}
	}

	if ($last_file eq '' and $parameter{'modus_scan'}  ne 'live') {
		# No file found
		print "\n";
		print $count_xml_files.' XML-files found'."\n";
		print 'But found no XML-file for project: '.$project."\n";
		print 'in dircetory '.$parameter{'path_dump'}."\n\n";
		die;
	}
	return($last_file);
}



##############################################################################

sub print_article_title_every_x{
	#print in the Loop every x article a short message
	#Output every x articles
	my $language       = shift;
	my $title          = shift;
	my $article_number = shift;
	my $steps          = shift;
	#print "$article_number \t$title\n";
	my $x = int( $article_number / $steps ) *$steps ;
	if ($article_number == $x) {
		print "$language $article_number $page_id $title\n";
	}	
}

##############################################################################


sub get_pd {
		
		if ($text ne '' and index($text, 'Personendaten') > -1) {
			my $vorlage = scan_for_geotemplate($text, "{{", "Personendaten", "}}");
			
			
			#Alle Datenfelder vorhanden?
			if (    index($vorlage, 'NAME=') == -1
			     or index($vorlage, 'ALTERNATIVNAMEN=') == -1
				 or index($vorlage, 'KURZBESCHREIBUNG=') == -1
				 or index($vorlage, 'GEBURTSDATUM=') == -1
				 or index($vorlage, 'GEBURTSORT=') == -1
				 or index($vorlage, 'STERBEDATUM=') == -1
				 or index($vorlage, 'STERBEORT=') == -1
				 
				){
					if 	($parameter{'modus_scan'}  ne 'only') {
						F001_Personendaten_nicht_komplett('check', 'yes');
					}
					#print $vorlage;
					#die;
					$text = '';
			}
			
			F005_Parameter_mehrfach('check', $vorlage); 		#test for double parameter
			F010_Reihenfolge_fehlerhaft('check', $vorlage); 	#test the order of the parameter
			

			#print "Vorlage:"."\n";
			#print "Vorlage=".$vorlage."\n";
			#print "Text=".$text."\n";
			
			
			if ($text ne '') {
				# Nur bei vollständiger Personendatenvorlage scannen
				my $pos1 = index($vorlage, 'NAME=') + length('NAME=');
				
				
				my $pos2 = index($vorlage, '|ALTERNATIVNAMEN=');
				
				$name = substr($vorlage, $pos1, $pos2-$pos1) if ($pos2 > $pos1);
				#print $name."\n";
				
				$pos1 = index($vorlage, 'ALTERNATIVNAMEN=') + length('ALTERNATIVNAMEN=');
				$pos2 = index($vorlage, '|KURZBESCHREIBUNG=');
				$aname = substr($vorlage, $pos1, $pos2-$pos1) if ($pos2 > $pos1);
				#print $aname."\n";

				$pos1 = index($vorlage, 'KURZBESCHREIBUNG=') + length('KURZBESCHREIBUNG=');
				$pos2 = index($vorlage, '|GEBURTSDATUM=');
				$kurz = substr($vorlage, $pos1, $pos2-$pos1) if ($pos2 > $pos1);
				#print $kurz."\n";		

				$pos1 = index($vorlage, 'GEBURTSDATUM=') + length('GEBURTSDATUM=');
				$pos2 = index($vorlage, '|GEBURTSORT=');
				$geburtsdatum = substr($vorlage, $pos1, $pos2-$pos1) if ($pos2 > $pos1);
				#print $geburtsdatum."\n";
				$geburtstag   = datum_tag($geburtsdatum);
				$geburtsmonat = datum_monat($geburtsdatum);
				$geburtsjahr  = datum_jahr($geburtsdatum);
				#print $geburtstag."\n";
				#print $geburtsmonat."\n";
				#print $geburtsjahr."\n";
				
				$pos1 = index($vorlage, 'GEBURTSORT=') + length('GEBURTSORT=');
				$pos2 = index($vorlage, '|STERBEDATUM=');
				$geburtsort = substr($vorlage, $pos1, $pos2-$pos1) if ($pos2 > $pos1);
				#print $geburtsort."\n";

				$pos1 = index($vorlage, 'STERBEDATUM=') + length('STERBEDATUM=');
				$pos2 = index($vorlage, '|STERBEORT=');
				$sterbedatum = substr($vorlage, $pos1, $pos2-$pos1) if ($pos2 > $pos1);
				#print $sterbedatum."\n";
				$sterbetag   = datum_tag($sterbedatum);
				$sterbemonat = datum_monat($sterbedatum);
				$sterbejahr  = datum_jahr($sterbedatum);		
				#print $sterbetag."\n";
				#print $sterbemonat."\n";
				#print $sterbejahr."\n";
				$pos1 = index($vorlage, 'STERBEORT=') + length('STERBEORT=');
				$sterbeort = substr($vorlage, $pos1) if ($pos1 > -1);
				#print $sterbeort."\n";	

				# führendes und abschließendes Leerzeichen entfernen
				$name =~s/^[ ]+//;
				$kurz =~s/^[ ]+//;
				$aname =~s/^[ ]+//;
				$geburtsort =~s/^[ ]+//;
				$sterbeort =~s/^[ ]+//;
				$geburtsdatum =~s/^[ ]+//;
				$sterbedatum =~s/^[ ]+//;

				$name =~s/[ ]+$//;
				$kurz =~s/[ ]+$//;
				$aname =~s/[ ]+$//;
				$geburtsort =~s/[ ]+$//;
				$sterbeort =~s/[ ]+$//;
				$geburtsdatum =~s/[ ]+$//;
				$sterbedatum =~s/[ ]+$//;



				# entlinken
				$kurz_plain = $kurz;
				$kurz_plain =~ s/(\[\[)([^[|]*)(\]\])/$2/g;
				$kurz_plain =~ s/(\[\[)([^[|]*)\|([^]]*)(\]\])/$3/g;
				
				$geburtsort_plain = $geburtsort;
				$geburtsort_plain =~ s/(\[\[)([^[|]*)(\]\])/$2/g;
				$geburtsort_plain =~ s/(\[\[)([^[|]*)\|([^]]*)(\]\])/$3/g;
				
				$sterbeort_plain = $sterbeort;
				$sterbeort_plain =~ s/(\[\[)([^[|]*)(\]\])/$2/g;
				$sterbeort_plain =~ s/(\[\[)([^[|]*)\|([^]]*)(\]\])/$3/g;
				
				$geburtsdatum_plain = $geburtsdatum;
				$geburtsdatum_plain =~ s/(\[\[)([^[|]*)(\]\])/$2/g;
				$geburtsdatum_plain =~ s/(\[\[)([^[|]*)\|([^]]*)(\]\])/$3/g;

				$sterbedatum_plain = $sterbedatum;
				$sterbedatum_plain =~ s/(\[\[)([^[|]*)(\]\])/$2/g;
				$sterbedatum_plain =~ s/(\[\[)([^[|]*)\|([^]]*)(\]\])/$3/g;



				
			}
		}
}



sub scan_for_geotemplate {
	#print "Scan article for geocoordinates\n";
	my $text = $_[0];
	my $template_start = $_[1];
	my $template_name  = $_[2];
	my $template_end   = $_[3];
	my $coordinates = "";
	
	#delet string for "Template:
	my $delete_temp = "Template:";	#english
	if (index($text,$delete_temp) >-1) {
		$text =~ s/$delete_temp//g;
		#print $title."\n";
		#die;
	}
	#print "x".$delete_temp."x\n";
	$delete_temp = "Vorlage:";	#special language
	if (index($text,$delete_temp) >-1) {
		#print $title."\n";
		$text =~ s/$delete_temp//g;
		#print "x".$text."x\n";
		#die;
	}
	
	# Sprachvorlagem entfrenen
	$text =~ s/(\{\{Ar)([^}]*)(\}\})//gi;
	$text =~ s/(\{\{RuS)([^}]*)(\}\})//gi;
	$text =~ s/(\{\{zh)([^}]*)(\}\})//gi;
	$text =~ s/(\{\{lang)([^}]*)(\}\})//gi;

	
	
	#print $template_name,"\n";
	
	if ($text =~ /$template_start[ ]*$template_name/i)  {
		$text =~ s/$template_start[ ]*$template_name/$template_start$template_name/gi;
		#print "Temp_Name: ".$template_name,"\n" if  ($title eq "Tula (Sardinien)");
		#print $text,"\n";	
		do{
			my $template_begin = $template_start.$template_name;
			my $coordinate = "";
			#$coordinates_number = $coordinates_number + 1;
			my $position   = index ($text, $template_begin);
			$text       = substr($text, $position);
			$position   = index ($text, $template_end);
			$coordinate = substr($text, 0, $position + 2);
			$text       = substr($text, $position + 2);

			if ($coordinates eq "") {
				#erste Koordinate
				$coordinates = $coordinate;
				#$Koordinaten_im_Artikel_Anzahl=1;
			} else {
				#weitere Koordinaten anh寧en
				$coordinates = $coordinates." ".$coordinate;
				#$Koordinaten_im_Artikel_Anzahl=$Koordinaten_im_Artikel_Anzahl+1;
			}
		}
		while ($text =~ /$template_start[ ]*$template_name/i);

		#seperator for more then one templates
		$coordinates = " ".$coordinates;


		#delete all tabs and linebreaks
		$coordinates =~ s/\t//g;
		$coordinates =~ s/\n//g;
		
		#$coordinates =~ s/ \|/\|/g;
		#$coordinates =~ s/\| /\|/g;
		
		$coordinates =~ s/\{\{Personendaten//g;
		$coordinates =~ s/\}\}$//g;
		
	}
	

	
	return($coordinates);


}


###########################################################

sub datum_tag {
	my $input = $_[0];
	my $back = datum($input);
	my @backsplit = split(/\t/, $back);
	my $result ="";
	$result = $backsplit[3] if ($backsplit[3]);
	return ($result);
}

sub datum_monat {
	my $input = $_[0];
	my $back = datum($input);
	my @backsplit = split(/\t/, $back);
	my $result ="";
	$result = $backsplit[2] if ($backsplit[2]);
	return ($result);
}

sub datum_jahr {
	my $input = $_[0];
	my $back = datum($input);
	my @backsplit = split(/\t/, $back);
	my $result ="";
	$result = $backsplit[1] if ($backsplit[1]);
	return ($result);
}



sub datum{
	my $input = $_[0];
	my $input_original = $input;
	
	my $tag = "";
	my $monat = "";
	my $jahr = "";
	
	if ($input ne "")
		{
			
			
			#testen ob vor Chrstus
			my $vorChr = 1;
			$vorChr = -1 if (index ($input, "v. Chr.")>=0);
			
			$input =~ s/v\. Chr\.//g;
				
			
			#Monat
			#23. Oktober 1954
			$monat = "1" if index($input, "Januar")>=0;
			$monat = "1" if index($input, "Jänner")>=0;	# Ausnahme für Österreich
			$monat = "2" if index($input, "Februar")>=0;
			$monat = "3" if index($input, "März")>=0;
			$monat = "4" if index($input, "April")>=0;
			$monat = "5" if index($input, "Mai")>=0;
			$monat = "6" if index($input, "Juni")>=0;
			$monat = "7" if index($input, "Juli")>=0;
			$monat = "8" if index($input, "August")>=0;
			$monat = "9" if index($input, "September")>=0;
			$monat = "10" if index($input, "Oktober")>=0;
			$monat = "11" if index($input, "November")>=0;
			$monat = "12" if index($input, "Dezember")>=0;
			
			$input =~ s/Januar/1\./;
			$input =~ s/Jänner/1\./;	# Ausnahme für Österreich
			$input =~ s/Februar/2\./;
			$input =~ s/März/3\./;
			$input =~ s/April/4\./;
			$input =~ s/Mai/5\./;
			$input =~ s/Juni/6\./;
			$input =~ s/Juli/7\./;
			$input =~ s/August/8\./;
			$input =~ s/September/9\./;
			$input =~ s/Oktober/10\./;
			$input =~ s/November/11\./;
			$input =~ s/Dezember/12\./;
			
			$input =~ s/\]//g;
			$input =~ s/\[//g;
			$input =~ s/ //g;
			
			# jetzt m?te hier stehen
			# 21.12.1992
			# 1.1.1
			
			#test ob noch andere Zeichen
			my $test = $input;
			$test =~ s/[0-9]*//g;
			$test =~ s/\.*//g;
			
			if ( length($test) == 0   ) {
#and length($input)>=5  and length($input)<=10
				my @datum = split(/\./ , $input);
				my $datum_anzahl = @datum;
				
				if ($datum_anzahl == 3) {
					# 18.3.1975
					$tag = $datum[0];
					$monat = $datum[1];
					$jahr = $datum[2];
					$jahr = $jahr * $vorChr;
				}
			
				if ($datum_anzahl == 2) {
					# 3.1975
					#print "nur Monat+Jahr: ".$input."\n";
					$monat = $datum[0];
					$jahr = $datum[1];
					$jahr = $jahr * $vorChr;
				}				
				
			}
			
			if (index ($input, '.') == -1 ) {
				#print "test: ".$test."\n";
				# 1975
				# vor 1975
				# evt. nur Zahlen
				my $test2 = $input;
				if ($test2 =~ /^[0-9]+$/ ) {
					# nur Zahlen
					# 1975
					$jahr = $input;
					$jahr = $jahr * $vorChr;
					#print 'nur Jahr: '.$jahr."\n";
				}
			}
			
			#print "x".$input."x\n";
			#print "y".$jahr."\t".$monat."\t".$tag."y\n";
		}
	return ($input_original."\t".$jahr."\t".$monat."\t".$tag);
}



######################################################################################################################################

sub F001_Personendaten_nicht_komplett{
	my $error_code = 1;
	my $attribut = $_[0];
	my $found_error = $_[1];
	if ($attribut eq 'get_description') {
		$error_description[$error_code][0] = 'Personendaten unvollständig';
		$error_description[$error_code][1] = 'In den Personendaten wurden nicht alle Datenfelder gefunden. Es müssen genau folgende Zeichenketten in der Vorlage zu finden sein:  "NAME=" , "ALTERNATIVNAMEN=", "KURZBESCHREIBUNG=", "GEBURTSDATUM=", "GEBURTSORT=", "STERBEDATUM=" und "STERBEORT=" ';
		$error_description[$error_code][2] = 1; 

	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		if ($found_error eq 'yes') { 
			fehlermeldung($error_code, $title, $name);
		}
	}
}

sub F002_Personendaten_fehlen{
	my $error_code = 2;
	my $attribut = $_[0];
	my $found_error = $_[1];
	if ($attribut eq 'get_description') {
			$error_description[$error_code][0] = 'Personendaten fehlen';
			$error_description[$error_code][1] = 'Der Artikel sollte Personendaten enthalten. Das Skript erkennt im Text eine  Kategorie:Mann, Frau oder Intersexueller oder Gestorben/Geboren. Alternativ dazu sucht das Skript auch nach einer Infobox z.B. ("Infobox Fußballspieler"). In den aufgelisteten fehlt das Personendatenfeld. Evt. ist es enthalten aber nicht in der korrekten Schreibweise. Sollte es sich aber um eine Personengruppe handeln, dann sollte hier noch die Kategorie Personengruppe, Geschwister, Künstlerpaar etc. eingebaut werden.<br />Siehe auch: <a href="http://de.wikipedia.org/wiki/Benutzer:APPER/Personendaten">Benutzer:APPER/Personendaten</a> (halbautomatische Unterstützung)';
			$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {	
		if ($found_error eq 'yes') { 
			if (index($text,'Löschantragstext') > -1 ){
				F006_Personendaten_fehlen_und_Loeschkandidat('check');
			} else {
				fehlermeldung($error_code, $title, $name);
			}
		}
	}
}

sub F003_Personendaten_fehlerhaft{
	my $error_code = 3;
	my $attribut = $_[0];
	my $found_error = $_[1];
	if ($attribut eq 'get_description') {
		$error_description[$error_code][0] = 'Personendaten vielleicht';
		$error_description[$error_code][1] = 'Der Artikel enthält die Worte "Personendaten" und "Kurzbeschreibung", aber es konnte keine korrekten Personendaten extrahiert werden.';
		$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {	
		if ($title ne 'Beethoven-Haus') {
			if ($found_error eq 'yes') { 
				fehlermeldung($error_code, $title, $name);
				#print "\t".$title."\n";
				#print "\t\t".$name."\n";
			}
		}
	}
}


sub F004_article_with_pd_cat{
	my $error_code = 4;
	my $attribut = $_[0];
	my $cat = $_[1];
	if ($attribut eq 'get_description') {
		$error_description[$error_code][0] = 'Personendaten fehlen, aber Personenkategorien vorhanden';
		$error_description[$error_code][1] = 'Der Artikel enthält keine Personendaten, aber eine Kategorie, die üblicherweise für Personen benutzt wird (z.B. Kategorie:Sänger). Weiterhin wurde auf Kategorien überprüft, die darauf hinweisen, dass es sich nicht um eine Person handelt (z.B. Kategorie:Bauwerk). Wenn ein Artikel hier aufgelistet ist und es sich nicht um eine Person handelt, dann ist entweder eine Personenkategorie eingefügt worden, die dort so nicht verwendet werden soll oder es fehlt einfach noch eine Kategorie, die darauf hinweist, dass es sich nicht um einen Artikel zu einer einzelnen Person handelt (z.B. z.B. Kategorie:Organisation, Kategorie:Band, Kategorie:Malerfamile, Kategorie:Künstlergruppe …)';
		$error_description[$error_code][2] = 3;
		$error_description[$error_code][1] = regelbasierter_hinweistext($error_description[$error_code][1]);
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {	
		if ($cat ne '') {
			fehlermeldung($error_code, $title, $cat);
			#print "\t".$title."\n";
			#print "\t\t".$cat."\n";
		}
	}
}

sub F005_Parameter_mehrfach{
	my $error_code = 5;
	my $attribut = $_[0];
	my $test_vorlage = $_[1];
	if ($attribut eq 'get_description') {
			$error_description[$error_code][0] = 'Personendaten mit doppelten Datenfeldern';
			$error_description[$error_code][1] = 'Der Artikel enthält Personendaten, in denen ein Parameter mehrmals vorkommt.';
			$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {	
		F005_Parameter_mehrfach_2($error_code, $test_vorlage, 'NAME=');
		F005_Parameter_mehrfach_2($error_code, $test_vorlage, 'ALTERNATIVNAMEN');
		F005_Parameter_mehrfach_2($error_code, $test_vorlage, 'KURZBESCHREIBUNG');
		F005_Parameter_mehrfach_2($error_code, $test_vorlage, 'GEBURTSDATUM');
		F005_Parameter_mehrfach_2($error_code, $test_vorlage, 'GEBURTSORT');
		F005_Parameter_mehrfach_2($error_code, $test_vorlage, 'STERBEDATUM');
		F005_Parameter_mehrfach_2($error_code, $test_vorlage, 'STERBEORT');
	}
}


sub F005_Parameter_mehrfach_2 {
	my $error_code =  $_[0];
	my $test_vorlage = $_[1];
	my $parameter    = $_[2];
	my $pos = index($test_vorlage, $parameter);
	if ($pos > -1 ) {
		$pos = $pos + length($parameter);
		my $test_vorlage2 = substr($test_vorlage, $pos);
		if (index($test_vorlage2, $parameter) > -1) {
			my $cat = $_[0];
			fehlermeldung($error_code, $title, '<pre><nowiki>'.$test_vorlage.'</nowiki></pre>');
			#print $title."\n";
			#print $test_vorlage."\n";
			
		}
	}
}



sub F006_Personendaten_fehlen_und_Loeschkandidat{
	my $error_code = 6;
	my $attribut = $_[0];
	my $found_error = $_[1];
	if ($attribut eq 'get_description') {
			$error_description[$error_code][0] = 'Personendaten fehlen und Löschkandiat';
			$error_description[$error_code][1] = 'Der Artikel enthält die Kategorie:Mann, Frau oder Intersexueller oder Gestorben/Geboren, aber es fehlt das Personendatenfeld. Außerdem wurde der Artikel zur Löschung vorgeschlagen und wird deswegen hier als Verbesserungsvorschlag mit niedriger Priorität eingestuft.';
			$error_description[$error_code][2] = 3;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		if ($found_error eq 'yes') {
			fehlermeldung($error_code, $title, '');
		}
	}
}

sub F007_Keine_Kategorie{
	my $error_code = 7; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
		$error_description[$error_code][0] = 'Kategorien fehlen';
		$error_description[$error_code][1] = 'Dieser Artikel hat keine PD und keine Kategorien. Bitte kategorisieren für die nächste Überprüfung. Evt. sind auch einfach nur die englischen Schlüsselwörter "Category" statt "Kategorie" im Artikel benutzt worden. Das sollte geändert werden.';
		$error_description[$error_code][2] = 3;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {	
		if ($all_categories eq '' and $is_redirect == 0 and $has_pd == 0 and index($text, '{{')== -1  ) {
			fehlermeldung($error_code, $title,'');		
			#print "\t".$title."\n";
			#print "\t000\n";
			#die;
		}
	}
}

sub F008_check_for_first_name{
	my $error_code = 8; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
		$error_description[$error_code][0] = 'Kategorien fehlen, aber Vorname gefunden';
		$error_description[$error_code][1] = 'Dieser Artikel hat keine PD und keine Kategorien. Es wurde aber im Artikeltitel ein Vorname gefunden. Vielleicht ist es eine Person. Bitte kategorisieren für die nächste Überprüfung.';
		$error_description[$error_code][2] = 3;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {	

	
		if ($all_categories eq '' and $is_redirect == 0 and $has_pd == 0 and index($text, '{{')== -1) {
		
			my $result = 0;
			# test for first name
			my $gefunden = '';
			for (my $i = 0; $i <= $vorname_anzahl; $i++) {
					
				if ( (index ($title , $first_name[$i].' ') == 0) # Klaus Meier
				  or (index ($title ,$first_name[$i].'-') == 0)  # Klaus-Dieter Meier
				  or (index ($title ,'. '.$first_name[$i]) == 1) # R. Klaus Meier
					)
				{
					$result = 1;
					$gefunden = $first_name[$i];
				}
			}	

			if ($result == 1) {
				my $test_output = Text_verkuerzen( $text, 100);
				fehlermeldung($error_code, $title, $gefunden.' - <nowiki>'.$test_output.'…</nowiki>');
				#print "\t".$title."\n";
				#print "\t\t".$gefunden."\n";
				#print "\t007\n";
				#die;
			}
		}
	}
}

sub F009_check_for_last_name{
	my $error_code = 9; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
			$error_description[$error_code][0] = 'Kategorien fehlen, aber Nachname gefunden';
			$error_description[$error_code][1] = 'Dieser Artikel hat keine Personendaten und auch keine Kategorien. Es wurde aber im Artikeltitel ein Nachname gefunden. Vielleicht ist es eine Person. Bitte kategorisieren diesen Artikel. Bei der nächsten Überprüfung wird der Artikel erneut überprüft.';
			$error_description[$error_code][2] = 3;

	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {	

	
		if ($all_categories eq '' and $is_redirect == 0 and $has_pd == 0 and index($text, '{{')== -1) {
			
			my $result = 0;
			# test for first name
			my $gefunden = '';
			for (my $i = 0; $i <= $familiennamen_anzahl; $i++) {
					
				if (   (index ($title ,' '.$family_name[$i]) > 0) 		# Klaus Meier
					or (index ($title ,' '.$family_name[$i].' (') > 0)  # Klaus-Dieter Meier (Fotograf)
					or (index ($title ,'. '.$family_name[$i]) > 0)		# R. Meier
					)
				{
					$result = 1;
					$gefunden = $family_name[$i];
				}
			}	

			if ($result == 1) {
				my $error_code = 9; 
				fehlermeldung($error_code, $title, $gefunden.' - <nowiki>'.substr($text, 0, 100).'…</nowiki>');
				#print "\t".$title."\n";
				#print "\t\t".$gefunden."\n";
				#print "\t008\n";
				#die;
			}
		}
	}
}



sub F010_Reihenfolge_fehlerhaft{
	my $error_code = 10; 
	my $attribut = $_[0];
	my $test_text = $_[1];
	if ($attribut eq 'get_description') {
		$error_description[$error_code][0] = 'Datenfeld falsch sortiert';
		$error_description[$error_code][1] = 'Die Datenfelder sind in der falschen Reihenfolge sortiert.';
		$error_description[$error_code][2] = 1;


	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {	

		
		if ($test_text ne '') {
			my $reihenfolge = 'ok';
			my $fehlertext = '';
			my $pos_name	 = index ($test_text, 'NAME=');
			my $pos_aname	 = index ($test_text, 'ALTERNATIVNAMEN=');
			my $pos_kurz	 = index ($test_text, 'KURZBESCHREIBUNG=');
			my $pos_gdatum	 = index ($test_text, 'GEBURTSDATUM=');
			my $pos_gort	 = index ($test_text, 'GEBURTSORT=');
			my $pos_sdatum	 = index ($test_text, 'STERBEDATUM=');
			my $pos_sort	 = index ($test_text, 'STERBEORT=');

			if (	$pos_name >= $pos_aname
				or $pos_name >= $pos_kurz
				or $pos_name >= $pos_gdatum
				or $pos_name >= $pos_gort	
				or $pos_name >= $pos_sdatum
				or $pos_name >= $pos_sort) 
				{$reihenfolge = 'false';
				 $fehlertext = 'NAME=';
				 }
				
			if (    $pos_aname >= $pos_kurz
				or $pos_aname >= $pos_gdatum
				or $pos_aname >= $pos_gort	
				or $pos_aname >= $pos_sdatum
				or $pos_aname >= $pos_sort) 
				{$reihenfolge = 'false';
				 $fehlertext = 'ALTERNATIVNAMEN=';
				 }
				 
			if (    $pos_kurz >= $pos_gdatum
				or $pos_kurz >= $pos_gort	
				or $pos_kurz >= $pos_sdatum
				or $pos_kurz >= $pos_sort) 
				{$reihenfolge = 'false';
				 $fehlertext = 'KURZBESCHREIBUNG=';
				 }
				 
			if (    $pos_gdatum >= $pos_gort	
				or $pos_gdatum >= $pos_sdatum
				or $pos_gdatum >= $pos_sort) 
				{$reihenfolge = 'false';
				 $fehlertext = 'GEBURTSDATUM=';
				 }

			if (    $pos_gort >= $pos_sdatum
				or $pos_gort >= $pos_sort) 
				{$reihenfolge = 'false';
				 $fehlertext = 'GEBURTSORT=';
				 }

			if (  $pos_sdatum >= $pos_sort) 
				{$reihenfolge = 'false';
				 $fehlertext = 'STERBEDATUM=';
				 }	
			
			if ($reihenfolge eq 'false'){
				fehlermeldung($error_code, $title, '"'.$fehlertext.'"');
				#print $code."\t".$title."\t".'<nowiki>'. $fehlertext.'</nowiki>'."\n";
			}
			
		}
	}
		 
}




sub F011_Zuviele_Leerzeichen {
	my $error_code = 11; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
		$error_description[$error_code][0] = 'Datenfeld mit vielen Leerzeichen';
		$error_description[$error_code][1] = 'In einem Datenfeld sind zu viele Leerzeichen hintereinander (z.B. "3  . Januar 2008"). Alle hier ausgegeben Datenfelder beinhalten mindestens 2 Leerzeichen hintereinander.';
		$error_description[$error_code][2] = 3;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {	
		F011_Zuviele_Leerzeichen2($error_code, $name);
		F011_Zuviele_Leerzeichen2($error_code, $aname);
		F011_Zuviele_Leerzeichen2($error_code, $kurz);
		F011_Zuviele_Leerzeichen2($error_code, $geburtsdatum);
		F011_Zuviele_Leerzeichen2($error_code, $geburtsort);
		F011_Zuviele_Leerzeichen2($error_code, $sterbedatum);
		F011_Zuviele_Leerzeichen2($error_code, $sterbeort);
	}
}

sub F011_Zuviele_Leerzeichen2{
	my $error_code = $_[0];
	my $datenfeld = $_[1];
	my $datenfeld2 = $datenfeld;
	$datenfeld2 =~ s/ //g;
	
	if ($datenfeld ne '' 
	    and index ($datenfeld, '  ') > -1
		and length( $datenfeld2 ) > 1
		)
	{
		fehlermeldung($error_code, $title, '"'.$datenfeld.'"');
		#print $code."\t".$title."\t".'<nowiki>'. $datenfeld.'</nowiki>'."\n";
	}
}




sub F012_Leerzeichen_vor_Komma{
	my $error_code = 12; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
		$error_description[$error_code][0] = 'Datenfeld mit Leerzeichen vor dem Komma';
		$error_description[$error_code][1] = 'In einem Datenfeld ist ein Leerzeichen vor einem Komma (z.B. "Mustermann , Max").';
		$error_description[$error_code][2] = 3;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		if ( 
			index($name, ' ,') >-1 or
			index($aname, ' ,') >-1 or
			index($kurz, ' ,') >-1 or
			index($geburtsdatum, ' ,') >-1 or
			index($geburtsort, ' ,') >-1 or
			index($sterbedatum, ' ,') >-1 or
			index($sterbeort, ' ,') >-1
			) {

			my $test_data = '';
			$test_data = $name 			if (index($name, ' ,') >-1);
			$test_data = $aname 		if (index($aname, ' ,') >-1);
			$test_data = $kurz 			if (index($kurz, ' ,') >-1 );
			$test_data = $geburtsdatum 	if (index($geburtsdatum, ' ,') >-1 );
			$test_data = $geburtsort 	if (index($geburtsort, ' ,') >-1 );
			$test_data = $sterbedatum 	if (index($sterbedatum, ' ,') >-1);
			$test_data = $sterbeort		if (index($sterbeort, ' ,') >-1);
			
			fehlermeldung($error_code, $title, '"'.$test_data.'"');
			#print "\t".$title."\n";
			#print "\t\t".$name."\n";
			#die;
		}
	}
}

sub F013_Verlinkung_schlecht{
	my $error_code = 13; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
		$error_description[$error_code][0] = 'Datenfeld mit schlechter Verlinkung';
		$error_description[$error_code][1] = 'In einem Datenfeld ist die Verlinkung schlecht. Hier stehen Leerzeichen vor oder nach den eckigen Klammern (z.B. <nowiki>[[ Test]], [[ ]], [[Test ]]</nowiki>). ';
		$error_description[$error_code][2] = 3;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		F013_Verlinkung_schlecht2($error_code, $name);
		F013_Verlinkung_schlecht2($error_code, $aname);
		F013_Verlinkung_schlecht2($error_code, $kurz);
		F013_Verlinkung_schlecht2($error_code, $geburtsdatum);
		F013_Verlinkung_schlecht2($error_code, $geburtsort);
		F013_Verlinkung_schlecht2($error_code, $sterbedatum);
		F013_Verlinkung_schlecht2($error_code, $sterbeort);
	}
}

sub F013_Verlinkung_schlecht2 {
	my $error_code = $_[0];
	my $test_data = $_[1];
	my $test_data1 = $_[1];
	my $test_data2 = $_[1];
	my $test_data3 = $_[1];
	if (  $test_data ne '' and
		(  $test_data1 =~ /\[\[ / 
		or $test_data2 =~ / \]\]/ 
		or $test_data3 =~ /\[\[( )*?\]\]/ 
		)
	){
		fehlermeldung($error_code, $title, '<nowiki>'.$test_data.'</nowiki>');	
	}
}

######################################################################

sub F014_Leerzeichen_nach_Komma{
	my $error_code = 14; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
		$error_description[$error_code][0] = 'Datenfeld ohne Leerzeichen nach Komma';
		$error_description[$error_code][1] = 'In einem Datenfeld fehlt ein Leerzeichen nach einem Komma (z.B. "Mustermann,Max"). Bekannte Ausnahmen sind: Dr. Scissors, Stiletto (Künstler).';
		$error_description[$error_code][2] = 3;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		F014_Leerzeichen_nach_Komma2($error_code, $name);
		F014_Leerzeichen_nach_Komma2($error_code, $aname);
		F014_Leerzeichen_nach_Komma2($error_code, $kurz);
		F014_Leerzeichen_nach_Komma2($error_code, $geburtsdatum);
		F014_Leerzeichen_nach_Komma2($error_code, $geburtsort);
		F014_Leerzeichen_nach_Komma2($error_code, $sterbedatum);
		F014_Leerzeichen_nach_Komma2($error_code, $sterbeort);
	}
}

sub F014_Leerzeichen_nach_Komma2{
	my $error_code = $_[0];
	my $test_data = $_[1];
	if ( $test_data =~ /(,)[^ ]/ 
		and $title ne 'Dr. Scissors'			# 3,1415
		and $title ne 'Stiletto (Künstler)'		#Ausnahme: Stiletto Studio,s (amtlich registrierter Künstlername) 

		) {
		fehlermeldung($error_code, $title, $test_data);
		#print $error_code."\t".$title."\t".'<nowiki>'. $test_data.'</nowiki>'."\n";
	}
}




################################################################################

sub F015_Name_mit_eckiger_Klammer{
	my $error_code = 15; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
		$error_description[$error_code][0] = 'Name mit eckigen Klammern ';
		$error_description[$error_code][1] = 'Das Feld NAME sollte keine Verlinkung enthalten. Es dient nur zur korrekten alphabetischen Sortierung und sollte keine eckigen Klammern enthalten.';
		$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		if (    $title ne 'Naqyrjinsan...'
			and $title ne 'Ka-nacht...') {
			if ( index($name, '[') >-1 or index($name, ']') >-1) {
				fehlermeldung($error_code, $title, $name);
			}
		}
	}
}

sub F016_Name_mit_roemischer_Zahl{
	my $error_code = 16; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
			$error_description[$error_code][0] = 'Name mit römischen Zahlen';
			$error_description[$error_code][1] = 'Das Feld NAME hat eine römischer Zahl am Anfang z.B. "XI., Alexander" richtige Schreibweise ist "Alexander XI.".';
			$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		my $test_name= $name;
		if ( $test_name =~ /^[ ]?+[XVI\/\.]+\.,/ ) {
			fehlermeldung($error_code, $title, $name);
			#print $title."\n";
			#print $name."\n";
			#die;
		}
	}
}

sub F017_Name_ist_kuerzer_als_Artikelname {
	my $error_code = 17; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
		$error_description[$error_code][0] = 'Name ist kürzer als Artikelname';
		$error_description[$error_code][1] = 'Das Feld NAME ist kürzer als der Artikelname, das heißt es fehlen Namensbestandteile bei der Sortierung (z.B. Ute Meier <> Meier, U.).';
		$error_description[$error_code][2] = 0;	#3
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		if ( 
			$name ne '' 
			and length($name) < length($title)
			and $aname eq ''
			and index ($title, '(') == -1
			and index ($title, ' von ') == -1
			and index ($title, ' de ') == -1
			and index ($title, ' da ') == -1
			and index ($title, ' zu ') == -1
			and index ($title, '.') == -1
			and index ($title, '“') == -1
			and index ($title, ' der ') == -1
			and index ($title, ' of ') == -1
			and index (lc($title), 'junior') == -1
			and index (lc($title), 'senior') == -1
			and index (lc($title), 'prinz') == -1
			and length($geburtsjahr) == 4
			and substr($geburtsjahr, 0, 2) eq '19'
			) {
				fehlermeldung($error_code, $title, $name);
		}
	}
}

sub F018_Name_hat_Komma_am_Ende{
	my $error_code = 18; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Name mit Komma am Ende';
				$error_description[$error_code][1] = 'Das Feld NAME sollte am Ende kein Komma haben. Entweder ist es ein Tippfehler oder die Vornamen wurden nicht mit eingetragen.';
				$error_description[$error_code][2] = 2;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		if ( $name ne '' 
			#and substr($name, length($name)-1,1) eq ","
			and $name =~ /,[ ]?+$/) {
				fehlermeldung($error_code, $title, $name);
		}
	}
}


sub F019_Name_erster_Buchstabe_klein{
	my $error_code = 19; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
			$error_description[$error_code][0] = 'Name mit kleinem ersten Buchstaben';
			$error_description[$error_code][1] = 'Das Feld NAME dient zur alphabetischen Sortierung. Der erste Buchstabe sollte dort eigentlich immer groß sein. (Falsch: "de Gaulle, Charles"; Korrekt: "Gaulle, Charles de"). Es gibt aber einige Ausnahmen z.B. bei arabischen Namen oder Künstlernamen. Deshalb ist die Auflistung nicht zu 100% richtig, aber es werden eben einige fehlerhafte Artikel mit angezeigt. ';
			$error_description[$error_code][2] = 0;	#3
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {

		if ( $name ne '' 
			and int(ord(substr($name, 0,1))) >=97 
			and int(ord(substr($name, 0,1))) <=122 
			and index($name,'ad-') <= 0	
			and index($name,'ad-') <= 0
			and index($name,'al-') <= 0
			and index($name,'an-') <= 0
			and index($name,'ar-') <= 0	
			and index($name,'as-') <= 0
			and index($name,'asch-') <= 0	
			and index($name,'ash-') <= 0	
			and index($name,'at-') <= 0
			and index($name,'az-') <= 0	
			and index($name,'ed-') <= 0	
			and index($name,'el-') <= 0
			and index($name,'ibn') <= 0	
			) {
				fehlermeldung($error_code, $title, $name);
				#print $title."\n";
				#print $name."\n";
				#die;
		}
	}
}


sub F020_Name_ein_Zeichen_kuerzer_als_Title{
	my $error_code = 20; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
			$error_description[$error_code][0] = 'Name mit weniger Zeichen als der Titel';
			$error_description[$error_code][1] = 'Das Feld NAME hat im Vergleich zum Artikeltitel eine minimal andere Schreibweise. Wenn im Titel kein Punkt und keine Klammer vorkommt, dann wird überprüft ob die Zeichenanzahl von Titel und Datefeld NAME identisch ist. Normal ist eine Abweichung von einem Zeichen ( Titel="Ute Meier" = 9 Zeichen, Name="Meier, Ute" = 10 Zeichen). Sollte aber der Name z.B. ein Zeichen kürzer sein als der Titel, dann deutet das auf einen Tippfehler hin. ';
			$error_description[$error_code][2] = 0; #3
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		if ( $name ne '' 
			and length($name) +1 == length($title)
			and index ($title, '(') == -1
			and index ($title, '.') == -1
			) {
				fehlermeldung($error_code, $title, $name);
				#print $title."\n";
				#print $name."\n";
		}
	}
}


sub F021_Name_Leerzeichen_nach_Punkt{
	my $error_code = 21; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Name ohne Leerzeichen nach Punkt';
				$error_description[$error_code][1] = 'Das Feld NAME hat an einer Stelle direkt hinter einem Punkt kein Leerzeichen (z.B. Johann I.von Hoya). Meist ist es ein Tippfehler.';
				$error_description[$error_code][2] = 3;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		my $test_name = $name;
		my $test_title = $title;
		if ( $name ne '' 
			and $test_name =~ /\.[a-z]/g 
			and not $test_title =~ /\.[a-z]/g 
			) {
				fehlermeldung($error_code, $title, $name);
				#print "\t".$title."\n";
				#print "\t\t\t".$name."\n";
		}
	}
}


sub F022_Name_mit_akademischem_Grad{
	my $error_code = 22; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Name mit akademischem Grad ';
				$error_description[$error_code][1] = 'Das Feld NAME enthält einen akademischen Grad (Prof., Dr.), was aber für die alphabetische Sorierung nicht benötigt wird.';
				$error_description[$error_code][2] = 2;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		if ( $name ne '' 
			and ((index ($name, 'Prof')> -1 and  index ($title, 'Prof') == -1)
			or (index ($name, 'Dr.')> -1 and  index ($title, 'Dr.') == -1)
			or (index ($name, 'Dipl')> -1 and  index ($title, 'Dipl') == -1))
			) {
				fehlermeldung($error_code, $title, $name);
				#print $title."\n";
				#print $name."\n";
		}
	}
}

sub F023_Name_ohne_Umlaut{
	my $error_code = 23; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Name ohne Umlaut';
				$error_description[$error_code][1] = 'Der Titel des Artikels enthält mindestens einen deutschen Umlaut (äüöÄÜÖß), der nicht im Datenfeld NAME gefunden werden konnte. Es ist im Datenfeld NAME nicht erforderlich Umlaute zu ersetzen (Müller bleibt Müller und wird nicht Mueller).';
				$error_description[$error_code][2] = 2;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		my $test_name = $name;
		my $test_title = $title;
		if ( $name ne '' 
		
			and ( 
						(index(lc($title), 'ä')>-1 and index(lc($name), 'ä')==-1)
					or	(index(lc($title), 'ü')>-1 and index(lc($name), 'ü')==-1)
					or	(index(lc($title), 'ö')>-1 and index(lc($name), 'ö')==-1)
					or	(index(lc($title), 'ß')>-1 and index(lc($name), 'ß')==-1)
			)	
					
			and index (lc($title), 'jünger') == -1
			and index (lc($title), 'ältere') == -1
			and index (lc($title), '(') == -1
			#and index (lc($title), 'von') == -1
			#and index (lc($title), 'zu') == -1
			) {
				fehlermeldung($error_code, $title, $name);
				#print "\t".$title."\n";
				#print "\t\t\t".$name."\n";
		}
	}
}

sub F024_Name_Leerzeichen_nach_Komma{
	my $error_code = 24; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Name ohne Leerzeichen nach Komma';
				$error_description[$error_code][1] = 'Das Feld NAME hat an einer Stelle direkt hinter einem Komma kein Leerzeichen (z.B. "Meier,Udo" ). Meist ist es ein Tippfehler.';
				$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		my $test_name = $name;
		my $test_name1 = $name;
		my $test_title = $title;
		if ( $name ne '' 
			and $test_name1 =~ /,[^ ]/g 
			#and not $test_title =~ /\.[a-z]/g 
			) {
				fehlermeldung($error_code, $title, $name);
				#print "\t".$title."\n";
				#print "\t\t\t".$name."\n";
				#die;
		}
	}
}

sub F025_Name_ist_leer{
	my $error_code = 25; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Name ohne Inhalt';
				$error_description[$error_code][1] = 'Das Feld NAME hat keinen Text.';
				$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		my $test_name = $name;
		$test_name =~ s/ //g;
		if ( $test_name eq '' and $text ne ''
			) {
				fehlermeldung($error_code, $title, $name);
				#print "\t".$title."\n";
				#print "\t\t\t".$name."\n";
				#die;
		}
	}
}


sub F026_Name_ohne_Leerzeichen{
	my $error_code = 26; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Name ohne Leerzeichen';
				$error_description[$error_code][1] = 'Das Feld NAME hat kein Leerzeichen, obwohl im Artikeltitel ein Leerzeichen enthalten ist. Das Feld Name soll genau die selben Namensbestandteile enthalten, wie im Titel des Artikels enthalten sind. Es darf also nix hinzugefügt oder weggelassen werden. (Ausnahmen bisher: Brendan der Reisende, Nikanor aus Stageira) ';
				$error_description[$error_code][2] = 2;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		if ( $name ne '' 
			and index ($title, ' ')>-1
			and index ($name, ' ') ==-1
			and index ($title, ' (') ==-1
			and not $title =~ /^[ ]?+[^ ]+ von/
			
			#Ausnahmen
			and $title ne 'Brendan der Reisende'
			and $title ne 'Nikanor aus Stageira'
			) {

				fehlermeldung($error_code, $title, $name);
				#print "\t".$title."\n";
				#print "\t\t\t".$name."\n";
				#die;
		}
	}
}

sub F027_Name_Anfang_mit_zwei_Grossbuchstaben{
	my $error_code = 27; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Name mit zwei Großbuchstaben am Anfang';
				$error_description[$error_code][1] = 'Das Feld NAME hat zwei Großbuchstaben am Anfang (z.B. "MEier, Ute" ). Meist ist es ein Tippfehler.';
				$error_description[$error_code][2] = 2;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		my $test_name = $name;
		my $test_name1 = $name;
		my $test_title = $title;
		if ( $name ne '' 
			and $test_name1 =~ /^[ ]?+[A-Z][A-Z]/g 
			and not $test_title =~ /[A-Z][A-Z]/g 
			#and not $test_title =~ /\.[a-z]/g 
			) {
				fehlermeldung($error_code, $title, $name);
				#print "\t".$title."\n";
				#print "\t\t\t".$name."\n";
				#die;
		}
	}
}

sub F028_Name_mit_Text{
	my $error_code = 28; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Name mit Text';
				$error_description[$error_code][1] = 'Das Feld NAME enhält Text (z.B. "NAME=Ute Meier war ein" oder "NAME=Ute Meier auch: Ute Schmidt" ). Das Datenfeld NAME sollte nur den Artikeltitel in korrekte Sortierungsreihenfolge enthalten.';
				$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
	
		my $test_name = $name;
		my $test_name1 = $name;
		my $test_name2 = $name;
		my $test_name3 = $name;
		my $test_name4 = $name;
		my $test_name5 = $name;
		my $test_name6 = $name;
		my $test_title = $title;
		if ( $name ne '' 
			and (   $test_name1 =~ / ist/g 
				 or $test_name2 =~ / war/g
				 or $test_name3 =~ / wurde/g
				 or $test_name4 =~ / ein/g
				 or $test_name5 =~ / :/g
				 #or ( index (lc($test_name6) , 'name') > -1 and index(lc($test_title) , 'name') == -1)
				 )
			#and not $test_title =~ /\.[a-z]/g 
			) {
				fehlermeldung($error_code, $title, $name);
				#print "\t".$title."\n";
				#print "\t\t\t".$name."\n";
				#die;
		}
	}
}

sub F029_Name_und_Apostroph{
	my $error_code = 29; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Name mit Apostroph';
				$error_description[$error_code][1] = 'Das Feld NAME enthält ein Apostroph oder ähnliches Zeichen, welches nicht so im Titel vorkommt. Am besten es wird das Zeichen direkt aus dem Titel erneut in das Datenfeld kopiert, damit auch wirklich das richtige Zeichen verwendet wird.';
				$error_description[$error_code][2] = 2;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		my $test_name = $name;
		$test_name = lc ($test_name);
		my $test_title = $title;
		$test_title = lc $test_title;
		my $pos = index($test_title, '(');
		$test_title = substr ($test_title, 0, $pos -1) if ($pos > -1);
		
		
		if ( $name ne '' 
		
			and ( 
						(index($test_title, chr(96) )>-1 and index($test_name, chr(96) )==-1)
					or	(index($test_title, chr(39) )>-1 and index($test_name, chr(39) )==-1)
					or	(index($test_title, chr(145) )>-1 and index($test_name, chr(145) )==-1)
					or	(index($test_title, chr(146) )>-1 and index($test_name, chr(146) )==-1)
					or	(index($test_title, chr(203) )>-1 and index($test_name, chr(203) )==-1)
					or	(index($test_title, chr(226) )>-1 and index($test_name, chr(226) )==-1)
					or	(index($test_title, chr(194) )>-1 and index($test_name, chr(194) )==-1)
			)	
			and index (lc($title), '(') == -1
			#and index (lc($title), 'von') == -1
			#and index (lc($title), 'zu') == -1
			) {
				fehlermeldung($error_code, $title, $title .' vs. '. $name);
				#print "\t".$title."\n";
				#print "\t\t\t".$name."\n";
				#die;
		}
	}
}

sub F030_Name_ohne_Komma{
	my $error_code = 30; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Name ohne Komma';
				$error_description[$error_code][1] = 'Das Feld NAME hat kein Komma. Der Titel ist gleich dem Datenfeld NAME und es gibt keine ALTERNATIVNAMEN, also ist es auch kein Künstlername. Das Geburtsjahr liegt nach 1945 und in der Kurzbeschreibung steht nichts von "koreanisch", "chinesisch" oder "vietnamesisch" … , weshalb es sich auch nicht um einen asiatischen Namen handelt. Auch "isländisch" wurde nicht gefunden. Also eigentlich müsste der Name nach dem Muster "Nachname, Vorname", also mit Komma schreibbar sein. Ausnahme: asiatische Namen und Adlige! Einige Artikel kann man auch durch das Eintragen eines Alternativnamen von der Liste bekommen.';
				$error_description[$error_code][2] = 2;
				$error_description[$error_code][1] = regelbasierter_hinweistext($error_description[$error_code][1]);
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {

		my $test_name = $name;
		if ( $name ne '' 
			and $aname eq ''
			and $title eq $name
			and index($title, ' ') > -1
			and index($title, ',') == -1
			and $geburtsdatum =~ /19[4-9][0-9]/
			and (index( lc($kurz_plain) , 'chinesisch') == -1
				 and index( lc($kurz_plain) , 'isländisch') == -1 
				 and index( lc($kurz_plain) , 'vietnamesisch') == -1 
				 and index( lc($kurz_plain) , 'koreanisch') == -1
				 and index( lc($kurz_plain) , 'kambodschanisch') == -1
				 and index( lc($kurz_plain) , 'malaysisch') == -1
				 and index( lc($kurz_plain) , 'taiwanisch') == -1
				 and index( lc($kurz_plain) , 'indonesisch') == -1
				 and index( lc($kurz_plain) , 'birmanisch') == -1
				 and index( lc($kurz_plain) , 'china') == -1
				 and index( lc($kurz_plain) , 'bengalisch') == -1
				 and index( lc($kurz_plain) , 'japanisch') == -1
				 and index( lc($kurz_plain) , 'myanmarisch') == -1
				 and index( lc($kurz_plain) , 'indisch') == -1
				 and index( lc($kurz_plain) , 'marokkanisch') == -1
				 and index( lc($kurz_plain) , 'malaysia') == -1
				 and index( lc($kurz_plain) , 'myanmar') == -1
				 and index( lc($kurz_plain) , 'thailändisch') == -1
				 and index( lc($kurz_plain) , 'sri-lankisch') == -1
				 and index( lc($kurz_plain) , 'vietnam') == -1
				 and index( lc($kurz_plain) , 'palästinensisch') == -1
				 and index( lc($kurz_plain) , 'singapurisch') == -1
				 and index( lc($kurz_plain) , 'saudi-arabisch') == -1
				 and index( lc($kurz_plain) , 'islam') == -1
				 and index( lc($kurz_plain) , 'jordanisch') == -1
				 and index( lc($kurz_plain) , 'lama') == -1		 
				 and index( lc($kurz_plain) , 'prinz') == -1
				 and index( lc($kurz_plain) , 'scheich') == -1
				 and index( lc($kurz_plain) , 'patriarch') == -1
				 and index( lc($kurz_plain) , 'rapper') == -1
				 and index( lc($kurz_plain) , 'könig') == -1
				 and index( lc($kurz_plain) , 'tibetisch') == -1
				 and index( lc($kurz_plain) , 'herzog') == -1
				)
			and $title ne 'Baby Fae'
			and $title ne 'DJ Hollywood'
			and $title ne 'DJ Stylewarz'
			and $title ne 'DJ Mosaken'
			and $title ne 'Jack the Stripper'
			and $title ne 'Kanda Bongo Man'
			and $title ne 'Lê Quan Ninh'
			and $title ne 'Papa A.P.'
			and $title ne 'Sara K.'
			and $title ne 'TAKI 183'
			and $title ne 'Schwester Sara'
			and $title ne 'Y Sa Lo'
			
			
			
		){
				fehlermeldung($error_code, $title, $name);
				#print "\t".$title."\n";
				#print "\t\t\t".$name."\n";
				#die;	
		}
	}
}

sub F031_Name_vertauscht{
	my $error_code = 31; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Name ohne Komma, aber Namen vertauscht';
				$error_description[$error_code][1] = 'Das Feld NAME hat kein Komma, aber der erste Namensteil des Titels steht hier hinter dem zweiten Namensteil (z.B. Titel= "Max Meier" und NAME="Meier Max"). Achtung: für römische Namen gilt diese Fehlerkartegorie nicht.';
				$error_description[$error_code][2] = 2;
				$error_description[$error_code][1] = regelbasierter_hinweistext($error_description[$error_code][1]);
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		my $test_name = $name;
		my $pos1 = index( $title, ' ');
		my $test_vorname1= substr($title, 0, $pos1);
		my $pos2 = index( $name, ' ');
		my $test_nachname2= substr($name, $pos2+1);	
		my $test_vorname2= substr($name, 0, $pos2);
		my $test_title = $title;
		
		#print 'x'.$title.'x'."\n";
		#print 'x'.$test_vorname1.'x'."\n";
		#print 'x'.$test_nachname2.'x'."\n";
		#print 'x'.$test_vorname2.'x'."\n";
		#die;

		if ( $name ne '' 
				and index($name, ',') == -1
				and index($name, ' ') >0
				and $test_vorname1 eq $test_nachname2
				and $test_nachname2 ne $test_vorname2
				and not ($test_title =~ /^[^ ] \(/) )
				{
				fehlermeldung($error_code, $title, $name);
		}
	}
}


#################################################


sub F032_Alternativnamen_ohne_Semikolon{
	my $error_code = 32; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Alternativnamen ohne Semikolon';
				$error_description[$error_code][1] = 'Das Feld ALTERNATIVNAMEN kann weitere Namen der Person enthalten. Jeder einzelne Name muss durch ein Semikolon getrennt sein (z.B. Meier, Ute; Krause, Ute (Geburtsname)).';
				$error_description[$error_code][2] = 2;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		if ( $aname ne '' 
			and index ($aname, ';') == -1 
			and index ($aname, ',') > -1
			and index ($aname, ',', index ($aname, ',')+1) >-1
			and not $aname =~ /\([^\)]+,/				#kein Komma in der Klammer
			
			and index ($aname, 'Duke') == -1
			and index ($aname, 'Baron') == -1
			and index ($aname, 'Viscount') == -1
			and index ($aname, 'Herzog') == -1
			and index ($aname, 'Marquess') == -1
			and index ($aname, 'marquis') == -1
			and index ($aname, 'Graf') == -1
			and index ($aname, 'Earl') == -1
			and index ($aname, 'duc de') == -1
			and index ($aname, 'Marquis') == -1
			and index ($aname, 'Reichsfreiherr') == -1
			and index ($aname, 'Duc d') == -1
			and index ($aname, 'Freiherr') == -1
			and index ($aname, 'comte de') == -1
			and index ($aname, 'Señor de') == -1
			and index ($aname, 'Prinz zu') == -1
			and index ($aname, 'vicomte de') == -1			

			)
			 {
				fehlermeldung($error_code, $title, $aname);
				#print $title."\n";
				#print $aname."\n";
				#die;
		}
	}
}

sub F033_Alternativnamen_nur_ein_Zeichen{
	my $error_code = 33; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Alternativnamen nur ein Zeichen lang';
				$error_description[$error_code][1] = 'Das Feld ALTERNATIVNAMEN enthält bei folgenden Namen nur ein Zeichen.';
				$error_description[$error_code][2] = 2;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		if ( $aname ne '' and
			$aname ne ' '
			and length($aname) == 1
			)
			 {
				fehlermeldung($error_code, $title, $aname);
				#print $title."\n";
				#print $aname."\n";
				#die;
		}
	}
}


sub F034_Alternativnamen_mit_fehlerhaften_Klammern{
	my $error_code = 34; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Alternativnamen mit fehlerhaften Klammern';
				$error_description[$error_code][1] = 'Das Feld ALTERNATIVNAMEN enthält eine Klammer aber nicht den entsprechenden öffnenden oder schließenden Klammerteil. ';
				$error_description[$error_code][2] = 2;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		if ( $aname ne '' 
			and (
			   (index($aname , '(') > -1 and index($aname , ')') == -1)
			or (index($aname , ')') > -1 and index($aname , '(') == -1)
			or (index($aname , '{') > -1 and index($aname , '}') == -1)
			or (index($aname , '}') > -1 and index($aname , '{') == -1)
			or (index($aname , '[') > -1 and index($aname , ']') == -1)
			or (index($aname , ']') > -1 and index($aname , '[') == -1)
			))
			 {
				fehlermeldung($error_code, $title, $aname);
				#print $title."\n";
				#print $aname."\n";
				#die;
		}
	}
}


sub F035_Alternativnamen_ohne_Leerzeichen_nach_Komma{
	my $error_code = 35; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Alternativnamen ohne Leerzeichen nach Komma';
				$error_description[$error_code][1] = 'Das Feld ALTERNATIVNAMEN hat ein Komma, dem kein Leerzeichen folgt. Bekannte Ausnahmen sind: Stiletto Studio,s.';
				$error_description[$error_code][2] = 3;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		my $test_data = $aname;
		my $test_data1 = $aname;
		
		if ($test_data ne '' 
			and $test_data1 =~ /,[^ 1234567890]/g 
			#and $test_data2 =~ /,[^1234567890]/g )
			and $title ne 'Stiletto (Künstler)'				#Ausnahme: Stiletto Studio,s (amtlich registrierter Künstlername) 
			)
			{
				fehlermeldung($error_code, $title, $aname);
				#print "\t".$title."\n";
				#print "\t\t\t". $aname."\n";
				#die if ($title eq "Dr. Scissors" ) ;		
		}	
	}
}




sub F036_Alternativnamen_mit_eckiger_Klammer{
	my $error_code = 36; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
			$error_description[$error_code][0] = 'Alternativnamen mit eckiger Klammer';
			$error_description[$error_code][1] = 'Im Feld ALTERNATIVNAMEN sind die Zeichenketten "[Künstlername]" und "[Geburtsname]" zu finden. Besser sind einfache runde Klammern. (Bsp.:Schmidt, Johann Kaspar (wirklicher Name)).';
			$error_description[$error_code][2] = 3;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		if ($this_article_is_with_errors == 0) {
			F036_Alternativnamen_mit_eckiger_Klammer2($aname);
		}
	}
}

sub F036_Alternativnamen_mit_eckiger_Klammer2{
	my $test_data = $_[0];
	if (  $test_data ne '' and
		(index( $test_data , '[Künstlername]') > -1 
		or index( $test_data , '[Geburtsname]') > -1 
		or index( $test_data , '[Pseudonym]') > -1 )
		)
		{
			my $error_code = 36; 
			fehlermeldung($error_code, $title, $test_data);
			#print "\t".$title."\n";
			#print "\t\t\t".$test_data."\n";
			#die;
	}
}

sub F037_Alternativnamen_ohne_Leerzeichen_nach_Semikolon{
	my $error_code = 37; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Alternativnamen ohne Leerzeichen nach Semikolon';
				$error_description[$error_code][1] = 'Das Feld ALTERNATIVNAMEN hat ein Semikolon, dem kein Leerzeichen folgt. ';
				$error_description[$error_code][2] = 3;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		my $test_data = $aname;
		my $test_data1 = $aname;
		
		if ($test_data ne '' 
			and $test_data1 =~ /;[^ 1234567890]/g 
			#and $test_data2 =~ /,[^1234567890]/g )
			)
			{
				fehlermeldung($error_code, $title, $aname);
				#print "\t".$title."\n";
				#print "\t\t\t". $aname."\n";
			}
	}
}

sub F038_Alternativnamen_identisch_mit_Titel{
	my $error_code = 38; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Alternativnamen identisch mit Titel';
				$error_description[$error_code][1] = 'Das Feld ALTERNATIVNAMEN ist identisch mit dem Titel. Wenn das der Fall ist, ist es kein alternativer Name und kann entfernt werden.';
				$error_description[$error_code][2] = 2;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {	
		if ($aname ne '' 
			and $aname eq $title
			)
			{
				fehlermeldung($error_code, $title, $aname);
				#print "\t".$title."\n";
				#print "\t\t\t". $aname."\n";
			}
	}
}

sub F039_Alternativnamen_identisch_mit_Name{
	my $error_code = 39; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Alternativnamen identisch mit Name';
				$error_description[$error_code][1] = 'Das Feld ALTERNATIVNAMEN ist identisch mit dem Feld NAME. Wenn das der Fall ist, ist es kein alternativer Name und kann entfernt werden.';
				$error_description[$error_code][2] = 2;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {	
		if ($aname ne '' 
			and $aname eq $name
			)
			{
				fehlermeldung($error_code, $title, $name.' und '.$aname);
				#print "\t".$title."\n";
				#print "\t\t\t". $aname."\n";
			}
	}
}

sub F040_Alternativnamen_mit_Doppelpunkt{
	my $error_code = 40; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Alternativnamen mit Doppelpunkt';
				$error_description[$error_code][1] = 'Das Feld ALTERNATIVNAMEN enthält mindestens einen Doppelpunkt (z.B. "schewdisch: Michaili"). Alle Informationen zum Namen sollten hinter dem Namen in Klammern stehen z.B. "Michaili (schwedisch)".';
				$error_description[$error_code][2] = 2;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		my $test_kurz1 = $aname;
		my $test_kurz2 = $aname;
		if ( $aname ne '' 
			 and index($aname,':') > -1
			 and index($aname,'em:óu') == -1			# Megumi Ogata
			 and index($aname,'Flo:Pee') == -1			# Florian Penner
		   ) 
			 {

				fehlermeldung($error_code, $title, $aname);
				#print "\t".$title."\n";
				#print "\t\t\t".$aname."\n";
				#die;
		}
	}
}

sub F041_Alternativnamen_mit_Slash{
	my $error_code = 41; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Alternativnamen mit Slash';
				$error_description[$error_code][1] = 'Das Feld ALTERNATIVNAMEN einen Slash (z.B. "Meier, Gitta / Meier, Maria"). Alle Namen sollten durch Semikolon getrennt werden. Nationalitäten z.B. "schwedisch/deutsch" sollten mit Bindestrich geschrieben werden "schwedisch-deutsch". Ausnahmen wie bei dem Pseudonym "Future/Past" bitte melden.';
				$error_description[$error_code][2] = 2;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		my $test_kurz1 = $aname;
		my $test_kurz2 = $aname;
		if ( $aname ne '' 
			 and index($aname,'/') > -1
			 and $title ne 'Richard Gleim'
			 and $title ne 'Kido Witbooi'
			 and $title ne 'Kirk Degiorgio'
		   ) 
			 {
				fehlermeldung($error_code, $title, '<nowiki>'.$aname.'</nowiki>');
				#print "\t".$title."\n";
				#print "\t\t\t".$aname."\n";
				#die;
		}
	}
}

sub F042_Alternativnamen_mit_Pseudonym{
	my $error_code = 42; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
			$error_description[$error_code][0] = 'Alternativnamen mit Pseudonym';
			$error_description[$error_code][1] = 'Das Feld ALTERNATIVNAMEN enthält ein Namen, der als Pseudonym gekennzeichnet ist. Leider ist die Form mangelhaft. Es sollte ordentlich ausgeschrieben in Klammern stehen z.B. "Max75 (Pseudonym)". Auch eine Verlinkung des Wortes "Pseudonym" ist nicht notwendig.';
			$error_description[$error_code][2] = 2;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
	my $test_kurz1 = $aname;
	my $test_kurz2 = $aname;
	if ( $aname ne '' 
		 and ( index($aname,'(Pseud.') > -1
		 or index($aname,'[Ps') > -1
		 or index($aname,'Pseudonyme') > -1
		 )
	   ) 
		 {
			fehlermeldung($error_code, $title, $aname);
			#print "\t".$title."\n";
			#print "\t\t\t".$aname."\n";
			#die;
		}
	}
}

sub F043_Alternativnamen_mit_Hochkomma{
	my $error_code = 43; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Alternativnamen mit Hochkomma';
				$error_description[$error_code][1] = 'Das Feld ALTERNATIVNAMEN enthält ein Namen mit Hochkommas (z.B. „ oder “ ). Diese sind nicht notwendig, da kein Name hier hervorgehoben werden muss.';
				$error_description[$error_code][2] = 2;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		my $test_kurz1 = $aname;
		if ( $aname ne '' 
			 and ( index($aname,'„') > -1
			 or index($aname,'“') > -1
			 )
		   ) 
			 {
				fehlermeldung($error_code, $title, '<nowiki>'.$aname.'</nowiki>');
				#print "\t".$title."\n";
				#print "\t\t\t".$aname."\n";
				#die;
		}
	}
}

sub F044_Alternativnamen_mit_eckigen_Klammern{
	my $error_code = 44; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
			$error_description[$error_code][0] = 'Alternativnamen mit eckigen Klammern';
			$error_description[$error_code][1] = 'Das Feld ALTERNATIVNAMEN enthält Informationen in einfachen eckigen Klammern. Alle Informationen zu Namen sollen in runden Klammen eingebaut werden (z.B. falsch: "Max Müller [Spitzname]",  richtig: "Max Müller (Spitzname)".';
			$error_description[$error_code][2] = 2;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		my $test_aname = $aname;
		$test_aname =~ s/\[\[//g;
		$test_aname =~ s/\]\]//g;
		
		if ( $aname ne '' 
		     and index ($aname, 'Day[9]') == -1			# Sean Pott
			 and ( index($test_aname, '[' ) > -1
			 or index($test_aname, ']' ) > -1
			 )
		   ) 
			 {
				fehlermeldung($error_code, $title, $aname);
				#print "\t".$title."\n";
				#print "\t\t\t".$aname."\n";
				#die;
		}
	}
}

sub F045_Alternativnamen_mit_schlechtem_Anfang{
	my $error_code = 45; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Alternativnamen mit schlechtem Anfang';
				$error_description[$error_code][1] = 'Das Feld ALTERNATIVNAMEN einen schlechten Anfang. Entweder ist dort ein kleingeschriebenes Wort oder ein Sonderzeichen. Oder nach dem Semikolon kommt ein unnötiges Wort am Anfang eines Alternativnamens (z.B. "Krause, Alex; auch Krause, Alexander").';
				$error_description[$error_code][2] = 3;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		if ( $aname ne '' 
			 and (     $aname =~ /^(\(|\.|,|;)/
					or $aname =~ /^[a-zäüöß]+\.[ :-]/
					or $aname =~ /(^|;)[ ]?+gen/
					or $aname =~ /(^|;)[ ]?+auch/
					or $aname =~ /(^|;)[ ]?+früher/
					or $aname =~ /(^|;)[ ]?+alias/
					or $aname =~ /(^|;)[ ]?+bekannt/
					or $aname =~ /(^|;)[ ]?+meist /
					or $aname =~ /(^|;)[ ]?+ursprüngl/
				  )
			){
				fehlermeldung($error_code, $title, $aname);
				#print "\t".$title."\n";
				#print "\t\t\t".$aname."\n";
				#die;
		}
	}
}

#################################################

sub F046_Kurzbeschreibung_schlechter_Anfang{
	my $error_code = 46; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Kurzbeschreibung mit schlechtem Anfang';
				$error_description[$error_code][1] = 'Das Feld KURZBESCHREIBUNG sollte möglichst mit der Nationalität und dem Beruf anfangen (russischer Schriftsteller). Die hier aufgelisteten Artikel fangen aber mit Wörtern ( war, ehm, seit, ist, der, die, das) an, die nicht in das gewünscht Schema passen. Diese Kurzbeschreibung sollte im Rahmen einer Datenveredelung verbessert werden.'."\n".'* "ehm. Präsident von Dänemark" besser "dänischer Politiker, Präsident von Dänemark (1874-1881)';
				$error_description[$error_code][2] = 3;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {

		my $test_kurz = lc($kurz);
		if ( $kurz ne '' 
			and 
			   (   $test_kurz =~ /^[ ]?+ehm\./
				or $test_kurz =~ /^[ ]?+war /
				or $test_kurz =~ /^[ ]?+ist /
				or $test_kurz =~ /^[ ]?+seit/
				or $test_kurz =~ /^[ ]?+der /
				or $test_kurz =~ /^[ ]?+die /
				or $test_kurz =~ /^[ ]?+das /
				or $test_kurz =~ /^[ ]?+dem /
				or $test_kurz =~ /^[ ]?+den /
				or $test_kurz =~ /^[ ]?+des /
				or $test_kurz =~ /^[ ]?+und/
				or $test_kurz =~ /^[ ]?+von/
				or $test_kurz =~ /^[ ]?+ein /
				or $test_kurz =~ /^[ ]?+eine/
				or $test_kurz =~ /^[ ]?+ab /
				or $test_kurz =~ /^[ ]?+ehem\./
				or $test_kurz =~ /^[ ]?+ehemalig/
				or $test_kurz =~ /^[ ]?+zeitgenössisch/
				or $test_kurz =~ /^[ ]?+früher/
				or $test_kurz =~ /^[ ]?+historisch/
				or $test_kurz =~ /^[ ]?+vom /
				or $test_kurz =~ /^[ ]?+nach /
				or $test_kurz =~ /^[ ]?+bis /
				or $test_kurz =~ /^[ ]?+bei /
				or $test_kurz =~ /^[ ]?+beim /
				or $test_kurz =~ /^[ ]?+wurde /
				or $test_kurz =~ /^[ ]?+hatte /
				or $test_kurz =~ /^[ ]?+hat /
				or $test_kurz =~ /^[ ]?+tätig /
				or $test_kurz =~ /^[ ]?+im /
				or $test_kurz =~ /^[ ]?+in /
				or $test_kurz =~ /^[ ]?+für /
				or $test_kurz =~ /^[ ]?+auf /
				or $test_kurz =~ /^[ ]?+auch /
				or $test_kurz =~ /^[ ]?+am /
				or $test_kurz =~ /^[ ]?+als /
				or $test_kurz =~ /^[ ]?+ursprünglich /
				or $test_kurz =~ /^[ ]?+ungefähr /
			))
			 {
				fehlermeldung($error_code, $title, $kurz);
				#print $title."\n";
				#print $aname."\n";
				#die;
		}
	}
}


sub F047_Kurzbeschreibung_mit_Prozentzeichen{
	my $error_code = 47; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
			$error_description[$error_code][0] = 'Kurzbeschreibung mit Prozentzeichen';
			$error_description[$error_code][1] = 'Das Feld KURZBESCHREIBUNG enthält ein Prozentzeichen. Das ist meisten ein Indiz für falsche Verlinkung.';
			$error_description[$error_code][2] = 3;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		if ( $kurz ne '' 
			and  index($kurz , '%') >-1) {
			my $code = 301; 

			fehlermeldung($error_code, $title, $kurz);
			#print $title."\n";
			#print $kurz."\n";
			#die;
		}
	}
}

sub F048_Kurzbeschreibung_fehlt{
	my $error_code = 48; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
			$error_description[$error_code][0] = 'Kurzbeschreibung ist leer';
			$error_description[$error_code][1] = 'Das Feld KURZBESCHREIBUNG enthält überhaupt nichts. Oft verrutschen die Bearbeiter in der Zeile oder wissen nicht was eingetragen werden soll. Korrekte Angaben wären z.B.:'."\n".'* deutscher Boxer, Weltmeister im Fliegengewicht (1922)'."\n".'* französischer Adliger, Prinz von Paris'."\n".'*polnischer Politiker, Präsident von Polen (1874–1881)';
			$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		if ( $kurz eq '' and $text ne ''  ) {
			fehlermeldung($error_code, $title, '');
		}
	}
}

sub F049_Kurzbeschreibung_falsch_verlinkt{
	my $error_code = 49; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
			$error_description[$error_code][0] = 'Kurzbeschreibung mit falscher Verlinkung';
			$error_description[$error_code][1] = 'Das Feld KURZBESCHREIBUNG enhält nach entfernen der Verlinkungen noch eckige Klammern, was auf eine fehlerhafte Verlinkung schließen lässt.';
			$error_description[$error_code][2] = 2;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		if ( $kurz_plain ne '' 
			and ( index($kurz_plain , '[') >-1
			or  index($kurz_plain , ']') >-1 )
			) {
			fehlermeldung($error_code, $title, $kurz);
			#print $title."\n";
			#print $kurz."\n";
			#print $kurz_plain."\n";
			#die;
		}
	}
}

sub F050_Kurzbeschreibung_falsch_erstes_Zeichen{
	my $error_code = 50; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
		$error_description[$error_code][0] = 'Kurzbeschreibung mit falschem ersten Zeichen';
		$error_description[$error_code][1] = 'Das Feld KURZBESCHREIBUNG enhält ein unerwünschtes Zeichen an erster Stelle.';
		$error_description[$error_code][2] = 2;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		if ( $kurz ne '' 
			and ( substr($kurz, 0,1) eq ')'
			  or  substr($kurz, 0,1) eq ')'
			  or  substr($kurz, 0,1) eq '.'
			  or  substr($kurz, 0,1) eq ','
			  or  substr($kurz, 0,1) eq '*'
			  or  substr($kurz, 0,1) eq ']'
			  or  substr($kurz, 0,1) eq '#'
			  or  substr($kurz, 0,1) eq '\\'
			  or  substr($kurz, 0,1) eq '/'
			  or  substr($kurz, 0,1) eq '}' 
			  or  substr($kurz, 0,1) eq '´'
			  or  substr($kurz, 0,1) eq '`'
			)) {
			fehlermeldung($error_code, $title, $kurz_plain);
			#print $title."\n";
			#print $kurz."\n";
			#print $kurz_plain."\n";
			#die;
		}
	}
}

sub F051_Kurzbeschreibung_mit_Name{
	my $error_code = 51; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Kurzbeschreibung mit Name';
				$error_description[$error_code][1] = 'Das Feld KURZBESCHREIBUNG enthält die Zeichenkette "Name", was eigentlich nicht sein sollte, da alle Alternativnamen im entsprechenden Feld stehen sollte. Phrasen wie "Name gemacht" können auch raus.';
				$error_description[$error_code][2] = 3;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
	
		if ( $kurz_plain ne '' 
			and (
				 index($kurz_plain , 'Name:') > -1 
			  or index($kurz_plain , 'Name)') > -1 
			  or index($kurz_plain , 'Namen mach') > -1 
			  or index($kurz_plain , 'Namen gemach') > -1 
			))
			 {
				fehlermeldung($error_code, $title, $kurz_plain);
				#print $title."\n";
				#print $kurz_plain."\n";
				#die;
		}
	}
}

sub F052_Kurzbeschreibung_mit_fehlerhaften_Klammern{
	my $error_code = 52; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Kurzbeschreibung mit fehlerhaften Klammern';
				$error_description[$error_code][1] = 'Das Feld KURZBESCHREIBUNG enthält eine Klammer aber nicht den entsprechenden öffnenden oder schließenden Klammerteil. ';
				$error_description[$error_code][2] = 3;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		if ( $kurz_plain ne '' 
			and (
			   (index($kurz_plain , '(') > -1 and index($kurz_plain , ')') == -1)
			or (index($kurz_plain , ')') > -1 and index($kurz_plain , '(') == -1)
			or (index($kurz_plain , '{') > -1 and index($kurz_plain , '}') == -1)
			or (index($kurz_plain , '}') > -1 and index($kurz_plain , '{') == -1)
			or (index($kurz_plain , '[') > -1 and index($kurz_plain , ']') == -1)
			or (index($kurz_plain , ']') > -1 and index($kurz_plain , '[') == -1)
			))
			 {
				fehlermeldung($error_code, $title, $kurz_plain);
				#print $title."\n";
				#print $kurz_plain."\n";
				#die;
		}
	}
}


sub F053_Kurzbeschreibung_mit_Abkuerzungen{
	my $error_code = 53; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Kurzbeschreibung mit Abkürzung';
				$error_description[$error_code][1] = 'Das Feld KURZBESCHREIBUNG enthält eine unnötige Abkürzung (z.B. "frz."). Diese Abkürzungen sollten hier immer ausgeschrieben werden.';
				$error_description[$error_code][2] = 2;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		if ( $kurz_plain ne '' 
			and (
				 (  index($kurz_plain , 'dt.') > -1 
				or index($kurz_plain , 'österr.') > -1 
				or index($kurz_plain , 'amerik.') > -1 
				or index($kurz_plain , 'amerikan.') > -1 
				or index($kurz_plain , 'bay.') > -1 
				or index($kurz_plain , 'brit.') > -1 
				or index($kurz_plain , 'dän.') > -1 
				or index($kurz_plain , 'ehem.') > -1 
				or index($kurz_plain , 'ehm.') > -1 
				or index($kurz_plain , 'eng.') > -1 
				or index($kurz_plain , 'engl.') > -1 
				or index($kurz_plain , 'evang.') > -1 
				or index($kurz_plain , 'franz.') > -1 
				or index($kurz_plain , 'frz.') > -1 
				or index($kurz_plain , 'geb.') > -1 
				or index($kurz_plain , 'gest.') > -1 
				or index($kurz_plain , 'griech.') > -1 
				or index($kurz_plain , 'griech.') > -1 
				or index($kurz_plain , 'insbes.') > -1 
				or index($kurz_plain , 'ital.') > -1 
				or index($kurz_plain , 'königl.') > -1 
				or index($kurz_plain , 'kath.') > -1 
				or index($kurz_plain , 'kgl.') > -1 
				or index($kurz_plain , 'luth.') > -1 
				or index($kurz_plain , 'niederl.') > -1 
				or index($kurz_plain , 'oström.') > -1 
				or index($kurz_plain , 'preuß.') > -1 
				or index($kurz_plain , 'röm.') > -1 
				or index($kurz_plain , 'kath.') > -1 
				or index($kurz_plain , 'sowj.') > -1 
				or index($kurz_plain , 'span.') > -1 
				or index($kurz_plain , 'venez.') > -1 
				or index($kurz_plain , 'österreich.') > -1 
				)
			or 	( 
				# Das Datenfeld KURZBESCHREIBUNG enthält am Anfang eine Abkürzung (z.B. "franz. Pilot"). Diese Abkürzung sollte besser ausgeschrieben werden.
				# ehmals Fehler 254
				$kurz_plain =~ /^[^ ]+\./ 
				and not ($kurz_plain =~ /[0-9]\./)	)

			or 	(
				# Im Feld KURZBESCHREIBUNG wurde eine Abkürzung mit zwei Buchstaben gefunden (z.B. Jh., Hl., ev., em. oder andere). 
				# Es wurden römische Jahreszahlen ausgeschlossen (z.B. XI.). Die Kurzbeschreibung sollte möglichst ohne Abkürzungen auskommen.'
				# ehemals Fehler 155
				$kurz_plain =~ /(^| )[[:alpha:]][[:alpha:]]\./i
				and $kurz_plain =~ /(^| )[^XVI0-9][^XVI0-9]\./
				and not $kurz_plain =~ /(^| )(St|Nr|Co|Dr|EM|Ed)\./
				)
			))	
			 {
				fehlermeldung($error_code, $title, $kurz_plain);
				#print $title."\n";
				#print $kurz_plain."\n";
				#die;
		}
	}
}



sub F054_Kurzbeschreibung_ist_Datum{
	my $error_code = 54; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Kurzbeschreibung ist Datum';
				$error_description[$error_code][1] = 'Das Feld KURZBESCHREIBUNG enthält ein Datum. Oft wurde hier die Zeile Geburtsdatum falsch eingetragen.';
				$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
	
		my $test_kurz1 = $kurz_plain;
		my $test_kurz2 = $kurz_plain;
		if ( $kurz_plain ne '' 
			and $test_kurz1 =~ /^[0-3]?[0-9]\. (Januar|Februar|März|April|Mai|Juni|Juli|August|September|Oktober|November|Dezember)/g 
			and $test_kurz2 =~ /[0-9]?[0-9]?[0-9]?[0-9]$/g 
			)
			 {
				fehlermeldung($error_code, $title, $kurz_plain);
				#print $title."\n";
				#print $kurz_plain."\n";
				#die;
		}
	}
}


sub F055_Kurzbeschreibung_fehlerhaftes_und{
	my $error_code = 55; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Kurzbeschreibung mit fehlerhaftem "und"';
				$error_description[$error_code][1] = 'Das Feld KURZBESCHREIBUNG enthält Zeichen, die nach einem fehlerhaften und aussehen (z.B. " unf ", " ud ", " un ").';
				$error_description[$error_code][2] = 3;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		my $test_kurz1 = $kurz_plain;
		my $test_kurz2 = $kurz_plain;
		if ( $kurz_plain ne '' 
			and ( index ($kurz_plain, ' unf ') > -1
			   or index ($kurz_plain, ' ud ') > -1
			   or index ($kurz_plain, ' un ') > -1
			))
			 {
				fehlermeldung($error_code, $title, $kurz_plain);
				#print $title."\n";
				#print $kurz_plain."\n";
				#die;
		}
	}
}


sub F056_Kurzbeschreibung_kein_Leerzeichen_vor_und{
	my $error_code = 56; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Kurzbeschreibung ohne Leerzeichen vor oder nach "und"';
				$error_description[$error_code][1] = 'Das Feld KURZBESCHREIBUNG enthält "und", aber davor oder danach ist kein Leerzeichen.';
				$error_description[$error_code][2] = 3;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		my $test_kurz1 = $kurz_plain;
		my $test_kurz2 = $kurz_plain;
		if ( $kurz_plain ne '' 
			and (( index (lc($kurz_plain), 'und ') > -1
			   and index (lc($kurz_plain), ' und ') == -1
			   and index (lc($kurz_plain), 'grund ') == -1
			   and index (lc($kurz_plain), 'ground ') == -1
			   and index (lc($kurz_plain), 'raimund ') == -1
			   and index (lc($kurz_plain), 'sigmund ') == -1
			   and index (lc($kurz_plain), 'sigismund ') == -1
			   and index (lc($kurz_plain), 'grmund ') == -1
			   and index (lc($kurz_plain), 'freund ') == -1
			   and index (lc($kurz_plain), 'burgund ') == -1
			   and index (lc($kurz_plain), ' bund ') == -1
			   and index (lc($kurz_plain), ' stralsund ') == -1
			   and index (lc($kurz_plain), ' pfund ') == -1
			   and index (lc($kurz_plain), ' fund ') == -1
			   and index (lc($kurz_plain), ' mund ') == -1
			   and index (lc($kurz_plain), ' rund ') == -1
			   and index (lc($kurz_plain), ' lund ') == -1
			   #or index (lc($kurz_plain), 'und ') == 0
			   and index (lc($kurz_plain), '(und ') == -1
			   and index (lc($kurz_plain), '(bund ') == -1
			   and index (lc($kurz_plain), 'schaftsbund ') == -1
			   and index (lc($kurz_plain), ' dschund ') == -1
			   and index (lc($kurz_plain), 'studentenbund ') == -1
			   and index (lc($kurz_plain), 'frauenbund ') == -1
			   and index (lc($kurz_plain), ' sound ') == -1
			   and index (lc($kurz_plain), 'bohemund ') == -1
			)
			#or $test_kurz2=~ s/ und[^ ]//g;
			))
			 {
				fehlermeldung($error_code, $title, $kurz_plain);
				#print $title."\n";
				#print $kurz_plain."\n";
				#die;
		}
	}
}





sub F057_Kurzbeschreibung_gleich_erstes_Wort_vom_Titel{
	my $error_code = 57; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Kurzbeschreibung erstes Wort identisch mit Titel';
				$error_description[$error_code][1] = 'Das erste Wort im Feld KURZBESCHREIBUNG ist identisch mit dem ersten Wort im Titel. Das muss nicht sein!';
				$error_description[$error_code][2] = 2;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		my $test_title = $title;
		my $test_kurz = $kurz_plain;
		if ( $kurz_plain ne '' 
			and $kurz_plain ne 'Moorleiche') 		# Ausnahme
			 {
			my $pos1 = index($test_kurz,' ');
			$pos1 = length($test_kurz) if ($pos1 == -1);
			$test_kurz =substr ($test_kurz,0, $pos1);
			
			my $pos2 = index($test_title,' ');
			$pos2 = length($test_title) if ($pos2 == -1);
			$test_title =substr ($test_title,0, $pos2);		

			
			if ( $test_title eq $test_kurz)
			 {
				fehlermeldung($error_code, $title, $kurz_plain);
				#print "\t".$title."\n";
				#print "\t\t\t".$kurz_plain."\n";
				#die;
			}
		}
	}
}


sub F058_Kurzbeschreibung_mit_Punkt_am_Ende{
	my $error_code = 58; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Kurzbeschreibung mit Punkt am Ende';
				$error_description[$error_code][1] = 'Das Feld KURZBESCHREIBUNG einen Punkt am Ende. Da es sich bei z.B. "deutscher Schriftsteller." um keinen Satz handelt, kann der Punkt weg. Meist ist er ein Überbleibsel vom Kopieren des ersten Satzes.';
				$error_description[$error_code][2] = 3;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		my $test_kurz = $kurz_plain;
		my $test_kurz1 = $kurz_plain;
		my $test_kurz2 = $kurz_plain;
		my $test_kurz3 = $kurz_plain;
		my $test_kurz4 = $kurz_plain;
		my $test_kurz5 = $kurz_plain;
		my $test_kurz6 = $kurz_plain;
		my $test_kurz7 = $kurz_plain;
		my $test_kurz8 = $kurz_plain;
		my $test_kurz9 = $kurz_plain;
		my $test_kurz10 = $kurz_plain;
		my $test_kurz11 = $kurz_plain;
		
		if ( $kurz_plain ne '' ) {
			if ( $test_kurz1 =~ /\.[ ]?+$/
			and not $test_kurz2 =~ /Chr\.[ ]?+$/
			and not $test_kurz3 =~ /V\.[ ]?+$/
			and not $test_kurz4 =~ /I\.[ ]?+$/
			and not $test_kurz5 =~ /C\.[ ]?+$/
			and not $test_kurz6 =~ /D\.[ ]?+$/
			and not $test_kurz7 =~ /Inc\.[ ]?+$/
			and not $test_kurz8 =~ /Ltd\.[ ]?+$/
			and not $test_kurz9 =~ /X\.[ ]?+$/
			and not $test_kurz10 =~ /\..\.[ ]?+$/
			and not $test_kurz10 =~ /Boney M\./				# Marcia Barrett - 	jamaikanische Sängerin, Mitglied der Popgruppe Boney M.

			and int(ord(substr($test_kurz8,0,1))) >= 97
			and not $test_kurz9 =~ /\. ./
			)
			 {
				fehlermeldung($error_code, $title, $kurz_plain);
				#print "\t".$title."\n";
				#print "\t\t\t".$kurz_plain."\n";
				#die;
			}
		}
	}
}




sub F059_Kurzbeschreibung_ohne_Leerzeichen_nach_Adjektiv{
	my $error_code = 59; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Kurzbeschreibung kein Leerzeichen nach Adjektiv';
				$error_description[$error_code][1] = 'Das Feld KURZBESCHREIBUNG enthält kein Leerzeichen nach einem Adjektiv (z.B. deutscherMaler).';
				$error_description[$error_code][2] = 2;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		my $test_kurz = $kurz_plain;
		my $test_kurz1 = $kurz_plain;
		my $test_kurz2 = $kurz_plain;
		my $test_kurz3 = $kurz_plain;

		if ( $kurz_plain ne '' ) {
			if ( $test_kurz1 =~ /scher[A-Z]/
				or $test_kurz2 =~ /sche[A-Z]/
				or $test_kurz3 =~ /schen[A-Z]/
			)
			 {
				fehlermeldung($error_code, $title, $kurz_plain);
				#print "\t".$title."\n";
				#print "\t\t\t".$kurz_plain."\n";
				#die;
			}
		}
	}
}




sub F060_Kurzbeschreibung_ohne_Leerzeichen_nach_Komma{
	my $error_code = 60; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Kurzbeschreibung ohne Leerzeichen nach Komma';
				$error_description[$error_code][1] = 'Das Feld KURZBESCHREIBUNG hat ein Komma, dem kein Leerzeichen folgt. ';
				$error_description[$error_code][2] = 2;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		my $test_data = $kurz_plain;
		
		if ($test_data ne '' 
			and $test_data =~ /,[^ ]/g )
			{
				fehlermeldung($error_code, $title, $kurz_plain);
				#print "\t".$title."\n";
				#print "\t\t\t". $kurz_plain."\n";
				#die;		
			}
	}
}


sub F061_Kurzbeschreibung_ohne_Leerzeichen_nach_Artikel{
	my $error_code = 61; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Kurzbeschreibung ohne Leerzeichen nach Artikel';
				$error_description[$error_code][1] = 'Das Feld KURZBESCHREIBUNG hat ein Artikel, dem kein Leerzeichen folgt. ';
				$error_description[$error_code][2] = 3;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		my $test_kurz  = $kurz_plain;
		my $test_kurz1 = $kurz_plain;
		
		if ($test_kurz ne '' 
			and 
				(
					(	$test_kurz1 =~ / des[^ ,]/ 
						and index ($test_kurz, ' dessen') == -1
						and index ($test_kurz, ' design') == -1
						and index ($test_kurz, ' deshalb') == -1
					)
				or  (	$test_kurz1 =~ / dem[^ ,]/ 
						and index ($test_kurz, ' demo') == -1
					)
				or  (	$test_kurz1 =~ / den[^ ]/ 
						and index ($test_kurz, ' denk') == -1
						and index ($test_kurz, ' denen') == -1
					)
				or  (	$test_kurz1 =~ / die[^ ,]/ 
						and index ($test_kurz, ' dieser') == -1
						and index ($test_kurz, ' dient') == -1
						and index ($test_kurz, ' diesen') == -1
						and index ($test_kurz, ' dienst') == -1
						and index ($test_kurz, ' diesem') == -1
					)
				or  (	$test_kurz1 =~ / das[^ ,]/ 
						and index ($test_kurz, ' dass') == -1
					)
				or  (	$test_kurz1 =~ / der[^ ,]/ 
						and index ($test_kurz, ' deren') == -1
						and index ($test_kurz, ' derzeit') == -1
						and index ($test_kurz, ' derer') == -1
					)
				)
			)
			
			{
				fehlermeldung($error_code, $title, $kurz_plain);
				#print "\t".$title."\n";
				#print "\t\t\t". $kurz_plain."\n";
				#die;		
			}
	}
}



sub F062_Kurzbeschreibung_mit_Schraegstrich_zwischen_Nationen{
	my $error_code = 62; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Kurzbeschreibung mit Schrägstrich zwischen Nationalitäten';
				$error_description[$error_code][1] = 'Das Feld KURZBESCHREIBUNG enthält zwischen zwei Nationalitäten einen Schrägstrich (z.B. polnisch/deutsch). Bei der Mehrheit der Daten wird hier aber ein Bindestrich geschrieben ("polnisch-deutsch" oder "spanisch-US-amerikanisch"). Im Rahmen der Vereinheitlichung der Daten sollten diese Daten angepasst werden.';
				$error_description[$error_code][2] = 2;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		my $test_kurz1 = $kurz_plain;
		my $test_kurz2 = $kurz_plain;
		

		if ( $kurz_plain ne '' 
			and ($test_kurz1 =~ /sch[^\/ ]*( )?\//g 
			or $test_kurz2 =~ /Schweiz(er)?[^\/ ]*( )?\//g )
			)
			 {
				fehlermeldung($error_code, $title, $kurz_plain);
				#print "\t".$title."\n";
				#print "\t\t\t".$kurz_plain."\n";
				#die;
		}
	}
}


sub F063_Kurzbeschreibung_nur_ein_kleingeschriebenes_Wort{
	my $error_code = 63; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Kurzbeschreibung hat nur ein kleingeschriebenes Wort';
				$error_description[$error_code][1] = 'Das Feld KURZBESCHREIBUNG enthält nur ein kleingeschriebenes Wort (z.B. "indisch"). Hier fehlt ein Teil der Kurzbeschreibung';
				$error_description[$error_code][2] = 3;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		my $test_kurz1 = $kurz_plain;
		my $test_kurz2 = $kurz_plain;
		

		if ( $kurz_plain ne '' 
			 and ( index ($kurz_plain,' ') == -1
			 and ($test_kurz1 =~ /^[a-z]/
			 or index ($test_kurz2, 'ä') == 0
			 or index ($test_kurz2, 'ü') == 0
			 or index ($test_kurz2, 'ö') == 0
			 )
			))
			 {
				fehlermeldung($error_code, $title, $kurz_plain);
				#print "\t".$title."\n";
				#print "\t\t\t".$kurz_plain."\n";
				#die;
		}
	}
}


sub F064_Kurzbeschreibung_einzelner_kleine_Buchstabe_am_Ende{
	my $error_code = 64; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Kurzbeschreibung mit einzelnem kleinen Buchstaben';
				$error_description[$error_code][1] = 'Das Feld KURZBESCHREIBUNG enthält am Ende einen einzelnen kleinen Buchstaben (z.B. "deutscher Autor u")';
				$error_description[$error_code][2] = 3;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		my $test_kurz1 = $kurz_plain;
		my $test_kurz2 = $kurz_plain;
		

		if ( $kurz_plain ne '' 
			 and ($test_kurz1 =~ / .[ ]?+$/
			 and $test_kurz2 =~ / [a-zäüö][ ]?+$/ 
			))
			 {
				fehlermeldung($error_code, $title, $kurz_plain);
				#print "\t".$title."\n";
				#print "\t\t\t".$kurz_plain."\n";
				#die;
		}
	}
}

sub F065_Kurzbeschreibung_schlechtem_Ende{
	my $error_code = 65; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Kurzbeschreibung mit schlechtem Ende';
				$error_description[$error_code][1] = 'Das Feld KURZBESCHREIBUNG enthält am Ende ein Zeichen (,:-´…) (z.B. "deutscher Autor,")';
				$error_description[$error_code][2] = 3;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		my $test_kurz1 = $kurz_plain;

		if ( $kurz_plain ne '' 
			 and $test_kurz1 =~ /[,´:…-][ ]?+$/ 
			)
			 {
				fehlermeldung($error_code, $title, $kurz_plain);
				#print "\t".$title."\n";
				#print "\t\t\t".$kurz_plain."\n";
				#die;
		}
	}
}

sub F066_Kurzbeschreibung_mit_geschweifter_Klammer{
	my $error_code = 66; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Kurzbeschreibung mit geschweiften Klammern';
				$error_description[$error_code][1] = 'Das Feld KURZBESCHREIBUNG enthält geschweifte Klammern, wie es bei Vorlagen der Fall ist. In der Kurzbeschreibung sollte keine Vorlage enthalten sein (z.B. <nowiki>Hawai{{Okina}}i</nowiki>).';
				$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		my $test_kurz1 = $kurz_plain;

		if ( $kurz_plain ne '' 
			 and $test_kurz1 =~ /{/ 
			)
			 {
				fehlermeldung($error_code, $title, $kurz_plain);
				#print "\t".$title."\n";
				#print "\t\t\t".$kurz_plain."\n";
				#die;
		}
	}
}

sub F067_Kurzbeschreibung_mit_HTML{
	my $error_code = 67; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Kurzbeschreibung mit HTML';
				$error_description[$error_code][1] = 'Das Feld KURZBESCHREIBUNG enthält HTML-Elemente.';
				$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		my $test_kurz1 = $kurz_plain;

		if ( $kurz_plain ne '' 
			 and index ($test_kurz1, chr(38).'lt' )>-1
			)
			 {
				fehlermeldung($error_code, $title, $kurz_plain);
				#print "\t".$title."\n";
				#print "\t\t\t".$kurz_plain."\n";
				#die;
		}
	}
}

sub F068_Kurzbeschreibung_mit_von{
	my $error_code = 68; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Kurzbeschreibung mit "von"';
				$error_description[$error_code][1] = 'Das Feld KURZBESCHREIBUNG enthält den Text "von", der aber kein Leerzeichen davor oder danach hat. (z.B. "Suffolk, 3. Herzog von")';
				$error_description[$error_code][2] = 3;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		my $test_kurz1 = $kurz_plain;

		if ( $kurz_plain ne '' 
			 and index ($kurz_plain, 'von' )>-1
			 and index ($kurz_plain, ' von ' )== -1
			 and not index ($kurz_plain, 'von ' )== 0
			 and not $test_kurz1 =~ /[\(\)\"„-]von/
			 and index ($kurz_plain, 'Devon' )== -1 
			)
			 {
				fehlermeldung($error_code, $title, $kurz_plain);
				#print "\t".$title."\n";
				#print "\t\t\t".$kurz_plain."\n";
				#die;
		}
	}
}

sub F069_Kurzbeschreibung_mit_Fett{
	my $error_code = 69; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Kurzbeschreibung mit fett geschriebenem Text';
				$error_description[$error_code][1] = 'Das Feld KURZBESCHREIBUNG enthält den Text der fett geschrieben wurde. Das ist im Datenfeld nicht notwendig.';
				$error_description[$error_code][2] = 2;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		my $test_kurz1 = $kurz_plain;

		if ( $kurz_plain ne '' 
			 and index ($kurz_plain, chr(39).chr(39).chr(39) )>-1
			)
			 {
				fehlermeldung($error_code, $title, $kurz_plain);
				#print "\t".$title."\n";
				#print "\t\t\t".$kurz_plain."\n";
				#die;
		}
	}
}

sub F070_Kurzbeschreibung_mit_Artikel_am_Anfang{
	my $error_code = 70; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Kurzbeschreibung mit Artikel am Anfang';
				$error_description[$error_code][1] = 'Das Feld KURZBESCHREIBUNG enthält einen Artikel am Anfang. (z.B. "der deutsche Bergsteiger" ). Durch Umschreiben der Kurzbeschreibung kann der Artikel meist entfernt werden. ';
				$error_description[$error_code][2] = 3;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		my $test_kurz = lc($kurz_plain);
		

		if ( $test_kurz ne '' 
			
			 and ($test_kurz =~ /^[ ]?+das /
			   or $test_kurz =~ /^[ ]?+der /
			   or $test_kurz =~ /^[ ]?+dem /
			   or $test_kurz =~ /^[ ]?+den /
			   or $test_kurz =~ /^[ ]?+des /
			   or $test_kurz =~ /^[ ]?+die /
			 )
			)
			 {
				fehlermeldung($error_code, $title, $kurz_plain);
				#print "\t".$title."\n";
				#print "\t\t\t".$kurz_plain."\n";
				#die;
		}
	}
}


sub F071_Kurzbeschreibung_mit_genealogischen_Zeichen{
	my $error_code = 71; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Kurzbeschreibung mit genealogischen Zeichen';
				$error_description[$error_code][1] = 'Das Feld KURZBESCHREIBUNG enthält genealogische Zeichen (z.B. * oder †). Die sollten dort eigentlich nicht vorkommen. Lebensdaten der Person gehören in die Felder Geburtstag und Sterbetag.';
				$error_description[$error_code][2] = 2;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {

		if ( $kurz_plain ne '' 
			 and (index ($kurz_plain, '*') > -1
			 or index ($kurz_plain, '†') >-1
			 )
			)
			 {
				fehlermeldung($error_code, $title, $kurz_plain);
				#print "\t".$title."\n";
				#print "\t\t\t".$kurz_plain."\n";
				#die;
		}
	}
}

sub F072_Kurzbeschreibung_kein_Leerzeichen_vor_Klammer{
	my $error_code = 72; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Kurzbeschreibung ohne Leerzeichen vor der Klammer';
				$error_description[$error_code][1] = 'Das Feld KURZBESCHREIBUNG enthält kein Leerzeichen vor einer Klammer z.B. "Politiker(ÖDP)".';
				$error_description[$error_code][2] = 3;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		my $test_kurz1 = $kurz_plain;
		if ( $kurz_plain ne '' 
			 and index ($kurz_plain, ' (') == -1
			 and $test_kurz1 =~ /[^ ]\(/g
			 and index ($kurz_plain, '(-Schwerin)') == -1
			 and index ($kurz_plain, 'Kom(m)ödchen') == -1
			 )
			
			 {
				fehlermeldung($error_code, $title, $kurz_plain);
				#print "\t".$title."\n";
				#print "\t\t\t".$kurz_plain."\n";
				#die;
		}
	}
}

sub F073_Kurzbeschreibung_beginnt_mit_Jahreszahl{
	my $error_code = 73; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Kurzbeschreibung mit Jahreszahl am Anfang';
				$error_description[$error_code][1] = 'Das Feld KURZBESCHREIBUNG beginnt mit einer Jahreszahl (z.B.  "1808 bis 1839 Sultan des Osmanischen Reiches"). Besser ist die Schreibweise "osmanischer Sultan (1808-1839)".';
				$error_description[$error_code][2] = 2;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		if ( $kurz_plain ne '' 
			 and ($kurz_plain =~ /^[ ]?+[0-9]/
			  or $kurz_plain =~ /^[ ]?+[0-9]+[ ]?+(-|bis|\/)/ )
			 and not $kurz_plain =~ /^[ ]?+[0-9]+\./
			 and not $kurz_plain =~ /^[ ]?+[0-9]+th\./
			 )
			
			 {
				fehlermeldung($error_code, $title, $kurz_plain);
				#print "\t".$title."\n";
				#print "\t\t\t".$kurz_plain."\n";
				#die;
		}
	}
}

sub F074_Kurzbeschreibung_Leerzeichen_vor_Komma{
	my $error_code = 74; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Kurzbeschreibung mit Leerzeichen vor Komma';
				$error_description[$error_code][1] = 'Das Feld KURZBESCHREIBUNG enthält ein Leerzeichen vor einem Komma (Bsp. "Maler , Elektriker").';
				$error_description[$error_code][2] = 3;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		my $test_kurz1 = $kurz_plain;
		my $test_kurz2 = $kurz_plain;
		if ( $kurz_plain ne '' 
			 and index($kurz_plain, ' , ')>-1)
			 {
				fehlermeldung($error_code, $title, $kurz_plain);
				#print "\t".$title."\n";
				#print "\t\t\t".$kurz_plain."\n";
				#die;
		}
	}
}

sub F075_Kurzbeschreibung_mit_Professor{
	my $error_code = 75; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Kurzbeschreibung mit "Professor"';
				$error_description[$error_code][1] = 'Das Feld KURZBESCHREIBUNG enthält als erstes oder zweites Wort "Professor" oder "Doktor" (Bsp. "Professor an der TU Dresden"). Besser wäre z.B. "russischer Chemiker, Professor für Anorganik"';
				$error_description[$error_code][2] = 2;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		my $test_kurz1 = $kurz_plain;
		my $test_kurz2 = $kurz_plain;
		if ( $kurz_plain ne '' 
			 and (
				($test_kurz1 =~ /^[^ ]+ Professor/ or $test_kurz2 =~ /^Professor/	 )
			 or ($test_kurz1 =~ /^[^ ]+ Doktor/ or $test_kurz2 =~ /^Doktor/	 )
			 or ($test_kurz1 =~ /^[^ ]+-Doktor/ or $test_kurz2 =~ /^[^ ]+ [^ ]+-Doktor/	)
			 or ($test_kurz1 =~ /^[^ ]+-Professor/ or $test_kurz2 =~ /^[^ ]+ [^ ]+-Professor/ )
			 )
		   ) 
			
			 {
				fehlermeldung($error_code, $title, $kurz_plain);
				#print "\t".$title."\n";
				#print "\t\t\t".$kurz_plain."\n";
				#die;
		}
	}
}

sub F076_Kurzbeschreibung_aus_Land{
	my $error_code = 76; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Kurzbeschreibung mit "aus Land XY"';
				$error_description[$error_code][1] = 'Das Feld KURZBESCHREIBUNG enthält eine Herkunft wie z.B. "Schauspieler aus Brasilien". Besser wäre die Schreibweise "brasilianischer Schauspieler". Siehe auch: <a href="http://de.wikipedia.org/wiki/Wikipedia:Namenskonventionen/Staaten">Wikipedia:Namenskonventionen/Staaten</a>';
				$error_description[$error_code][2] = 2;
				$error_description[$error_code][1] = regelbasierter_hinweistext($error_description[$error_code][1]);
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		my $test_kurz1 = $kurz_plain;
		my $test_kurz2 = $kurz_plain;
		if ( $kurz_plain ne '' 
				and (index($kurz_plain, 'aus ') == 0  or index($kurz_plain, ' aus ') > -1 )
				and index($kurz_plain, 'aus dem') == -1
				and index($kurz_plain, 'Zeit') == -1
				and index($kurz_plain, 'Dynastie') == -1
				and index($kurz_plain, 'Familie') == -1
				and index($kurz_plain, 'Linie') == -1
				and index($kurz_plain, 'aus einem ') == -1
				and index($kurz_plain, 'aus Internierungslager ') == -1
				
				# Ausnahme Inseln 
				# z.B. Radsportler aus den Vereinigten Arabischen Emiraten
				and index($kurz_plain, 'Kongo') == -1
				and index($kurz_plain, 'Kitts und Nevis') == -1
				and index($kurz_plain, 'Trinidad und Tobago') == -1
				and index($kurz_plain, 'Vereinigte Arabische Emirate') == -1
				and index($kurz_plain, 'Vereinigten Arabischen Emiraten') == -1
				and index($kurz_plain, 'Westsahara') == -1
				and index($kurz_plain, 'Trinidad & Tobago') == -1
				and index($kurz_plain, 'St. Kitts und Nevis') == -1
				and index($kurz_plain, 'Cookinseln') == -1
				and index($kurz_plain, 'Guadeloupe') == -1
				and index($kurz_plain, 'Anguilla') == -1
		
			)	
			 {
				fehlermeldung($error_code, $title, $kurz_plain);
				#print "\t".$title."\n";
				#print "\t\t\t".$kurz_plain."\n";
				#die;
		}
	}
	
}

sub F077_Kurzbeschreibung_mit_unnoetigem_Wort{
	my $error_code = 77; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Kurzbeschreibung mit unnötigem Wort';
				$error_description[$error_code][1] = 'Das Feld KURZBESCHREIBUNG enthält ein unnötiges Wort (z.B. "bekannt", "berühmt", "wichtig", "prominent", "berüchtigter", "renommierter", "einflussreich", "beste", "führend", "erfolgreich" ). Die Begriffe stoßen sich mit der Neutralitätsregel. Statt "bekannter russischer Maler" reicht einfach "russischer Maler".';
				$error_description[$error_code][2] = 2;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		if ( $kurz_plain ne '' and 
			   index(lc($kurz_plain), 'bekannt') > -1
			or index(lc($kurz_plain), 'berühmt') > -1 
			#or index(lc($kurz_plain), 'bedeutend') > -1 
			or index(lc($kurz_plain), 'einflussreich') > -1 
			or ( (index(lc($kurz_plain), 'beste') > -1  and index(lc($kurz_plain), 'besteig') == -1) )
			or ( (index(lc($kurz_plain), 'führend') > -1  and index(lc($kurz_plain), 'amtsführend') == -1)  and index(lc($kurz_plain), 'geschäftsführend') == -1 )
			or index(lc($kurz_plain), 'erfolgreich') > -1 
			or index(lc($kurz_plain), 'wichtig') > -1 
			or index(lc($kurz_plain), 'renommierter') > -1 
			or index(lc($kurz_plain), 'berüchtigter') > -1 
			#or index(lc($kurz_plain), 'legendär') > -1
			or index(lc($kurz_plain), 'prominent') > -1
			or index(lc($kurz_plain), '-legende') > -1  # Box-Legende
			or index($kurz_plain, '-Größe') > -1  # Kiez-Größe
		
			)
			{
				fehlermeldung($error_code, $title, $kurz_plain);
				#print $error_code."\t".$title."\t".'<nowiki>'. $kurz_plain.'</nowiki>'."\n";
			}
	}
}

#################################################

sub F078_Geburtsjahr_in_Zunkunft{
	my $error_code = 78; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
			$error_description[$error_code][0] = 'Geburtsdatum liegt in der Zukunft';
			$error_description[$error_code][1] = 'Das Feld GEBURTSDATUM enthält eine Jahresangabe, die in der Zukunft liegt (Bsp. 2145). Meist ist es ein Tippfehler oder ein Zahlendreher.';
			$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		if ( ($geburtsjahr) and $geburtsjahr > $akJahr) {
			fehlermeldung($error_code, $title, $geburtsjahr);
		}
	}
}

sub F079_Geburtsjahr_contra_Sterbedatum{
	my $error_code = 79; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
			$error_description[$error_code][0] = 'Geburtsdatum passt zu nicht Sterbedatum';
			$error_description[$error_code][1] = 'Das Feld GEBURTSDATUM enthält kein "v. Chr." obwohl das STERBEDATUM ein "v. Chr." enthält. Da stimmt was nicht.';
			$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		my $test_geb = $geburtsdatum;
		my $test_sterb = $sterbedatum;
		$test_geb =~ s/ //g;
		$test_sterb =~ s/ //g;
		
		if ( ($geburtsdatum) and 
		
			 ( $test_sterb ne ""  and index($sterbedatum,'v. Chr.') >-1 
			 and $test_geb ne "" and index($geburtsdatum,'v. Chr.') == -1 )) {

			fehlermeldung($error_code, $title, $geburtsdatum . ' - '. $sterbedatum );
			#print $title."\n";
			#print $geburtsdatum . ' - '. $sterbedatum."\n";
			#die;
		}
	}
}



#################################################



sub F080_Ort_mit_Sonderzeichen{
	my $error_code = 80; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
		$error_description[$error_code][0] = 'Geburts- oder Sterbeort mit Sonderzeichen';
		$error_description[$error_code][1] = 'Das Feld GEBURTSORT oder STERBEORT enthält eine Sonderzeichen( *?,;={}&) was dort nicht notwendig sein sollte.';
		$error_description[$error_code][2] = 2;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		F080_Ort_mit_Sonderzeichen2($error_code, $geburtsort_plain);
		F080_Ort_mit_Sonderzeichen2($error_code, $sterbeort_plain);
	}
}

sub F080_Ort_mit_Sonderzeichen2{
	my $error_code =$_[0];
	my $test_ort = $_[1];
	if ((   index($test_ort, '&') > -1
		or index($test_ort, '{') > -1 
		or index($test_ort, '}') > -1 
		or index($test_ort, '*') > -1
		#or index($test_ort1, '?') > -1 or index($test_ort2, '?') > -1
		or index($test_ort, ';') > -1 
		or index($test_ort, '=') > -1 
		or index($test_ort, ',') == 0 
		or index($test_ort, '-') == 0 
		
		) 
		and (index($test_ort, 'Nations & Nationalities') == -1)
	   )
	{

		fehlermeldung($error_code, $title, $test_ort);
		#print $title."\n";
		#print $test_ort."\n";
		#die;
	}
}

sub F081_Ort_mit_eckigen_Klammern{
	my $error_code = 81; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
		$error_description[$error_code][0] = 'Geburts- oder Sterbeort mit eckigen Klammern';
		$error_description[$error_code][1] = 'Das Feld GEBURTSORT oder STERBEORT enthält eckige Klammern, die keine korrekt Verlinkung sind .( z.B. [Rom] statt [[Rom]]) ';
		$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		F081_Ort_mit_eckigen_Klammern2($error_code, $geburtsort_plain);
		F081_Ort_mit_eckigen_Klammern2($error_code, $sterbeort_plain);
	}
}

sub F081_Ort_mit_eckigen_Klammern2{
	my $error_code =$_[0];
	my $test_ort  = $_[1];
	my $test_ort1 = $_[1];
	my $test_ort2 = $_[1];
	my $test_ort3 = $_[1];
	my $test_ort4 = $_[1];
	if (   
		   $test_ort1 =~ / \[[^\[]/g 
		or $test_ort2 =~ /[0-9]\]$/g 
		or $test_ort3 =~ /[a-z0-9]\] /g 
		or $test_ort4 =~ /\[[a-z0-9]/g 
		
		) 
	{
		fehlermeldung($error_code, $title, $test_ort);
		#print $title."\n";
		#print $test_ort."\n";
		#die;
	}
}

sub F082_Ort_mit_Geburtsname{
	my $error_code = 82; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
			$error_description[$error_code][0] = 'Geburtsort mit Geburtsnamen';
			$error_description[$error_code][1] = 'Das Feld GEBURTSORT enthält die Zeichenkette " als ", was auf den Zusatz eines Geburtsnamens deutet, der da nicht hin gehört.( z.B. Berlin als Manfred Mustermann) ';
			$error_description[$error_code][2] = 2;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		if ($geburtsort_plain ne '' and index ($geburtsort_plain, ' als ') > -1){
			fehlermeldung($error_code, $title, $geburtsort_plain);
			#print $title."\n";
			#print $geburtsort_plain."\n";
		}
	}
} 

sub F083_Ort_mit_fehlerhaften_Klammern{
	my $error_code = 83; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
			$error_description[$error_code][0] = 'Geburts- oder Sterbeort mit fehlerhaften Klammern';
			$error_description[$error_code][1] = 'Das Feld GEBURTSORT oder STERBEORT enthält eine Klammer aber nicht den entsprechenden öffnenden oder schließenden Klammerteil. ';
			$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		F083_Ort_mit_fehlerhaften_Klammern2($error_code, $geburtsort_plain);
		F083_Ort_mit_fehlerhaften_Klammern2($error_code, $sterbeort_plain);
	}
}

sub F083_Ort_mit_fehlerhaften_Klammern2{
	my $error_code= $_[0];
	my $test_ort  = $_[1];
	if ( $test_ort ne '' 
		and (
		   (index($test_ort , '(') > -1 and index($test_ort , ')') == -1)
		or (index($test_ort , ')') > -1 and index($test_ort , '(') == -1)
		or (index($test_ort , '{') > -1 and index($test_ort , '}') == -1)
		or (index($test_ort , '}') > -1 and index($test_ort , '{') == -1)
		or (index($test_ort , '[') > -1 and index($test_ort , ']') == -1)
		or (index($test_ort , ']') > -1 and index($test_ort , '[') == -1)
		))
		 {
			fehlermeldung($error_code, $title, $test_ort);
			#print $title."\n";
			#print $test_ort."\n";
			#die;
	}
}

sub F084_Ort_falsch_verlinkt{
	my $error_code = 84; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
		$error_description[$error_code][0] = 'Geburts- oder Sterbeort mit falscher Verlinkung';
		$error_description[$error_code][1] = 'Das Feld GEBURTSORT oder STERBEORT enhält nach entfernen der Verlinkungen noch eckige Klammern, was auf eine fehlerhafte Verlinkung schließen lässt.';
		$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		F084_Ort_falsch_verlinkt2($error_code, $geburtsort_plain);
		F084_Ort_falsch_verlinkt2($error_code, $sterbeort_plain);
	}
}

sub F084_Ort_falsch_verlinkt2{
	my $error_code= $_[0];
	my $test_ort  = $_[1];
	if ( $test_ort ne '' 
		and ( index($test_ort , '[') >-1
			or  index($test_ort , ']') >-1 
			or  index($test_ort , '|') >-1 
			)
		) {
		fehlermeldung($error_code, $title, $test_ort);
		#print $title."\n";
		#print $test_ort."\n";
		#die;
	}
}

sub F085_Ort_ohne_Leerzeichen_nach_Komma{
	my $error_code = 85; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
			$error_description[$error_code][0] = 'Geburts- oder Sterbeort ohne Leerzeichen nach Komma';
			$error_description[$error_code][1] = 'Das Feld GEBURTSORT oder STERBEORT hat ein Komma, dem kein Leerzeichen folgt. ';
			$error_description[$error_code][2] = 3;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		F085_Ort_ohne_Leerzeichen_nach_Komma2($error_code, $geburtsort_plain);
		F085_Ort_ohne_Leerzeichen_nach_Komma2($error_code, $sterbeort_plain);
	}
}

sub F085_Ort_ohne_Leerzeichen_nach_Komma2{
	my $error_code = $_[0];
	my $test_data = $_[1];
	my $test_data1 = $_[1];
	
	if ($test_data ne '' 
		and $test_data1 =~ /,[^ ]/g )
		{
			fehlermeldung($error_code, $title, $test_data);
			#print "\t".$title."\n";
			#print "\t\t\t". $test_data."\n";
			#die;		
		}
}

sub F086_Ort_falschem_Anfang{
	my $error_code = 86; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
			$error_description[$error_code][0] = 'Geburts- oder Sterbeort mit unnötiges Wort am Anfang';
			$error_description[$error_code][1] = 'Das Feld GEBURTSORT oder STERBEORT hat am Anfang ein unnötiges Wort (z.B. "in Dresden"). Es gibt auch einige Ortsbeschreibungen, die gekürzt werden können. So sollte "in der Nähe von Leipzig" umgeschrieben werden in "bei Leipzig".';
			$error_description[$error_code][2] = 3;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		F086_Ort_falschem_Anfang2($error_code, $geburtsort_plain);
		F086_Ort_falschem_Anfang2($error_code, $sterbeort_plain);
	}
}

sub F086_Ort_falschem_Anfang2{
	my $error_code = $_[0];
	my $test_data = $_[1];
	my $test_data1 = $_[1];
	my $test_data2 = $_[1];
	
	if ($test_data ne '' 
		#and ($test_data1 =~ /[,-\/;]$/g 
		#or
		and (	$test_data2 =~ /^[ ]?+(i|I)n / 
			 or $test_data2 =~ /^[ ]?+(i|I)m /
			 or $test_data2 =~ /^[ ]?+(u|U)m / )
		)
		{
			fehlermeldung($error_code, $title, $test_data);
			#print "\t".$title."\n";
			#print "\t\t\t". $test_data."\n";
			#die;		
		}
}

sub F087_Ort_falschem_Ende{
	my $error_code = 87; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
			$error_description[$error_code][0] = 'Geburts- oder Sterbeort mit falschem Zeichen am Ende';
			$error_description[$error_code][1] = 'Das Feld GEBURTSORT oder STERBEORT hat ein falsches Zeichen am Ende (z.B. "Dresden-"). ';
			$error_description[$error_code][2] = 3;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		F087_Ort_falschem_Ende2($error_code, $geburtsort_plain);
		F087_Ort_falschem_Ende2($error_code, $sterbeort_plain);
	}
}

sub F087_Ort_falschem_Ende2{
	my $error_code = $_[0];
	my $test_data = $_[1];
	my $test_data1 = $_[1];
	my $test_data2 = $_[1];
	
	if ($test_data ne '' 
		#and ($test_data1 =~ /[,-\/;]$/g 
		#or
		and ($test_data2 =~ /\.[ ]?+$/g  
			#and index ($test_data, 'N.Y.') < 0
			and not $test_data =~ /N\.Y\.[ ]?+/
			and not $test_data =~ /D\.C\.[ ]?+/
			and not $test_data =~ /D\. C\.[ ]?+/
			and not $test_data =~ /N\.C\.[ ]?+/
			and not $test_data =~ /\/Sa\.[ ]?+/
			and not $test_data =~ /\/Vogtl\.[ ]?+/
			and not $test_data =~ /\/Erzgeb\.[ ]?+/
			and not $test_data =~ /\/O\.L\.[ ]?+/
			and not $test_data =~ /O\.S\.[ ]?+/
			and not $test_data =~ /V\. L\.[ ]?+/
			and not $test_data =~ /i\. Pom\.[ ]?+/
			and not $test_data =~ /i\. Pr\.[ ]?+/				# Königsberg i. Pr.
			and not $test_data =~ /i\. Niederschles.\.[ ]?+/				#Landkreis Freystadt i. Niederschles.
		))
		{
			fehlermeldung($error_code, $title, $test_data);
			
			#print "\t".$title."\n";
			#print "\t\t\t". $test_data."\n";
			#die;		
		}
}



sub F088_Ort_erstes_Wort_nicht_im_Text {
	my $error_code = 88; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Geburts- oder Sterbeort fehlt im Text';
				$error_description[$error_code][1] = 'Das erste Wort des Geburts- oder Sterbeort konnte nicht im Text gefunden werden. Ausnahmen sind die Wörter "bei", "unsicher:", "begraben", "getauft" oder "zwischen". ';
				$error_description[$error_code][2] = 2;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		F088_Ort_erstes_Wort_nicht_im_Text2($error_code, $geburtsort) if ($geburtsort ne '') ;
		F088_Ort_erstes_Wort_nicht_im_Text2($error_code, $sterbeort)  if ($sterbeort ne '');
	}
}

sub F088_Ort_erstes_Wort_nicht_im_Text2 {
	my $error_code = $_[0];
	my $testort = $_[1];
	my $testort2 = $testort;
	$testort2 =~ s/\[\[//g;

	my $pos2 = index($testort2, ' ');
	my $pos3 = index($testort2, '|');
	my $pos4 = index($testort2, ']');
	my $pos5 = index($testort2, ',');
	my $pos6 = index($testort2, '/');
	$pos2 = $pos3 if ($pos2 == -1 or ($pos3 > -1 and $pos3 < $pos2));
	$pos2 = $pos4 if ($pos2 == -1 or ($pos4 > -1 and $pos4 < $pos2));
	$pos2 = $pos5 if ($pos2 == -1 or ($pos5 > -1 and $pos5 < $pos2));
	$pos2 = $pos6 if ($pos2 == -1 or ($pos6 > -1 and $pos6 < $pos2));
	#print $pos2."\n";
	$testort2 = substr($testort2, 0, $pos2) if ($pos2 > -1);	
	$testort2 =~ s/ //g;
	
	
	my $testort3 = lc($testort2);
	
	my $testtext = $text;
	my $pos = index($testtext, '{{Personendaten');
	$testtext = substr($testtext, 0, $pos);
	$pos = index($testtext, '[[Kategorie:');
	$testtext = substr($testtext, 0, $pos);	
	$testtext = lc($testtext);
	
	if ( $is_redirect == 0 
	     and length($testort3)>0 
		 and index($testtext, $testort3) == -1
		 and $testort3 ne 'bei'
		 and $testort3 ne 'unsicher:'
		 and $testort3 ne 'begraben'
		 and $testort3 ne 'getauft'
		 and $testort3 ne 'zwischen'
		
		 ) {
				fehlermeldung($error_code, $title, $testort2);
				#print "\t".$title."\n";
				#print "\t\t\t". $testort2."\n";
				#print "\t\t\t". $testort3."\n";
				#die;
	}
	
}







#################################################
sub F089_Sterbejahr_in_Zunkunft{
	my $error_code = 89; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
			$error_description[$error_code][0] = 'Sterbedatum liegt in der Zukunft';
			$error_description[$error_code][1] = 'Das Feld STERBEDATUM enthält eine Jahresangabe, die in der Zukunft liegt (Bsp. 2145). Meist ist es ein Tippfehler oder ein Zahlendreher.';
			$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		if ( ($sterbejahr) and $sterbejahr > $akJahr) {
			fehlermeldung($error_code, $title, $sterbejahr);
		}
	}
}



sub F090_Sterbejahr_fehlt_viele_Jahre{
	my $error_code = 90; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Sterbedatum fehlt, Geburtsjahr schon 120 Jahre her';
				$error_description[$error_code][1] = 'Das Feld STERBEDATUM ist leer, aber das Geburtsjahr ist schon über 120 Jahre her. Hier sollte ein Sterbedatum (z.B. "1992" oder "20. Jahrhundert" oder "zwischen 1940 und 1950" ) eingefügt werden.';
				$error_description[$error_code][2] = 2;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		if ( $sterbedatum eq '' ) {
			my @kategorien = split ( /\t/, $all_categories);

			my $rest_cat = '';
				
			foreach (@kategorien) {
				my $current_categorie = $_;
				$current_categorie =~ s/\[\[Kategorie://g;
				$current_categorie =~ s/\|.+//g;
				$current_categorie =~ s/\]\]//g;
				#print $current_categorie ."\n";

				# Geboren
				if (index ($current_categorie, 'Geboren') == 0) {
					if ($current_categorie ne 'Geboren im 21. Jahrhundert' and 
					    $current_categorie ne 'Geboren im 20. Jahrhundert' and
						$current_categorie ne 'Geboren im 19. oder 20. Jahrhundert' and
						$current_categorie ne 'Geboren im 20. oder 21. Jahrhundert') {
						my $found = 'false';
						for (my $i = int($akJahr)-120; $i <= $akJahr; $i++) {
							$found = 'true'	if ($current_categorie eq 'Geboren '.$i);						
						} 

						if ($found eq 'false') {					
							fehlermeldung($error_code, $title, $current_categorie);
							#print $error_code."\t".$title."\t".'<nowiki>'. $all_categories.'</nowiki>'."\n";
							#die;	
						}
					}
				}
			} #end foreach
		}
	}
}

sub F090_Sterbejahr_fehlt_viele_Jahre_old{
	my $error_code = 90; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Sterbedatum fehlt, Geburtsjahr schon 120 Jahre her';
				$error_description[$error_code][1] = 'Das Feld STERBEDATUM ist leer, aber das Geburtsjahr ist schon über 120 Jahre her. Hier sollte ein Sterbedatum (z.B. "1992" oder "20. Jahrhundert" oder "zwischen 1940 und 1950" ) eingefügt werden.';
				$error_description[$error_code][2] = 2;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {


		if ( $sterbedatum eq '' and $geburtsjahr ne '') {
			if ($geburtsjahr > 10 and $geburtsjahr +120 < $akJahr) {
				fehlermeldung($error_code, $title, $geburtsjahr);
				#print "\t".$title."\n";
				#die;	
			}
		}
	}
}

#################################################

sub F091_Sterbeort_ebenda{
	my $error_code = 91; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
			$error_description[$error_code][0] = 'Sterbeort mit "ebenda"';
			$error_description[$error_code][1] = 'Das Feld STERBEORT ist mit "ebenda" gefüllt. Das mag im Text gehen, aber ist nicht für dieses Datenfeld sinnvoll. Bitte den korrekten Ort angeben.';
			$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		if ( $sterbeort_plain eq 'ebenda' 
			or $sterbeort_plain eq 'ebda.'
			or $sterbeort_plain eq 'ebd.'
			or $sterbeort_plain eq 's.O.'
			or $sterbeort_plain eq 'daselbst'
			or $sterbeort_plain eq 'ebendort'
			) {
			fehlermeldung($error_code, $title, $sterbeort_plain);
			#print $title."\n";
			#print $sterbeort_plain."\n";
		}
	}
}





#################################################
sub F092_Datum_ohne_Zahl{
	my $error_code = 92; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
		$error_description[$error_code][0] = 'Geburts- oder Sterbedatum ohne Zahl';
		$error_description[$error_code][1] = 'Das Feld GEBURTSDATUM oder STERBEDATUM sollte ein Datum enthalten und somit mindestens eine Ziffer besitzen. Alle hier aufgelisteten Artikel haben keine Ziffer im entsprechenden Feld';
		$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		F092_Datum_ohne_Zahl2($error_code, $geburtsdatum);
		F092_Datum_ohne_Zahl2($error_code, $sterbedatum);
	}
}

sub F092_Datum_ohne_Zahl2{
	my $error_code  = $_[0];
	my $test_datum  = $_[1];
	my $test_datum2 = $test_datum;
	if ( $test_datum ne '' and $test_datum ne ' '
		 #and $test_datum ne 'unbekannt'
		 and not $test_datum2 =~ s/[0-9]//g
		) {
		fehlermeldung($error_code, $title, '"'.$test_datum.'"');
		#print $title."\n";
		#print $test_datum2."\n";
		#die;
	}
}

sub F093_Datum_mit_Sonderzeichen{
	my $error_code = 93; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
		$error_description[$error_code][0] = 'Geburts- oder Sterbedatum mit Sonderzeichen';
		$error_description[$error_code][1] = 'Das Feld GEBURTSDATUM oder STERBEDATUM enthält eine Sonderzeichen( *?,;:/?={}&%~´`) was dort nicht sein sollte. (AUSNAHMEN: Daten des antiken griechischen Kalenders die beispielsweise "450/49 v. Chr." können aus irgenwelchen Gründen nicht genau in den Gregorianischen Kalender umgerechnet werden.) ';
		$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		F093_Datum_mit_Sonderzeichen2($error_code, $geburtsdatum);
		F093_Datum_mit_Sonderzeichen2($error_code, $sterbedatum);
	}
}

sub F093_Datum_mit_Sonderzeichen2{
	my $error_code = $_[0];
	my $test_datum = $_[1];
	if ($test_datum ne ''
		and (
			index($test_datum, '?') > -1
		or index($test_datum, '-') > -1 
		or (index($test_datum, '/') > -1 and index($test_datum, 'v. Chr.') == -1)
		or index($test_datum, '=') > -1 
		or (index($test_datum, ':') > -1 and index($test_datum, 'unsicher:') == -1)
		or index($test_datum, ')') > -1 
		or index($test_datum, '(') > -1
		or index($test_datum, ';') > -1 
		or index($test_datum, '&') > -1 
		or index($test_datum, '*') > -1 
		or index($test_datum, '~') > -1 
		or index($test_datum, '+') > -1 
		or index($test_datum, '´') > -1 
		or index($test_datum, '`') > -1
		or index($test_datum, '%') > -1
			
		)) 
	{
		fehlermeldung($error_code, $title, $test_datum);
		#print $title."\n";
		#print $test_datum."\n";
		#die;
	}
}


sub F094_Datum_nicht_korrekt_angegeben{
	my $error_code = 94; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
		$error_description[$error_code][0] = 'Geburts- oder Sterbedatum mit falschem Format';
		$error_description[$error_code][1] = 'Das Feld GEBURTSDATUM oder STERBEDATUM ist nicht im korrektem Format (z.B. "v.Chr" statt "v. Chr.")';
		$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		F094_Datum_nicht_korrekt_angegeben2($error_code, $geburtsdatum, $geburtsmonat);
		F094_Datum_nicht_korrekt_angegeben2($error_code, $sterbedatum, $sterbemonat);
	}
}

sub F094_Datum_nicht_korrekt_angegeben2{
	my $error_code = $_[0];
	my $test_datum =  $_[1];
	my $monat =  $_[2];
	my $test_datum2 = $test_datum;
	if ($test_datum ne '' 
		and $monat eq ''
		and $test_datum2 =~ s/[a-z]//g
		and index($test_datum, 'nach ') == -1
		and index($test_datum, 'um ') == -1
		and index($test_datum, 'vor ') == -1
		and index($test_datum, 'zwischen ') == -1
		and index($test_datum, ' oder ') == -1
		and index($test_datum, 'Jahrhundert') == -1
		and index($test_datum, 'getauft ') == -1
		and index($test_datum, 'begraben ') == -1
		#and index($test_datum, 'unbekannt') == -1
		and index($test_datum, 'v. Chr.') == -1
		and index($test_datum, 'n. Chr.') == -1
		and index($test_datum, 'unsicher:') == -1
		and index($test_datum, 'Jahrtausend') == -1
		)
	{
		fehlermeldung($error_code, $title, $test_datum);
		#print $title."\n";
		#print $test_datum."\n";
		#die;
	}
}


sub F095_Datum_nur_einer_eckigen_Klammer{
	my $error_code = 95; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
		$error_description[$error_code][0] = 'Geburts- oder Sterbedatum mit nur einer eckigen Klammer';
		$error_description[$error_code][1] = 'Das Feld GEBURTSDATUM oder STERBEDATUM mit nur einer eckigen Klammer ( <nowiki>[1989] statt [[1989]]</nowiki>)';
		$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		F095_Datum_nur_einer_eckigen_Klammer2($error_code, $geburtsdatum);
		F095_Datum_nur_einer_eckigen_Klammer2($error_code, $sterbedatum);
	}
}

sub F095_Datum_nur_einer_eckigen_Klammer2{
	my $error_code = $_[0];
	my $test_datum = $_[1];
	my $test_datum1 = $test_datum;
	my $test_datum2 = $test_datum;
	my $test_datum3 = $test_datum;
	my $test_datum4 = $test_datum;
	if ($test_datum ne '' 
		and ( $test_datum1=~ s/ \[[^\[]//g
		or $test_datum2=~ s/[0-9]\]$//g
		or $test_datum3=~ s/[a-z0-9]\] //g
		or $test_datum3=~ s/ \[[a-z0-9]//g
		))
	{
		fehlermeldung($error_code, $title, $test_datum);
		#print $title."\n";
		#print $test_datum."\n";
		#die;
	}
}


sub F096_Datum_Alter_zu_hoch{
	my $error_code = 96; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Geburts- und Sterbedatum ergeben zu hohes Alter';
				$error_description[$error_code][1] = 'Die Felder GEBURTSDATUM und STERBEDATUM ergeben zusammen ein sehr hohes Alter der Person. (Maximum ist aktuell 122 Jahre, <a href="http://de.wikipedia.org/wiki/Jeanne Calment">Jeanne Calment</a>) Wenn das Alter über 115 liegt, dann wird die Person hier mit angezeigt und sollte überprüft werden. Meist ist es ein Tippfehler. Folgende Personen wurden schon automatisch vorher ausgeschlossen: [[Jimmu]], [[Kōan (Tennō)]], [[Kōrei]], [[Xu Xun]], [[Yohani Kinyala Lauwo]], [[Sujin]], [[Zhaozhou]] [[Jeanne Calment]], [[Christian Mortensen]], [[Izumi Shigechiyo]], [[Joseph Brunner]], [[Zhang Daoling]] , [[Zhaozhou Congshen]] und [[Anton Adner]]. Weitere Personen, die hier nicht angezeigt werden sollen, können auf der Diskussionsseite gemeldet werden.';
				$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		if ($geburtsdatum ne '' and $sterbedatum ne '' and $geburtsjahr ne '' and $sterbejahr ne '') {
			
			#print $title."\n";

			my $alter = int($sterbejahr) - int($geburtsjahr);
			#print $alter."\n";
			#die;
			if ($alter > 115
				and $title ne 'Jimmu'
				and $title ne 'Kōan (Tennō)'
				and $title ne 'Kōrei'
				and $title ne 'Thomas Parr'
				and $title ne 'Xu Xun'
				and $title ne 'Yohani Kinyala Lauwo'
				and $title ne 'Sujin'
				and $title ne 'Zhaozhou'
				and $title ne 'Jeanne Calment'
				and $title ne 'Christian Mortensen'
				and $title ne 'Izumi Shigechiyo'
				and $title ne 'Joseph Brunner'
				and $title ne 'Zhang Daoling'
				and $title ne 'Anton Adner'
				and $title ne 'Zhaozhou Congshen'
				and $title ne 'Martin Kaschke'
				and $title ne 'Yuthog Nyingma Yönten Gönpo'
				and $title ne 'Nagata Tokuhon'
				) {
				#print $geburtsjahr."\n";
				#print $sterbejahr."\n";
				fehlermeldung($error_code, $title, $alter);
				#print $title."\n";
				#print $alter."\n";
				#die;
			}
		}
	}
}

sub F097_Datum_Punkt_nach_Tag_fehlt{
	my $error_code = 97; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
		$error_description[$error_code][0] = 'Geburts- oder Sterbedatum ohne Punkt nach dem Tag';
		$error_description[$error_code][1] = 'Das Feld GEBURTSDATUM oder STERBEDATUM hat ein Datum, bei dem nach dem Tag der Punkt fehlt (4 Mai 1234 statt 4. Mai 1234)';
		$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		F097_Datum_Punkt_nach_Tag_fehlt2($error_code, $geburtsdatum);
		F097_Datum_Punkt_nach_Tag_fehlt2($error_code, $sterbedatum);
	}
}

sub F097_Datum_Punkt_nach_Tag_fehlt2{
	my $error_code = $_[0];
	my $test_datum = $_[1];
	my $test_datum1 = $test_datum;
	if ($test_datum ne '' 
		and ( $test_datum1=~ s/([1-9]|[0-3][0-9])([^.]) (Januar|Februar|März|April|Mai|Juni|Juli|August|September|Oktober|November|Dezember)//g
		))
	{
		fehlermeldung($error_code, $title, $test_datum);
		#print $title."\n";
		#print $test_datum."\n";
		#die;
	}
}

sub F098_Datum_nach_Punkt_fehlt_Leerzeichen{
	my $error_code = 98; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
		$error_description[$error_code][0] = 'Geburts- oder Sterbedatum ohne Leerzeichen oder Komma nach dem Punkt';
		$error_description[$error_code][1] = 'Das Feld GEBURTSDATUM oder STERBEDATUM hat ein Datum, bei dem nach dem Punkt ein Leerzeichen oder Komma fehlt (Bsp. 4.Mai 1234 statt 4. Mai 1234).';
		$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		F098_Datum_nach_Punkt_fehlt_Leerzeichen2($error_code, $geburtsdatum_plain);
		F098_Datum_nach_Punkt_fehlt_Leerzeichen2($error_code, $sterbedatum_plain);
	}
}

sub F098_Datum_nach_Punkt_fehlt_Leerzeichen2{
	my $error_code = $_[0]; 
	my $test_datum = $_[1];
	my $test_datum1 = $test_datum;
	if ($test_datum ne '' 
		and ( $test_datum1=~ s/\.([^ ^,])//g
		))
	{
		fehlermeldung($error_code , $title, $test_datum);
		#print $title."\n";
		#print $test_datum."\n";
		#die;
	}
}


sub F099_Datum_Punkt_nach_Buchstabe_vor_Zahl{
	my $error_code = 99; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
		$error_description[$error_code][0] = 'Geburts- oder Sterbedatum mit Punkt nach Buchstabe und vor Zahl';
		$error_description[$error_code][1] = 'Das Feld GEBURTSDATUM oder STERBEDATUM hat ein Datum, bei dem nach einem Buchstaben und vor einer Zahl ein Punkt kommt (Bsp. 4. Mai. 1234 statt 4. Mai 1234)';
		$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		F099_Datum_Punkt_nach_Buchstabe_vor_Zahl2($error_code, $geburtsdatum_plain);
		F099_Datum_Punkt_nach_Buchstabe_vor_Zahl2($error_code, $sterbedatum_plain);
	}
}

sub F099_Datum_Punkt_nach_Buchstabe_vor_Zahl2{
	my $error_code = $_[0];
	my $test_datum = $_[1];
	my $test_datum1 = $test_datum;
	if ($test_datum ne '' 
		and ( $test_datum1=~ s/[a-z]\. [0-9]//g
		))
	{
		fehlermeldung($error_code, $title, $test_datum);
		#print $title."\n";
		#print $test_datum."\n";
		#die;
	}
}

sub F100_Datum_mit_Punkt_und_falschem_Format{
	my $error_code = 100; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
		$error_description[$error_code][0] = 'Geburts- oder Sterbedatum mit Punkt und falschem Format';
		$error_description[$error_code][1] = 'Das Feld GEBURTSDATUM oder STERBEDATUM enthält einen Punkt, aber hat nicht das korrekte Format.';
		$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		F100_Datum_mit_Punkt_und_falschem_Format2($error_code, $geburtsdatum_plain);
		F100_Datum_mit_Punkt_und_falschem_Format2($error_code, $sterbedatum_plain);
	}
}

sub F100_Datum_mit_Punkt_und_falschem_Format2{
	my $error_code = $_[0];
	my $test_datum = $_[1];
	my $test_datum1 = $test_datum;
	my $test_datum2 = $test_datum;
	my $test_datum3 = $test_datum;
	my $test_datum4 = $test_datum;
	my $test_datum5 = $test_datum;
	my $test_datum6 = $test_datum;
	my $test_datum7 = $test_datum;
	my $test_datum8 = $test_datum;
	my $test_datum9 = $test_datum;
	my $test_datum10 = $test_datum;
	my $test_datum11 = $test_datum;
	if ($test_datum ne '' 
		and ( index($test_datum, '.')>-1
		and not $test_datum1=~ s/0\.//g
		and not $test_datum2=~ s/1\.//g
		and not $test_datum3=~ s/2\.//g
		and not $test_datum4=~ s/3\.//g
		and not $test_datum5=~ s/4\.//g
		and not $test_datum6=~ s/5\.//g
		and not $test_datum7=~ s/6\.//g
		and not $test_datum8=~ s/7\.//g
		and not $test_datum9=~ s/8\.//g
		and not $test_datum10=~ s/9\.//g
		and not $test_datum11=~ s/Chr//g
		))
	{
		fehlermeldung($error_code, $title, $test_datum);
		#print $title."\n";
		#print $test_datum."\n";
		#die;
	}
}

sub F101_Datum_mit_Punkt_und_Zahl_in_falschem_Format{
	my $error_code = 101; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
		$error_description[$error_code][0] = 'Geburts- oder Sterbedatum mit Punkt und Zahl aber falschem Format';
		$error_description[$error_code][1] = 'Das Feld GEBURTSDATUM oder STERBEDATUM enthält einen Punkt und Zahl, aber hat nicht das korrekte Format.';
		$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		F101_Datum_mit_Punkt_und_Zahl_in_falschem_Format2($error_code, $geburtsdatum_plain);
		F101_Datum_mit_Punkt_und_Zahl_in_falschem_Format2($error_code, $sterbedatum_plain);
	}
}

sub F101_Datum_mit_Punkt_und_Zahl_in_falschem_Format2{
	my $error_code = $_[0];
	my $test_datum = $_[1];
	my $test_datum1 = $test_datum;
	my $test_datum2 = $test_datum;
	my $test_datum3 = $test_datum;
	my $test_datum4 = $test_datum;

	if ($test_datum ne '' 
		and ( index($test_datum, '.')>-1
		and not $test_datum1=~ s/[A-Z]//g
		and $test_datum2=~ s/[0-9]//g
		and $test_datum3=~ s/^( |[0-9]|.)*$//g
		))
	{
		fehlermeldung($error_code, $title, $test_datum);
		#print $title."\n";
		#print $test_datum."\n";
		#die;
	}
}


sub F102_Datum_auf_Zahl_folgt_Leerzeichen_und_Buchstabe{
	my $error_code = 102; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
		$error_description[$error_code][0] = 'Geburts- oder Sterbedatum mit Zahl vor Leerzeichen und Buchstabe';
		$error_description[$error_code][1] = 'Im Feld GEBURTSDATUM oder STERBEDATUM folgt auf eine Zahl ein Leerzeichen und ein Buchstabe.';
		$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		F102_Datum_auf_Zahl_folgt_Leerzeichen_und_Buchstabe2($error_code, $geburtsdatum_plain);
		F102_Datum_auf_Zahl_folgt_Leerzeichen_und_Buchstabe2($error_code, $sterbedatum_plain);
	}
}

sub F102_Datum_auf_Zahl_folgt_Leerzeichen_und_Buchstabe2{
	my $error_code = $_[0];
	my $test_datum = $_[1];
	my $test_datum1 = $test_datum;
	my $test_datum2 = $test_datum;
	my $test_datum3 = $test_datum;
	my $test_datum4 = $test_datum;
	my $test_datum5 = $test_datum;
	my $test_datum6 = $test_datum;
	
	if ($test_datum ne '' 
		and (  $test_datum1=~ s/[0-9] [A-Z]//g
		and not $test_datum2=~ s/[0-9] v\.//g
		and not $test_datum3=~ s/[0-9] n\.//g
		and not $test_datum4=~ s/[0-9] und//g
		and not $test_datum5=~ s/[0-9] oder//g
		and not $test_datum6=~ s/[0-9] (Januar|Februar|März|April|Mai|Juni|Juli|August|September|Oktober|November|Dezember)//g
		))
	{
		fehlermeldung($error_code, $title, $test_datum);
		#print $title."\n";
		#print $test_datum."\n";
		#die;
	}
}



sub F103_Datum_ohne_zwischen{
	my $error_code = 103; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
		$error_description[$error_code][0] = 'Geburts- oder Sterbedatum ohne "zwischen"';
		$error_description[$error_code][1] = 'Im Feld GEBURTSDATUM oder STERBEDATUM gibt es "vor" und "nach", was als "zwischen" beschrieben werden kann (Bsp.: "nach 555, vor 562" statt "zwischen 555 und 562")';
		$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		F103_Datum_ohne_zwischen2($error_code, $geburtsdatum_plain);
		F103_Datum_ohne_zwischen2($error_code, $sterbedatum_plain);
	}
}

sub F103_Datum_ohne_zwischen2{
	my $error_code = $_[0];
	my $test_datum = $_[1];
	my $test_datum1 = $test_datum;
	my $test_datum2 = $test_datum;
	
	if ($test_datum ne '' 
		and (  $test_datum1=~ s/vor//g
		and $test_datum2=~ s/nach//g
		))
	{
		fehlermeldung($error_code, $title, $test_datum);
		#print $title."\n";
		#print $test_datum."\n";
		#die;
	}
}

sub F104_Datum_mit_leeren_Klammern{
	my $error_code = 104; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
		$error_description[$error_code][0] = 'Geburts- oder Sterbedatum mit leeren Klammern';
		$error_description[$error_code][1] = 'Im Feld GEBURTSDATUM oder STERBEDATUM enthält leere Klammern (Bsp.: "<nowiki>[] 1980</nowiki>").';
		$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		F104_Datum_mit_leeren_Klammern2($error_code, $geburtsdatum_plain);
		F104_Datum_mit_leeren_Klammern2($error_code, $sterbedatum_plain);
	}
}

sub F104_Datum_mit_leeren_Klammern2{
	my $error_code = $_[0];
	my $test_datum = $_[1];
	my $test_datum1 = $test_datum;
	my $test_datum2 = $test_datum;
	
	if ($test_datum ne '' 
		and (  $test_datum1=~ s/\[\]//g ))
	{
		fehlermeldung($error_code, $title, $test_datum);
		#print $title."\n";
		#print $test_datum."\n";
		#die;
	}
}

sub F105_Datum_mit_Punkt_am_Anfang{
	my $error_code = 105; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
		$error_description[$error_code][0] = 'Geburts- oder Sterbedatum mit Punkt am Anfang';
		$error_description[$error_code][1] = 'Im Feld GEBURTSDATUM oder STERBEDATUM enthält einen Punkt am Anfang (Bsp.: ". Mai 1956").';
		$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		F105_Datum_mit_Punkt_am_Anfang2($error_code, $geburtsdatum_plain);
		F105_Datum_mit_Punkt_am_Anfang2($error_code, $sterbedatum_plain);
	}
}

sub F105_Datum_mit_Punkt_am_Anfang2{
	my $error_code = $_[0];
	my $test_datum = $_[1];
	my $test_datum1 = $test_datum;
	my $test_datum2 = $test_datum;
	
	if ($test_datum ne '' 
		and (  $test_datum1=~ s/^[ ]?+\.//g ))
	{
		fehlermeldung($error_code, $title, $test_datum);
		#print $title."\n";
		#print $test_datum."\n";
		#die;
	}
}

sub F106_Datum_mit_ueberfluessigem_Wort{
	my $error_code = 106; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
		$error_description[$error_code][0] = 'Geburts- oder Sterbedatum mit überflüssigem Wort';
		$error_description[$error_code][1] = 'Im Feld GEBURTSDATUM oder STERBEDATUM enthält ein überflüssiges Wort (am, im, in, geboren, gestorben, wohl, angeblich).';
		$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		F106_Datum_mit_ueberfluessigem_Wort2($error_code, $geburtsdatum_plain);
		F106_Datum_mit_ueberfluessigem_Wort2($error_code, $sterbedatum_plain);
	}
}

sub F106_Datum_mit_ueberfluessigem_Wort2{
	my $error_code = $_[0];
	my $test_datum = $_[1];
	my $test_datum1 = $test_datum;

	
	if ($test_datum ne '' 
		and ( index( $test_datum, 'am ') > -1
		or index( $test_datum, 'im ') > -1
		or index( $test_datum, 'in ') > -1
		or index( $test_datum, 'geboren') > -1
		or index( $test_datum, 'gestorben') > -1
		or index( $test_datum, 'wohl') > -1
		or index( $test_datum, 'angeblich') > -1
		
		))
	{
		fehlermeldung($error_code, $title, $test_datum);
		#print $title."\n";
		#print "\t\t".$test_datum."\n";
		#die;
	}
}


sub F107_Datum_mit_fehlerhaften_Klammern{
	my $error_code = 107; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
			$error_description[$error_code][0] = 'Geburts- oder Sterbedatum mit fehlerhaften Klammern';
			$error_description[$error_code][1] = 'Das Feld GEBURTSDATUM oder STERBEDATUM enthält eine Klammer aber nicht den entsprechenden öffnenden oder schließenden Klammerteil. ';
			$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		F107_Datum_mit_fehlerhaften_Klammern2($error_code, $geburtsdatum_plain);
		F107_Datum_mit_fehlerhaften_Klammern2($error_code, $sterbedatum_plain);
	}
}

sub F107_Datum_mit_fehlerhaften_Klammern2{
	my $error_code = $_[0];
	my $test_datum = $_[1];
	if ( $test_datum ne '' 
		and (
		   (index($test_datum , '(') > -1 and index($test_datum , ')') == -1)
		or (index($test_datum , ')') > -1 and index($test_datum , '(') == -1)
		or (index($test_datum , '{') > -1 and index($test_datum , '}') == -1)
		or (index($test_datum , '}') > -1 and index($test_datum , '{') == -1)
		or (index($test_datum , '[') > -1 and index($test_datum , ']') == -1)
		or (index($test_datum , ']') > -1 and index($test_datum , '[') == -1)
		))
		 {
			fehlermeldung($error_code, $title, $test_datum);
			#print $title."\n";
			#print $test_datum."\n";
			#die;
	}
}

sub F108_Datum_mit_mehr_als_31_Tagen{
	my $error_code = 108; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
			$error_description[$error_code][0] = 'Geburts- oder Sterbedatum mit zu hoher Tagesanzahl im Monat';
			$error_description[$error_code][1] = 'Das Feld GEBURTSDATUM oder STERBEDATUM enthält ein Datum mit mehr Tagen als in diesem Monat erlaubt sind. (Bsp.: 35. Mai 1975). ';
			$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		F108_Datum_mit_mehr_als_31_Tagen2($error_code, $geburtsdatum_plain, $geburtsjahr);
		F108_Datum_mit_mehr_als_31_Tagen2($error_code, $sterbedatum_plain, $sterbejahr);
	}
}

sub F108_Datum_mit_mehr_als_31_Tagen2{
	my $error_code  = $_[0];
	my $test_datum  = $_[1];
	my $test_jahr   = $_[2];
	
	my $test_datum1 = $test_datum;
	my $test_datum2 = $test_datum;
	my $test_datum3 = $test_datum;
	my $test_datum4 = $test_datum;
	my $test_datum5 = $test_datum;
	if ( $test_datum ne '' 
		and $title ne 'Georg Neuhofer'
		and (
				   ( $test_datum1=~ s/[3][2-9]\. (Januar|Februar|März|April|Mai|Juni|Juli|August|September|Oktober|November|Dezember)//g
				  or $test_datum2=~ s/[4-9][0-9]\. (Januar|Februar|März|April|Mai|Juni|Juli|August|September|Oktober|November|Dezember)//g 
				  or $test_datum3=~ s/31\. (Februar|April|Juni|September|November)//g
				  or $test_datum4=~ s/30\. Februar//g
				   )
		  or
				  ( 
				  # 29. Februar in Nicht-Schaltjahren
				  $test_jahr ne ''
				  and int($test_jahr) > 0
				  and not int($test_jahr) % 4 == 0
				  and $test_datum5 =~ s/29\. Februar//g
				   )
		   )
		)
		{
			fehlermeldung($error_code, $title, $test_datum);
			#print "\t".$title."\n";
			#print "\t\t\t".$test_datum."\n";
			#die;
	}
}


sub F109_Datum_Alter_zu_niedrig{
	my $error_code = 109; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Geburts- und Sterbedatum ergeben zu niedriges Alter';
				$error_description[$error_code][1] = 'Die Felder GEBURTSDATUM und STERBEDATUM ergeben zusammen ein zu niedriges Alter der Person. (z.B. -180)';
				$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		if ($geburtsdatum ne '' and $sterbedatum ne "" and $geburtsjahr ne "" and $sterbejahr ne "") {
			
			#print $title."\n";

			my $alter = int($sterbejahr) - int($geburtsjahr);
			#print $alter."\n";
			#die;
			if ($alter < 0 ) {
				#print $geburtsjahr."\n";
				#print $sterbejahr."\n";
				fehlermeldung($error_code, $title, $alter);
				#print $title."\n";
				#print $alter."\n";
				#die;
			}
		}
	}
}


sub F110_Datum_fehlt_aber_Kurzbeschreibung_mit_Jahr{
	my $error_code = 110; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Geburts- und Sterbedatum leer, aber Zeitangabe in Kurzbeschreibung';
				$error_description[$error_code][1] = 'Die Felder GEBURTSDATUM und STERBEDATUM sind leer, aber in der Kurzbeschreibung gibt es eine Jahresangabe. Diese kann man nutzen um wenigstens ein Datenfeld mit einem Datumswert zu füllen (z.B. "zwischen 1850 und 1900" oder "vor 1340").';
				$error_description[$error_code][2] = 3;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		my $test_kurz = $kurz;
		if ($geburtsdatum eq '' and $sterbedatum eq "" and $kurz ne "" ) {
			
			if (   index($kurz, 'Jahr') > -1
				or index($kurz, 'Jh.') > -1 
				or index($kurz, 'Jhr.') > -1
				or index($kurz, 'Chr.') > -1
				or index($kurz, 'v. Chr') > -1
				or ($test_kurz=~ s/[0-9][0-9]//g and index($kurz, 'Dynastie') == -1)
				)
				{
				#print $geburtsjahr."\n";
				#print $sterbejahr."\n";
				fehlermeldung($error_code, $title, $kurz);
				#print $title."\n";
				#print $kurz."\n";
				#die;
			}
		}
	}
}

sub F111_Datum_Geburtsjahr_nicht_vor_Chr{
	my $error_code = 111; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Geburts- und Sterbedatum ohne v. Chr.';
				$error_description[$error_code][1] = 'Die Felder STERBEDATUM steht "v. Chr" aber im Feld GEBURTSDATUM findet sich kein "v. Chr.". Das muss ein Fehler sein.';
				$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		my $test_kurz = $kurz;
		my $test_geb = $geburtsdatum;
		my $test_sterb = $sterbedatum;
		$test_geb =~ s/ //g;
		$test_sterb =~ s/ //g;
		if ($test_geb ne '' and $test_sterb ne '' ) {
			
			if (    index($sterbedatum, 'v. Chr') > -1
				and  index($geburtsdatum, 'v. Chr') == -1
				)
				{
				#print $geburtsjahr."\n";
				#print $sterbejahr."\n";
				fehlermeldung($error_code, $title, $geburtsdatum.", ".$sterbedatum);
				#print "\t".$title."\n";
				#print "\t\t\t".$geburtsdatum."\t".$sterbedatum."\n";
				#die;
			}
		}
	}
}



sub F112_Datum_ohne_Leerzeichen_nach_Komma{
	my $error_code = 112; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
			$error_description[$error_code][0] = 'Geburts- oder Sterbedatum ohne Leerzeichen nach Komma';
			$error_description[$error_code][1] = 'Das Feld GEBURTSDATUM oder STERBEDATUM hat ein Komma, dem kein Leerzeichen folgt. ';
			$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		F112_Datum_ohne_Leerzeichen_nach_Komma2($error_code, $geburtsdatum_plain);
		F112_Datum_ohne_Leerzeichen_nach_Komma2($error_code, $sterbedatum_plain);
	}
}

sub F112_Datum_ohne_Leerzeichen_nach_Komma2{
	my $error_code = $_[0];
	my $test_data = $_[1];
	my $test_data1 = $_[1];
	
	if ($test_data ne '' 
		and $test_data1 =~ /,[^ ]/g )
		{
			fehlermeldung($error_code, $title, $test_data);
			#print "\t".$title."\n";
			#print "\t\t\t". $test_data."\n";
			#die;		
		}
}

sub F113_Datum_ohne_Leerzeichen_vor_und_nach_Monat{
	my $error_code = 113; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
			$error_description[$error_code][0] = 'Geburts- oder Sterbedatum ohne Leerzeichen vor oder nach Monat';
			$error_description[$error_code][1] = 'Das Feld GEBURTSDATUM oder STERBEDATUM hat kein Leerzeichen vor oder nach dem Monat (z.B. "4.Januar1943"). In den meisten hier aufgelisteten Fällen sind es Tippfehler. In manchen Fällen wurde auch ein geschütztes Leerzeichen eingebaut <code>&amp;nbsp;</code>, was entfernt werden sollte.' ;
			$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		F113_Datum_ohne_Leerzeichen_vor_und_nach_Monat2($error_code, $geburtsdatum_plain);
		F113_Datum_ohne_Leerzeichen_vor_und_nach_Monat2($error_code, $sterbedatum_plain);
	}
}

sub F113_Datum_ohne_Leerzeichen_vor_und_nach_Monat2{
	my $error_code  = $_[0];
	my $test_datum  = $_[1];
	my $test_datum1 = $test_datum;
	my $test_datum2 = $test_datum;
	my $test_datum3 = $test_datum;
	my $test_datum4 = $test_datum;

	if ( $test_datum ne '' 
		and 
		  ( $test_datum1=~ s/(Januar|Februar|März|April|Mai|Juni|Juli|August|September|Oktober|November|Dezember)//g
		and not $test_datum2=~ s/(Januar|Februar|März|April|Mai|Juni|Juli|August|September|Oktober|November|Dezember)$//g
		and ( not $test_datum3=~ s/(Januar|Februar|März|April|Mai|Juni|Juli|August|September|Oktober|November|Dezember) //g
		or $test_datum4=~ s/[^ ](Januar|Februar|März|April|Mai|Juni|Juli|August|September|Oktober|November|Dezember)//g
		)))
		{
			fehlermeldung($error_code, $title, $test_datum);
			#print "\t".$title."\n";
			#print "\t\t\t". $test_datum."\n";
			#die;		
		}
}

sub F114_Datum_fehlerhaften_Text{
	my $error_code = 114; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
			$error_description[$error_code][0] = 'Geburts- oder Sterbedatum mit fehlerhaftem Text';
			$error_description[$error_code][1] = 'Das Feld GEBURTSDATUM oder STERBEDATUM enthält fehlerhaften Text (z.B. "getauft am 3. März 1345" statt "getauft 3. März 1345").' ;
			$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		F114_Datum_fehlerhaften_Text2($error_code, $geburtsdatum_plain);
		F114_Datum_fehlerhaften_Text2($error_code, $sterbedatum_plain);
	}
}

sub F114_Datum_fehlerhaften_Text2{
	my $error_code  = $_[0];
	my $test_datum  = $_[1];
	my $test_datum1 = $test_datum;
	my $test_datum2 = $test_datum;
	my $test_datum3 = $test_datum;
	my $test_datum4 = $test_datum;

	if ( $test_datum ne '' 
		and 
		  ( index ($test_datum, 'um oder nach') >-1
		or index ($test_datum, 'um oder vor') >-1
		or index ($test_datum, 'getauft') >0  # steht nicht am anfang
		or index ($test_datum, 'getauft am') >-1
		or index ($test_datum, 'auf der') >-1)
		)
		{
			fehlermeldung($error_code, $title, $test_datum);
			#print "\t".$title."\n";
			#print "\t\t\t". $test_datum."\n";
			#die;		
		}
}

sub F115_Datum_falsches_Ende{
	my $error_code = 115; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
			$error_description[$error_code][0] = 'Geburts- oder Sterbedatum mit falschem Ende';
			$error_description[$error_code][1] = 'Das Feld GEBURTSDATUM oder STERBEDATUM enthält ein fehlerhaftes Ende (z.B. "3. März 1345 verschollen" statt "um 3. März 1345"). Manchmal sind auch einfach nur zahlreiche Leerzeichen oder Tabulatoren hinter der letzten eigentlichen Zeichen des Datums eingetragen, die problemlos entfernt werden können. ' ;
			$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		F115_Datum_falsches_Ende2($error_code, $geburtsdatum_plain);
		F115_Datum_falsches_Ende2($error_code, $sterbedatum_plain);
	}
}
sub F115_Datum_falsches_Ende2{
	my $error_code  = $_[0];
	my $test_datum  = $_[1];
	my $test_datum1 = $test_datum;
	my $test_datum2 = $test_datum;
	my $test_datum3 = $test_datum;
	my $test_datum4 = $test_datum;
	my $test_datum5 = $test_datum;
	my $test_datum6 = $test_datum;

	if ( $test_datum ne '' and $test_datum ne ' '  
		and 
		  not ( $test_datum1=~ s/(Januar|Februar|März|April|Mai|Juni|Juli|August|September|Oktober|November|Dezember)$//g
		  or $test_datum2=~ s/[0-9]$//g
		  or $test_datum3=~ s/[0-9] $//g
		  or $test_datum4=~ s/ v. Chr.$//g
		  or $test_datum5=~ s/ Jahrhundert$//g
		  or $test_datum6=~ s/ Jahrtausend$//g
		  
		  #or $test_datum6=~ s/unbekannt//g
		))
		{
			fehlermeldung($error_code, $title, '"'.$test_datum.'"');
			#print "\t".$title."\n";
			#print "\t\t\t". $test_datum."\n";
			#die;		
		}
}

sub F116_Datum_falsches_zwischen{
	my $error_code = 116; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
			$error_description[$error_code][0] = 'Geburts- oder Sterbedatum mit falschem Format für Zeiträume';
			$error_description[$error_code][1] = 'Das Feld GEBURTSDATUM oder STERBEDATUM enthält ein fehlerhaftes Format für Zweiträume (z.B. "zwischen 3. und 9. April 1992" statt "zwischen 3. April 1992 und 9. April 1992").' ;
			$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		F116_Datum_falsches_zwischen2($error_code, $geburtsdatum_plain);
		F116_Datum_falsches_zwischen2($error_code, $sterbedatum_plain);
	}
}

sub F116_Datum_falsches_zwischen2{
	my $error_code  = $_[0];
	my $test_datum  = $_[1];
	my $test_datum1 = $test_datum;
	my $test_datum2 = $test_datum;
	my $test_datum3 = $test_datum;
	my $test_datum4 = $test_datum;
	my $test_datum5 = $test_datum;
	my $test_datum6 = $test_datum;

	if ( $test_datum ne '' and
		 index($test_datum1, 'zwischen')> -1 and
		 ( $test_datum2=~ /[0-9]?[0-9]\. und [0-9]?[0-9]\./ or
		   $test_datum3=~ /[0-9]?[0-9]\. oder [0-9]?[0-9]\./ )
		)
		{
			fehlermeldung($error_code, $title, '"'.$test_datum.'"');
			#print "\t".$title."\n";
			#print "\t\t\t". $test_datum."\n";
			#die;		
		}
}



sub F117_Datum_ebenda {
	my $error_code = 117; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Sterbeort im Text als ebenda';
				$error_description[$error_code][1] = 'Das Feld GEBURTSORT ist gefüllt, aber ein STERBEORT fehlt. Im Text wurde das Sterbejahr gefolgt von "ebenda" gefunden (z.B. "*1615 in Hof; † 1675 ebenda")';
				$error_description[$error_code][2] = 2;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		if ( $sterbeort eq '' and $sterbejahr ne '') {
			my $test_datum = $sterbejahr;
			if (   index ($text, $sterbejahr.' ebenda') > -1 
				or index ($text, $sterbejahr.']] ebenda') > -1 
				or index ($text, $sterbejahr.' ebda.') > -1
				or index ($text, $sterbejahr.']] ebda.') > -1
				or index ($text, $sterbejahr.' ebd.') > -1
				or index ($text, $sterbejahr.']] ebd.') > -1
				or index ($text, $sterbejahr.' daselbst') > -1
				or index ($text, $sterbejahr.']] daselbst') > -1
				or index ($text, $sterbejahr.' ebendort') > -1
				or index ($text, $sterbejahr.']] ebendort') > -1
				)  {
			
				my $pos = index ($text, $sterbejahr.' ebenda');
				$pos = index ($text, $sterbejahr.']] ebenda') if ($pos == -1);
				$pos = index ($text, $sterbejahr.' ebda.')    if ($pos == -1);
				$pos = index ($text, $sterbejahr.']] ebda.')  if ($pos == -1);
				$pos = index ($text, $sterbejahr.' ebd.')     if ($pos == -1);
				$pos = index ($text, $sterbejahr.']] ebd.')   if ($pos == -1);			
				$pos = index ($text, $sterbejahr.' daselbst')     if ($pos == -1);
				$pos = index ($text, $sterbejahr.']] daselbst')   if ($pos == -1);		
				$pos = index ($text, $sterbejahr.' ebendort')     if ($pos == -1);
				$pos = index ($text, $sterbejahr.']] ebendort')   if ($pos == -1);				
				my $fehlertext = substr( $text, $pos, 50);
				fehlermeldung($error_code, $title, '<nowiki>'.$fehlertext.'</nowiki>');			
				#print "\t".$title."\n";
				#print "\t\t\t". $fehlertext."\n";
				#die;				
			}
		
		}
	}
}

sub F118_Datum_mit_Null{
	my $error_code = 118; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
			$error_description[$error_code][0] = 'Geburts- oder Sterbedatum mit führender Null';
			$error_description[$error_code][1] = 'Das Feld GEBURTSDATUM oder STERBEDATUM enthält eine führende Null (z.B. "05. Mai 1846").';
			$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		F118_Datum_mit_Null2($error_code, $geburtsdatum_plain);
		F118_Datum_mit_Null2($error_code, $sterbedatum_plain);
	}
}

sub F118_Datum_mit_Null2{
	my $error_code = $_[0];
	my $test_datum  = $_[1];
	my $test_datum2 = $test_datum;
	if ( $test_datum2  =~ /0[1-9]\. (Januar|Februar|März|April|Mai|Juni|Juli|August|September|Oktober|November|Dezember)/) {

			fehlermeldung($error_code, $title, '"'.$test_datum.'"');	
			#print "\t".$title."\n";
			#print "\t\t\t". $test_datum."\n";
			#die;				
	}
}

sub F119_Datum_mit_geboren_Jahrhundert{
	my $error_code = 119; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
					$error_description[$error_code][0] = 'Geburtsdatum ohne Inhalt, aber Kategorie:Geboren im Jahrhundert';
					$error_description[$error_code][1] = 'Das Feld GEBURTSDATUM ist leer, aber es gibt eine Kategorie Geboren im Jahrhundert. (z.B. "Geboren im 5. Jahrhundert"). Das sollte dann auch in den Personendaten eingetragen werden.';
					$error_description[$error_code][2] = 2;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		if ($geburtsdatum eq '' and $all_categories ne '') {
			my $test_cat = $all_categories;
			if ( $test_cat =~ /Geboren im /) {
				my $fehler_text = substr ($all_categories, index($all_categories, 'Geboren'));
				$fehler_text = substr ($fehler_text, 0, index($fehler_text, ']]'));
				$fehler_text = substr ($fehler_text, 0, index($fehler_text, '|')) if (index($fehler_text, '|') > -1);
				fehlermeldung($error_code, $title, '<nowiki>'.$fehler_text.'</nowiki>');
				#print "\t".$title."\n";
				#print "\t\t\t". $fehler_text."\n";
				#die;	
			}
		}
	}
}

sub F120_Datum_mit_gestorben_Jahrhundert{
	my $error_code = 120; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
					$error_description[$error_code][0] = 'Sterbedatum ohne Inhalt, aber Kategorie:Gestorben im Jahrhundert';
					$error_description[$error_code][1] = 'Das Feld STERBEDATUM ist leer, aber es gibt eine Kategorie Gestorben im Jahrhundert. (z.B. "Gestorben im 5. Jahrhundert"). Das sollte dann auch in den Personendaten eingetragen werden.';
					$error_description[$error_code][2] = 2;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		if ($sterbedatum eq '' and $all_categories ne '') {
				my $test_cat = $all_categories;
				if ( $test_cat =~ /Gestorben im /) {
					my $fehler_text = substr ($all_categories, index($all_categories, 'Gestorben'));
					$fehler_text = substr ($fehler_text, 0, index($fehler_text, ']]'));
					$fehler_text = substr ($fehler_text, 0, index($fehler_text, '|')) if (index($fehler_text, '|') > -1);
					fehlermeldung($error_code, $title, '<nowiki>'.$fehler_text.'</nowiki>');
					#print "\t".$title."\n";
					#print "\t\t\t". $fehler_text."\n";
				}
		}
	}
}


sub F121_Datum_Jahr_nicht_im_Text {
	my $error_code = 121; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Geburts- oder Sterbejahr fehlt im Text';
				$error_description[$error_code][1] = 'Das Geburts- oder Sterbejahr konnte nicht im Text gefunden werden, obwohl es in den Personendaten enthalten ist. Hier ist ein Fehler in den Personendaten enthalten, der unbedingt entfernt werden sollte.';
				$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {	
		F121_Datum_Jahr_nicht_im_Text2($error_code, $geburtsjahr) if ($geburtsjahr ne '') ;
		F121_Datum_Jahr_nicht_im_Text2($error_code, $sterbejahr)  if ($sterbejahr ne '');
	}
}

sub F121_Datum_Jahr_nicht_im_Text2 {
	my $error_code = $_[0];
	my $testjahr = $_[1];
	my $testtext = $text;
	my $pos = index($testtext, '{{Personendaten');
	$testtext = substr($testtext, 0, $pos);
	$pos = index($testtext, '[[Kategorie:');
	$testtext = substr($testtext, 0, $pos);	
	
	if ( $is_redirect == 0 and $testjahr > 0 and  index($testtext, $testjahr) == -1) {
				fehlermeldung($error_code, $title, $testjahr);
				#print "\t".$title."\n";
				#print "\t\t\t". $testjahr."\n";
				#die;
	}
	
}


sub F122_Datum_Tag_Monat_nicht_im_Text {
	my $error_code = 122; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
			$error_description[$error_code][0] = 'Geburts- oder Sterbetag fehlt im Text';
			$error_description[$error_code][1] = 'Das Geburts- oder Sterbetag und Monat konnte nicht im Text gefunden werden, obwohl es in den Personendaten enthalten ist. Hier ist ein Fehler in den Personendaten enthalten, der unbedingt entfernt werden sollte.';
			$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {		
		F122_Datum_Tag_Monat_nicht_im_Text2($error_code, $geburtstag, $geburtsmonat) if ($geburtstag ne '') ;
		F122_Datum_Tag_Monat_nicht_im_Text2($error_code, $sterbetag, $sterbemonat)  if ($sterbetag ne '');
	}
}

sub F122_Datum_Tag_Monat_nicht_im_Text2 {
	my $error_code = $_[0];
	my $testtag = $_[1];
	my $testmonat = $_[2];
	my $testtext = $text;
	my $pos = index($testtext, '{{Personendaten');
	$testtext = substr($testtext, 0, $pos);
	$pos = index($testtext, '[[Kategorie:');
	$testtext = substr($testtext, 0, $pos);	
	
	#print $title, $testtag, $testmonat."\n";
	my $testmonat2 = '';
	$testmonat2 = 'Januar' 		if ($testmonat == 1);
	$testmonat2 = 'Februar' 	if ($testmonat == 2);
	$testmonat2 = 'März' 		if ($testmonat == 3);
	$testmonat2 = 'April' 		if ($testmonat == 4);
	$testmonat2 = 'Mai' 		if ($testmonat == 5);
	$testmonat2 = 'Juni' 		if ($testmonat == 6);
	$testmonat2 = 'Juli' 		if ($testmonat == 7);
	$testmonat2 = 'August' 		if ($testmonat == 8);
	$testmonat2 = 'September' 	if ($testmonat == 9);
	$testmonat2 = 'Oktober' 	if ($testmonat == 10);
	$testmonat2 = 'November' 	if ($testmonat == 11);
	$testmonat2 = 'Dezember' 	if ($testmonat == 12);
	
	if ( $is_redirect == 0 and $testtag > 0 and $testmonat >0) {
		my $found = 'no';
		$found = 'yes' if (index($testtext, $testtag.'. '.$testmonat2) > -1);
		$found = 'yes' if (index($testtext, $testtag.'.&nbsp;'.$testmonat2) > -1); # festes Leerzeichen [[2.&nbsp;Mai]]
		$found = 'yes' if (index($testtext, $testtag.'. Jänner') > -1 and $testmonat == 1) ; #Ausname für Österreich
		$found = 'yes' if (index(lc($testtext), 'julgregdatum|'.$testtag.'|'.$testmonat.'|') > -1);
		$found = 'yes' if (index(lc($testtext), 'julgregdatum|0'.$testtag.'|'.$testmonat.'|') > -1);
		$found = 'yes' if (index(lc($testtext), 'julgregdatum|0'.$testtag.'|0'.$testmonat.'|') > -1);
		$found = 'yes' if (index(lc($testtext), 'julgregdatum|'.$testtag.'|0'.$testmonat.'|') > -1);
		$found = 'yes' if (index(lc($testtext), 'julgregdatum|link="true"|'.$testtag.'|'.$testmonat.'|') > -1);
		$found = 'yes' if (index(lc($testtext), 'julgregdatum|link="true"|0'.$testtag.'|'.$testmonat.'|') > -1);
		$found = 'yes' if (index(lc($testtext), 'julgregdatum|link="true"|0'.$testtag.'|0'.$testmonat.'|') > -1);
		$found = 'yes' if (index(lc($testtext), 'julgregdatum|link="true"|'.$testtag.'|0'.$testmonat.'|') > -1);
		#$found = 'yes' if (index($testtext, $testtag.'. '.$testmonat) == -1);
		
		if ($found eq 'no') {
			fehlermeldung($error_code, $title, $testtag.'. '.$testmonat2);
			#print "\t".$title."\n";
			#print "\t\t\t". $testtag.'. '.$testmonat2."\n";
			#die;
		}
	}
	
}





sub F123_Datum_Restfehler{
	my $error_code = 123; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
			$error_description[$error_code][0] = 'Geburts- oder Sterbedatum mit unbekannten Fehler';
			$error_description[$error_code][1] = 'Das Feld GEBURTSDATUM oder STERBEDATUM entspricht nicht dem üblichen Standard, aber es wurde kein definierter Fehler gefunden.';
			$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {	
	
		if ($this_article_is_with_errors == 0) {
			F123_Datum_Restfehler2($error_code, $geburtsdatum_plain);
			F123_Datum_Restfehler2($error_code, $sterbedatum_plain);
		}
	}
}

sub F123_Datum_Restfehler2{
	my $error_code  = $_[0];
	my $test_datum  = $_[1];
	my $test_datum1 = $test_datum;
	my $test_datum2 = $test_datum;
	my $test_datum3 = $test_datum;
	my $test_datum4 = $test_datum;
	my $test_datum5 = $test_datum;
	my $test_datum6 = $test_datum;
	my $test_datum7 = $test_datum;
	if ( $test_datum ne '' 
		and (
		  ( not $test_datum1=~ s/^(Januar|Jänner|Februar|März|April|Mai|Juni|Juli|August|September|Oktober|November|Dezember)//g
		and not $test_datum2=~ s/^([1-9])//g
		and not $test_datum3=~ s/^(0[1-9])//g
		and not $test_datum4=~ s/^( |um|vor|nach|zwischen|begraben|getauft|unsicher)//g
		#and not $test_datum5=~ s/^(Anfang|Mitte|Ende)//g
		#and not $test_datum6=~ s/^(Frühling|Sommer|Herbst|Winter)//g
		#and not $test_datum7=~ s/^(unbekannt|vermutlich)//g
		)))
		{
			fehlermeldung($error_code, $title, $test_datum);
			#print "\t".$title."\n";
			#print "\t\t\t".$test_datum."\n";
			#die;
	}
}



#####################################
# Sonstige Fehler

sub F124_Artikelname_im_Datenfeld{
	my $error_code = 124; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Artikelname im Datenfeld';
				$error_description[$error_code][1] = 'Ein Datenfeld (Kurzbeschreibung, Geburts- und Sterbedatum und -ort) enthält die gleiche Zeichenkette wie der Titel des Artikels';
				$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {	
		if (   $title eq $kurz
			or $title eq $geburtsdatum
			or $title eq $sterbedatum
			or $title eq $geburtsort
			or $title eq $sterbeort
			)
			{
				fehlermeldung($error_code, $title, '');
				#print "\t".$title."\n";
				#print "\t\t\t".$test_datum."\n";
				#die;
		}
	}
}


sub F125_Kategorie_im_Datenfeld{
	my $error_code = 125; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
			$error_description[$error_code][0] = 'Datenfeld mit Kategorie';
			$error_description[$error_code][1] = 'In einem Datenfeld steht die Zeichenkette "Kategorie", was auf einen Datenfehler hinweist.';
			$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {	
		F125_Kategorie_im_Datenfeld2($error_code, $name);
		F125_Kategorie_im_Datenfeld2($error_code, $aname);
		F125_Kategorie_im_Datenfeld2($error_code, $kurz);
		F125_Kategorie_im_Datenfeld2($error_code, $geburtsdatum);
		F125_Kategorie_im_Datenfeld2($error_code, $geburtsort);
		F125_Kategorie_im_Datenfeld2($error_code, $sterbedatum);
		F125_Kategorie_im_Datenfeld2($error_code, $sterbeort);
	}
}

sub F125_Kategorie_im_Datenfeld2{
	my $error_code = $_[0];
	my $test_data = $_[1];
	if (   index( $test_data, 'Kategorie:') > -1		)
		{
			$test_data = '<nowiki>'. $test_data .'</nowiki>';
			fehlermeldung($error_code, $title, $test_data);
			#print "\t".$title."\n";
			#print "\t\t\t".$test_data."\n";
			#die;
	}
}

sub F126_Interwiki_im_Datenfeld{
	my $error_code = 126; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
			$error_description[$error_code][0] = 'Datenfeld mit Interwiki';
			$error_description[$error_code][1] = 'In einem Datenfeld steht ein Interwiki-Link auf eine andere Sprache, was nicht sein sollte.';
			$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {	
		F126_Interwiki_im_Datenfeld2($error_code, $name);
		F126_Interwiki_im_Datenfeld2($error_code, $aname);
		F126_Interwiki_im_Datenfeld2($error_code, $kurz);
		F126_Interwiki_im_Datenfeld2($error_code, $geburtsdatum);
		F126_Interwiki_im_Datenfeld2($error_code, $geburtsort);
		F126_Interwiki_im_Datenfeld2($error_code, $sterbedatum);
		F126_Interwiki_im_Datenfeld2($error_code, $sterbeort);
	}
}

sub F126_Interwiki_im_Datenfeld2{
	my $error_code = $_[0];
	my $test_data = $_[1];
	my $test_data1 = $test_data;
	if (  $test_data1=~ s/\[..://g)
		{
			fehlermeldung($error_code, $title, $test_data);
			#print "\t".$title."\n";
			#print "\t\t\t".$test_data."\n";
			#die;
	}
}




sub F127_Verlinkung_fehlerhaft{
	my $error_code = 127; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
			$error_description[$error_code][0] = 'Datenfeld mit fehlerhafter Verlinkung ';
			$error_description[$error_code][1] = 'In einem Datenfeld ist eine fehlerhafte Verlinkung enthalten (Bsp. <nowiki>[[Rom|Rom|Römer]] statt [[Rom|Römer]]</nowiki>).';
			$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		F127_Verlinkung_fehlerhaft2($error_code, $name);
		F127_Verlinkung_fehlerhaft2($error_code, $kurz);
		F127_Verlinkung_fehlerhaft2($error_code, $geburtsdatum);
		F127_Verlinkung_fehlerhaft2($error_code, $geburtsort);
		F127_Verlinkung_fehlerhaft2($error_code, $sterbedatum);
		F127_Verlinkung_fehlerhaft2($error_code, $sterbeort);
	}
}

sub F127_Verlinkung_fehlerhaft2{
	my $error_code = $_[0];
	my $test_data = $_[1];
	my $test_data1 = $test_data;
	$test_data1=~ s/(\[\[)([^|]*)\|([^]]*)(\]\])/$3/g;
	if (  index( $test_data1 , '|') > -1 )
		{
			fehlermeldung($error_code, $title, $test_data);
			#print "\t".$title."\n";
			#print "\t\t\t".$test_data."\n";
			#die;
	}
}



sub F128_Kategorie_Mann_Frau_fehlt{
	my $error_code = 128; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Kategorie:Mann oder Kategorie:Frau fehlt';
				$error_description[$error_code][1] = 'Bei einem Artikel über eine Person muss entweder die "Kategorie:Mann",  "Kategorie:Frau", "Kategorie:Intersexueller" enthalten sein. Handelt es sich um eine Personengruppe sollte diese mit der "Kategorie:Personengruppe" oder ähnlichen gekennzeichnet werden. Wenn die Personendaten auf einer Seite mit REDIRECT (Weiterleitung) untergebracht sind, dann sollten die Personenkategorien auch dort enthalten sein. ';
				$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		if ( $all_categories ne '' 
			 and index( $all_categories , ':Mann') == -1 
			 and index( $all_categories , ':Frau') == -1 
			 and index( $all_categories , ':Intersexueller') == -1 
			 and index( $all_categories , ':Geschlecht unbekannt') == -1 )
			{

				fehlermeldung($error_code, $title, "");
				#print "\t".$title."\n";
				#print "\t\t\t".$all_categories."\n\n\n";
				#die;
		}
	}
}


sub F129_Kategorie_geboren_doppelt {
	my $error_code = 129; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Kategorie:Geboren doppelt';
				$error_description[$error_code][1] = 'Die Kategorie Geboren ist in diesem Artikel mehrmals enthalten.';
				$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		my $test = lc($all_categories);
		if ( $all_categories ne '' and 
			$test =~ /kategorie:geboren /) 
			{
			#print $error_code ,"\n\n".$title,' ', $all_categories, "\n\n" if ($title eq 'Otto Stern');
			
			# restlichen Kategorien rausfiltern, für erneuten Vergleich
			my $pos = index (lc ($all_categories), 'kategorie:geboren ');
			$test = substr($all_categories, $pos );
			#print $test, "\n\n" if ($title eq 'Otto Stern');
			$pos = index (lc ($test), ']');
			$test = lc(substr($test, $pos ));
			#print $test, "\n\n" if ($title eq 'Otto Stern');
			
			if ( $test =~ /kategorie:geboren /	# /kategorie:geboren [0-9]+[^\.]/
			     or $test =~ /kategorie:geboren im/) {
				fehlermeldung($error_code, $title, '<nowiki>'. $all_categories . '</nowiki>' );
				#print $error_code."\t".$title."\t".'<nowiki>'. $all_categories.'</nowiki>'."\n";
				#die;
			}
			#die if ($title eq 'Otto Stern');
		}
	}
}

sub F130_Kategorie_gestorben_doppelt {
	my $error_code = 130; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Kategorie:Gestorben doppelt';
				$error_description[$error_code][1] = 'Die Kategorie Gestorben ist in diesem Artikel mehrmals enthalten.';
				$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		my $test = lc($all_categories);
		if ( $all_categories ne '' and 
			$test =~ /kategorie:gestorben /)  						# /kategorie:gestorben [0-9]+[^\.]/
			{
			#print 'F988'.$title,' ', $all_categories, "\n";
			#die;
			my $pos = index (lc ($all_categories), 'kategorie:gestorben');
			$test = substr($all_categories, $pos );
			$pos = index (lc ($test), ']');
			$test = lc(substr($test, $pos ));
			if ( $test =~ /kategorie:gestorben /) {		# /kategorie:gestorben [0-9]+[^\.]/
				fehlermeldung($error_code, $title, '<nowiki>'. $all_categories . '</nowiki>' );
				#print "\t".$title."\n";
				#print "\t\t\t".$all_categories."\n";			
				#die;
			}
		}
	}
}


sub F131_Kategorie_geboren_Geburtsjahr_Differenz {
	my $error_code = 131; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Geburtsdatum anders als Kategorie:Geboren';
				$error_description[$error_code][1] = 'Das Feld GEBURTSDATUM enthält eine Jahreszahl, die nicht in der vorhandenen "Kategorie:Geboren" vorkommt (z.B. "GEBURTSDATUM=1. Februar 1966" und "Kategorie:Geboren 1968"). Meist handelt es sich um Tippfehler oder Copy-and-Paste-Fehler.';
				$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		if ( #$parameter{'modus_scan'}  eq "live" and 
			 $all_categories ne '' and 
			 $geburtsjahr ne '') {
			
			#print $all_categories."\n";
			#die;
			my $test = lc($all_categories);
			if (#$test =~ /kategorie:geboren [0-9]+[^\.]/
				#and 
				index ($all_categories, 'Kategorie:Geboren '.$geburtsjahr) == -1
				and $geburtsjahr >1 ){
				my $pos = index (lc ($all_categories), 'kategorie:geboren');
				my $pos2 = index (lc ($all_categories), ']',$pos);
				my $notice = $geburtsjahr.' vs. '.substr($all_categories, $pos , $pos2 - $pos);
				fehlermeldung($error_code, $title, $notice);
				#print "\t".$title."\n"; 
				#print "\t".$geburtsjahr."\n";
				#print "\t\t\t".$all_categories."\n";
				#die;
			}
			
		}
	}
}




sub F132_Kategorie_gestorben_Sterbejahr_Differenz {
	my $error_code = 132; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Sterbedatum anders als Kategorie:Gestorben';
				$error_description[$error_code][1] = 'Das Feld STERBEDATUM enthält eine Jahreszahl, die nicht in der vorhandenen "Kategorie:Gestorben" vorkommt (z.B. "STERBEDATUM=1. Februar 1966" und "Kategorie:Gestorben 1968"). Meist handelt es sich um Tippfehler oder Copy-and-Paste-Fehler.';
				$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		if ( #$parameter{'modus_scan'}  eq "live" and 
			 $all_categories ne '' and 
			 $sterbejahr ne '') {
			
			#print $all_categories."\n";
			#die;
			my $test = lc($all_categories);
			if (#$test =~ /kategorie:gestorben [0-9]+[^\.]/
				#and 
				index ($all_categories, 'Kategorie:Gestorben '.$sterbejahr) == -1
				and $sterbejahr >1 ){
				my $pos = index (lc ($all_categories), 'kategorie:gestorben');
				my $pos2 = index (lc ($all_categories), ']',$pos);
				my $notice = $sterbejahr.' vs. '.substr($all_categories, $pos , $pos2 - $pos);
				fehlermeldung($error_code, $title, $notice);
				#print "\t".$title."\n";
				#print "\t".$sterbejahr.' vs. '. substr($all_categories, $pos , 24)."\n";
				#print "\t\t\t".$all_categories."\n";
				#die;
			}
			
		}
	}
}

sub F133_Kategorie_geboren_vorhanden {
	my $error_code = 133; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Geburtsdatum leer, aber Kategorie:Geboren vorhanden';
				$error_description[$error_code][1] = 'Das Feld GEBURTSDATUM ist leer, aber es gibt einen Kategorieneintrag (z.B. "Kategorie:Geboren 1955").';
				$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		if ( #$parameter{'modus_scan'}  eq "live" and 
			 $all_categories ne '' and 
			 $geburtsdatum eq '' and 
			 $text ne '') {
			
			#print $all_categories."\n";
			#die;
			my $test = lc($all_categories);
			if ($test =~ /kategorie:geboren [0-9]+[^\.]/){
				fehlermeldung($error_code, $title, "");
				#print "\t".$title."\n";
				#print "\t\t\t".$all_categories."\n";
				#die;
			}
			
		}
	}
}

sub F134_Kategorie_gestorben_vorhanden {
	my $error_code = 134; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Sterbedatum leer, aber Kategorie:Gestorben vorhanden';
				$error_description[$error_code][1] = 'Das Feld STERBEDATUM ist leer, aber es gibt einen Kategorieneintrag (z.B. "Kategorie:Gestorben 2008").';
				$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		if ( #$parameter{'modus_scan'}  eq "live" and 
			 $all_categories ne '' and 
			 $sterbedatum eq '' and 
			 $text ne '') {
			
			#print $all_categories."\n";
			#die;
			my $test = lc($all_categories);
			if ($test =~ /kategorie:gestorben [0-9]+[^\.]/){
				fehlermeldung($error_code, $title, "");
				#print "\t".$title."\n";
				#print "\t\t\t".$all_categories."\n";
				#die;
			}
			
		}
	}
}



sub F135_Kategorie_geboren_und_gestorben_fehlt{
	my $error_code = 135; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Kategorie:Geboren und Kategorie:Gestorben fehlen';
				$error_description[$error_code][1] = 'Es gibt im Feld GEBURTSDATUM und im STERBEDATUM eine Jahreszahl, aber es fehlt der Kategorieneintrag (z.B. "Kategorie:Geboren 1958" sowie "Kategorie:Gestorben 2008").';
				$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		if ( #$parameter{'modus_scan'}  eq "live" and 
			 $all_categories ne '' and 
			 $geburtsjahr ne '' and
			 $sterbejahr ne '') {
			
			#print $all_categories."\n";
			#die;
			my $test = lc($all_categories);
			my $test2 = lc($all_categories);
			my $test3 = lc($all_categories);
			my $test4 = lc($all_categories);
			if ( ($test =~ /kategorie:geboren/  and $test2 =~ /kategorie:gestorben/) 
				  or ($test3 =~ /kategorie:geboren/)
				  or ($test4 =~ /kategorie:gestorben/)
				){
				#ok
			} else {
				fehlermeldung($error_code, $title, $geburtsjahr);
				#print "\t".$title."\n";
				#print "\t\t\t".$geburtsjahr."\n";
				#die;
			}
			
		}
	}
}


sub F136_Kategorie_geboren_fehlt{
	my $error_code = 136; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Kategorie:Geboren fehlt';
				$error_description[$error_code][1] = 'Es gibt im Feld GEBURTSDATUM eine Jahreszahl, aber es fehlt der Kategorieneintrag (z.B. "Kategorie:Geboren 1958" oder "Kategorie:Geboren im 16. Jahrhundert").';
				$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		if ( #$parameter{'modus_scan'}  eq "live" and 
			 $all_categories ne '' and 
			 $geburtsjahr ne '') {
			
			#print $all_categories."\n";
			#die;
			my $test = lc($all_categories);
			if ($test =~ /kategorie:geboren/ ){
				#ok
			} else {
				fehlermeldung($error_code, $title, $geburtsjahr);
				#print "\t".$title."\n";
				#print "\t\t\t".$geburtsjahr."\n";
				#die;
			}
			
		}
	}
}

sub F137_Kategorie_gestorben_fehlt{
	my $error_code = 137; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Kategorie:Gestorben fehlt';
				$error_description[$error_code][1] = 'Es gibt im Feld STERBEDATUM eine Jahreszahl, aber es fehlt der Kategorieneintrag (z.B. "Kategorie:Gestorben 1958" oder "Kategorie:Gestorben im 16. Jahrhundert").';
				$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		if ( #$parameter{'modus_scan'}  eq "live" and 
			 $all_categories ne '' and 
			 $sterbejahr ne '') {
			
			#print $all_categories."\n";
			#die;
			my $test = lc($all_categories);
			if ($test =~ /kategorie:gestorben/ ){
				#ok
			} else {
				fehlermeldung($error_code, $title, $sterbejahr);
			}
			
		}
	}
}


sub F138_Geburtsdatum_fehlt{
	my $error_code = 138; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Geburtsdatum ohne Inhalt';
				$error_description[$error_code][1] = 'Das Feld GEBURTSDATUM ist leer, aber es sollte sich eigentlich bei jeder Person ein Geburtsjahr eintragen lassen. Notfalls geht auch eine Zeitperiode (z.B. "zwischen 1960 und 1970") oder das Jahrhundert (z.B. "20. Jahrhundert") geben.';
				$error_description[$error_code][2] = 0;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		if ( $geburtsdatum eq '') {
				fehlermeldung($error_code, $title, "");
				#print $title."\n";
				#print "\t\t".$test_datum."\n";
				#print "\n";	
		}
	}
}




sub F139_Geburtsdatum_im_Text{
	my $error_code = 139; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Geburtsdatum ohne Inhalt, aber Geburtsdatum eventuell im Text';
				$error_description[$error_code][1] = 'Das Feld GEBURTSDATUM ist leer oder nur mit sehr grober Angabe z.B. "20. Jahrhundert", aber im Text (ersten 600 Zeichen) wurde ein Geburtsdatum gefunden (z.B. "* 1923"). Personen mit dem Eintrag "* 19??" kann man auch als Geburtsdatum "20. Jahrhundert" geben.';
				$error_description[$error_code][2] = 3;
				$error_description[$error_code][1] = regelbasierter_hinweistext($error_description[$error_code][1]);
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		if ( $geburtsdatum eq ''
		     #or index ($geburtsdatum , 'Jahrhundert') > -1		# später
		     #or index ($geburtsdatum , 'Jahrtausend') > -1		# später
			 ) {

			my $test1 = $text;
			$test1 = Text_verkuerzen( $test1, 600);
			
			#my $test2 = $text;
			#my $test3 = $text;
			if (   $test1 =~ /\([ ]?\*[ ]?+(\[\[)?[1-9]+/ ) {
			
				my $hilfstext = $text;
				$hilfstext = Text_verkuerzen( $hilfstext, 600);
				$hilfstext =~ s/†[ ]?+(\[\[)?[1-9]+/@@@@/;
				my $pos = index ($hilfstext, '@@@@') -20;
				$hilfstext = $text;
				$hilfstext = substr($hilfstext, $pos);
				$hilfstext = Text_verkuerzen( $hilfstext, 100);

				fehlermeldung($error_code, $title, '<nowiki>'.$hilfstext.'…</nowiki>');
				#print $error_code."\t".$title."\t".'<nowiki>'. $hilfstext.'</nowiki>'."\n";
				#die;
			}
		}
	}
}

sub F140_Sterbedatum_im_Text{
	my $error_code = 140; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Sterbedatum ohne Inhalt, aber Sterbedatum eventuell im Text';
				$error_description[$error_code][1] = 'Das Feld STERBEDATUM ist leer oder nur mit sehr grober Angabe z.B. "20. Jahrhundert" , aber im Text (ersten 600 Zeichen) wurde ein Sterbedatum gefunden (z.B. "† 1923"). Personen mit dem Eintrag "† 19??" kann man auch als Sterbedatum "20. Jahrhundert" geben.';
				$error_description[$error_code][2] = 3;
				$error_description[$error_code][1] = regelbasierter_hinweistext($error_description[$error_code][1]);
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		if ( $sterbedatum eq ''
		     #or index ($sterbedatum , 'Jahrhundert') > -1		# später
		     #or index ($sterbedatum , 'Jahrhundert') > -1		# später
			 ) {
			
			my $test1 = $text;
			
			my $pos = index($test1, ')' );				# Text bis zum ende des ersten Satzes kürzen nach der schließenden Klammer
			$pos = index($test1, '.', $pos);
			$test1 = substr ($test1, 0, $pos);
			$test1 = Text_verkuerzen( $test1, 600);
			
			my $test2 = $test1;
			
			
			#my $test2 = $text;
			#my $test3 = $text;
			if (   $test1 =~ /†[ ]?+(\[\[)?[1-9]+/ 
				or $test2 =~ /&dagger;[ ]?+(\[\[)?[1-9]+/ ) {

				my $hilfstext = $text;
				$hilfstext = Text_verkuerzen( $hilfstext, 600);
				$hilfstext =~ s/†[ ]?(\[\[)?[1-9]+/@@@@/;
				$hilfstext =~ s/&dagger;[ ]?(\[\[)?[1-9]+/@@@@/;
				my $pos = index ($hilfstext, '@@@@') -20;
				$hilfstext = $text;
				$hilfstext = substr($hilfstext, $pos);
				$hilfstext = Text_verkuerzen( $hilfstext, 100);

				fehlermeldung($error_code, $title, '<nowiki>…'.$hilfstext.'…</nowiki>');
				#print $error_code."\t".$title."\t".'<nowiki>'. $hilfstext.'</nowiki>'."\n";
				#die;
	
			}
		}
	}
}




sub F141_Sterbezeichen_im_Text{
	my $error_code = 141; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Text ohne Semikolon vor dem Sterbezeichen';
				$error_description[$error_code][1] = 'Im Fließtext wurde kein "; †" gefunden. Aber es sind Einträge in Geburtsdatum oder -ort sowie in Sterbedatum oder -ort. Deshalb müsste eigentlich dieses Zeichenkette im Text auftauchen.';
				$error_description[$error_code][2] = 3;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {

		if ( ( $geburtsort ne '' or $geburtsdatum ne '' )
			 and ($sterbeort ne '' or $sterbedatum ne '')
			 and not ($geburtsort eq '' and index( $geburtsdatum , 'Jahrhundert')> -1 )
			 and not ($sterbeort  eq '' and index( $sterbedatum  , 'Jahrhundert')> -1 )
			 and index ($text, '; †') == -1
			 and index ($text, '†') > -1
			)
			{
			my $hilfstext = '';
			$hilfstext = '; † '.$sterbedatum;
			if ($geburtsort ne '') {
				$hilfstext = 'in '.$geburtsort.$hilfstext;
				$hilfstext = $geburtsdatum.' '.$hilfstext if ($geburtsdatum ne '');
			} else {
				$hilfstext = $geburtsdatum.$hilfstext if ($geburtsdatum ne '');
			}
			$hilfstext = '* '.$hilfstext;
		
			fehlermeldung($error_code, $title, '<nowiki>'.$hilfstext.'</nowiki>');
			#print $error_code."\t".$title."\t".'<nowiki>'. $hilfstext.'</nowiki>'."\n";
			#die;
		}
	}

}



sub F142_Geburtsort_und_Todesort{
	#unbekannter Geburtsort bzw. Sterbeort aber im Text
	my $error_code = 142; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
			$error_description[$error_code][0] = 'Geburts- oder Sterbeort eventuell im Text';
			$error_description[$error_code][1] = 'In den Personendaten fehlt die Angabe des Geburts- oder Sterbeortes. Im Text (ersten 600 Zeichen) findet sich aber die Jahreszahl vom Geburts- oder Sterbejahr gefolgt von "in", "im", "auf", "bei". (z.B. 1956 in Wien). Eventuell handelt es sich dabei um die fehlende Ortsangabe. '; 
			$error_description[$error_code][2] = 3;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
	
		$double_error = 0;
		F142_Geburtsort_und_Todesort2($error_code, $geburtsjahr, $geburtsort);
		F142_Geburtsort_und_Todesort2($error_code, $sterbejahr, $sterbeort);
		
		if ( $double_error > 1 
			and $title ne 'Katharina von Hanau (gestorben 1581)') {
			F143_Geburtsort_und_Todesort3('check');
		}
	}
}

sub F142_Geburtsort_und_Todesort2{
	my $error_code = $_[0];
	my $test_jahr = $_[1];
	my $test_ort  = $_[2];

	if ( $test_jahr ne ''
		and $test_ort eq '') {
	
		my $suchtext = $test_jahr;
		my $suchtext1 = $suchtext;
		 $suchtext1 =~ s/\[//g;
		 $suchtext1 =~ s/\]//g;

		 my $hilfstext = $text;
		 $hilfstext = Text_verkuerzen( $hilfstext, 600);
		 
		 
		if ( $hilfstext =~ /($suchtext1(\]\])? (in|auf|bei|ebenda) [^ ]+)/
			) {
			$double_error = $double_error +1;
			#Text ausschneiden
			my $hilfstext2 = $hilfstext;
			$hilfstext =~ s/($suchtext1(\]\])? (in|auf|bei|ebenda) [^ ]+)/@@@@/;
			my $pos = index ($hilfstext, '@@@@');
			my $lenght1 = length($hilfstext);
			my $lenght2 = length($hilfstext2);
			#$hilfstext = substr($text, $pos, $lenght2 - $lenght1 +4);		old way
			$hilfstext = $text;
			$hilfstext = substr($hilfstext, $pos);
			$hilfstext = Text_verkuerzen( $hilfstext, 100);
		    #$hilfstext =~ s/\[//g;
		    #$hilfstext =~ s/\]//g;
			#$hilfstext =~ s/\|//g;
			fehlermeldung($error_code, $title, '<nowiki>'.$hilfstext.'…</nowiki>');
			#print $title."\n";
			#print "\t\t".$hilfstext."\n";
			#print "\n";
			#die;
		}		
		}
}

sub F143_Geburtsort_und_Todesort3{
	my $error_code = 143; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
			$error_description[$error_code][0] = 'Geburts- und Sterbeort eventuell im Text';
			$error_description[$error_code][1] = 'In den Personendaten fehlt die Angabe des Geburts- und Sterbeortes. Im Text findet sich aber die Jahreszahl vom Geburts- und Sterbejahr gefolgt von "in", "im", "auf", "bei". (z.B. 1956 in Wien). Eventuell handelt es sich dabei um die fehlende Ortsangabe. (Ausnahmen bisher [[Katharina von Hanau (gestorben 1581)]])'; 
			$error_description[$error_code][2] = 3;	
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		if ( $double_error > 1){ 
			#Fehler wird von F142 auch mit aufgerufen
			fehlermeldung($error_code, $title, '');			
			#$double_error = $double_error +1;
		}
	}
}






sub F144_Komplettes_Datum_im_Text{
	my $error_code = 144; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
			$error_description[$error_code][0] = 'Geburts- oder Sterbedatum unvollständig';
			$error_description[$error_code][1] = 'In einem Datumsfeld steht nur eine Jahreszahl (z.B. 1975). Im Text (ersten 600 Zeichen) findet sich aber zusätzlich noch eine Monatsangabe (z.B. 23. Februar 1975). Eventuell könnte man hier das Datumsfeld verbessern (z.B. "23. Februar 1975", "vor 23. Februar 1975", "nach 23. Februar 1975", "getauft 23. Februar 1975" oder "begraben 23. Februar 1975"). Bekannte Ausnahmen sind: Theda (Ostfriesland), Weiprecht I. von Helmstatt, Luis Antonio Duhau.'; 
			$error_description[$error_code][2] = 2;			
			$error_description[$error_code][1] = regelbasierter_hinweistext($error_description[$error_code][1]);
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {	
		$double_error = 0;
		#nur jahr
		F144_Komplettes_Datum_im_Text2($error_code, $geburtsdatum, $geburtsjahr, $geburtsmonat, $geburtstag);
		F144_Komplettes_Datum_im_Text2($error_code, $sterbedatum, $sterbejahr, $sterbemonat, $sterbetag);
		#nur Tag und Monat
		F144_Komplettes_Datum_im_Text3($error_code, $geburtsdatum, $geburtsjahr, $geburtsmonat, $geburtstag);
		F144_Komplettes_Datum_im_Text3($error_code, $sterbedatum, $sterbejahr, $sterbemonat, $sterbetag);
		
		F145_Komplettes_Datum_im_Text4('check') if ($double_error > 1);
	}
}


sub F144_Komplettes_Datum_im_Text2{
	# nur jahr vorhanden
	my $error_code = $_[0];
	my $test_datum = $_[1];
	my $test_jahr  = $_[2];
	my $test_monat = $_[3];
	my $test_tag   = $_[4];
	
	my $test_data1 = $test_datum;
	#print $title."\n";
	
	if ( $test_datum ne ''
		and $test_monat eq ''
		and $test_tag eq ''
		and index ($test_datum, ' oder ')== -1
		and index ($test_datum, ' und ')== -1
		and index ($test_datum, 'zwischen ')== -1
		and index ($test_datum, 'vor ')== -1
		and index ($test_datum, 'nach ')== -1
		and index ($test_datum, 'um ')== -1
		and index ($test_datum, 'unsicher:' )== -1
		and index ($test_datum, '.' )== -1
		and $test_data1 =~ /[1-2][0-9][0-9][0-9]/
		and $title ne 'Theda (Ostfriesland)'				# Ausnahmen, meist daten von den Eltern
		and $title ne 'Weiprecht I. von Helmstatt'
		and $title ne 'Luis Antonio Duhau'
		and $title ne 'Augustin Jamund'
		and $title ne 'Eugen Rittweger de Moor'
		
		) 
				
		
		{
		
		#print  $title."\t".$test_datum."\n";
		my $suchtext = $test_datum;
		
		my $suchtext1 = $suchtext;
		 $suchtext1 =~ s/\[//g;
		 $suchtext1 =~ s/\]//g;
		 $suchtext1 =~ s/\)//g;
		 $suchtext1 =~ s/\(//g;
		
		my $hilfstext = $text;
		$hilfstext = Text_verkuerzen( $hilfstext, 600);
		if ( $hilfstext =~ s/(Jänner|Januar|Februar|März|April|Mai|Juni|Juli|August|September|Oktober|November|Dezember)(\]\])? (\[\[)?$suchtext1/@@@@@/ ) {

			my $pos = index($hilfstext, '@@@@@');
			$hilfstext = substr($text, 0, $pos);			# 30 zeichen davor
			$hilfstext = reverse( $hilfstext);		
			$hilfstext = Text_verkuerzen( $hilfstext, 30);
			$hilfstext = reverse( $hilfstext);
			$hilfstext = $hilfstext. Text_verkuerzen( substr($text, $pos), 60);	# 60 Zeichen danach
			$hilfstext = '<nowiki>…'.$hilfstext.'…</nowiki>';
			fehlermeldung($error_code, $title, $test_datum.'; '.$hilfstext);
			#print $title."\n";
			#print $test_datum."\n";
			#print $hilfstext."\n";
			#die;
			$zaehler_fehler_998_nur_jahr = $zaehler_fehler_998_nur_jahr + 1;
			$double_error = $double_error +1;
		}
		
	}

}

sub F144_Komplettes_Datum_im_Text3{
	# nur Tag und Monat vorhanden
	my $error_code = $_[0];
	my $test_datum = $_[1];
	my $test_jahr  = $_[2];
	my $test_monat = $_[3];
	my $test_tag   = $_[4];
	
	my $test_data1 = $test_datum;
	#print $title."\n";
	
	if ( $test_datum ne ''
		and $test_jahr eq ''
		and index ($test_datum, ' oder ')== -1
		and index ($test_datum, ' und ')== -1
		and index ($test_datum, 'zwischen ')== -1
		and index ($test_datum, 'vor ')== -1
		and index ($test_datum, 'nach ')== -1
		and index ($test_datum, 'um ')== -1
		and $test_data1 =~ /[1-3]?[0-9]\. (Jänner|Januar|Februar|März|April|Mai|Juni|Juli|August|September|Oktober|November|Dezember)(\]\])?$/
		) 
		{
		#print "\t".$test_datum."\n";
		#die;
		my $suchtext = $test_datum;
		
		my $suchtext1 = $suchtext;
		 $suchtext1 =~ s/\[//g;
		 $suchtext1 =~ s/\]//g;
		 $suchtext1 =~ s/\)//g;
		 $suchtext1 =~ s/\(//g;
		
		my $hilfstext = $text;
		if ( $hilfstext =~ /$suchtext1(\]\])? (\[\[)?[0-9]/ ) {
			fehlermeldung($error_code, $title, $test_datum);
			#print $title."\n";
			#print "\t\t".$test_datum."\n";
			#print "\n";
			#die;
			$zaehler_fehler_998_nur_jahr = $zaehler_fehler_998_nur_jahr + 1;
			$double_error = $double_error +1;
		}
		
	}

}


sub F145_Komplettes_Datum_im_Text4{
	my $error_code = 145; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
			$error_description[$error_code][0] = 'Geburts- und Sterbedatum unvollständig';
			$error_description[$error_code][1] = 'In beiden Datumsfeldern steht nur eine Jahreszahl (z.B. 1975). Im Text findet sich aber zusätzlich noch eine Monatsangabe (z.B. 23. Februar 1975). ';
			$error_description[$error_code][2] = 2;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {	
		if ($double_error > 1){
			fehlermeldung($error_code, $title, '');
			#print $title."\n";
			#print "\t\t".$test_datum."\n";
			#print "\n";
			#die;
			$zaehler_fehler_998_nur_jahr = $zaehler_fehler_998_nur_jahr + 1;
			$double_error = $double_error +1;
		}
	}
}



sub F146_Rechtschreibung {
	my $error_code = 146; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
			$error_description[$error_code][0] = 'Kurzbeschreibung mit Rechtschreibfehler';
			$error_description[$error_code][1] = 'In einem Datenfeld befindet sich ein Rechtschreibfehler (z.B. deusch, deutch, deutsh)';
			$error_description[$error_code][2] = 3;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		#Rechtschreibung2($name);
		#Rechtschreibung2($aname);
		Rechtschreibung2($error_code, $kurz_plain);
		#Rechtschreibung2($geburtsdatum_plain);
		#Rechtschreibung2($geburtsort_plain);
		#Rechtschreibung2($sterbedatum_plain);
		#Rechtschreibung2($sterbeort_plain);
	}
}

sub Rechtschreibung2 {
	my $error_code = $_[0];
	my $test_data = $_[1];

	if ($test_data ne '') {
		#Rechtschreibung3 ($error_code, $test_data, 'richtig', 'falsch');
		Rechtschreibung3 ($error_code, $test_data, 'taiwanisch', 'taiwanesisch');		
		Rechtschreibung3 ($error_code, $test_data, 'Gymnasiallehrer',	'Gymnaiallehrer');
		Rechtschreibung3 ($error_code, $test_data, 'Sängerin',	'Sängein');
		Rechtschreibung3 ($error_code, $test_data, 'Fotograf',	'Fotograph');
		Rechtschreibung3 ($error_code, $test_data, 'griechisch',	'friechisch');
		Rechtschreibung3 ($error_code, $test_data, 'Fürst von',	'Fürstvon');
		Rechtschreibung3 ($error_code, $test_data, 'Fußball',	'Fußall');
		Rechtschreibung3 ($error_code, $test_data, 'Fußball',	'Fußballball');
		Rechtschreibung3 ($error_code, $test_data, 'Fußballspieler',	'Fußballerspieler');
		Rechtschreibung3 ($error_code, $test_data, 'Funktionär',	'Funktiönär');
		Rechtschreibung3 ($error_code, $test_data, 'Fußballspieler',	'Fußballlspieler');
		Rechtschreibung3 ($error_code, $test_data, 'Fußballtrainer',	'Fußballltrainer');
		Rechtschreibung3 ($error_code, $test_data, 'Fußballspieler',	'Fußballpieler');
		Rechtschreibung3 ($error_code, $test_data, 'Fußballspieler',	'Fußballspielerspieler');
		Rechtschreibung3 ($error_code, $test_data, 'Fußballtorhüter',	'Fußballtorhuter');
		Rechtschreibung3 ($error_code, $test_data, 'Fußballspieler',	'Fußbalspieler');
		Rechtschreibung3 ($error_code, $test_data, 'Schiedsrichter',	'Schiedrichter');
		Rechtschreibung3 ($error_code, $test_data, 'Geheimdienstler',	'Geheimdiensler');
		Rechtschreibung3 ($error_code, $test_data, 'Gelehrter',	'Gelehrterr');
		Rechtschreibung3 ($error_code, $test_data, 'General',	'Genaral');
		Rechtschreibung3 ($error_code, $test_data, 'Generaladmiral',	'Generaldmiral');
		Rechtschreibung3 ($error_code, $test_data, 'Generalleutnant',	'Gene­ralleutnant');
		Rechtschreibung3 ($error_code, $test_data, 'Sekretär',	'Sektretär');
		Rechtschreibung3 ($error_code, $test_data, 'Generalstabschef',	'Generalstabschaf');
		Rechtschreibung3 ($error_code, $test_data, 'Geräteturner',	'Gerätturner');
		Rechtschreibung3 ($error_code, $test_data, 'Gerontolog',	'Gerontolg');
		Rechtschreibung3 ($error_code, $test_data, 'Geschichtsschreiber',	'Geschichtschreiber');
		Rechtschreibung3 ($error_code, $test_data, 'Philosoph',	'Pilosoph');
		Rechtschreibung3 ($error_code, $test_data, 'Wissenschaftler',	'Wissenschftler');
		Rechtschreibung3 ($error_code, $test_data, 'Gewichtheber',	'Gewichtsheber');
		#Rechtschreibung3 ($error_code, $test_data, 'Gewerkschafter',	'Gewerkschaftler');
		Rechtschreibung3 ($error_code, $test_data, 'Gewerkschaft',	'Gewerksschaft');
		Rechtschreibung3 ($error_code, $test_data, 'Gouverneur',	'Gouverneuer');
		Rechtschreibung3 ($error_code, $test_data, 'Gouverneur',	'Gourverneur');
		Rechtschreibung3 ($error_code, $test_data, 'Graffiti',	'Grafitti');
		Rechtschreibung3 ($error_code, $test_data, 'großherzoglich',	'großherzolich');
		Rechtschreibung3 ($error_code, $test_data, 'Großherzogtum',	'Grossherzogtum');
		Rechtschreibung3 ($error_code, $test_data, 'Assyriologe',	'Assyrologe');
		Rechtschreibung3 ($error_code, $test_data, 'Anthropolog',	'Anthropholog');
		Rechtschreibung3 ($error_code, $test_data, 'Architekt',	'Architelt');
		Rechtschreibung3 ($error_code, $test_data, 'Architekt',	'Architkekt');
		Rechtschreibung3 ($error_code, $test_data, 'Architekt',	'Archtitekt');
		Rechtschreibung3 ($error_code, $test_data, 'manager',	'manger');
		Rechtschreibung3 ($error_code, $test_data, 'Außenminister',	'Aussenminister');
		Rechtschreibung3 ($error_code, $test_data, 'Ingenieur',	'Ingieur');
		Rechtschreibung3 ($error_code, $test_data, 'Bibliothekar',	'Biblothekar');
		Rechtschreibung3 ($error_code, $test_data, 'Balletttänzer',	'Ballettänzer');
		Rechtschreibung3 ($error_code, $test_data, 'Ingenieur',	'Ingeneur');
		Rechtschreibung3 ($error_code, $test_data, 'Bibliothekar',	'Biliothekar');
		Rechtschreibung3 ($error_code, $test_data, 'Chansonnier',	'Chansonier');
		Rechtschreibung3 ($error_code, $test_data, 'Doppelbürgerschaft',	'Dopperbürgerschaft');
		Rechtschreibung3 ($error_code, $test_data, 'Diplomat',	'Diplomatge');
		Rechtschreibung3 ($error_code, $test_data, 'Eisschnellläufer',	'Eissschnellläufer');
		Rechtschreibung3 ($error_code, $test_data, 'Eistänzer',	'Eitänzer');
		Rechtschreibung3 ($error_code, $test_data, 'Feministin',	'Feminstin');
		Rechtschreibung3 ($error_code, $test_data, 'Ethnograph',	'Etnograph');
		Rechtschreibung3 ($error_code, $test_data, 'Journalist',	'Jpournalist');
		Rechtschreibung3 ($error_code, $test_data, 'Ethnologe',	'Etnologe');
		Rechtschreibung3 ($error_code, $test_data, 'Moderatorin',	'Modorator');
		Rechtschreibung3 ($error_code, $test_data, 'ägyptisch',	'ägypticher');
		Rechtschreibung3 ($error_code, $test_data, 'Schauspieler',	'Schausapieler');
		Rechtschreibung3 ($error_code, $test_data, 'Ökonom',	'Ökonnom');
		Rechtschreibung3 ($error_code, $test_data, 'österreichisch',	'östereichisch');
		Rechtschreibung3 ($error_code, $test_data, 'österreichisch',	'österrischisch');
		Rechtschreibung3 ($error_code, $test_data, 'Übersetzer',	'Übersetzunger');
		Rechtschreibung3 ($error_code, $test_data, 'ägyptisch',	'ögyptisch');
		Rechtschreibung3 ($error_code, $test_data, 'österreichisch',	'ösrterreichisch');
		Rechtschreibung3 ($error_code, $test_data, 'Österreicher',	'Österreichi ');
		Rechtschreibung3 ($error_code, $test_data, 'färöisch',	'färöerisch');
		Rechtschreibung3 ($error_code, $test_data, 'färöisch',	'färörisch');
		#Rechtschreibung3 ($error_code, $test_data, 'Feldmarschallleutnant',	'Feldmarschalleutnant');
		#Rechtschreibung3 ($error_code, $test_data, 'Feldmarschallleutnant',	'Feldmarschalleutnant');
		#Rechtschreibung3 ($error_code, $test_data, 'Leutnant',	'Lieutenant');
		Rechtschreibung3 ($error_code, $test_data, 'Feldmarschall',	'Feldmarshall');
		Rechtschreibung3 ($error_code, $test_data, 'Großmeister',	'Grossmeister');
		Rechtschreibung3 ($error_code, $test_data, 'Fernseh',	'Ferseh');
		Rechtschreibung3 ($error_code, $test_data, 'fränkisch',	'ffränkisch');
		Rechtschreibung3 ($error_code, $test_data, 'Regisseur',	'Ressigeur');
		Rechtschreibung3 ($error_code, $test_data, 'Regisseur',	'Regosseur');
		Rechtschreibung3 ($error_code, $test_data, 'Schauspieler',	'Schaauspieler');
		Rechtschreibung3 ($error_code, $test_data, 'Schauspieler',	'Schaupieler');
		Rechtschreibung3 ($error_code, $test_data, 'Schauspieler',	'Schauspielerer');
		Rechtschreibung3 ($error_code, $test_data, 'Filmregisseur',	'Fimregisseur');
		#Rechtschreibung3 ($error_code, $test_data, 'finnisch-schwedisch',	'finnlandschwedisch');
		Rechtschreibung3 ($error_code, $test_data, 'finnisch-schwedisch',	'finnisch-schwedsich');
		Rechtschreibung3 ($error_code, $test_data, 'Flugzeug',	'Fluigzeug');
		Rechtschreibung3 ($error_code, $test_data, 'slowenisch',	'sloewenisch');
		Rechtschreibung3 ($error_code, $test_data, 'slowakisch',	'slovakisch');
		Rechtschreibung3 ($error_code, $test_data, 'slowenisch',	'slovenisch');
		Rechtschreibung3 ($error_code, $test_data, 'sri-lankisch',	'srilankisch');
		Rechtschreibung3 ($error_code, $test_data, 'südafrikanisch',	'südarfikanisch');
		Rechtschreibung3 ($error_code, $test_data, 'koreanisch',	'koreranisch');
		Rechtschreibung3 ($error_code, $test_data, 'ungarisch',	'unarisch');
		Rechtschreibung3 ($error_code, $test_data, 'ungarisch',	'ungatisch');
		Rechtschreibung3 ($error_code, $test_data, 'ungarisch',	'ungrisch');
		Rechtschreibung3 ($error_code, $test_data, 'uruguayisch',	'urugayisch');
		Rechtschreibung3 ($error_code, $test_data, 'uruguayisch',	'uruguaisch');
		Rechtschreibung3 ($error_code, $test_data, 'venezolanisch',	'venezolnisch');
		Rechtschreibung3 ($error_code, $test_data, 'venezolanisch',	'venezuelanisch');
		Rechtschreibung3 ($error_code, $test_data, 'venezolanisch',	'venzolanisch');
		Rechtschreibung3 ($error_code, $test_data, 'walisisch',	'walisch');
		Rechtschreibung3 ($error_code, $test_data, 'Dartspieler',	'Dartsspieler');
		Rechtschreibung3 ($error_code, $test_data, 'deutsch',	'deursch');
		Rechtschreibung3 ($error_code, $test_data, 'deutsch',	'deurtsch');
		Rechtschreibung3 ($error_code, $test_data, 'deutsch',	'deutscch');
		Rechtschreibung3 ($error_code, $test_data, 'deutsch',	'deutschh');
		Rechtschreibung3 ($error_code, $test_data, 'deutscher',	'deutschler');
		Rechtschreibung3 ($error_code, $test_data, 'deutscher',	'deutschr');
		Rechtschreibung3 ($error_code, $test_data, 'deutschsprachig',	'deutschssprachig');
		Rechtschreibung3 ($error_code, $test_data, 'deutsch',	'deutscj');
		Rechtschreibung3 ($error_code, $test_data, 'deutsch',	'deutsdeutsch');
		Rechtschreibung3 ($error_code, $test_data, 'dominikanisch',	'dominikisch');
		Rechtschreibung3 ($error_code, $test_data, 'jährig',	'jaehrig');
		Rechtschreibung3 ($error_code, $test_data, 'Drehbuch',	'Drehhbuch');
		Rechtschreibung3 ($error_code, $test_data, 'Eisschnelllauf',	'Eisschnellauf');
		Rechtschreibung3 ($error_code, $test_data, 'Eisschnellläufer',	'Eisschnelläufer');
		Rechtschreibung3 ($error_code, $test_data, 'Energie',	'Engerie');
		Rechtschreibung3 ($error_code, $test_data, 'ehemalig',	'ehamalig');
		Rechtschreibung3 ($error_code, $test_data, 'ehemalig',	'ehmalig');
		Rechtschreibung3 ($error_code, $test_data, 'ehemalig',	'ehemalug');
		Rechtschreibung3 ($error_code, $test_data, 'Edelstein',	'Edestein');
		Rechtschreibung3 ($error_code, $test_data, 'ehemalig',	'ehamalig');
		Rechtschreibung3 ($error_code, $test_data, 'ehemalig',	'ehemalug');
		Rechtschreibung3 ($error_code, $test_data, 'ehemalig',	'ehmalig');
		Rechtschreibung3 ($error_code, $test_data, 'Ethnologe',	'Etnologe');
		Rechtschreibung3 ($error_code, $test_data, 'Europaabgeordnete',	'Europabgeordnete');
		Rechtschreibung3 ($error_code, $test_data, 'kroatisch',	'krotatisch');
		Rechtschreibung3 ($error_code, $test_data, 'kubanisch',	'kuabnisch');
		Rechtschreibung3 ($error_code, $test_data, 'liberianisch',	'libaresisch');
		Rechtschreibung3 ($error_code, $test_data, 'libysch',	'libyisch');
		Rechtschreibung3 ($error_code, $test_data, 'malaysisch',	'malaiisch');
		Rechtschreibung3 ($error_code, $test_data, 'malaysisch',	'malaisch');
		Rechtschreibung3 ($error_code, $test_data, 'malaysisch',	'malayisch');
		Rechtschreibung3 ($error_code, $test_data, 'malaysisch',	'malesisch');
		Rechtschreibung3 ($error_code, $test_data, 'marokkanisch',	'marokanisch');
		Rechtschreibung3 ($error_code, $test_data, 'mongolisch',	'monolisch');
		Rechtschreibung3 ($error_code, $test_data, 'namibisch',	'namibianisch');
		Rechtschreibung3 ($error_code, $test_data, 'niederländisch',	'niederlaendisch');
		Rechtschreibung3 ($error_code, $test_data, 'niederländisch',	'niederländerisch');
		Rechtschreibung3 ($error_code, $test_data, 'niederländisch',	'niederlandisch');
		Rechtschreibung3 ($error_code, $test_data, 'niederländisch',	'nieferländisch');
		Rechtschreibung3 ($error_code, $test_data, 'niederländisch',	'nierderländisch');
		Rechtschreibung3 ($error_code, $test_data, 'österreichisch',	'östereichisch');
		Rechtschreibung3 ($error_code, $test_data, 'österreichisch',	'österreicherisch');
		Rechtschreibung3 ($error_code, $test_data, 'österreichisch',	'osterreichisch');
		Rechtschreibung3 ($error_code, $test_data, 'österreichisch',	'österreischisch');
		Rechtschreibung3 ($error_code, $test_data, 'österreichisch',	'östreichisch');
		Rechtschreibung3 ($error_code, $test_data, 'papua-neuguineisch',	'papua-neuguineeisch');
		Rechtschreibung3 ($error_code, $test_data, 'papua-neuguineisch',	'paraguayanisch');
		Rechtschreibung3 ($error_code, $test_data, 'portugiesisch',	'portugisisch');
		Rechtschreibung3 ($error_code, $test_data, 'portugiesisch',	'portuguiesisch');
		Rechtschreibung3 ($error_code, $test_data, 'portugiesisch',	'potugiesisch');
		Rechtschreibung3 ($error_code, $test_data, 'puerto-ricanisch',	'puertoricabisch');
		Rechtschreibung3 ($error_code, $test_data, 'puerto-ricanisch',	'puertorikanisch');
		Rechtschreibung3 ($error_code, $test_data, 'rumänisch',	'rumämisch');
		Rechtschreibung3 ($error_code, $test_data, 'san-marinesisch',	'san marinesisch');
		Rechtschreibung3 ($error_code, $test_data, 'saudi-arabisch',	'saudiarabisch');
		Rechtschreibung3 ($error_code, $test_data, 'schweizerisch',	'scheizerisch');
		Rechtschreibung3 ($error_code, $test_data, 'schweizerisch',	'schweisch');
		Rechtschreibung3 ($error_code, $test_data, 'sierra-leonisch',	'sierra leonisch');
		Rechtschreibung3 ($error_code, $test_data, 'simbabwisch',	'simbabwesisch');
		Rechtschreibung3 ($error_code, $test_data, 'britisch',	'british');
		Rechtschreibung3 ($error_code, $test_data, 'Chirurg',	'Chriurg');
		Rechtschreibung3 ($error_code, $test_data, 'britisch',	'btitisch');
		Rechtschreibung3 ($error_code, $test_data, 'bulgarisch',	'buglarisch');
		Rechtschreibung3 ($error_code, $test_data, 'Bildner',	'Bilner');
		Rechtschreibung3 ($error_code, $test_data, 'byzantinisch',	'byzantinsch');
		Rechtschreibung3 ($error_code, $test_data, 'amerikanisch',	'aerikanisch');
		Rechtschreibung3 ($error_code, $test_data, 'amerikanisch',	'amerianisch');
		Rechtschreibung3 ($error_code, $test_data, 'amerikanisch',	'amerikainisch');
		Rechtschreibung3 ($error_code, $test_data, 'amerikanisch',	'amerikamisch');
		Rechtschreibung3 ($error_code, $test_data, 'amerikanisch',	'amerikanerisch');
		Rechtschreibung3 ($error_code, $test_data, 'amerikanisch',	'amerikaninisch');
		Rechtschreibung3 ($error_code, $test_data, 'amerikanisch',	'amerikansisch');
		Rechtschreibung3 ($error_code, $test_data, 'amerikanisch',	'amerinkanisch');
		Rechtschreibung3 ($error_code, $test_data, 'argentinisch',	'argentienisch');
		Rechtschreibung3 ($error_code, $test_data, 'amerikanisch',	'amerkianisch');
		Rechtschreibung3 ($error_code, $test_data, 'amerikanisch',	'amerkikanisch');
		Rechtschreibung3 ($error_code, $test_data, 'amerikanisch',	'ameruikanisch');
		Rechtschreibung3 ($error_code, $test_data, 'amerikanisch',	'amrikanisch');
		Rechtschreibung3 ($error_code, $test_data, 'aserbaidschanisch',	'asberbaidschanisch ');
		Rechtschreibung3 ($error_code, $test_data, 'Comic',	'Comnic');
		#Rechtschreibung3 ($error_code, $test_data, 'bengalisch',	'bangladeschisch');
		Rechtschreibung3 ($error_code, $test_data, 'botsuanisch',	'botwanischer');
		Rechtschreibung3 ($error_code, $test_data, 'britisch',	'brtisch');
		Rechtschreibung3 ($error_code, $test_data, 'US-amerikanisch',	'US-merikanisch');
		Rechtschreibung3 ($error_code, $test_data, 'ecuadorianisch',	'ecuadoranisch');
		#Rechtschreibung3 ($error_code, $test_data, 'estnisch',	'estisch');
		Rechtschreibung3 ($error_code, $test_data, 'evangelisch',	'eveangelisch');
		Rechtschreibung3 ($error_code, $test_data, 'evangelisch',	'evnagelisch');
		Rechtschreibung3 ($error_code, $test_data, 'französisch',	'fränzösisch');
		Rechtschreibung3 ($error_code, $test_data, 'französisch',	'franzsösisch');
		Rechtschreibung3 ($error_code, $test_data, 'ghanaisch',	'gahnaisch');
		Rechtschreibung3 ($error_code, $test_data, 'griechisch',	'grichisch');
		Rechtschreibung3 ($error_code, $test_data, 'griechisch',	'griechsich');
		Rechtschreibung3 ($error_code, $test_data, 'guyanisch',	'guayanisch');
		Rechtschreibung3 ($error_code, $test_data, 'guyanisch',	'guyanesisch');
		Rechtschreibung3 ($error_code, $test_data, 'israelisch',	'isrealisch');
		Rechtschreibung3 ($error_code, $test_data, 'italienisch',	'itaienisch');
		Rechtschreibung3 ($error_code, $test_data, 'italienisch',	'italiänisch');
		Rechtschreibung3 ($error_code, $test_data, 'italienisch',	'italiene');
		Rechtschreibung3 ($error_code, $test_data, 'italienisch',	'italieneisch');
		Rechtschreibung3 ($error_code, $test_data, 'jamaikanisch',	'jamikanisch');
		Rechtschreibung3 ($error_code, $test_data, 'kap-verdisch',	'kapverdisch');
		Rechtschreibung3 ($error_code, $test_data, 'deutscher',	'deuscherscher');
		Rechtschreibung3 ($error_code, $test_data, 'deutsch',	'deurtsch');
		Rechtschreibung3 ($error_code, $test_data, 'deutsch',	'deusch');
		Rechtschreibung3 ($error_code, $test_data, 'ägyptisch',	'ägptisch');
		Rechtschreibung3 ($error_code, $test_data, 'französisch',	'franzödisch ');
		Rechtschreibung3 ($error_code, $test_data, 'französisch',	'franzoesisch ');
		Rechtschreibung3 ($error_code, $test_data, 'französisch',	'franzos ');
		Rechtschreibung3 ($error_code, $test_data, 'französisch',	'französich ');
		Rechtschreibung3 ($error_code, $test_data, 'Schauspieler',	'Schaupieler');
		Rechtschreibung3 ($error_code, $test_data, 'Archäologe',	'Archäoloe');
		Rechtschreibung3 ($error_code, $test_data, 'Professor',	'Porfessor');
		Rechtschreibung3 ($error_code, $test_data, 'Pädagog',	'Pädagogg');
		Rechtschreibung3 ($error_code, $test_data, 'paraguayisch',	'paraguayanisch');
		Rechtschreibung3 ($error_code, $test_data, 'paraguayisch',	'paraguyanisch');
		Rechtschreibung3 ($error_code, $test_data, 'persisch',	'persich');
		Rechtschreibung3 ($error_code, $test_data, 'ägyptisch',	'ägytisch');
		Rechtschreibung3 ($error_code, $test_data, 'albanisch',	'albanischis');
		Rechtschreibung3 ($error_code, $test_data, 'Alchemist',	'Alchimist');
		Rechtschreibung3 ($error_code, $test_data, 'Historikerin',	'Historikern');
		Rechtschreibung3 ($error_code, $test_data, 'amerikanisch',	'amerikansich');
		Rechtschreibung3 ($error_code, $test_data, 'amerikanisch',	'amerkanisch');
		Rechtschreibung3 ($error_code, $test_data, 'andalusisch',	'andalisich');
		#Rechtschreibung3 ($error_code, $test_data, 'angolanisch',	'anguillisch');
		Rechtschreibung3 ($error_code, $test_data, 'Architekt',	'Architekte ');
		Rechtschreibung3 ($error_code, $test_data, 'argentinisch',	'argeninisch');
		Rechtschreibung3 ($error_code, $test_data, 'argentinisch',	'argenitinisch');
		Rechtschreibung3 ($error_code, $test_data, 'argentinisch',	'argentisch');
		Rechtschreibung3 ($error_code, $test_data, 'argentinisch',	'arrgentinisch');
		Rechtschreibung3 ($error_code, $test_data, 'aserbaidschanisch',	'aserbaischanisch');
		Rechtschreibung3 ($error_code, $test_data, 'äthiopisch',	'äthopisch');
		Rechtschreibung3 ($error_code, $test_data, 'australisch',	'autralisch');
		Rechtschreibung3 ($error_code, $test_data, 'Bordellbetreiber',	'Bordelbetreiber');
		Rechtschreibung3 ($error_code, $test_data, 'bosnisch-herzegowinisch',	'bosnien-herzegowinisch');
		Rechtschreibung3 ($error_code, $test_data, 'bosnisch',	'bosnienisch');
		Rechtschreibung3 ($error_code, $test_data, 'brandenburgisch',	'brandenburisch');
		Rechtschreibung3 ($error_code, $test_data, 'Brandenburg',	'Brandenmburg');
		Rechtschreibung3 ($error_code, $test_data, 'brasilianisch',	'brasialianisch');
		Rechtschreibung3 ($error_code, $test_data, 'brasilianisch',	'brasilanisch');
		Rechtschreibung3 ($error_code, $test_data, 'brasilianisch',	'brasilisanisch');
		Rechtschreibung3 ($error_code, $test_data, 'britisch',	'britischerastronom');
		Rechtschreibung3 ($error_code, $test_data, 'nigrisch',	'nigerisch');

		
		
		
	}

}

sub Rechtschreibung3 {
	my $error_code = $_[0];
	my $test_data = $_[1];
	my $test_data1 = lc($test_data );
	my $richtig = $_[2];
	my $falsch = $_[3];	
	
	if (index ($test_data, $falsch) > -1) {
			fehlermeldung($error_code, $title, 'richtig: '.$richtig .', falsch: '.$falsch.' in: '.$test_data);
			#print "\t".$title."\n";
			#print "\t\t\t".$test_data."\n";
			#print $richtig ."\t".$falsch."\n";
			#die;
	}
}


sub F147_Monat_vor_oder{
	my $error_code = 147; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
			$error_description[$error_code][0] = 'Geburts- oder Sterbedatum mit Monat vor "oder"';
			$error_description[$error_code][1] = 'Das Geburtsdatum oder Sterbedatum enthält ein Text vor dem Wort "oder" (z.B. "17. Mai oder 31. Mai 1568"). Laut Festlegung sollte auch vor dem Wort "oder" ein komplettes Datum stehen (z.B. "17. Mai 1568 oder 31. Mai 1568"). ';
			$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		F147_Monat_vor_oder2($error_code, $geburtsdatum_plain);
		F147_Monat_vor_oder2($error_code, $sterbedatum_plain);
	}
}


sub F147_Monat_vor_oder2{
	# select error, title, geburtsdatum_plain from pd
	# where regexp_like (geburtsdatum_plain, '[a-z] oder ')
	# and not regexp_like (geburtsdatum_plain, 'hundert oder ');
	
	my $error_code = $_[0];
	my $test_datum = $_[1];
	
	#print $test_datum ."\n";
	#print $geburtsdatum_plain."\n";
	#print $sterbedatum_plain."\n";
	
	if ($test_datum =~ /[a-z] oder/
		and $test_datum =~ /[0-9]$/
	    and not $test_datum =~ /hundert oder/
		){
			fehlermeldung($error_code, $title, $test_datum);
			#print $title."\n";
			#print "\t\t".$test_datum."\n";
			#print "\n";
			#die;
	}
}



sub F148_Tag_vor_oder{
	my $error_code = 148; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
			$error_description[$error_code][0] = 'Geburts- oder Sterbedatum mit Tag vor "oder"';
			$error_description[$error_code][1] = 'Das Geburtsdatum oder Sterbedatum enthält ein Text vor dem Wort "oder" (z.B. "10. oder 11. August 1730"). Laut Festlegung sollte auch vor dem Wort "oder" ein komplettes Datum stehen (z.B. "10. August 1730 oder 11. August 1730"). ';
			$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		F148_Tag_vor_oder2($error_code, $geburtsdatum_plain);
		F148_Tag_vor_oder2($error_code, $sterbedatum_plain);
	}
}


sub F148_Tag_vor_oder2{
	# select error, title, '1001' "Fehler", geburtsdatum_plain from pd
	# where regexp_like (geburtsdatum_plain, '\. oder ')
	# and not regexp_like (geburtsdatum_plain, 'Chr\. oder ');
	
	my $error_code = $_[0];
	my $test_datum = $_[1];
	
	#print $test_datum ."\n";
	#print $geburtsdatum_plain."\n";
	#print $sterbedatum_plain."\n";
	
	if ($test_datum =~ /\. oder/
	    and not $test_datum =~ /Chr\. oder/
		){
			fehlermeldung($error_code, $title, $test_datum);
			#print $title."\n";
			#print "\t\t".$test_datum."\n";
			#print "\n";
			#die;
	}
}

sub F149_kein_zwischen_am_Anfang{
	my $error_code = 149; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
			$error_description[$error_code][0] = 'Geburts- oder Sterbedatum ohne "zwischen" am Anfang ';
			$error_description[$error_code][1] = 'Das Geburtsdatum oder Sterbedatum enthält nicht das Wort "zwischen" am Anfang, obwohl ein "und" enthalten ist (z.B. "29. Januar und 13. Februar 1766"). Laut Festlegung sollte am Anfang ein zwischen stehen (z.B. "zwischen 29. Januar 1766 und 13. Februar 1766"). ';
			$error_description[$error_code][2] = 1;	
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		F149_kein_zwischen_am_Anfang2($error_code, $geburtsdatum_plain);
		F149_kein_zwischen_am_Anfang2($error_code, $sterbedatum_plain);
	}
}


sub F149_kein_zwischen_am_Anfang2{
	my $error_code = $_[0];
	my $test_datum = $_[1];

	if ($test_datum =~ / und /
		and not $test_datum =~ /(Januar|Februar|März|April|Mai|Juni|Juli|August|September|Oktober|November|Dezember) zwischen/
	    and not $test_datum =~ /^[ ]?+zwischen/
		and not $test_datum =~ /oder zwischen /
		and not $test_datum =~ /^unsicher/
		){
			fehlermeldung($error_code, $title, $test_datum);
			#print $title."\n";
			#print "\t\t".$test_datum."\n";
			#print "\n";
			#die;
	}
}

sub F150_keine_Jahreszahl_vor_und{
	my $error_code = 150; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
			$error_description[$error_code][0] = 'Geburts- oder Sterbedatum ohne Jahreszahl vor "und"';
			$error_description[$error_code][1] = 'Das Geburtsdatum oder Sterbedatum enthält keine Jahreszahl vor dem Wort "und" (z.B. "zwischen 3. Mai und 12. Oktober 1700"). Laut Festlegung sollte aber ein komplettes Datum vor dem "und" stehen (z.B. "zwischen 3. Mai 1700 und 12. Oktober 1700"). ';
			$error_description[$error_code][2] = 1;	
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		F150_keine_Jahreszahl_vor_und2($error_code, $geburtsdatum_plain);
		F150_keine_Jahreszahl_vor_und2($error_code, $sterbedatum_plain);
	}
}


sub F150_keine_Jahreszahl_vor_und2{
	my $error_code = $_[0];
	my $test_datum = $_[1];

	if ($test_datum =~ / und /
	    and not $test_datum =~ /([0-9]|Jahrhundert)( v. Chr.)? und /
		){
	
			fehlermeldung($error_code, $title, $test_datum);
			#print $title."\n";
			#print "\t\t".$test_datum."\n";
			#print "\n";
			#die;
	}
}


sub F151_Mehrere_Kommas_im_Aname{
	my $error_code = 151; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Alternativnamen mit mehreren Kommas';
				$error_description[$error_code][1] = 'Im Feld ALTERNATIVNAMEN sind mehrere Kommas zu finden (z.B. "Otto, Manfred, Alfred Müller"). Außer bei Namen von adligen Personen ist es eigentlich nicht notwendig.';
				$error_description[$error_code][2] = 3;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		if ($aname ne '') {
			if ($aname =~ /(^|;)[^;\(]+,[^;\(]+,/ 
			and not lc($aname) =~ /(lord|fürst|baron|herzog|edler|viscount|earl|marquis|duc|comte|contessa|graf|duke|marquess|reichsfreiherr|prinz|princesse|divinity|herrn)/
			and not lc($aname) =~ /(countess|gräfin|lady)/
			and $title ne 'Wolf von Schönberg'
			and $title ne 'Budiwoj von Krumau'
			and $title ne 'Witiko I. von Krumau'
			and $title ne 'Witiko II. von Krumau'
			and $title ne 'Hans Fugger'
			and $title ne 'Augusta Karoline von Cambridge'
			and $title ne 'Blaise de Montesquiou, seigneur de Montluc'
			and $title ne 'Dominicus Bassus'
			and $title ne 'Diego Dávila Coello y Pacheco'
			and $title ne 'Pino Belli'
			){
				fehlermeldung($error_code, $title, $aname);
				#print $title."\n";
				#print "\t\t".$test_datum."\n";
				#die;
			}
		}
	}
}

sub F152_Tageszahl_ohne_Punkt{
	my $error_code = 152; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
			$error_description[$error_code][0] = 'Geburts- oder Sterbedatum mit Tag ohne Punkt';
			$error_description[$error_code][1] = 'Im Feld GEBURTSDATUM oder STERBEDATUM fehlt nach dem Tag der Punkt (z.B. "8 Mai 1991").';
			$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		F152_Tageszahl_ohne_Punkt2($error_code, $geburtsdatum_plain);
		F152_Tageszahl_ohne_Punkt2($error_code, $sterbedatum_plain);
	}
}

sub F152_Tageszahl_ohne_Punkt2{
	my $error_code = $_[0];
	my $test_datum = $_[1];

	if ($test_datum =~ /[0-9] (Januar|Februar|März|April|Mai|Juni|Juli|August|September|Oktober|November|Dezember)/
		){
			fehlermeldung($error_code, $title, $test_datum);
			#print $title."\n";
			#print "\t\t".$test_datum."\n";
			#die;
	}
}

sub F153_Tageszahl_dreistellig{
	my $error_code = 153; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
			$error_description[$error_code][0] = 'Geburts- oder Sterbedatum ohne "zwischen" am Anfang';
			$error_description[$error_code][1] = 'Im Feld GEBURTSDATUM oder STERBEDATUM wird der Tag mit einer dreistelligen Zahl angegeben (z.B. "128 Mai 1991").';
			$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		F153_Tageszahl_dreistellig2($error_code, $geburtsdatum_plain);
		F153_Tageszahl_dreistellig2($error_code, $sterbedatum_plain);
	}
}

sub F153_Tageszahl_dreistellig2{
	my $error_code = $_[0];
	my $test_datum = $_[1];

	if ($test_datum =~ /[0-9][0-9][0-9]\./
		){
			fehlermeldung($error_code, $title, $test_datum);
			#print $title."\n";
			#print "\t\t".$test_datum."\n";
			#die;
	}
}

sub F154_Datum_mit_Komma_vor_oder{
	my $error_code = 154; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
			$error_description[$error_code][0] = 'Geburts- oder Sterbedatum mit Komma vor "oder"';
			$error_description[$error_code][1] = 'Im Feld GEBURTSDATUM oder STERBEDATUM gibt es ein Komma vor dem Wort "oder" (z.B. "um 1760, oder 1769").';
			$error_description[$error_code][2] = 1;	
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		F154_Datum_mit_Komma_vor_oder2($error_code, $geburtsdatum_plain);
		F154_Datum_mit_Komma_vor_oder2($error_code, $sterbedatum_plain);
	}
}

sub F154_Datum_mit_Komma_vor_oder2{
	my $error_code = $_[0];	
	my $test_datum = $_[0];

	if ($test_datum =~ /, oder/
		){
			fehlermeldung($error_code, $title, $test_datum);
			#print $title."\n";
			#print "\t\t".$test_datum."\n";
			#die;
	}
}

#new
sub F155_Text_ohne_Geburts_oder_Sterbedatum{
	my $error_code = 155; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Text ohne Geburts- oder Sterbedatum';
				$error_description[$error_code][1] = 'Im Text konnten nicht die Geburts- bzw. Sterbedaten in gewünschter Form gefunden werden. Es wurde wurde eine ordentliche Datumsangabe im Personendatenfeld gefunden. Dazu wurde nach passenden folgendern Textbestandteil gesucht "(*", "; *", ", *","; †", "(†" welche aber nicht oder nur teilweise gefunden wurden. Siehe hierzu auch <a href="http://de.wikipedia.org/wiki/Wikipedia:Formatvorlage_Biografie">Wikipedia:Formatvorlage Biografie</a>';
				$error_description[$error_code][2] = 3;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {	

		if ( $is_redirect == 0 ) {
			my $born = 'false';
			my $die = 'false';
			my $test_text = $text;
			$test_text =~ s/&nbsp;/ /gi;

			if ($geburtsdatum ne '' 
				and ($geburtsdatum =~ /(Januar|Februar|März|April|Mai|Juni|Juli|August|September|Oktober|November|Dezember)/ )
					 or $geburtsdatum =~ /^[ ]?[0-9]+[ ]?$/){
				$born = 'true';
			}	
			if ($sterbedatum ne '' 
				and ($sterbedatum =~ /(Januar|Februar|März|April|Mai|Juni|Juli|August|September|Oktober|November|Dezember)/ 
				or $sterbedatum =~ /^[ ]?[0-9]+[ ]?$/)) {
				$die = 'true';
			}			
			
			my $notice = '';
			if ($born eq 'true'
					and index($test_text ,'(*') == -1
					and index($test_text ,'; *') == -1
					and index($test_text ,', *') == -1
					and index($test_text ,"''; *") == -1
					and index($test_text ,"'', *") == -1
					and index($test_text ,"'''; *") == -1
					and index($test_text ,"'''; *") == -1
					and index($test_text ,'(getauft') == -1
					and index($test_text ,'; getauft') == -1
					and index($test_text ,', getauft') == -1
					and index($test_text ,"''; getauft") == -1
					and index($test_text ,"'', getauft") == -1
					and index($test_text ,"'''; getauft") == -1
					and index($test_text ,"'''; getauft") == -1
					){
				$notice = 'Geburtsdatum fehlt z.B. "(* '.$geburtsdatum.')" ';
			}	
			if ($born eq 'true'
					 and $die eq 'true'
					 and index($test_text ,'; †') == -1
					 and index($test_text ,'; begraben') == -1
				) {
					 
				$notice = 'Lebensdaten fehlen z.B. "(* '.$geburtsdatum.'; † '.$sterbedatum.')"';
			}
			if ($born eq 'false'
					 and $die eq 'true'
					 and index($test_text ,'; †') == -1
					 and index($test_text ,'(†') == -1
					 and index($test_text ,'; begraben') == -1
					 and index($test_text ,'(begraben') == -1
				){
				$notice = 'Sterbedatum fehlt z.B. "(† '.$sterbedatum.')"';
			}			if ($notice ne '') {
					fehlermeldung($error_code, $title, $notice);
					#print $title."\n";
					#print $born."\n";
					#print $die."\n";
					#print "\t\t".$geburtsdatum.' '.$sterbedatum."\n";
					#print Text_verkuerzen( $test_text, 100)."\n";
					#die;
			}
		}
	}
}
#old
sub F155_Zweibuchstabige_Abkuerzungen_in_kurz_old{
	my $error_code = 155; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Kurzbeschreibung mit Abkürzung';
				$error_description[$error_code][1] = 'Im Feld KURZBESCHREIBUNG wurde eine Abkürzung mit zwei Buchstaben gefunden (z.B. Jh., Hl., ev., em. oder andere). Es wurden römische Jahreszahlen ausgeschlossen (z.B. XI.). Die Kurzbeschreibung sollte möglichst ohne Abkürzungen auskommen.';
				$error_description[$error_code][2] = 0;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {	

		if ($kurz_plain =~ /(^| )[[:alpha:]][[:alpha:]]\./i
			and $kurz_plain =~ /(^| )[^XVI0-9][^XVI0-9]\./
			and not $kurz_plain =~ /(^| )(St|Nr|Co|Dr|EM|Ed)\./
			){
				fehlermeldung($error_code, $title, $kurz_plain);
				#print $title."\n";
				#print "\t\t".$test_datum."\n";
				#die;
		}
	}
}

sub F156_Kurzbeschreibung_mit_unnoetigem_Jahrhundert{
	my $error_code = 156; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Kurzbeschreibung mit unnötigem Jahrhundert';
				$error_description[$error_code][1] = 'Im Feld KURZBESCHREIBUNG wird das Jahrhundert mit angegeben, obwohl die Zeit durch das Geburts- und/oder Sterbedatum schon angegeben wird.';
				$error_description[$error_code][2] = 3;	
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {	

		if ($kurz_plain =~ /Jahrhundert/
			and (($geburtsdatum_plain ne '' ) #and not $geburtsdatum_plain =~/Jahrhundert/ ) 
			or   ($sterbedatum_plain  ne '' )) #and not $sterbedatum_plain  =~/Jahrhundert/ ))
			){
				fehlermeldung($error_code, $title, $kurz_plain);
				#print $title."\n";
				#print "\t\t".$test_datum."\n";
				#die;
		}
	}
}

sub F157_Kurzbeschreibung_mit_falsch_formatierten_Zeitraeumen{
	my $error_code = 157; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Kurzbeschreibung mit falsch formatierten Zeiträumen';
				$error_description[$error_code][1] = 'Das Feld KURZBESCHREIBUNG enthält die Angabe eines Zeitraumes (z.B. 1945-60), wobei die Endjahreszahl nicht ausgeschrieben wurde. Laut <a href="http://de.wikipedia.org/wiki/Wikipedia:Datumskonventionen">Wikipedia:Datumskonventionen</a> soll man sie ausschreiben (z.B. 1943–1967).';
				$error_description[$error_code][2] = 3;	
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {	
		if (    $kurz_plain =~ /[0-9][0-9][0-9][0-9][ ]?+-[ ]?+[0-9]([^\.0-9]|$)/
			 or $kurz_plain =~ /[0-9][0-9][0-9][0-9][ ]?+-[ ]?+[0-9][0-9]([^\.0-9]|$)/
			 or $kurz_plain =~ /[0-9][0-9][0-9][0-9][ ]?+-[ ]?+[0-9][0-9][0-9]([^\.0-9]|$)/
			){
				fehlermeldung($error_code, $title, $kurz_plain);
				#print $title."\n";
				#print "\t\t".$kurz_plain."\n";
				#die;
		}
	}
}	

sub F158_Personendaten_vor_Kategorien{
	my $error_code = 158; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Personendaten stehen vor den Kategorien';
				$error_description[$error_code][1] = 'Die Personendaten sollen am Ende des Artikels stehen und ihnen dürfen nur noch die Interwikilinks folgen (Siehe: <a href="http://de.wikipedia.org/wiki/Wikipedia:Personendaten">Wikipedia:Personendaten</a>)';
				$error_description[$error_code][2] = 3;	
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		my $pos  = index($text, 'Personendaten');
		my $hilfstext = substr($text, $pos);
		#print $hilfstext."\n"."\n";
		my $pos2 = index($hilfstext, 'STERBEORT');
		$hilfstext = substr($hilfstext, $pos2);
		#print $hilfstext."\n"."\n";
		my $pos3 = index($hilfstext, '}}');
		$hilfstext = substr($hilfstext, $pos3);
		$hilfstext =~ s/\}\}//;
		$hilfstext =~ s/\n/ /;
		#print $hilfstext."\n"."\n";

		if ($hilfstext ne ''
			and $hilfstext=~ /Kategorie:/ 
			) {
				fehlermeldung($error_code, $title, '');
				#print $title."\n";
				#print "\t\t".$hilfstext."\n";
				#die;
		}
	}
}


sub F159_Ort_mit_Slash{
	my $error_code = 159; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
			$error_description[$error_code][0] = 'Geburts- oder Sterbeort mit Schrägstrich';
			$error_description[$error_code][1] = 'Im Feld GEBURTSORT oder STERBEORT enthalten einen Schrägstrich. Es gibt zwar zahlreiche Orte, die einen Schrägstrich im amtlichen Namen haben, aber bei allen hier aufgelisteten Orte wurde das Wort davor oder danach oder beide verlinkt. Da immer auf den Ort direkt verlinkt werden soll heißt das, dass hier zwei Artikel exitieren (z.B. "<nowiki>[[Dresden]]/Sachsen</nowiki>"). Diese Ortsbeschreibung sollte aber durch ein Komma getrennt werden (z.B. "<nowiki>[[Dresden]], Sachsen</nowiki>".';
			$error_description[$error_code][2] = 2;		
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		F159_Ort_mit_Slash2($error_code, $geburtsort);
		F159_Ort_mit_Slash2($error_code, $sterbeort);
	}
}

sub F159_Ort_mit_Slash2{
	my $error_code = $_[0];	
	my $test_ort = $_[1];

	if ($test_ort =~ /\/[ ]?+\[/
		or $test_ort =~ /\][ ]?+\//
		){
			fehlermeldung($error_code, $title, $test_ort);
			#print $title."\n";
			#print "\t\t".$test_ort."\n";
			#die;
	}
}



sub F160_Datenfeld_mit_fehlerhafter_Klammer{
	my $error_code = 160; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
			$error_description[$error_code][0] = 'Datenfeld mit fehlerhafter Klammer';
			$error_description[$error_code][1] = 'In einem Datenfeld wurden fehlende öffnende oder schließende Klammern gefunden (z.B. "Rom), Italien").';
			$error_description[$error_code][2] = 1;	
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {

		if ($title ne 'Falco') 		# Alternativnamen T>>MA (Pseudonym); T>>MB (Pseudonym)	
		{
			F160_Datenfeld_mit_fehlerhafter_Klammer2($error_code, $name);
			F160_Datenfeld_mit_fehlerhafter_Klammer2($error_code, $aname);
			F160_Datenfeld_mit_fehlerhafter_Klammer2($error_code, $kurz_plain);
			F160_Datenfeld_mit_fehlerhafter_Klammer2($error_code, $geburtsort_plain);
			F160_Datenfeld_mit_fehlerhafter_Klammer2($error_code, $sterbeort_plain);
		}
	}
}

sub F160_Datenfeld_mit_fehlerhafter_Klammer2{
	my $error_code = $_[0];		
	my $test_datenfeld = $_[1];

	my $found_error = '';

	# keine öffnende bzw. schließende Klammer 
	if ($test_datenfeld =~ /^[^(]+\)/
		or $test_datenfeld=~ /\([^)]+$/
		){
		$found_error = $test_datenfeld;
	}

	# Anzahl der Klammern fehlerhaft
	my $z1 = ($test_datenfeld =~ tr/\(//);
	my $z2 = ($test_datenfeld =~ tr/\)//);
	my $z3 = ($test_datenfeld =~ tr/\{//);
	my $z4 = ($test_datenfeld =~ tr/\}//);
	my $z5 = ($test_datenfeld =~ tr/\[//);
	my $z6 = ($test_datenfeld =~ tr/\]//);
	my $z7 = ($test_datenfeld =~ tr/\<//);
	my $z8 = ($test_datenfeld =~ tr/\>//);

	if ($z1 != $z2 or $z3 != $z4 or $z5 != $z6 or $z7 != $z8) {
		$found_error = $test_datenfeld;
	}

	# Reihenfolge beachten z.B: "({sd)}"
    if ($test_datenfeld =~ /[\{\}\<\>\(\)\[\]]/) {
		my $test_datenfeld2 = $test_datenfeld;
		my $loop_counter = 0;
		my $end_loop = 'true';
		my $old_datenfeld = $test_datenfeld;
		my $new_datenfeld = '';
		do {
			$end_loop = 'true';

			$test_datenfeld2 =~ s/[^\{^\}^\<^\>^\(^\)^\[^\]]//g;
			$test_datenfeld2 =~ s/\[\]//g;					# [xy] entferne jedes einfache Klammernpaar
			$test_datenfeld2 =~ s/\{\}//g;					# {xy}
			$test_datenfeld2 =~ s/\<\>//g;					# <xy>
			$test_datenfeld2 =~ s/\(\)//g;					# (xy)
			my $z1 = ($test_datenfeld2 =~ tr/\(//);
			my $z2 = ($test_datenfeld2 =~ tr/\)//);
			my $z3 = ($test_datenfeld2 =~ tr/\{//);
			my $z4 = ($test_datenfeld2 =~ tr/\}//);
			my $z5 = ($test_datenfeld2 =~ tr/\[//);
			my $z6 = ($test_datenfeld2 =~ tr/\]//);
			my $z7 = ($test_datenfeld2 =~ tr/\<//);
			my $z8 = ($test_datenfeld2 =~ tr/\>//);	
			$new_datenfeld = $test_datenfeld2;
			#print $z1,$z2,$z3,$z4,$z5,$z6,$z7,$z8."\n";

			# Loop weiter wenn noch Klammern übrig
			if ($z1 + $z2 > 0 or 
				$z3 + $z4 > 0 or 
				$z5 + $z6 > 0 or 
				$z7 + $z8 > 0 ) {
				$end_loop = 'false';
			}
			$loop_counter ++;
			
			# Abbruch nach zu vielen Schleifen
			if ( $loop_counter > 10) {
				$end_loop = 'true';
				$found_error = $test_datenfeld;	
			}

			# Abbruch wenn keine Änderung beim letzten Ersetzen
			if ($old_datenfeld eq $new_datenfeld) {
				$end_loop = 'true';
				$found_error = $test_datenfeld;	
			}
			$old_datenfeld = $new_datenfeld;

			#print $loop_counter.' '.$test_datenfeld2."\n";
		}
		until ( $end_loop eq 'true');
	}

	if ($found_error ne '') {
			fehlermeldung($error_code, $title, $test_datenfeld);
			#print $error_code."\t".$title."\t".'<nowiki>'. $test_datenfeld.'</nowiki>'."\n";
			#die;
	}

}


sub F161_Kurzbeschreibung_Rest_kleingeschrieben{
	my $error_code = 161; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Kurzbeschreibung in ungewöhnlicher Form';
				$error_description[$error_code][1] = 'In dieser Fehlerrubrik sind sehr unübliche Kurzbeschreibungen aufgelistet, die alle einen kleingeschriebene Anfänge enthalten. Dafür wurden alle Nationalitäten herausgefiltert (Siehe auch <a href="http://de.wikipedia.org/wiki/Wikipedia:Namenskonventionen/Staaten">Wikipedia:Namenskonventionen/Staaten</a>). Weiterhin wurden verschiedene Kategorien von kleingeschriebenen Wörtern, die aber häufiger eingesetzt werden rausgefiltert. Was jetzt hier noch in der Liste steht ist der Rest, was dann noch übrigbleibt. Sehr häufig sind es einfach falsch geschriebene Nationalitäten oder unübliche Kurzbeschreibungen. Wenn hier Kurzbeschreibungen auftauchen, die eigentlich akzeptabel sind, dann gib auf der Diskussionseite bescheid.';
				$error_description[$error_code][2] = 3;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		my $test01 = $kurz_plain;
		my $test02 = $kurz_plain;

		if (
			$kurz_plain =~ /^[ ]?+[[:lower:]]+( |,)/
			and not $kurz_plain =~ /^[ ]?+[^ ]+(isch|ysch|tsch|schweiz|österreich)/
			and not $kurz_plain =~ /^[ ]?+(südtirol|vorarlberger|tiroler|florentiner|kärntner|vorarlberger|regionale|pommersche|liechtensteiner|hannoversche)/
			and not $kurz_plain =~ /^[ ]?+(erste|zweit|dritt|viert|fünft|sechst|sieben|siebter|acht|neunt|zehnt|elft|zwölft|dreizehnt|vierzehnt|fünfzehnt|sechzehn|siebzehn)/
			and not $kurz_plain =~ /^[ ]?+(mittelalterlich|hochmittelalterlich|vorgeschichtlich|antik|spätantik|barocker|spätmittelalterlich|frühmittelalterlich|modern)/
			and not $kurz_plain =~ /^[ ]?+(kurzzeitig|langjährig|gegenwärtig|zeitweise)/
			and not $kurz_plain =~ /^[ ]?+(vom|während|ungefähr|frühe)( |,)/
			and not $kurz_plain =~ /^[ ]?+(schweizer|berlin|brasilian|staatenlose)/
			and not $kurz_plain =~ /^[ ]?+(religiös|selige|fromme|heiliggesprochen|spirituell|heilige|freikirchlich|orthodox|christliche|reformiert|altkirchlich|alttestamentlich|evangelikal|frühchristlich|geistig|geistlich|heiliger|kirch)/
			and not $kurz_plain =~ /^[ ]?+(amtierende|geschäftsführend|stellvertre|derzeitige|designiert|leitende|regierende|regiert)/
			and not $kurz_plain =~ /^[ ]?+(im|in|am|durch|aus|bis|durch|als|auch|auf|beim|bei|mit)( |,)/
			and not $kurz_plain =~ /^[ ]?+(einzig|zweifach|mehrmals|mehrmalig|dreifach|dreimalig|fünfmalig|mehrfach|zweimalig|viermalige)/
			and not $kurz_plain =~ /^[ ]?+(eventuel|angeblich|mutmaßlich|sagenhaft|vermutlich|laut|möglicherweise|wahrscheinlich)/
			and not $kurz_plain =~ /^[ ]?+(liberal|linksgerichtete|unabhängige|grüne|linksliberale|frei|rechtskonservative|rechtsradikale|parteilose|militante|konservativ|revolutionäre|jungkonservativ|nationalliberale|radikale|rechtsextreme)/
			and not $kurz_plain =~ /^[ ]?+(diplomierte|ordentlicher|hochgelehrter|intellektuelle|wissenschaftlich|studier|emeritiert|promoviert|gelehrt|geistreich|gelernt|hochbegabte)/
			and not $kurz_plain =~ /^[ ]?+(bildend|kreativ|ungegenständlich|volkstümliche)/
			and not $kurz_plain =~ /^[ ]?+(führende|namhaft|weltweit|angesehen|außerordentlich|international|bedeuten|erfolgreich|zentrale|wichtig|vielseitige|schillernde|preisgekrönte|professionelle|legendär|mächtig|maßgeblich|große|erfolgreiche|populär|bekannt|berühmt|prägend|prominent|renommierte|berüchtigte)/
			and not $kurz_plain =~ /^[ ]?+(frühere|ehemalig|zeitweilig|zunächst)/
			and not $kurz_plain =~ /^[ ]?+(königlich|kaiserlich|markgräflich|großherzoglich|kurfürstlich|erzherzoglich|adliger|adelig|herzoglich|fürstlich|gräflich)/
			and not $kurz_plain =~ /^[ ]?+(kurhannoversch|kurbrandenburg)/
			and not $kurz_plain =~ /^[ ]?+(wurde|ist|hat|gilt)/
			and not $kurz_plain =~ /^[ ]?+(uneheliche|natürliche|adoptiert|illegitime|geborene|gebürtiger|außereheliche|geschiedene|angeheiratete|entstammt)/		
			and not $kurz_plain =~ /^[ ]?+(vornehme|reiche|wohlhabende)/	
			and not $kurz_plain =~ /^[ ]?+(jüngste|ältest|vorletzte|letzte|ältere|jünger|erstgeborene)/
			and not $kurz_plain =~ /^[ ]?+(transsexuelle|schwule|homosexuell)/		
			and not $kurz_plain =~ /^[ ]?+(weiße|schwarz|indigene|aborigene|farbige)/	
			and not $kurz_plain =~ /^[ ]?+(weiblicher|männlich)/
			and not $kurz_plain =~ /^[ ]?+(armlose|blind|gehörlos|erblindet|gehörlos|sehbehindert|taubblind|tauber)/		
			){
				fehlermeldung($error_code, $title, $kurz_plain);
				#print "\t".$title."\n";
				#print "\t\t".$error_code.' x'.$kurz_plain."x\n";
				#die;
		}
	}
}


sub F162_Name_kleingeschrieben{
	my $error_code = 162; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Name kleingeschrieben';
				$error_description[$error_code][1] = 'Im Datenfeld NAME fängt der Nachname kleingeschrieben an (z.B. "meier, Franz"). Ausnahmen: K.d. lang, Gustaf nagel';
				$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		if ($name =~ /^[[:lower:]]+,/ 
			and $title ne 'Gustaf nagel'    #Künstlername
			and $title ne 'K.d. lang'    	#Künstlername
			){
				fehlermeldung($error_code, $title, $name);
				#print $title."\n";
				#print "\t\t".$name."\n";
				#die;
		}
	}
}

sub F163_Alternativname_kleingeschrieben{
	my $error_code = 163; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Alternativnamen kleingeschrieben';
				$error_description[$error_code][1] = 'Im Datenfeld ALTERNATIVNAMEN fängt der Nachname kleingeschrieben an (z.B. "meier, Franz"). ';
				$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		if ($aname =~ /(^|;)[ ]?+[[:lower:]]+,/ ){
				fehlermeldung($error_code, $title, $aname);
				#print $title."\n";
				#print "\t\t".$aname."\n";
				#die;
		}
	}
}

sub F164_Name_mit_Klammer_vor_Komma{
	my $error_code = 164; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Namen mit Klammer vor Komma';
				$error_description[$error_code][1] = 'Im Datenfeld NAME hat eine Klammer vor dem Komma (z.B. "Witte (Bischof), Karl"). Das sorgt für eine falsche Einsortierung und muss geändert werden.';
				$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		if ($name =~ /^[^,]+\),/ ){
				fehlermeldung($error_code, $title, $name);
				#print $title."\n";
				#print "\t\t".$name."\n";
				#die;
		}
	}
}

sub F165_Alternativname_mit_Klammer_vor_Komma{
	my $error_code = 165; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
			$error_description[$error_code][0] = 'Alternativnamen mit Klammer vor einem Komma';
			$error_description[$error_code][1] = 'Im Datenfeld ALTERNATIVNAMEN hat eine Klammer vor dem Komma (z.B. "Witte (Bischof), Karl"). Das sorgt für eine falsche Einsortierung und muss geändert werden.';
			$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		if ($aname =~ /\),/ ){
				fehlermeldung($error_code, $title, $aname);
				#print $title."\n";
				#print "\t\t".$aname."\n";
				#die;
		}
	}
}

sub F166_Alternativname_mit_Verlinkung{
	my $error_code = 166; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Alternativnamen mit Verlinkung';
				$error_description[$error_code][1] = 'Im Datenfeld ALTERNATIVNAMEN sind keine Verlinkungen erlaubt.';
				$error_description[$error_code][2] = 3;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		if ( index ($aname, 'Day[9]') == -1) 	# Ausnahme [[Sean Plott]]
		{
			if ($aname =~ /\[/ 
				or $aname =~ /\]/ ){
					fehlermeldung($error_code, $title, '<nowiki>'.$aname.'</nowiki>');
					#print $title."\n";
					#print "\t\t".$aname."\n";
					#die;
			}
		}
	}
}

sub F167_Alternativname_ohne_Klammer_am_Ende{
	my $error_code = 167; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Alternativnamen ohne Klammer am Ende';
				$error_description[$error_code][1] = 'Im Datenfeld ALTERNATIVNAMEN gibt es kein Semikolon, weshalb es sich um einen einzigen Namen handeln muss. Die enthaltene Klammer ist aber nicht am Ende des Datenfeldes, wo sie eigentlich sein müsste. (z.B. "Böhmer, Justus (Jobst) Henning"). Hier muss umgebaut werden.';
				$error_description[$error_code][2] = 2;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		if ($aname =~ /,/ 
			and $aname =~ /\)/ 
			and not $aname =~ /;/ 
			and not $aname =~ /\)[ ]?+$/ 
			){
				fehlermeldung($error_code, $title, $aname);
				#print $title."\n";
				#print "\t\t".$aname."\n";
				#die;
		}
	}
}

sub F168_Name_mit_Namensvariationen{
	my $error_code = 168; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Name mit Namensvariationen';
				$error_description[$error_code][1] = 'Im Datenfeld NAME enthält eine Klammer, deren Inhalt kleingeschrieben Buchstaben anfängt (z.B. "Mayr, Johann(es) Simon" oder "Peren, Maggie (geb. Neureuther)"). Es sollten alle Namensvariationen ausgeschrieben werden. Ausgeschlossen werden hier erstmal alle Klammerinhalte mit Artikel (z.B. "die Ältere" oder "der Jüngere").' ;
				$error_description[$error_code][2] = 2;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		if ($name =~ /\([[:lower:]]+/ 
			and not $name =~ /\((der|die)/ 
			and $title ne 'An(jotef)'
			and $title ne 'Meister des (ehem.) Hochaltars der Marienkirche in Lübeck'
			){
				fehlermeldung($error_code, $title, $name);
				#print $title."\n";
				#print "\t\t".$name."\n";
				#die;
		}
	}
}

sub F169_Name_mit_geboren{
	my $error_code = 169; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Name mit "geboren"';
				$error_description[$error_code][1] = 'Im Datenfeld NAME das Wort "geboren" oder "geb.". Das sollte nicht enthalten sein. ' ;
				$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		if ($name =~ /geb\./ 
			or $name =~ / geboren/ 
			){
				fehlermeldung($error_code, $title,$name);
				#print $title."\n";
				#print "\t\t".$name."\n";
				#die;
		}
	}
}


sub F170_Alternativname_mit_und_oder{
	my $error_code = 170; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Alternativname mit falschem "und" oder "oder"';
				$error_description[$error_code][1] = 'Im Datenfeld ALTERNATIVNAME sollte das Wort "und" bzw. "oder" nur bei Adelstiteln (z.B. "von und zu …"). ' ;
				$error_description[$error_code][2] = 2;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		if ($aname =~ /( |\()oder / 
			or (
				index ($aname, ' und ') > -1
			and index ($aname, ' von') == -1
			and index ($aname, ' de ') == -1
			and index ($aname, ' la ') == -1
			and index ($aname, '.') == -1
			and index ($aname, 'Freiherr') == -1
			and index ($aname, 'Ritter') == -1
			and index ($aname, 'Fürst') == -1
			and index ($aname, 'Graf') == -1
			and index ($aname, 'Print') == -1
			and index ($aname, 'Reichsgraf') == -1									# Georg August Graf Ysenburg
			and index ($aname, 'Herrn') == -1
			and index ($aname, 'Prinz') == -1
			and index ($aname, 'Ritter') == -1
			and index ($aname, 'Lispelnde und Lahme') == -1							# Erik XI. (Schweden)
			and index ($aname, 'Chimeira und Nettos-Maler') == -1					# Nessos-Maler
			and index ($aname, 'Colloredo-Mels und Wallsee') == -1
			and index ($aname, 'vom und zum') == -1
			and index ($aname, 'Thurn und Taxis') == -1
			and index ($aname, 'von und zu') == -1
			and index ($aname, 'auf Daldorf und Günthersdorf') == -1			# Leopold Friedrich Günther von Goeckingk
			and index ($aname, 'Sachsen-Coburg und Gotha') == -1					# Sibylla von Sachsen-Coburg und Gotha
			and index ($aname, 'Braunschweig und Lüneburg') == -1					# Wilhelm (Braunschweig)
			and index ($aname, 'Pfalzgraf und Reichsbaron de Petersen')== -1		#Jakob de Petersen 

			
			and not $aname =~ /\([^\)]* (und|oder) / 
			)){
				fehlermeldung($error_code, $title,$aname);
				#print $title."\n";
				#print "\t\t".$aname."\n";
				#die;
		}
	}
}

sub F171_Alternativname_ohne_Geburtsname{
	my $error_code = 171;
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Alternativname ohne Geburtsname';
				$error_description[$error_code][1] = 'Im Datenfeld ALTERNATIVNAME stehen Wörter wie "geb.", "geborene", "geboren", "gebürtig", "Familienname". Hier sollte man alles vereinheitlichen auf "Meier, Eva (Geburtsname)".' ;
				$error_description[$error_code][2] = 2;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		if ((   $aname =~ /geb\./ 
			or $aname =~ /geboren/ 
			or $aname =~ /geborene/ 
			or $aname =~ /gebürtig/ 
			or ($aname =~ /Familienname/ and $title ne 'Rahel Varnhagen von Ense')
			)
			and index ($aname, 'Eingeborenen') == -1
			)
			{
				fehlermeldung($error_code, $title,$aname);
				#print $title."\n";
				#print "\t\t".$aname."\n";
				#die;
		}
	}
}


sub F172_Datenfeld_mit_Unterstrich{
	my $error_code = 172;
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
			$error_description[$error_code][0] = 'Datenfeld mit Unterstrich';
			$error_description[$error_code][1] = 'Ein Datenfeld enthält im Text oder in der Verlinkung einen Unterstrich "_", der entfernt werden sollte.';
			$error_description[$error_code][2] = 3;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		F172_Datenfeld_mit_Unterstrich2($error_code, $name);
		#F172_Datenfeld_mit_Unterstrich2($aname);
		F172_Datenfeld_mit_Unterstrich2($error_code, $kurz);
		F172_Datenfeld_mit_Unterstrich2($error_code, $geburtsort);
		F172_Datenfeld_mit_Unterstrich2($error_code, $sterbeort);
	}
}
		
sub F172_Datenfeld_mit_Unterstrich2{
	my $error_code = $_[0];
	my $test = $_[1];

	if ( $test ne '' 
		 and index ($test, '_' ) > -1
		)
		 {
			fehlermeldung($error_code, $title, '<nowiki>'.$test.'</nowiki>');
			#print "\t".$title."\n";
			#print "\t\t\t".$test."\n";
			#die;
	}
}

sub F173_Alternativname_mit_Abkuerzung{
	my $error_code = 173;
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Alternativname mit kleingeschriebener Abkürzung';
				$error_description[$error_code][1] = 'Im Datenfeld ALTERNATIVNAME stehen kleingeschriebene Abkürzungen wie "geb.", "dt.", "fr.", die unbedingt ausgeschrieben werden sollen. Akademische Titel gehören nicht in die Alternativnamen. Ausgenommen sind "jun.", "jr.", "sen." und "sr.".' ;
				$error_description[$error_code][2] = 3;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		if (        $aname =~ /( |\()[a-z][a-z]+\./
			and index ($aname, 'jr.') == -1
			and index ($aname, 'jun.') == -1
			and index ($aname, 'sen.') == -1
			and index ($aname, 'sr.') == -1
			and index ($aname, 'Anton feat.') == -1 				# DJ Ötzi
			and index ($aname, 'apl.de.ap') == -1 					# Apl De Ap
			and index ($aname, 'T Minus 20 sec.') == -1 			# Dr. Scissors
			and index ($aname, 'ta. bang hrwe hti') == -1 			# Tabinshwehti
			and index ($aname, 'Y.tim.K') == -1 					# Tim Commerford
			and index ($aname, 'Dr. med. Fabian') == -1 			# Willy E. J. Schneidrzik
			and index ($aname, 'ml.') == -1							# Teil tschechischer Namen (Jan Jiři ml. in [[Josef Bartoň-Dobenín der Jüngere]])
			and index ($aname, 'lotte ohm.') == -1					# Vincent Wilkie			
			){

				fehlermeldung($error_code, $title,$aname);
				#print $title."\n";
				#print "\t\t".$aname."\n";
				#die;
		}
	}
}

sub F174_Datenfeld_mit_Semikolon_am_Ende{
	my $error_code = 174;
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Datenfeld mit Semikolon am Ende';
				$error_description[$error_code][1] = 'Ein Datenfeld hat am Ende ein Semikolon ohne das danach noch ein weiterer Eintrag kommt. Das ist in keinem Datenfeld erwünscht.' ;
				$error_description[$error_code][2] = 2;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		F174_Datenfeld_mit_Semikolon_am_Ende2($error_code, $name);
		F174_Datenfeld_mit_Semikolon_am_Ende2($error_code, $aname);
		F174_Datenfeld_mit_Semikolon_am_Ende2($error_code, $kurz);
		F174_Datenfeld_mit_Semikolon_am_Ende2($error_code, $geburtsdatum);
		F174_Datenfeld_mit_Semikolon_am_Ende2($error_code, $geburtsort);
		F174_Datenfeld_mit_Semikolon_am_Ende2($error_code, $sterbedatum);
		F174_Datenfeld_mit_Semikolon_am_Ende2($error_code, $sterbeort);
	}

}

sub F174_Datenfeld_mit_Semikolon_am_Ende2{
	my $error_code = $_[0];
	my $test = $_[1];

	if (        $test =~ /;[ ]?+$/
		and not $test =~ /&quot;[ ]?+$/ 
		and not $test =~ /&gt;[ ]?+$/ 
		){
			fehlermeldung($error_code, $title,$test);
			#print $title."\n";
			#print "\t\t".$aname."\n";
			#die;
	}

}

sub F175_Datenfeld_ohne_Leerzeichen_vor_Klammer{
	my $error_code = 175;
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
			$error_description[$error_code][0] = 'Datenfeld ohne Leerzeichen vor der Klammer';
			$error_description[$error_code][1] = 'In einem Datenfeld befindet sich Text, der kein Leerzeichen vor der öffnenden Klammer hat (z.B. "Dresden(Sachsen)".' ;
			$error_description[$error_code][2] = 2;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		F175_Datenfeld_ohne_Leerzeichen_vor_Klammer2($error_code, $name);
		F175_Datenfeld_ohne_Leerzeichen_vor_Klammer2($error_code, $aname);
		F175_Datenfeld_ohne_Leerzeichen_vor_Klammer2($error_code, $kurz);
		F175_Datenfeld_ohne_Leerzeichen_vor_Klammer2($error_code, $geburtsort);
		F175_Datenfeld_ohne_Leerzeichen_vor_Klammer2($error_code, $sterbeort);
	}
}

sub F175_Datenfeld_ohne_Leerzeichen_vor_Klammer2{
	my $error_code = $_[0];
	my $test = $_[1];
	
	if ( 	index ($test, '(') > -1
		and index ($test, ' (') == -1
		and index ($test, '_(') == -1 
		and $title ne '(.)p(...)nin'
		and $title ne 'An(jotef)'
		and index ($test, 'Kom(m)ödchen') == -1 
		){
			fehlermeldung($error_code, $title, '<nowiki>'.$test.'</nowiki>');
			#print $title."\n";
			#print "\t\t".$test."\n";
			#die;
	}
}

sub F176_Ort_mit_lokaler_Praeposition{
	my $error_code = 176;
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
			$error_description[$error_code][0] = 'Ort mit lokaler Präposition';
			$error_description[$error_code][1] = 'In einem Datenfeld GEBURTSORT oder STERBEORT ist ein "in" oder "im" vor oder nach einer Verlinkung (z.B. "<nowiki>Dresden in [[Sachsen]]</nowiki>"). Das sollte korrigiert werden (z.B. "Dresden, Sachsen").' ;
			$error_description[$error_code][2] = 2;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		F176_Ort_mit_lokaler_Praeposition2($error_code, $geburtsort);
		F176_Ort_mit_lokaler_Praeposition2($error_code, $sterbeort);
	}
}

sub F176_Ort_mit_lokaler_Praeposition2{
	my $error_code = $_[0];
	my $test = $_[1];
	
	if (    ( $test =~ / im /
			  and (   $test =~ /\][ ]?+ im / 
				   or $test =~ / im [ ]?+(der )?\[/ 
			       or $test =~ /\)[ ]?+ im / 
			  )
			) or
			( $test =~ / in /
			  and (   $test =~ /\][ ]?+ in / 
				   or $test =~ / in [ ]?+(der )?\[/ 
			       or $test =~ /\)[ ]?+ in / 
			  )
			)
		){
			fehlermeldung($error_code, $title, '<nowiki>'.$test.'</nowiki>');
			#print $title."\n";
			#print "\t\t".$test."\n";
			#die;
			
	}
}


sub F177_Personendaten_mit_Minimaleintrag {
	my $error_code = 177;
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Personendaten erfüllen nur Minimalanforderungen';
				$error_description[$error_code][1] = 'Die Personendaten in diesem Artikel erfüllen nur minimale Anforderungen. Das heißt hier sind weder Gebursdatum und -ort noch Sterbedatum und -ort enthalten. Außerdem ist die Kurzbeschreibung nur ein Wort lang. Hier sollte genau überprüft werden, ob nicht z.B. die Nationalität und für das Geburts- oder Sterbedatum wenigstens das Jahrhundert eingetragen werden können.' ;
				$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		if (     $geburtsdatum eq ''
			and  $sterbedatum eq ''
			and  $geburtsort eq ''
			and  $sterbeort eq ''
			and  $kurz =~ /^[ ]?+[^ ]+[ ]?+$/
			){
				fehlermeldung($error_code, $title, '<nowiki>'.$kurz.'</nowiki>');
				#print $title."\n";
				#print "\t\t".$kurz."\n";
				#die;
		}
	}
}

sub F178_Personen_ohne_Taetigkeit {
	my $error_code = 178;
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
			$error_description[$error_code][0] = 'Artikel mit minimalen Kategorien';
			$error_description[$error_code][1] = 'Der Artikel enthält nur Kategorien zum Geburtsjahr oder Sterbejahr oder Geschlecht oder Nationalität. Diese Person hat keine Tätigkeitskategorie (<a href="http://de.wikipedia.org/wiki/Kategorie:Person nach Tätigkeit">Kategorie:Person nach Tätigkeit</a>), welche aber, wenn möglich, eingefügt werden sollte.' ;
			$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
	
		my $test = $all_categories;
		$test =~ s/\|[^\t]+\]\]/\]\]/g;
		
		$test =~ s/Kategorie:Geboren[^\]]?+//g;
		$test =~ s/Kategorie:Gestorben[^\]]?+//g;
		$test =~ s/Kategorie:(Mann|Frau|Intersexueller|Geschlecht unbekannt)//g;
		
		# Auswahl von Nationalitäten (kann erweitert werden)
		$test =~ s/Kategorie:(Afghane|Ägypter|Albaner|Algerier|Andorraner|Angolaner|Antiguaner)//g;
		$test =~ s/Kategorie:(Äquatorialguineer|Argentinier|Armenier|Aserbaidschaner|Äthiopier|Australier)//g;
		$test =~ s/Kategorie:(Bahamaer|Bahrainer|Bangladescher|Barbadier|Belgier|Belizer|Beniner|Bhutaner)//g;
		$test =~ s/Kategorie:(Bolivianer|Bosnier|Botsuaner|Brasilianer|Brite|Bruneier|Bulgare|Burkiner|Burundier)//g;
		$test =~ s/Kategorie:(Chilene|Chinese|Costa\-Ricaner)//g;
		$test =~ s/Kategorie:(Däne|Danziger|Person \(Demokratische Republik Kongo\)|Deutscher|Dominicaner|Person \(Dominikanische Republik\)|Dschibutier)//g;
		$test =~ s/Kategorie:(Ecuadorianer|Eritreer|Este|Engländer)//g;
		$test =~ s/Kategorie:(Färinger|Fidschianer|Finne|Franzose)//g;
		$test =~ s/Kategorie:(Gabuner|Gambier|Georgier|Ghanaer|Grenader)//g;
		$test =~ s/Kategorie:(Grieche|Griechischer Staatsbürger|Grönländer|Guatemalteke|Guinea\-Bissauer|Guineer|Guyaner)//g;
		$test =~ s/Kategorie:(Haitianer|Honduraner)//g;
		$test =~ s/Kategorie:(Inder|Indonesier|Iraker|Iraner|Ire|Isländer|Israeli|Italiener|Ivorer)//g;
		$test =~ s/Kategorie:(Jamaikaner|Japaner|Jemenit|Jordanier|Jugoslawe)//g;
		$test =~ s/Kategorie:(Kambodschaner|Kameruner|Kanadier|Kap\-Verdier|Kasache|Katarer|Kenianer)//g;
		$test =~ s/Kategorie:(Kirgise|Kiribatier|Kolumbianer|Komorer|Koreaner|Kosovare|Kroate|Kubaner|Kuwaiter)//g;
		$test =~ s/Kategorie:(Laote|Lesother|Lette|Libanese|Liberianer|Libyer|Liechtensteiner|Litauer|Lucianer|Luxemburger)//g;
		$test =~ s/Kategorie:(Madagasse|Malawier|Malaysier|Malediver|Malier|Malteser|Marokkaner|Marshaller|Mauretanier)//g;
		$test =~ s/Kategorie:(Mauritier|Mazedonier|Mexikaner|Mikronesier|Moldawier|Monegasse|Mongole|Montenegriner|Mosambikaner|Myanmare)//g;
		$test =~ s/Kategorie:(Namibier|Nauruer|Nepalese|Neuseeländer|Nicaraguaner|Niederländer|Nigerianer|Nigrer|Niueaner|Norweger|Nordkoreaner)//g;
		$test =~ s/Kategorie:(Omaner|Österreicher|Osttimorese)//g;
		$test =~ s/Kategorie:(Österreicher)//g;
		$test =~ s/Kategorie:(Pakistaner|Palauer|Panamaer|Papua\-Neuguineer|Paraguayer|Peruaner|Philippiner|Pole|Portugiese|Puertoricaner)//g;
		$test =~ s/Kategorie:(Person \(Republik Kongo\)|Ruander|Rumäne|Russe)//g;
		$test =~ s/Kategorie:(Salomoner|Salvadorianer|Sambier|Samoaner|San\-Marinese|São\-Toméer|Saudi\-Araber)//g;
		$test =~ s/Kategorie:(Schwede|Schweizer|Senegalese|Serbe|Seycheller|Sierra\-Leoner|Simbabwer|Singapurer|Slowake)//g;
		$test =~ s/Kategorie:(Slowene|Somalier|Sowjetbürger|Spanier|Sri\-Lanker|Person \(St.Kitts und Nevis\)|Südafrikaner)//g;
		$test =~ s/Kategorie:(Sudanese|Surinamer|Swasi|Syrer|Südkoreaner|Schweizer)//g;
		$test =~ s/Kategorie:(Tadschike|Tansanier|Thailänder|Togoer|Tongaer|Person \(Trinidad und Tobago\)|Tschader)//g;
		$test =~ s/Kategorie:(Tscheche|Tschechoslowake|Tunesier|Türke|Turkmene|Tuvaluer|Taiwaner)//g;
		$test =~ s/Kategorie:(Ugander|Ukrainer|Ungar|Uruguayer|US\-Amerikaner|Usbeke)//g;
		$test =~ s/Kategorie:(Vanuatuer|Venezolaner|Person \(Vereinigte Arabische Emirate\)|Vietnamese|Vincenter)//g;
		$test =~ s/Kategorie:(Weißrusse)//g;
		$test =~ s/Kategorie:(Zypriot)//g;
		
		$test =~ s/\[//g;
		$test =~ s/\]//g;
		$test =~ s/\t//g;
		$test =~ s/ //g;
		
		#print "\tx".$test."x\n";

		
		if ( length($test) == 0) {
			#print $title."\n";
			#print $all_categories."\n";
			#print $test."\n";

			fehlermeldung($error_code, $title, '<nowiki>'.$kurz.'</nowiki>'."\n".'<nowiki>'.$all_categories.'</nowiki>');
			#print $title."\n";
			#print "\t\t".$kurz."\n";
			
		}
	}

}

sub F179_Kategorie_mit_Jahrhundert {
	my $error_code = 179;
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Kategorie mit Jahrhundert';
				$error_description[$error_code][1] = 'Der Artikel enthält kein Geburts- und Sterbedatum, aber es gibt mindestens eine Kategorie in der ein Jahrhundert angegeben wird. Hier sollten die Personendaten verbessert werden.' ;
				$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		if (     $geburtsdatum eq ''
			and  $sterbedatum eq ''
			and  not $all_categories =~ /(G|g)eboren [1-9]/
			and  not $all_categories =~ /(G|g)eboren im/
			and  not $all_categories =~ /(G|g)estorben [1-9]/
			and  not $all_categories =~ /(G|g)estorben im/
			and  $all_categories =~ /Jahrhundert/
			){

				
				my $test = $all_categories;
				my @test_split = split (/\t/ , $test);
				$test = '';
				foreach (@test_split) {
					$test = $test." - ".$_ if ( $_ =~ /Jahrhundert/ );
				}
				$test =~ s/^ - //;

				fehlermeldung($error_code, $title, '<nowiki>'.$test.'</nowiki>');
				#print $title."\n";
				#print "\t\t".$all_categories."\n";
				#print "\t\t".$test."\n"."\n";
				#die;
		}
	}
}

sub F180_Personendaten_nach_Interwiki{
	my $error_code = 180;
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
					$error_description[$error_code][0] = 'Personendaten stehen nach den Interwiki-Links';
					$error_description[$error_code][1] = 'Die Personendaten sollen am Ende des Artikels stehen und ihnen dürfen nur noch die Interwikilinks folgen (Siehe: <a href="http://de.wikipedia.org/wiki/Wikipedia:Personendaten">Wikipedia:Personendaten</a>). Deshalb ist es fehlerhaft wenn die Interwiki-Links vor den Personendaten auftauchen.';
					$error_description[$error_code][2] = 3;	
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		my $pos_pd  = index($text, 'Personendaten');
		
		
		if ($all_interwikis ne '') {
			my @split_interwikis = split( /\t/, $all_interwikis);
			my $info_text= '';
			
			my $pos_min_interwiki = $pos_pd ;
			foreach(@split_interwikis) {
				my $current_interwiki = $_;
				my $pos_inter = index($text, $current_interwiki);
				if ($pos_inter < $pos_min_interwiki) {
					$info_text = $current_interwiki;
					$pos_min_interwiki = $pos_inter;
				}
			}
			
			
			if ($pos_min_interwiki < $pos_pd ) {
	
					fehlermeldung($error_code, $title, '<nowiki>'.$info_text.'</nowiki>');
					#print $title."\n";
					#print "\t\t".$info_text."\n";
					#print $pos_pd.' PD'."\n";
					#print $pos_min_interwiki.' Interwiki'."\n";
					#die;
			}
		}
	}
}



sub F181_Personendaten_vor_letzter_Ueberschrift{
	my $error_code = 181;
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Personendaten stehen vor der letzten Überschrift';
				$error_description[$error_code][1] = 'Die Personendaten sollen am Ende des Artikels stehen und ihnen dürfen nur noch die Interwikilinks folgen (Siehe: <a href="http://de.wikipedia.org/wiki/Wikipedia:Personendaten">Wikipedia:Personendaten</a>). In den aufgelisteten Artikeln wurde nach den Personendaten noch ein "<nowiki>==</nowiki>" gefunden. Die Personendaten sollten hier verschoben werden.';
				$error_description[$error_code][2] = 3;	
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		my $pos  = index($text, 'Personendaten');
		my $hilfstext = substr($text, $pos);
		#print $hilfstext."\n"."\n";
		my $pos2 = index($hilfstext, 'STERBEORT');
		$hilfstext = substr($hilfstext, $pos2);
		#print $hilfstext."\n"."\n";
		my $pos3 = index($hilfstext, '}}');
		$hilfstext = substr($hilfstext, $pos3);
		$hilfstext =~ s/\}\}//;
		$hilfstext =~ s/\n/ /;
		#print $hilfstext."\n"."\n";

		if ($hilfstext ne ''
			and $hilfstext=~ /==/ 
			) {
				fehlermeldung($error_code, $title, '');
				#print $title."\n";
				#print "\t\t".$hilfstext."\n";
				#die;
		}
	}
}



sub F182_Genealogisches_Zeichen_ohne_Leerzeichen_danach{
	my $error_code = 182;
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
			$error_description[$error_code][0] = 'Genealogisches Zeichen ohne Leerzeichen danach';
			$error_description[$error_code][1] = 'Am Artikelanfang befindet sich in Klammern ein "*" oder "†" ohne Leerzeichen danach. Korrekt müsste die Schreibweise "* 1945; † 1977" lauten.';
			$error_description[$error_code][2] = 3;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		my $test1 = $text;

		if (index ($test1, '==') > -1) {			# nur bis zur ersten Überschrift 
			$test1 = substr( $test1, 0, index ($test1, '=='));
		}
		
		my $pos = index($test1, ')' );				# Text bis zum ende des ersten Satzes kürzen nach der schließenden Klammer
		$pos = index($test1, '.', $pos);
		$test1 = substr ($test1, 0, $pos);
		$test1 = substr ($test1, index($test1, '(' ));
		


		if (   
			(	$test1 =~ /(\(|;)([ ]?)+\*[^ ]/   
			  and index ($test1, '*&nbsp;') == -1
			  and index ($test1, '*&amp;nbsp;') == -1
			  and index ($test1, '*&thinsp;') == -1
			  and index ($test1, '*&amp;thinsp;') == -1
			)  
			or 
			(   $test1 =~ /(\(|;)([ ]?)+†[^ ]/ 
			  and index ($test1, '†&nbsp;') == -1
			  and index ($test1, '†&amp;nbsp;') == -1
			  and index ($test1, '†&thinsp;') == -1
			  and index ($test1, '†&amp;thinsp;') == -1
			)
		   )
		   {
			
			$test1 =~ s/<\/nowiki>/<_nowiki>/g;
			$test1 = Text_verkuerzen( $test1, 600);


			fehlermeldung($error_code, $title, '<nowiki>'.$test1.'</nowiki>');
			#print $title."\n";
			#print "\t\t".$test1."\n";
			#print "\n";
			#die;
		}
	}
}

sub F183_Genealogisches_Zeichen_mit_Ort_danach{
	my $error_code = 183;
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
			$error_description[$error_code][0] = 'Genealogisches Zeichen und Ort danach';
			$error_description[$error_code][1] = 'Am Artikelanfang befindet sich in Klammern ein "*" oder "†" mit "im", "nahe", "bei", "auf" oder "im", aber der Ort ist noch nicht bei den Personendaten eingetragen.';
			$error_description[$error_code][2] = 3;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		my $test1 = $text;
		my $pos = index($test1, ')' );				# Text bis zum ende des ersten Satzes kürzen nach der schließenden Klammer
		$pos = index($test1, '.', $pos);
		$test1 = substr ($test1, 0, $pos);
		$test1 = substr ($test1, index($test1, '(' ));


		if (   ($test1 =~ /\*[ ]?+(amp;nbsp;)?(in|nahe|bei|auf|im)/  and $geburtsort eq '' and $geburtsdatum eq '')
			or ($test1 =~  /†[ ]?+(amp;nbsp;)?(in|nahe|bei|auf|im)/  and $sterbeort eq ''  and $sterbedatum eq '')
			){

			
			$test1 = Text_verkuerzen( $test1, 600);
			fehlermeldung($error_code, $title, '<nowiki>'.$test1.'</nowiki>');
			#print $title."\n";
			#print "\t\t".$test1."\n";
			#print "\n";
			#die;
		}
	}
}



sub F184_Genealogisches_Zeichen_mit_Zeitraum_danach{
	my $error_code = 184;
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
			$error_description[$error_code][0] = 'Genealogisches Zeichen und Zeitraum danach';
			$error_description[$error_code][1] = 'Am Artikelanfang befindet sich in Klammern ein "*" oder "†" mit "um", "vor" "nach" oder "zwischen" aber das Datum ist noch nicht bei den Personendaten eingetragen. ';
			$error_description[$error_code][2] = 3;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		my $test1 = $text;
		my $pos = index($test1, ')' );				# Text bis zum ende des ersten Satzes kürzen nach der schließenden Klammer
		$pos = index($test1, '.', $pos);
		$test1 = substr ($test1, 0, $pos);
		$test1 = substr ($test1, index($test1, '(' ));
		if (   ($test1 =~ /\*[ ]?+(amp;nbsp;)?(vor|zwischen|nach|um|ca|etwa|gegen|ungefähr)/  and $geburtsdatum eq '')
			or ($test1 =~  /†[ ]?+(amp;nbsp;)?(vor|zwischen|nach|um|ca|etwa|gegen|ungefähr)/  and $sterbedatum eq '')
			){
			$test1 = Text_verkuerzen( $test1, 600);
			fehlermeldung($error_code, $title, '<nowiki>'.$test1.'</nowiki>');
			#print $title."\n";
			#print "\t\t".$test1."\n";
			#print "\n";
			#die;
		}
	}
}

sub F185_Alternativnamen_mit_Semikolon_in_Klammer{
	my $error_code = 185;
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Alternativnamen mit Semikolon in Klammer';
				$error_description[$error_code][1] = 'Das Feld ALTERNATIVNAMEN enthält ein Semikolon in einem Klammerausdruck. In Klammer sollten nur Kommas genutzt werden, da das Semikolon hier zur Trennung der einzelnen Alternativnamen dient.';
				$error_description[$error_code][2] = 2;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		if ( $aname ne '' 
			and $aname =~ /\([^\)]+;/
			)
			 {
				fehlermeldung($error_code, $title, $aname);
				#print $title."\n";
				#print $aname."\n";
				#die;
		}
	}
}

sub F186_Name_versus_Defaultsort{
	my $error_code = 186;
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Name versus Defaultsort';
				$error_description[$error_code][1] = 'Das Datenfeld Name oder die Vorlage DEFAULTSORT enthalten ein unterschiedliche Sortierung. Das Skript erkennt in einem ein Komma in dem anderen kein Komma. Hier sollten das Datenfeld Name und die Vorlage DEFAULTSORT angepasst werden.';
				$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		if (  $defaultsort ne '' 
			  and not $title =~ /^[ ]?+[^ ]+ \(/ 
			  and
			  (
				(index ( $defaultsort, ',') >  -1 and index ($name, ',') == -1)
				 or (index ( $defaultsort, ',') == -1 and index ($name, ',') >  -1)
			  )
			)
			 {

				fehlermeldung($error_code, $title, $name.' - '.$defaultsort);
				#print 't: '.$title."\n";
				#print 'n: '.$name."\n";
				#print 'd: x'.$defaultsort."x\n";
				#die;
		}
	}
}


sub F187_Alternativnamen_ohne_Semikolon_nach_Klammer{
	my $error_code = 187;
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Alternativnamen ohne Semikolon nach Klammer';
				$error_description[$error_code][1] = 'Das Datenfeld Alternativname hat eine schließende Klammer hinter der kein Semikolon kommt (z.B.: "Max (Michael) Müller"). Nach einer schließenden Klammer sollte entweder ein Semikolon kommen oder das Ende des Datenfeldes (also ein Zeilenumbruch). Bei Namensvariationen sollte diese besser als extra Alternativname geschrieben werden.';
				$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		my $aname_test = $aname;
		$aname_test =~ s/ $//g;
		
		if (  $aname_test ne '' 
			  and $aname_test =~ /\)[ ]+?[^;]/ 
			)
			 {
				fehlermeldung($error_code, $title, $aname);
				#print $title."\n";
				#print "\t\t".$aname."\n";
				#print 'd: x'.$defaultsort."x\n";
				#die;
		}
	}
}



sub F188_Ort_unsicher{
	my $error_code = 188;
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
			$error_description[$error_code][0] = 'Geburts- oder Sterbeort unsicher';
			$error_description[$error_code][1] = 'Im Feld GEBURTSORT oder STERBEORT ist eine Unsicherheit verzeichnet. Hier wird ein Wort wie "vermutlich", "vielleicht", "mutmaßlich" … oder ein Fragezeichen benutzt. Es ist für eine Auswertung wesentlich besser, wenn diese Felder einheitlich mit dem Wort "unsicher" gekennzeichnet werden wie z.B. "unsicher: Dresden".';
			$error_description[$error_code][2] = 2;	
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		F188_Ort_unsicher2($error_code, $geburtsort);
		F188_Ort_unsicher2($error_code, $sterbeort);
	}
}

sub F188_Ort_unsicher2{
	my $error_code 	 = $_[0];
	my $test_ort = $_[1];

	if (
		      index ($test_ort ,'angeblich') > -1
		  or  index ($test_ort ,'eventuell') > -1
		  or  index ($test_ort ,'evt.') > -1
		  or  index ($test_ort ,'evtl.') > -1
		  or  index ($test_ort ,'möglicherweise') > -1
		  or  index ($test_ort ,'mutmaßlich') > -1
		  or  index ($test_ort ,'umstritten') > -1
		  or  index ($test_ort ,'ungewiss') > -1
		  or  index ($test_ort ,'unsicher ') > -1
		  or  index ($test_ort ,'verm.') > -1
		  or  index ($test_ort ,'vermutlich') > -1
		  or  index ($test_ort ,'vielleicht') > -1
		  or  index ($test_ort ,'wahrscheinlich') > -1
		  or  index ($test_ort ,'wohl ') > -1
		  or  index ($test_ort ,'?') > -1
		  ){
			fehlermeldung($error_code, $title, $test_ort);
			#print $title."\n";
			#print "\t\t".$test_ort."\n";
			#die;
	}
}

sub F189_Ort_beginnt_mit_Artikel{
	my $error_code = 189;
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
			$error_description[$error_code][0] = 'Geburts- oder Sterbeort mit Artikel am Anfang';
			$error_description[$error_code][1] = 'Im Feld GEBURTSORT oder STERBEORT beginnt mit einem Artikel am Anfang (z.B. "der Schweiz"). Der Artikel ist nicht notwendig und kann entfernt werden.';
			$error_description[$error_code][2] = 2;	
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		F189_Ort_beginnt_mit_Artikel2($error_code, $geburtsort);
		F189_Ort_beginnt_mit_Artikel2($error_code, $sterbeort);
	}
}

sub F189_Ort_beginnt_mit_Artikel2{
	my $error_code 	 = $_[0];
	my $test_ort = $_[1];

	if ( $test_ort ne ''
	     and $test_ort =~ /^[ ]?+(der|die|das|dem|den|des)/
		  ){
			fehlermeldung($error_code, $title, $test_ort);
			#print $title."\n";
			#print "\t\t".$test_ort."\n";
			#die;
	}
}

sub F190_Ort_bei_Ort{
	my $error_code = 190;
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
			$error_description[$error_code][0] = 'Geburts- oder Sterbeort bei anderem Ort';
			$error_description[$error_code][1] = 'Im Feld GEBURTSORT oder STERBEORT liegt "nahe", "außerhalb", "unweit" oder "in der Nähe" von einem anderen Ort. Hier sollte nur das Wort "bei" benutzt werden (z. B. "Becchi bei Turin"). Achtung: Wenn es einen eigenen Artikel für den Ort gibt, dann diesen direkt verlinken und das "bei" weglassen. (z.B. nicht "Radeberg bei Dresden" sondern besser "[[Radeberg]]")';
			$error_description[$error_code][2] = 3;	
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		F190_Ort_bei_Ort2($error_code, $geburtsort);
		F190_Ort_bei_Ort2($error_code, $sterbeort);
	}
}

sub F190_Ort_bei_Ort2{
	my $error_code 	 = $_[0];
	my $test_ort = $_[1];

	if ( $test_ort ne ''
	     and ($test_ort =~ /[^a-z](nähe|außerhalb|unweit|nahe)/
		      or $test_ort =~ /der Nähe/
			  )
		 and index( $test_ort, 'Anaheim') == -1
		 ){
			fehlermeldung($error_code, $title, $test_ort);
			#print $title."\n";
			#print "\t\t".$test_ort."\n";
			#die;
	}
}


sub F191_Kurzbeschreibung_mit_Nationalitaet_nicht_klein {
	my $error_code = 191;
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Kurzbeschreibung mit einer großgeschriebenen Nationalität';
				$error_description[$error_code][1] = 'Das Datenfeld Kurzbeschreibung hat eine großgeschriebene Nationalität (z.B.: "Deutscher Maler"). Dieses Adjektiv sollte nach <a href="http://de.wikipedia.org/wiki/Wikipedia:Namenskonventionen/Staaten">Wikipedia:Namenskonventionen/Staaten</a> kleingeschrieben werden. Für diese Fehlerrubrik wurden nach Wörtern gesucht, die ein "isch" enthalten oder ein "Deutsch", "Libysch", "Luxemburger" oder "Lichtensteiner".';
				$error_description[$error_code][2] = 3;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		my $first_word = $kurz_plain;
		$first_word =~ s/^ //g;
		my $pos = index ($first_word, ' ');
		if ($pos > -1) {
			$first_word = substr( $first_word, 0, $pos);
		}
			
		if (  ( index ($first_word , 'isch') > -1
				or index ($first_word , 'Deutsch') > -1 
				or index ($first_word , 'Libysch') > -1
				or index ($first_word , 'Luxemburger') > -1
				or index ($first_word , 'Lichtensteiner') > -1
			  )	
			  and $first_word =~ /^[A-ZÄÜÖ]/
			  and not $first_word =~ /^US-amerikanisch/
			  and not $first_word =~ /^ö/
			  and not $first_word =~ /^ü/
			  and not $first_word =~ /^ä/
			  and not $first_word =~ /ischof/
			  and $first_word ne 'Tischler'

			)
			 {
				fehlermeldung($error_code, $title, $kurz_plain);
				#print $title."\n";
				#print "\t\t".$kurz_plain."\n";
				#print "\t\t".$first_word."\n";
				#print 'd: x'.$defaultsort."x\n";
				#die;
		}
	}
}

sub F192_Name_nicht_im_Text {
	my $error_code = 192;
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
						$error_description[$error_code][0] = 'Name fehlt im Text';
						$error_description[$error_code][1] = 'Im Feld Name ist ein Namensbestandteil enthalten, der nicht im Text enthalten ist.';
						$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		my $test_name = $name;
		$test_name =~ s/,//g;
		my @test_name = split(' ', $test_name);

		my $testtext = $text;
		my $pos = index($testtext, '{{Personendaten');
		$testtext = substr($testtext, 0, $pos);
		$pos = index($testtext, '[[Kategorie:');
		$testtext = substr($testtext, 0, $pos);	
		$testtext = lc($testtext);	
		
		foreach ( @test_name) {
			#print $_."\n";
			my $test_name = $_;
			my $test_name_lc = lc ($test_name);
			
			
			if ( $is_redirect == 0 
				 and (length($test_name_lc) > 2 													
					  or (length($test_name_lc) > 1 and substr($test_name_lc,1,1) ne '.') )		# Personen mit  ein-Buchstaben-Abkürzung  Edward J. King ausschliessen
				 and index($testtext, $test_name_lc) == -1
				 ) {
						fehlermeldung($error_code, $title, $name.' - '.$test_name);
						#print "\t".$title."\n";
						#print "\t\t\t". $name."\n";
						#print "\t\t\t". $test_name."\n";
						#die;
			}		
			
		}
	}
}

sub F193_Kategorie_geboren_fehlt_total {
	my $error_code = 193;
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Kategorie:Geboren fehlt total';
				$error_description[$error_code][1] = 'Es gibt im Feld GEBURTSDATUM einen Eintrag, aber es fehlt der Kategorieneintrag (z.B. "Kategorie:Geboren 1958" oder "Kategorie:Geboren im 16. Jahrhundert" oder "Kategorie:Geboren unbekannt").';
				$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		if ( #$parameter{'modus_scan'}  eq "live" and 
			 $all_categories ne '' and 
			 $geburtsdatum =~ /[^ ]/ ) {

			 my $test = lc($all_categories);
			if ($test =~ /geboren/ ){
				#ok
			} else {
				fehlermeldung($error_code, $title, $geburtsdatum);
				#print "\t".$error_code."\t".$title."\t".$geburtsdatum."\n";
			}
		}
	}
}

sub F194_Kategorie_gestorben_fehlt_total {
	my $error_code = 194;
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Kategorie:Gestorben fehlt total';
				$error_description[$error_code][1] = 'Es gibt im Feld STERBEDATUM einen Eintrag, aber es fehlt der Kategorieneintrag (z.B. "Kategorie:Gestorben 1958" oder "Kategorie:Gestorben im 16. Jahrhundert" oder "Kategorie:Gestorben unbekannt").';
				$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		if ( #$parameter{'modus_scan'}  eq "live" and 
			 $all_categories ne '' and 
			 $sterbedatum =~ /[^ ]/ ) {

			 my $test = lc($all_categories);
			if ($test =~ /gestorben/ ){
				#ok
			} else {
				fehlermeldung($error_code, $title, $sterbedatum);
				#print "\t".$error_code."\t".$title."\t".$sterbedatum."\n";
			}
		}
	}
}

sub F195_Zeitlos {
	my $error_code = 195;
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
					$error_description[$error_code][0] = 'Zeitlose Person';
					$error_description[$error_code][1] = 'Bei diesen Personen konnte die Lebenszeit nicht über die Kategorien oder die Personendaten ermittelt werden. Hier sollten die Kategorien (Geboren/Gestorben) und die Personendaten ergänzt werden.';
					$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		if ( #$parameter{'modus_scan'}  eq "live" and 
			 $all_categories ne '' ) {
			 my $test = $all_categories;
			if (   $test =~ /(G|g)eboren [1-9]/ 
				or $test =~ /(G|g)eboren im/
				or $test =~ /(G|g)estorben [1-9]/
				or $test =~ /(G|g)estorben im/
				or $test =~ /Jahrhundert/
				){
				#ok
			} else {
				my $test2 = $geburtsdatum;
				$test2 =~ s/\t//g;
				$test2 =~ s/ //g;
				my $test3 = $sterbedatum;
				$test2 =~ s/\t//g;
				$test2 =~ s/ //g;
				if ($test2 eq '' 
					and $test3 eq '') {
					fehlermeldung($error_code, $title, $geburtsdatum);
					#print $error_code."\t".$title."\t".'<nowiki>'.$all_categories.'</nowiki>'."\n";
				}
			}
		}
	}
}

sub F196_Kurzbeschreibung_mit_falschem_schweizer {
	my $error_code = 196;
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Kurzbeschreibung mit falschem Schweizer';
				$error_description[$error_code][1] = 'Das Datenfeld Kurzbeschreibung wurde die Schreibweise "schweizer" gefunden. Schreib bitte "schweizerisch" oder "Schweizer". Siehe auch <a href="http://de.wikipedia.org/wiki/Wikipedia:Namenskonventionen/Staaten">Wikipedia:Namenskonventionen/Staaten</a>.';
				$error_description[$error_code][2] = 3;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {		
		if ( index ($kurz_plain , 'schweizer') > -1
			 and index ($kurz_plain , 'schweizeri') == -1 
			  )	
			 {
				fehlermeldung($error_code, $title, $kurz_plain);
				#print $error_code."\t".$title."\t".'<nowiki>'. $kurz_plain.'</nowiki>'."\n";
		}
	}
}

sub F197_Kurzbeschreibung_mit_falschem_US_amerikanisch {
	my $error_code = 197;
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Kurzbeschreibung mit falschem US-amerikanisch';
				$error_description[$error_code][1] = 'Das Datenfeld Kurzbeschreibung wurde eine fehlerhafte Schreibweise von "US-amerikanisch" gefunden. Siehe auch <a href="http://de.wikipedia.org/wiki/Wikipedia:Namenskonventionen/Staaten">Wikipedia:Namenskonventionen/Staaten</a>.';
				$error_description[$error_code][2] = 3;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {			
		if ( index (lc($kurz_plain), 'us-ameri') > -1
			 and index ($kurz_plain , 'US-ameri') == -1 
			 and index ($kurz_plain , 'US-Ameri') == -1 
			  )	
			 {
				fehlermeldung($error_code, $title, $kurz_plain);
				#print $error_code."\t".$title."\t".'<nowiki>'. $kurz_plain.'</nowiki>'."\n";
		}
	}
}

sub F198_Kurzbeschreibung_mit_Star {
	my $error_code = 198;
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Kurzbeschreibung mit Star';
				$error_description[$error_code][1] = 'Das Datenfeld Kurzbeschreibung wurde die Beschreibung "Filmstar", "Starkoch" oder ähnliches gefunden. Bitte nimm das "Star" raus und formuliere die Kurzbeschreibung neutraler.';
				$error_description[$error_code][2] = 3;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {		
		if (( $kurz_plain =~ /(^| |-)Star( |,|\.|;|$|\-)/
			 and index ($kurz_plain , 'White Star Line') == -1 
			 and index ($kurz_plain , 'Grauen Star') == -1 
			 and index ($kurz_plain , 'Nouvelle Star') == -1 
			 and index ($kurz_plain , 'Star-Club') == -1 
			  )	
			or 
			( $kurz_plain =~ /star( |,|\.|;|$|-)/
			 and index ($kurz_plain , 'Mostar') == -1 )
			 and index ($kurz_plain , 'Bastar') == -1 
			 )  
			 
			 {
				fehlermeldung($error_code, $title, $kurz_plain);
				#print $$error_code."\t".$title."\t".'<nowiki>'. $kurz_plain.'</nowiki>'."\n";
		}
	}
}

sub F199_Kurzbeschreibung_mit_falschem_Artikel_fuer_Frau {
	my $error_code = 199;
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Kurzbeschreibung mit falschem Artikel für eine Frau';
				$error_description[$error_code][1] = 'Das Datenfeld Kurzbeschreibung enthält ein falsches Adjektiv wie "deutscher Schwimmerin" oder "polnischer Malerin" obwohl es sich nach der Kategorie um eine Frau handelt.';
				$error_description[$error_code][2] = 3;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {		
		my $pos = index ($kurz_plain, ' ');
		my $erstes_wort = substr ($kurz_plain, 0, $pos);
		#print $erstes_wort.' - '.$kurz_plain."\n";
		
		if ( $erstes_wort ne '' 
			 and $all_categories =~ /Kategorie:Frau(\||\])/ 
			 and index ($all_categories, 'Kategorie:Transgender') == -1
			 and index ($all_categories, 'Kategorie:Mann|') == -1
			 and index ($all_categories, 'Kategorie:Mann]') == -1
			 )
			 {
			if ((
					(index ($erstes_wort , 'isch') > -1
					and index ($erstes_wort , 'ische') == -1
					and index ($erstes_wort , 'isch-') == -1
					and index ($erstes_wort , 'Bischöfin') == -1
					) 
				or index ($erstes_wort , 'deutscher') > -1
				or index ($erstes_wort , 'libyscher') > -1
				)
				and index ($erstes_wort , 'schsprachig') == -1
				and index ($erstes_wort , 'schstämmig') == -1
				) {
				fehlermeldung($error_code, $title, $kurz_plain);
				#print $error_code."\t".$title."\t".'<nowiki>'. $kurz_plain.'</nowiki>'."\n";
			}

		}
	}
}

sub F200_Kurzbeschreibung_mit_falschem_Artikel_fuer_Mann {
	my $error_code = 200;
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Kurzbeschreibung mit falschem Artikel für einen Mann';
				$error_description[$error_code][1] = 'Das Datenfeld Kurzbeschreibung enthält ein falsches Adjektiv wie "deutsche Schwimmer" oder "polnisches Maler" obwohl es sich nach der Kategorie um eine Mann handelt.';
				$error_description[$error_code][2] = 3;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {			
		my $pos = index ($kurz_plain, ' ');
		my $erstes_wort = substr ($kurz_plain, 0, $pos);
		#print $erstes_wort.' - '.$kurz_plain."\n";
		
		if ( $erstes_wort ne '' 
			 and $all_categories =~ /Kategorie:Mann(\||\])/ 
			 and index ($all_categories, 'Kategorie:Transgender') == -1
			 and index ($all_categories, 'Kategorie:Frau|') == -1
			 and index ($all_categories, 'Kategorie:Frau]') == -1
			 )
			 {
			if (
					(index ($erstes_wort , 'deutsch') > -1
					and index ($erstes_wort , 'deutscher') == -1
					and index ($erstes_wort , 'deutsches') == -1
					and index ($erstes_wort , 'deutsch-') == -1
					and index ($erstes_wort , 'deutschsprachig') == -1
					and index ($erstes_wort , 'deutschstämmig') == -1
					and index ($erstes_wort , 'deutschbaltisch') == -1
					and index ($erstes_wort , 'deutschamerikanisch') == -1
					) 
				or (index ($erstes_wort , 'ische') > -1
					and index ($erstes_wort , 'ischer') == -1
					and index ($erstes_wort , 'isches') == -1
					and index ($erstes_wort , 'ischsprachig') == -1
					and index ($erstes_wort , 'ischstämmig') == -1
				))			{
				fehlermeldung($error_code, $title, $kurz_plain);
				#print $error_code."\t".$title."\t".'<nowiki>'. $kurz_plain.'</nowiki>'."\n";
			}
		}
	}
}

sub F201_Kurzbeschreibung_mit_zwei_Grossbuchstaben_am_Anfang {
	my $error_code = 201;
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Kurzbeschreibung mit zwei Grossbuchstaben am Anfang';
				$error_description[$error_code][1] = 'Das Datenfeld Kurzbeschreibung enthält am Anfang eine Abkürzung aus 2 Großbuchstaben. Hier sollte die Nationalität mit eingetragen werden. Siehe auch <a href="http://de.wikipedia.org/wiki/Wikipedia:Namenskonventionen/Staaten">Wikipedia:Namenskonventionen/Staaten</a>. Statt "US-Diplomat" sollte z.B. besser "US-amerikanischer Diplomat" geschrieben werden.';
				$error_description[$error_code][2] = 3;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {	
		if ( $kurz_plain ne '') {

			if ( $kurz_plain =~ /^([ ]+)?[[:upper:]]{2}/
				 and index ($kurz_plain, 'US-a') == -1
				 and index ($kurz_plain, ' US-a') == -1
			  )	
			 {
				fehlermeldung($error_code, $title, $kurz_plain);
				#print $error_code."\t".$title."\t".'<nowiki>'. $kurz_plain.'</nowiki>'."\n";
			}
		}
	}
}


sub F202_Name_ohne_erstes_Wort_aus_dem_Titel {
	my $error_code = 202;
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Name ohne erstes Wort aus dem Titel';
				$error_description[$error_code][1] = 'Dem Datenfeld NAME fehlt das erste Wort aus dem Artikeltitel. Es müssen alle Namensbestandteile aus dem Titel in diesem Datenfeld enthalten sein. (Beispiel: Titel "Alicia Silverstone" NAME="Silverstone, Alica") Oft sind es Tippfehler oder verkürzte Namen, die aber so geschrieben werden sollen, wie sie im Titel stehen. ';
				$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		if ( $name ne ''
			 and index ($title, ' ') > -1) {

			 my $first_word = substr ($title,  0, index ($title, ' '));
			if ( index ($name , $first_word)  == -1 ) {
				fehlermeldung($error_code, $title, $name);
				#print $error_code."\t".$title."\t".'<nowiki>'. $name.'</nowiki>'."\n";
			}
		}
	}
}

sub F203_Geburtsjahr_gleich_Sterbejahr{
	my $error_code = 203;
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Geburtsjahr gleich Sterbejahr';
				$error_description[$error_code][1] = 'Das Geburtsjahr und das Sterbejahr sind identisch. Nur bei wenigen Ausnahmen gibt es für Kleinkinder einen Wikipedia-Artikel. Meist handelt es sich hier um einen Tippfehler.';
				$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		if ( $geburtsjahr ne ''
			 and $sterbejahr ne '') {

			 if ( $geburtsjahr == $sterbejahr
				  and $title ne 'Johann I. (Frankreich)'
				  and $title ne 'Baby Fae'
				  ) {
				fehlermeldung($error_code, $title, $geburtsdatum .' und '. $sterbedatum);
				#print $error_code."\t".$title."\t".'<nowiki>'. $geburtsdatum .' und '. $sterbedatum.'</nowiki>'."\n";
			}
		}
	}
}

sub F204_Ergaenzungsstrich_mit_Leerzeichen {
	my $error_code = 204;
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Ergänzungsstrich mit Leerzeichen';
				$error_description[$error_code][1] = 'Das Datenfeld Kurzbeschreibung enthält einen Ergänzungsstrich mit einem Leerzeichen davor (z.B. Fußballspieler und - trainer). ';
				$error_description[$error_code][2] = 2;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		if ( $kurz_plain ne '') {

			if (     index ($kurz_plain, ' - und') > -1
				 or  index ($kurz_plain, 'und - ') > -1
				 or  index ($kurz_plain, ', - ') > -1
				 or  index ($kurz_plain, ' - ,') > -1
			  )	
			 {
				fehlermeldung($error_code, $title, $kurz_plain);
				#print $error_code."\t".$title."\t".'<nowiki>'. $kurz_plain.'</nowiki>'."\n";
			}
		}
	}
}

sub F205_Kurzbeschreibung_mit_Schweitzer {
	my $error_code = 205;
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Kurzbeschreibung mit Schweitzer';
				$error_description[$error_code][1] = 'Das Datenfeld Kurzbeschreibung enthält "Schweitzer" (Tippfehler). Schreib bitte "Schweizer" oder "schweizerisch".';
				$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		if ( $kurz_plain ne '') {

			if ((     index ($kurz_plain, 'Schweitzer') > -1
				 or  index ($kurz_plain, 'schweitzer') > -1)
				 and  index ($kurz_plain, 'Albert') == -1
			  )	
			 {
				fehlermeldung($error_code, $title, $kurz_plain);
				print $error_code."\t".$title."\t".'<nowiki>'. $kurz_plain.'</nowiki>'."\n";
			}
		}
	}
}

sub F206_Kurzbeschreibung_mit_Politiker {
	my $error_code = 206;
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Kurzbeschreibung mit Politiker';
				$error_description[$error_code][1] = 'Das Datenfeld Kurzbeschreibung enthält den Text "-Politiker" ( z.B. "deutscher CDU-Politiker"). Bei der Mehrzahl der Einträge hat sich bei Politikern die Schreibweise "deutscher Politiker (CDU)" eingebürgert. ';
				$error_description[$error_code][2] = 3;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		if ( $kurz_plain ne '') {

			if ( $kurz_plain =~ /-Politiker/  )	
			 {
				fehlermeldung($error_code, $title, $kurz_plain);
				#print $error_code."\t".$title."\t".'<nowiki>'. $kurz_plain.'</nowiki>'."\n";
			}
		}
	}
}

sub F207_Kurzbeschreibung_mit_Politiker_ohne_Partei {
	my $error_code = 207;
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Kurzbeschreibung mit Politiker ohne Partei';
				$error_description[$error_code][1] = 'Das Datenfeld Kurzbeschreibung enthält den Text "Politiker" aber keine Klammer ( z.B. "deutscher Politiker"). Bei der Mehrzahl der Einträge zu Politikern hat sich die Schreibweise "deutscher Politiker (CDU)" eingebürgert. Alle hier aufgelisteten Politiker sind nach 1900 geboren und haben eine Kategorie in der "Mitglied" steht. (z.B. Kategorie:SPD-Mitglied). Bitte trage die Partei bei der Kurzbeschreibung noch mit ein. Im Augenblick werden nur deutsche Politiker ausgegeben!';
				$error_description[$error_code][2] = 3;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		if ( $kurz_plain ne '' and $all_categories ne '') {

			if ( index( $kurz_plain , 'Politiker') > -1
				 and index( $kurz_plain , '(') ==  -1
				 and index( $kurz_plain , '(') ==  -1
				 and index($all_categories, 'Deutscher') >  -1
				 and index($all_categories, 'Geboren 19') >  -1
				 and index($all_categories, 'Mitglied') >  -1
			)	
			 {
				fehlermeldung($error_code, $title, $kurz_plain);
				#print $error_code."\t".$title."\t".'<nowiki>'. $kurz_plain.'</nowiki>'."\n";
			}
		}
	}
}

sub F208_Datum_mit_falschem_Wort_am_Anfang{
	my $error_code = 208;
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
		$error_description[$error_code][0] = 'Geburts- oder Sterbedatum mit falschem Wort am Anfang';
		$error_description[$error_code][1] = 'Im Feld GEBURTSDATUM oder STERBEDATUM enthält ein falsches Wort am Anfang. Erlaubt sind nur "um", "zwischen", "vor", "nach", "getauft", "begraben", "unsicher:" und die Monatsnamen.';
		$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		F208_Datum_mit_falschem_Wort_am_Anfang2($error_code, $geburtsdatum_plain);
		F208_Datum_mit_falschem_Wort_am_Anfang2($error_code, $sterbedatum_plain);
	}
}

sub F208_Datum_mit_falschem_Wort_am_Anfang2{
	my $error_code = $_[0];
	my $test_datum = $_[1];
	my $test_datum1 = $test_datum;
	
	$test_datum =~ s/^[ ]+//;
	if ($test_datum ne '' 
		and ( $test_datum =~ /^[[:alpha:]]/
			 and not $test_datum =~ /^um /
			 and not $test_datum =~ /^zwischen /
			 and not $test_datum =~ /^vor /
			 and not $test_datum =~ /^nach /
			 and not $test_datum =~ /^getauft /
			 and not $test_datum =~ /^begraben /
			 and not $test_datum =~ /^unsicher: /
			 and not $test_datum =~ /^Januar /
			 and not $test_datum =~ /^Februar /
			 and not $test_datum =~ /^März /
			 and not $test_datum =~ /^April /
			 and not $test_datum =~ /^Mai /
			 and not $test_datum =~ /^Juni /
			 and not $test_datum =~ /^Juli /
			 and not $test_datum =~ /^August /
			 and not $test_datum =~ /^September /
			 and not $test_datum =~ /^Oktober /
			 and not $test_datum =~ /^November /
			 and not $test_datum =~ /^Dezember /
		
		))
	{
		fehlermeldung($error_code, $title, $test_datum);
		#print $error_code."\t".$title."\t".'<nowiki>'. $test_datum.'</nowiki>'."\n";
	}
}



sub F209_Datum_mit_J{
	my $error_code = 209;
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
		$error_description[$error_code][0] = 'Geburts- oder Sterbedatum mit J';
		$error_description[$error_code][1] = 'Im Feld GEBURTSDATUM oder STERBEDATUM enthält ein "J" oder "j" aber es steht kein "Januar", "Jänner", "Juni", "Juli" oder "Jahrhundert" drin. Meist ist es eine unerwünschte Abkürzung des Wortes Jahrhundert. Bitte schreib das Wort immer aus (z.B. "6. Jahrhundert v. Chr.").';
		$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		F209_Datum_mit_J2($error_code, $geburtsdatum_plain);
		F209_Datum_mit_J2($error_code, $sterbedatum_plain);
	}
}

sub F209_Datum_mit_J2{
	my $error_code = $_[0];
	my $test_datum = $_[1];
	my $test_datum1 = $test_datum;

	if ($test_datum ne '' 
		and ( index (lc($test_datum) , 'j') > -1
		      and index (lc($test_datum) , 'januar') == -1
			  and index (lc($test_datum) , 'jänner') == -1
			  and index (lc($test_datum) , 'juni') == -1
			  and index (lc($test_datum) , 'juli') == -1
			  and index (lc($test_datum) , 'jahrhundert') == -1
			  and index (lc($test_datum) , 'jahrtausend') == -1
		))
	{
		fehlermeldung($error_code, $title, $test_datum);
		#print "\t".$error_code."\t".$title."\t".'<nowiki>'. $test_datum.'</nowiki>'."\n";
	}
}

sub F210_Datenfeld_mit_HTML{
	my $error_code = 210;
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
			$error_description[$error_code][0] = 'Datenfeld mit HTML';
			$error_description[$error_code][1] = 'In einem Datenfeld wurden HTML-Entities gefunden. (z.B. "<nowiki>&nbsp;</nowiki>")';
			$error_description[$error_code][2] = 1;	
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		F210_Datenfeld_mit_HTML2($error_code, $name);
		F210_Datenfeld_mit_HTML2($error_code, $aname);
		F210_Datenfeld_mit_HTML2($error_code, $kurz_plain);
		F210_Datenfeld_mit_HTML2($error_code, $geburtsdatum);
		F210_Datenfeld_mit_HTML2($error_code, $geburtsort_plain);
		F210_Datenfeld_mit_HTML2($error_code, $geburtsort_plain);
		F210_Datenfeld_mit_HTML2($error_code, $sterbedatum);
	}
}

sub F210_Datenfeld_mit_HTML2{
	my $error_code = $_[0];	
	my $test_datenfeld = $_[1];
	$test_datenfeld  = lc ($test_datenfeld );
	if (   index ($test_datenfeld , '&nbsp;') > -1
		or index ($test_datenfeld , '&ndash;') > -1
		){
			fehlermeldung($error_code, $title, $test_datenfeld);
			#print $title."\n";
			#print "\t\t".$test_datenfeld."\n";
			#die;
	}
}

sub F211_Datenfeld_falsche_Klammeranzahl{
	my $error_code = 211;
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
			$error_description[$error_code][0] = 'Datenfeld mit falscher Klammeranzahl';
			$error_description[$error_code][1] = 'In einem Datenfeld befindet sich Text, der nicht die gleiche Anzahl von öffnenden und schließenden Klammern besitzt (z.B. "San Rafael (Argentinien))" ).' ;
			$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		F211_Datenfeld_falsche_Klammeranzahl2($error_code, $name);
		F211_Datenfeld_falsche_Klammeranzahl2($error_code, $aname);
		F211_Datenfeld_falsche_Klammeranzahl2($error_code, $kurz);
		F211_Datenfeld_falsche_Klammeranzahl2($error_code, $geburtsdatum);
		F211_Datenfeld_falsche_Klammeranzahl2($error_code, $geburtsort);
		F211_Datenfeld_falsche_Klammeranzahl2($error_code, $sterbedatum);
		F211_Datenfeld_falsche_Klammeranzahl2($error_code, $sterbeort);
	}	
}

sub F211_Datenfeld_falsche_Klammeranzahl2{
	my $error_code = $_[0];	
	my $test = $_[1];
	
	my $test1 = $test;
	my $test2 = $test;
	$test1=~ s/[^\(]//g;
	$test2=~ s/[^\)]//g;
	
	if ( length($test1) != length($test2) ){
			fehlermeldung($error_code, $title, '<nowiki>'.$test.'</nowiki>');
			#print $title."\n";
			#print "\t\t".$test."\n";
			#die;
	}
}

sub F212_Kurzbeschreibung_mit_Herkunft {
	my $error_code = 212;
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Kurzbeschreibung mit Herkunft';
				$error_description[$error_code][1] = 'Das Datenfeld Kurzbeschreibung enthält eine Herkunftsangabe, die nicht mit der Standardform der Personendaten konform ist (z.B. "Kameramann australischer Herkunft"). Es sollte besser "australischer Kammeramann" geschrieben werden. Bei Einwanderern werden die mehrfachen Nationalitäten mit Bindestrich getrennt (z.B. "russisch-deutsche Ärztin").';
				$error_description[$error_code][2] = 2;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		if ( $kurz_plain ne '' ) {

			if ((     index( $kurz_plain , 'Herkunft') > -1
				 or  index( $kurz_plain , 'Staatsangehörigkeit') >  -1
				 or  index( $kurz_plain , 'Staatsbürgerschaft') >  -1
				 ) and 	index( $kurz_plain , 'US-amerikanisch') == -1 
				   and  $kurz_plain =~ /^([ ]+)?[[:upper:]]/ 
			)	
			 {
				fehlermeldung($error_code, $title, $kurz_plain);
				#print $error_code."\t".$title."\t".'<nowiki>'. $kurz_plain.'</nowiki>'."\n";
			}
		}
	}
}

sub F213_Kurzbeschreibung_ohne_Nationalitaet {
	my $error_code = 213;
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
					$error_description[$error_code][0] = 'Kurzbeschreibung ohne Nationalität';
					$error_description[$error_code][1] = 'Das Datenfeld Kurzbeschreibung enthält keine Nationalität am Anfang (z.B. "Erfinder des World Wide Web"). Es werden hier alle Personen ausgegeben, deren Kurzbeschreibung mit einem Großbuchstaben anfängt (außer "US-amerikanische ..." und "Schweizer") und deren Geburtsjahr nach 1900 bzw. das Sterbejahr nach 1975 liegt. Diesen Personen sollten alle einer oder mehrere Nationalitäten zugeordnet werden können.';
					$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		if ( $kurz_plain ne '' ) {

			if ( 
				 $kurz_plain =~ /^([ ]+)?[[:upper:]]/ 
				 # großgeschriebene Nationalitäten
				 and index ($kurz_plain,  'US-a' ) == -1
				 and index ($kurz_plain,  'Schweizer' ) == -1
				 and index ($kurz_plain,  'Liechtensteiner' ) == -1
				 
				 # Insulaner
				 and index ($kurz_plain, 'Trinidad und Tobago' ) == -1
				 and index ($kurz_plain, 'Vereinigte Arabische Emirate' ) == -1
				 and index ($kurz_plain, 'Vereinigten Arabischen Emirate' ) == -1
				 and index ($kurz_plain, 'Cookinseln' ) == -1
				 and index ($kurz_plain, 'St. Kitts und Nevis' ) == -1
				 ){
			
				my $ok = 'false';
				if ($geburtsjahr ne ''){
					$ok = 'true' if ( $geburtsjahr > 1900);
				}
				if ($sterbejahr ne ''){
					$ok = 'true' if ( $sterbejahr > 1975);
				}	
			
				if ($ok eq 'true') 
				 {
					fehlermeldung($error_code, $title, $kurz_plain);
					#print $error_code."\t".$title."\t".'<nowiki>'. $kurz_plain.'</nowiki>'."\n";
				}
			}
		}
	}
}

sub F214_Datenfeld_kleingeschrieben{
	my $error_code = 214;
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
			$error_description[$error_code][0] = 'Datenfeld kleingeschrieben';
			$error_description[$error_code][1] = 'In einem Datenfeld (Kurzbeschreibung, Geburtsort oder Sterbeort) befindet sich Text, der nur kleingeschrieben wurde (z.B. "wien" ).' ;
			$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		F214_Datenfeld_kleingeschrieben2($error_code, $kurz_plain);
		F214_Datenfeld_kleingeschrieben2($error_code, $geburtsort_plain);
		F214_Datenfeld_kleingeschrieben2($error_code, $sterbeort_plain);
	}
}

sub F214_Datenfeld_kleingeschrieben2{
	my $error_code = $_[0];
	my $test = $_[1];
	my $test2 = $test;
	$test2 =~ s/ //g;
	
	my $test3 = $test2;
	$test3 =~ s/[a-zäüö0-9]//gi;
	
	
	if ( $test eq lc($test2) 
	     and length($test2) > 0
		 and length($test3) == 0
		 ){
			fehlermeldung($error_code, $title, '<nowiki>'.$test.'</nowiki>');
			#print $error_code."\t".$title."\t".'<nowiki>'. $test.'</nowiki>'."\n";
	}
}

sub F215_Datum_mit_Artikel{
	my $error_code = 215;
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
		$error_description[$error_code][0] = 'Geburts- oder Sterbedatum mit Artikel';
		$error_description[$error_code][1] = 'Im Feld GEBURTSDATUM oder STERBEDATUM enthält ein Artikel (dem, den), der gelöscht werden sollte (z.B. "nach dem 22. Juni 1653").';
		$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		F215_Datum_mit_Artikel2($error_code, $geburtsdatum_plain);
		F215_Datum_mit_Artikel2($error_code, $sterbedatum_plain);
	}
}

sub F215_Datum_mit_Artikel2{
	my $error_code = $_[0];
	my $test_datum = $_[1];
	my $test_datum1 = $test_datum;
	
	$test_datum =~ s/^[ ]+//;
	if ($test_datum ne '' 
		and ( index ($test_datum, 'dem') > -1
		      or index ($test_datum, 'den') > -1
	
		))
	{
		fehlermeldung($error_code, $title, $test_datum);
		#print $error_code."\t".$title."\t".'<nowiki>'. $test_datum.'</nowiki>'."\n";
	}
}

sub F216_Datum_mit_doppeltem_Monat{
	my $error_code = 216;
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
		$error_description[$error_code][0] = 'Geburts- oder Sterbedatum mit doppeltem Monat';
		$error_description[$error_code][1] = 'Im Feld GEBURTSDATUM oder STERBEDATUM enthält mehrere Monatsangaben (z.B. "5. Mai September 1926").';
		$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		F216_Datum_mit_doppeltem_Monat2($error_code, $geburtsdatum_plain);
		F216_Datum_mit_doppeltem_Monat2($error_code, $sterbedatum_plain);
	}
}

sub F216_Datum_mit_doppeltem_Monat2{
	my $error_code = $_[0];
	my $test_datum = $_[1];
	my $test_datum1 = $test_datum;
	
	$test_datum =~ s/^[ ]+//;
	if ($test_datum ne '' 
		and $test_datum =~ /(Januar|Februar|März|April|Mai|Juni|Juli|August|September|Oktober|November|Dezember)[ ]?\.?[ ]?(Januar|Februar|März|April|Mai|Juni|Juli|August|September|Oktober|November|Dezember)/
		)
	{
		fehlermeldung($error_code, $title, $test_datum);
		#print $error_code."\t".$title."\t".'<nowiki>'. $test_datum.'</nowiki>'."\n";
	}
}

sub F217_Datum_mit_fehlerhaften_Jahrhundert{
	my $error_code = 217;
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
		$error_description[$error_code][0] = 'Geburts- oder Sterbedatum mit fehlerhaftem Jahrhundert ';
		$error_description[$error_code][1] = 'Im Feld GEBURTSDATUM oder STERBEDATUM enthält eine fehlerhafte Angabe des Jahrhunderts (z.B. "5. Jahrhunderts" oder "5. Jahrhunder"). ';
		$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		F217_Datum_mit_fehlerhaften_Jahrhundert2($error_code, $geburtsdatum_plain);
		F217_Datum_mit_fehlerhaften_Jahrhundert2($error_code, $sterbedatum_plain);
	}
}

sub F217_Datum_mit_fehlerhaften_Jahrhundert2{
	my $error_code = $_[0];
	my $test_datum = $_[1];
	my $test_datum1 = $test_datum;
	
	$test_datum =~ s/^[ ]+//;
	if ($test_datum ne '' 
		and (index ($test_datum , 'Jahrhunderts') > -1
		     or $test_datum =~ /Jahrhunder[^t]/
		))
	{
		fehlermeldung($error_code, $title, $test_datum);
		#print $error_code."\t".$title."\t".'<nowiki>'. $test_datum.'</nowiki>'."\n";
	}
}


sub F218_Datum_mit_bis{
	my $error_code = 218;
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
		$error_description[$error_code][0] = 'Geburts- oder Sterbedatum mit "bis"';
		$error_description[$error_code][1] = 'Im Feld GEBURTSDATUM oder STERBEDATUM enthält das Wort "bis" (z.B. "um 1120 bis 1127"). Die korrekte Schreibweise für Zeiträume bei den Personendaten ist mit "zwischen ... und ..." (z.B. "zwischen 1350 und 1360").';
		$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		F218_Datum_mit_bis2($error_code, $geburtsdatum_plain);
		F218_Datum_mit_bis2($error_code, $sterbedatum_plain);
	}
}

sub F218_Datum_mit_bis2{
	my $error_code = $_[0];
	my $test_datum = $_[1];
	my $test_datum1 = $test_datum;
	
	$test_datum =~ s/^[ ]+//;
	if ($test_datum ne '' 
		and index ($test_datum , 'bis') > -1
		)
	{
		fehlermeldung($error_code, $title, $test_datum);
		#print $error_code."\t".$title."\t".'<nowiki>'. $test_datum.'</nowiki>'."\n";
	}
}

sub F219_Datum_mit_Punkt_vor_vor{
	my $error_code = 219;
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
		$error_description[$error_code][0] = 'Geburts- oder Sterbedatum mit Punkt vor "v. Chr."';
		$error_description[$error_code][1] = 'Im Feld GEBURTSDATUM oder STERBEDATUM enthält einen Punkt vor v. Chr. (z.B. "um 1. v. Chr."). Die vor dem "v. Chr." sollte kein Punkt stehen.';
		$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		F219_Datum_mit_Punkt_vor_vor2($error_code, $geburtsdatum_plain);
		F219_Datum_mit_Punkt_vor_vor2($error_code, $sterbedatum_plain);
	}
}

sub F219_Datum_mit_Punkt_vor_vor2{
	my $error_code = $_[0];
	my $test_datum = $_[1];
	my $test_datum1 = $test_datum;
	
	$test_datum =~ s/^[ ]+//;
	if ($test_datum ne '' 
		and index ($test_datum , '. v.') > -1
		)
	{
		fehlermeldung($error_code, $title, $test_datum);
		#print $error_code."\t".$title."\t".'<nowiki>'. $test_datum.'</nowiki>'."\n";
	}
}

sub F220_Datum_mit_zwischen_ohne_und{
	my $error_code = 220;
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
		$error_description[$error_code][0] = 'Geburts- oder Sterbedatum mit fehlerhaftem Zeitraum';
		$error_description[$error_code][1] = 'Im Feld GEBURTSDATUM oder STERBEDATUM enthält das Wort "zwischen", aber das Wort "und" fehlt (z.B. "zwischen 1050 und 1100"). ';
		$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		F220_Datum_mit_zwischen_ohne_und2($error_code, $geburtsdatum_plain);
		F220_Datum_mit_zwischen_ohne_und2($error_code, $sterbedatum_plain);
	}
}

sub F220_Datum_mit_zwischen_ohne_und2{
	my $error_code = $_[0];
	my $test_datum = $_[1];
	my $test_datum1 = $test_datum;
	
	$test_datum =~ s/^[ ]+//;
	if ($test_datum ne '' 
		and index ($test_datum , 'zwischen') > -1
		and index ($test_datum , 'und') == -1
		)
	{
		fehlermeldung($error_code, $title, $test_datum);
		#print $error_code."\t".$title."\t".'<nowiki>'. $test_datum.'</nowiki>'."\n";
	}
}


sub F221_Kurzbeschreibung_mit_Name_identisch {
	my $error_code = 221;
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Kurzbeschreibung identisch mit Name';
				$error_description[$error_code][1] = 'Das Datenfeld KURZBESCHREIBUNG enthält den gleichen Text wie das Datenfeld NAME.';
				$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		if ( $kurz_plain eq $name 
			 and $kurz_plain ne '')	
			 {
				fehlermeldung($error_code, $title, $kurz_plain);
				#print $error_code."\t".$title."\t".'<nowiki>'. $kurz_plain.'</nowiki>'."\n";
		}
	}
}

sub F222_Kurzbeschreibung_mit_Alternativnamen_identisch {
	my $error_code = 222;
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Kurzbeschreibung identisch mit Alternativnamen';
				$error_description[$error_code][1] = 'Das Datenfeld KURZBESCHREIBUNG enthält den gleichen Text wie das Datenfeld ALTERNATIVNAMEN.';
				$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		if ( $kurz_plain eq $aname 
			 and $kurz_plain ne '')	
			 {
				fehlermeldung($error_code, $title, $kurz_plain);
				#print $error_code."\t".$title."\t".'<nowiki>'. $kurz_plain.'</nowiki>'."\n";
		}
	}
}

sub F223_Name_fehlt_im_Titel {
	my $error_code = 223;
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Name fehlt im Titel';
				$error_description[$error_code][1] = 'Alle Wörter aus dem Feld Name müssen im Titel vorkommen.';
				$error_description[$error_code][2] = 3;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		#print 'Name='.$name."\n";
		my $found_not_word = '';
		my @words = split( / /, $name);
		foreach (@words){
			my $current_word = $_;
			my @word_variantes;
			$current_word =~ s/,$//;
			$found_not_word = $current_word; 			
			push (@word_variantes, $current_word);
			my $current_word2 = $current_word;
			$current_word2 =~ s/^a/A/;			# arabic Names: Al-Bayhaqi 
			$current_word2 =~ s/^de/De/;		# z.B. Albert de Brudzewo
			$current_word2 =~ s/^le/Le/;		# z.B. Gertrud von le Fort
			$current_word2 =~ s/^ibn/Ibn/;		# z.B. Abbas ibn Firnas
			$current_word2 =~ s/^van/Van/;		# z.B. William van Alen


			push (@word_variantes, $current_word2);

			my $found_word_variant = '';
			foreach (@word_variantes){
				#print 'Check Variante='.$_."\n";
				if (    index($title, $_) > -1) {
					$found_word_variant = $_;
					#print 'Found in title'."\n";
					last;
				}
			}
			if ($found_word_variant ne '') {
				$found_not_word = '';
				next;
			}
			last if ($found_not_word  ne '');
	
		}
	
		if ($found_not_word  ne '') {
			fehlermeldung($error_code, $title, 'Fand kein: '.$found_not_word );
			#print $error_code."\t".$title."\t".'<nowiki>'. $defaultsort.'</nowiki>'."\n";
			#die;
		}


	}
}

sub F224_Kategoriensortierung_mit_Kleinbuchstaben_am_Wortanfang {
	my $error_code = 224;
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Kategoriensortierung mit Kleinbuchstaben am Wortanfang';
				$error_description[$error_code][1] = 'Für die korrekte Einsortierung einer Person in den Kategorien, ist es notwendig, dass alle Buchstaben am Wortanfang großgeschrieben werden (z.B. Abbo von und zu Auxerre: "Abbo Von Und Zu Auxerre"). Siehe auch <a href="http://de.wikipedia.org/wiki/Hilfe:Kategorien">Hilfe:Kategorien</a>. (Derzeit werden nur die ersten 10 Buchstaben überprüft.)';
				$error_description[$error_code][2] = 3;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {	
		my $test_defaultsort = $defaultsort;
		# TODO:  Buchstabenzahl langsam erhöhen
		#$test_defaultsort = substr ($test_defaultsort, 0, 10) if (length ($test_defaultsort) >10 );
		
		#if ( $test_defaultsort ne ''
		#	 and $test_defaultsort =~ / [[:lower:]]/
		#	)	
		#	 {
				# deaktiviert, da Media-Wiki-Software das jetzt nicht mehr braucht
				# fehlermeldung($error_code, $title, $defaultsort);
				#print $error_code."\t".$title."\t".'<nowiki>'. $defaultsort.'</nowiki>'."\n";
		#}
	}
}

sub F225_Kategoriensortierung_mit_Buchstaben_nach_Komma {
	my $error_code = 225;
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Kategoriensortierung mit Buchstaben nach Komma';
				$error_description[$error_code][1] = 'Für die korrekte Einsortierung einer Person in den Kategorien, ist es notwendig, dass nach einem Komma ein Leerzeichen steht (z.B. "Rockwell,Alexandre"). Siehe auch <a href="http://de.wikipedia.org/wiki/Hilfe:Kategorien">Hilfe:Kategorien</a>.';
				$error_description[$error_code][2] = 3;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {	
	
		if ( $defaultsort ne ''
			 and $defaultsort =~ /,[^ ]/
			)	
			 {
				fehlermeldung($error_code, $title, $defaultsort);
				#print $error_code."\t".$title."\t".'<nowiki>'. $defaultsort.'</nowiki>'."\n";
		}
	}
}

sub F226_Kategorie_geboren_doppelt_Jahr_und_Jahrhundert {
	my $error_code = 226;
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
			$error_description[$error_code][0] = 'Kategorie:Geboren doppelt mit Jahr und Jahrhundert';
			$error_description[$error_code][1] = 'Die Kategorie Geboren ist in diesem Artikel mehrmals enthalten.';
			$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {	
		my $test = lc($all_categories);
		if ( $all_categories ne '' 
			 and $test =~ /kategorie:geboren [0-9]/
			 and $test =~ /kategorie:geboren im/
			) 
			{
			fehlermeldung($error_code, $title, '<nowiki>'. $all_categories . '</nowiki>' );
			#print $error_code."\t".$title."\t".'<nowiki>'. $all_categories.'</nowiki>'."\n";
		}
	}
}

sub F227_Kategorie_gestorben_doppelt_Jahr_und_Jahrhundert {
	my $error_code = 227;
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
			$error_description[$error_code][0] = 'Kategorie:Gestorben doppelt mit Jahr und Jahrhundert';
			$error_description[$error_code][1] = 'Die Kategorie Gestorben ist in diesem Artikel mehrmals enthalten.';
			$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {	
		my $test = lc($all_categories);
		if ( $all_categories ne '' 
			 and $test =~ /kategorie:gestorben [0-9]/
			 and $test =~ /kategorie:gestorben im/
			) 
			{
			fehlermeldung($error_code, $title, '<nowiki>'. $all_categories . '</nowiki>' );
			#print $error_code."\t".$title."\t".'<nowiki>'. $all_categories.'</nowiki>'."\n";
		}
	}
}





sub F228_Ort_mit_Verb{
	my $error_code = 228;
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
			$error_description[$error_code][0] = 'Geburts- oder Sterbeort mit Verb';
			$error_description[$error_code][1] = 'Im Feld GEBURTSORT oder STERBEORT enhält ein Verb (war, wurde, ist z.B. "Straubing war ein deutscher Maler")';
			$error_description[$error_code][2] = 2;	
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {	
		F228_Ort_mit_Verb2($error_code, $geburtsort);
		F228_Ort_mit_Verb2($error_code, $sterbeort);
	}
}

sub F228_Ort_mit_Verb2{
	my $error_code 	 = $_[0];
	my $test_ort = $_[1];

	if ( $test_ort ne ''
	     and $test_ort =~ / (war|wurde|ist) /
		){
			fehlermeldung($error_code, $title, $test_ort);
			#print $error_code."\t".$title."\t".'<nowiki>'. $test_ort.'</nowiki>'."\n";
	}
}

sub F229_Ort_nur_Zahl{
	my $error_code = 229;
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
			$error_description[$error_code][0] = 'Geburts- oder Sterbeort nur Zahl';
			$error_description[$error_code][1] = 'Im Feld GEBURTSORT oder STERBEORT enhält nur eine Zahl (z.B. "1984"). Hier ist meist nur eine Zeile vertauscht.';
			$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		F229_Ort_nur_Zahl2($error_code, $geburtsort);
		F229_Ort_nur_Zahl2($error_code, $sterbeort);
	}
}

sub F229_Ort_nur_Zahl2{
	my $error_code	 = $_[0];
	my $test_ort = $_[1];

	if ( $test_ort ne ''
	     and $test_ort =~ /^([ ]+)?[0-9]+([ ]+)?$/
		){
			fehlermeldung($error_code, $title, $test_ort);
			#print $error_code."\t".$title."\t".'<nowiki>'. $test_ort.'</nowiki>'."\n";
	}
}



sub F230_Ort_mit_Monat{
	my $error_code = 230;
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
			$error_description[$error_code][0] = 'Geburts- oder Sterbeort mit Monatsname';
			$error_description[$error_code][1] = 'Im Feld GEBURTSORT oder STERBEORT enhält einen Monatsnamen (z.B. "Mai"). Hier ist meist ein Datum in das falsche Feld geschrieben.';
			$error_description[$error_code][2] = 1;	
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		F230_Ort_mit_Monat2($error_code, $geburtsort);
		F230_Ort_mit_Monat2($error_code, $sterbeort);
	}
}

sub F230_Ort_mit_Monat2{
	my $error_code = $_[0];
	my $test_ort = $_[1];

	if ( $test_ort ne ''
	     and $test_ort =~ / (Jänner|Januar|Februar|März|April|Mai|Juni|Juli|August|September|Oktober|November|Dezember) /
		){
			fehlermeldung($error_code, $title, $test_ort);
			#print $error_code."\t".$title."\t".'<nowiki>'. $test_ort.'</nowiki>'."\n";
	}
}

sub F231_Ort_mit_Zeit{
	my $error_code = 231;
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
			$error_description[$error_code][0] = 'Geburts- oder Sterbeort mit Zeitangabe';
			$error_description[$error_code][1] = 'Im Feld GEBURTSORT oder STERBEORT enhält eine Zeitangabe am Anfang (z.B. "um 1990"). An erster Stelle soll immer der eigentliche Ort stehen.';
			$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		F231_Ort_mit_Zeit2($error_code, $geburtsort);
		F231_Ort_mit_Zeit2($error_code, $sterbeort);
	}
}

sub F231_Ort_mit_Zeit2{
	my $error_code 	= $_[0];
	my $test_ort = $_[1];

	if ( $test_ort ne ''
	     and $test_ort =~ /^([ ]+)?(vor|nach|um|zwischen) [0-9]/
		){
			fehlermeldung($error_code, $title, $test_ort);
			#print $error_code."\t".$title."\t".'<nowiki>'. $test_ort.'</nowiki>'."\n";
	}
}

sub F232_Ort_mit_Kreuz{
	my $error_code = 232;
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
			$error_description[$error_code][0] = 'Geburts- oder Sterbeort mit Kreuz';
			$error_description[$error_code][1] = 'Im Feld GEBURTSORT oder STERBEORT enhält eine Kreuz (z.B. "Dresden, † 1912").';
			$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		F232_Ort_mit_Kreuz2($error_code, $geburtsort);
		F232_Ort_mit_Kreuz2($error_code, $sterbeort);
	}
}

sub F232_Ort_mit_Kreuz2{
	my $error_code = $_[0];	
	my $test_ort = $_[1];

	if ( $test_ort ne ''
	     and $test_ort =~ /†/
		){
			fehlermeldung($error_code, $title, $test_ort);
			#print $error_code."\t".$title."\t".'<nowiki>'. $test_ort.'</nowiki>'."\n";
	}
}


sub F233_Sterbeort_mit_zwei_Zeilen{
	my $error_code = 233;
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
							$error_description[$error_code][0] = 'Sterbedatum mit zwei Zeilen';
							$error_description[$error_code][1] = 'Im Feld STERBEORT enhält mehr als eine Zeile. Es wurden mehrere Zeilenümbrüche vor dem Ende der Personendaten gefunden.';
							$error_description[$error_code][2] = 1;	
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		
		my $pos1 = index( $text,'{{Personendaten');
		my $pos = index( $text, 'STERBEDATUM', $pos1);
		
		
		if ( $pos > -1) {
			my $pos2 = index( $text, '}}', $pos);
			if ( $pos2 > -1) {
				my $test = substr ( $text, $pos, $pos2-$pos);
				my $pos3 = index( $test, "\n");
				if ($pos3 > -1) {
					my $pos4 = index( $test, "\n", $pos3+1);
					if ( $pos4 > -1 ) {
						my $pos5 = index( $test, "\n", $pos4+1);
						if ( $pos5 > -1 )
						   {
							fehlermeldung($error_code, $title, $sterbeort);
							#print $error_code."\t".$title."\t".'<nowiki>'. $sterbeort.'</nowiki>'."\n";
						}
					}			
				}
			}	
		}
	}
}


sub F234_Name_mit_Punkt_aber_Titel_ohne_Punkt {
	my $error_code = 234;
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Name mit Punkt, Titel ohne Punkt';
				$error_description[$error_code][1] = 'Das Datenfeld NAME hat einen Punkt, der so nicht im Titel steht. Es sollen im Datenfeld Name nur die Namensbestandteile auftauchen, die auch im Titel enthalten sind. Hat eine Name noch weitere Namensbestandteile, dann kann diese Version im Datenfeld ALTERNATIVNAMEN untergebracht werden.';
				$error_description[$error_code][2] = 2;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {	

		if ( index ( $name , '.') > -1
			 and index ( $title , '.') == -1
			)	
			 {
				fehlermeldung($error_code, $title, $name);
				#print $error_code."\t".$title."\t".'<nowiki>'. $defaultsort.'</nowiki>'."\n";
		}
	}
}


sub F235_Kurzbeschreibung_mit_Zeitraum {
	my $error_code = 235;
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Kurzbeschreibung mit Zeitraum';
				$error_description[$error_code][1] = 'Das Datenfeld KURZBESCHREIBUNG enthält einen Zeitraum (z.B. "von 1875 bis 1892"). Solche Zeitangaben sollten immer in Klammern mit <a href="http://de.wikipedia.org/wiki/Bis-Strich">Bis-Strich</a> am Ende stehen, also z.B. so: "britischer Sänger der Band XY (1928–1934)". In der vorliegen Kurzbeschreibung konnte keine Klammer gefunden werden.';
				$error_description[$error_code][2] = 2;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {	

		if ( $kurz_plain ne ''
			 and $kurz_plain =~ /von [0-9]+( v\. Chr\.)? bis [0-9]+/
			 and index ( $kurz_plain , '(') == -1
			)	
			 {
				fehlermeldung($error_code, $title, $kurz_plain);
				#print $error_code."\t".$title."\t".'<nowiki>'. $kurz_plain.'</nowiki>'."\n";
		}
	}
}

sub F236_Kurzbeschreibung_mit_Amerikanern {
	my $error_code = 236;
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Kurzbeschreibung mit "amerikanisch"';
				$error_description[$error_code][1] = 'Das Datenfeld KURZBESCHREIBUNG enthält am Anfang das Wort "amerikanisch". Dieses Wort ist nicht so eindeutig wie "US-amerikanisch". Da die große Mehrzahl der US-Amerikaner in den Personendaten mit "US-amerikanisch" beschrieben wird (29649 "US-amerikanische ..." und 1919 "amerikanische ..." im Dump vom 10. Juli 2009), sollte diese Bezeichnung vereinheitlich werden. Die hier gelistete Personen sind nach 1910 geboren oder gestorben. Im Jahr 1910 wurden der letzte Bundesstaat auf dem Festland in die USA eingegliedert.';
				$error_description[$error_code][2] = 0; #3
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {	

		if ( $kurz_plain ne ''
			 and $kurz_plain =~ /^([ ]+)?amerikanisch/
			 and ($geburtsjahr ne '' )
			) 
			{
			my $ok = 'false';
			if  ( $geburtsjahr ne '') {
				if ($geburtsjahr > 1910) {
					$ok = 'true';
				}
			}
			if  ( $sterbejahr ne '') {
				if ($sterbejahr > 1910) {
					$ok = 'true';
				}
			}

			if ($ok eq 'true') 
			
			 {
				fehlermeldung($error_code, $title, $kurz_plain);
				#print $error_code."\t".$title."\t".'<nowiki>'. $kurz_plain.'</nowiki>'."\n";
				
			}
		}
	}
}



sub F237_Kurzbeschreibung_mit_Bindestrich_nach_und {
	my $error_code = 237;
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Kurzbeschreibung mit Bindestrich nach und';
				$error_description[$error_code][1] = 'Das Datenfeld KURZBESCHREIBUNG enthält einen Bindestrich nach dem Wort "und" (z.B. "Fußballspieler und-trainer"). Dieser Tippfehler sollte beseitigt werden.';
				$error_description[$error_code][2] = 3;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {	

		if ( $kurz_plain ne ''
			 and $kurz_plain =~ / und-/
			)	
			 {
				fehlermeldung($error_code, $title, $kurz_plain);
				#print $error_code."\t".$title."\t".'<nowiki>'. $kurz_plain.'</nowiki>'."\n";
		}
	}
}

sub F238_Kurzbeschreibung_mit_Bindestrich_zwischen_Zahlen {
	my $error_code = 238;
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Kurzbeschreibung mit Bindestrich zwischen Zahlen';
				$error_description[$error_code][1] = 'Das Datenfeld KURZBESCHREIBUNG enthält einen Bindestrich zwischen zwei Zahlen (z.B. "König von Siam (1868-1910)"). Laut <a href="http://de.wikipedia.org/wiki/Wikipedia:Datumskonventionen">Wikipedia:Datumskonventionen</a> muss aber ein <a href="http://de.wikipedia.org/wiki/Bis-Strich">Bis-Strich</a> dort eingefügt werden (z.B. "König von Siam (1868–1910)")';
				$error_description[$error_code][2] = 2;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {		

		if ( $kurz_plain ne ''
			 and $kurz_plain =~ /[0-9]+([ ]+)?\-([ ]+)?[0-9]/
			)	
			 {
				fehlermeldung($error_code, $title, $kurz_plain);
				#print $error_code."\t".$title."\t".'<nowiki>'. $kurz_plain.'</nowiki>'."\n";
		}
	}
}

sub F239_Kurzbeschreibung_mit_Bis_Strich_ohne_Klammer {
	my $error_code = 239;
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Kurzbeschreibung mit Bis-Stich und ohne Klammer';
				$error_description[$error_code][1] = 'Das Datenfeld KURZBESCHREIBUNG enthält einen Bis-Stich zwischen vor oder nach einer Zahlen (z.B. "König von Siam 1868–1910"). Laut <a href="http://de.wikipedia.org/wiki/Hilfe:Personendaten">Hilfe:Personendaten</a> sollen solche Zeitangaben immer in Klammer eingefügt werden, also z.B. "König von Siam (1868–1910)"';
				$error_description[$error_code][2] = 2;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {		

		if ( $kurz_plain ne ''
			 and ($kurz_plain =~ /[0-9]+([ ]+)?\–/
				 or $kurz_plain =~ /\–([ ]+)?[0-9]+/)
			 and index ($kurz_plain ,'(' ) == -1
			)	
			 {
				fehlermeldung($error_code, $title, $kurz_plain);
				#print $error_code."\t".$title."\t".'<nowiki>'. $kurz_plain.'</nowiki>'."\n";
		}
	}
}

sub F240_Datenfeld_fett_oder_schraeg{
	my $error_code = 240;
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
		$error_description[$error_code][0] = 'Datenfeld fett oder kursiv';
		$error_description[$error_code][1] = 'In einem Datenfeld (Kurzbeschreibung, Alternativnamen, Geburtsort oder Sterbeort) befindet sich Text, der durch ein doppeltes einfaches Hochkomma fett oder kursiv geschrieben wurde (z.B. Kapitän der '.chr(39).chr(39).'Titanic'.chr(39).chr(39).'). Diese Wikisyntax sollte in den Personendaten nicht genutzt werden. Wenn das entfernen nicht möglich ist, dann sollte besser die Anführungszeichen genutzen werden (z.B. Kapitän der „Titanic“)' ;
		$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {	
		F240_Datenfeld_fett_oder_schraeg2($error_code, $aname);
		F240_Datenfeld_fett_oder_schraeg2($error_code, $kurz_plain);
		F240_Datenfeld_fett_oder_schraeg2($error_code, $geburtsort_plain);
		F240_Datenfeld_fett_oder_schraeg2($error_code, $sterbeort_plain);
	}
		
}

sub F240_Datenfeld_fett_oder_schraeg2{
	my $error_code = $_[0];
	my $test = $_[1];

	if ( $test ne ''
	     and  index($test, chr(39).chr(39) ) > -1
		){
		fehlermeldung($error_code, $title, '<nowiki>'.$test.'</nowiki>');
		#print $error_code."\t".$title."\t".'<nowiki>'. $test.'</nowiki>'."\n";
		
	}
}

sub F241_Kurzbeschreibung_mit_Zeit_ohne_Klammer {
	my $error_code = 241;
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Kurzbeschreibung mit Zeitangabe und ohne Klammer';
				$error_description[$error_code][1] = 'Das Datenfeld KURZBESCHREIBUNG enthält einen Zeitangabe. Dazu wurden nach verschiedenen Wörtern vor einer Zahl gesucht ( von, zwischen, ab, seit, zwischen dem, bis, bis zum, bis um, um ) wie z.B. in "Graf von Barcelona ab 947". Bitte solche Zeitangabe immer in eine Klammer einbauen, also z.B. "Graf von Barcelona (ab 947)".';
				$error_description[$error_code][2] = 3;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {		

		if ( $kurz_plain ne ''
			 and $kurz_plain =~ /(von|zwischen|ab|seit|zwischen dem|bis|bis zum|bis um|um) [0-9]/
			 and index ($kurz_plain ,'(' ) == -1
			)	
			 {
				fehlermeldung($error_code, $title, $kurz_plain);
				#print $error_code."\t".$title."\t".'<nowiki>'. $kurz_plain.'</nowiki>'."\n";
		}
	}
}

sub F242_Kategoriensortierung_fehlt {
	my $error_code = 242;
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Kategoriensortierung fehlt';
				$error_description[$error_code][1] = 'Der Artikel enthält im Datenfeld Name ein Komma, aber im Titel gibt es kein Komma. Außerdem existiert eine Kategorie, die keine Sortierungsangabe hat. Das heißt dieser Artikel wird in der Kategorie nicht korrekt sortiert. Bitte ändere das, indem du z.B. die Vorlage SORTIERUNG korrekt einfügst oder das Datenfeld NAME korrigierst. (Siehe auch: <a href="de.wikipedia.org/wiki/Hilfe:Kategorien">Hilfe:Kategorien</a>)';
				$error_description[$error_code][2] = 2;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		if ( $defaultsort eq ''
			 and $all_categories ne ''
			 and index ($name ,',' ) > -1
			 and index ($title ,',' ) == -1
			 and $all_categories =~ /\[\[[^|]+(\[|$)/
			)	
			 {
				fehlermeldung($error_code, $title, $name);
				#print $error_code."\t".$title."\t".'<nowiki>'. $kurz_plain.'</nowiki>'."\n";
		}
	}
}

sub F243_Geburtsdatum_mit_falschem_Jahrhundert {
	my $error_code = 243;
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Geburtsdatum mit falschem Jahrhundert';
				$error_description[$error_code][1] = 'Der Artikel hat im Datenfeld GEBURTSDATUM ein Jahrhundert (z.B. 19. Jahrhundert). Im Datenfeld STERBEDATUM ist ein konkretes Datum angegeben. In den aufgelisteten Artikel ist der Abstand zwischen den Zeitangaben größer als 1 Jahrhundert, also z.B. "19. Jahrhundert - 2008". Das <a href="http://de.wikipedia.org/wiki/19. Jahrhundert">19. Jahrhundert</a> ging z.B. von 1. Januar 1801 bis zum 31. Dezember 1900. Das bedeutet, das eine Person die 2009 gestorben ist mindestens 108 Jahre alt geworden ist. Meist handelt es sich aber um einen Eingabefehler beim Jahrhundert.';
				$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		if ( $sterbejahr ne ''
			 and $geburtsdatum =~ /^([ ]+)?[0-9]+\. Jahrhundert$/
			)	
			{		
			#print $geburtsdatum. ' - '. $sterbejahr."\n";
		
			my $jhg = $geburtsdatum;
			$jhg =~ s/^[ ]//g;
			$jhg =~ s/[ ]$//g;
			$jhg =~ s/\. .+$//g;
			#print $jhg."\n";
			
			my $jhs = $sterbejahr;
			$jhs = (($jhs - ($jhs % 100)) / 100)+1;
			#print $jhs."\n";
			
			if (abs($jhs - $jhg) > 1 ) {
				fehlermeldung($error_code, $title, $geburtsdatum.' - '. $sterbejahr);
				#print $error_code."\t".$title."\t".'<nowiki>'. $geburtsdatum. ' - '. $sterbejahr.'</nowiki>'."\n";
			}
		}
	}
}

sub F244_Geburtsdatum_und_Sterbejahr_ohne_v_Chr {
	my $error_code = 244;
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Geburts- oder Sterbedatum mit falschem Jahrhundert';
				$error_description[$error_code][1] = 'Der Artikel hat im Datenfeld GEBURTSDATUM und STERBEDATUM fehlerhafte Angaben (z.B. "12. Jahrhundert v. Chr. vs. 13. Jahrhundert v. Chr."). Entweder es fehlt "v. Chr." oder die Jahrhunderte sind vertauscht.';
				$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		if ( $geburtsdatum =~ /^([ ]+)?[0-9]+\. Jahrhundert( v\. Chr\.)?[ ]?+$/
			 and $sterbedatum =~ /^([ ]+)?[0-9]+\. Jahrhundert( v\. Chr\.)?[ ]?+$/
			)	
			{		
			#print $geburtsdatum. ' - '. $sterbejahr."\n";
		
			my $jhg = $geburtsdatum;
			$jhg =~ s/^[ ]//g;
			$jhg =~ s/[ ]$//g;
			$jhg =~ s/\. .+$//g;
			#print $jhg."\n";
			
			my $jhs = $sterbedatum;
			$jhs =~ s/^[ ]//g;
			$jhs =~ s/[ ]$//g;
			$jhs =~ s/\. .+$//g;
			#print $jhs."\n";
			#print (-$jhg + $jhs)."\n\n";
			
			if ( ((-$jhg + $jhs) > 0  and index ($geburtsdatum, ' v. Chr.') > -1 )
				or ((-$jhg + $jhs) < 0  and index ($geburtsdatum, ' v. Chr.') == -1 )
				){
				fehlermeldung($error_code, $title, $geburtsdatum.' - '. $sterbedatum);
				#print $error_code."\t".$title."\t".'<nowiki>'. $geburtsdatum.' - '. $sterbedatum.'</nowiki>'."\n";
			}
		}
	}
}


sub F245_Datum_mit_Komma_ohne_oder{
	my $error_code = 245;
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
		$error_description[$error_code][0] = 'Geburts- oder Sterbedatum mit Komma';
		$error_description[$error_code][1] = 'Es wurde ein Datenfeld gefunden, dass ein Komma enthält, aber kein Wort "oder" (z.B. "1. März 1966, 1. März 1968"). Bei mehreren möglichen Zeitangaben bitte immer mit "oder" arbeiten (z.B. "1. März 1966 oder 1. März 1968") .' ;
		$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		F245_Datum_mit_Komma_ohne_oder2($error_code, $geburtsdatum);
		F245_Datum_mit_Komma_ohne_oder2($error_code, $sterbedatum);
	}
		
}

sub F245_Datum_mit_Komma_ohne_oder2{

	my $error_code = $_[0];
	my $test = $_[1];

	if ( $test ne ''
	     and index($test, ',' ) > -1
		 and index($test, ' oder ' ) == -1
		){
		fehlermeldung($error_code, $title, '<nowiki>'.$test.'</nowiki>');
		#print $error_code."\t".$title."\t".'<nowiki>'. $test.'</nowiki>'."\n";
		
	}
}


sub F246_Kurzbeschreibung_mit_fehlerhaften_Chr {
	my $error_code = 246;
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Kurzbeschreibung mit fehlerhaften v. Chr. oder n. Chr.';
				$error_description[$error_code][1] = 'Das Datenfeld KURZBESCHREIBUNG enthält Schreibweise von "v. Chr." oder "n. Chr.", die nicht dem Standard von <a href="http://de.wikipedia.org/wiki/Wikipedia:Datumskonventionen">Wikipedia:Datumskonventionen</a> entspricht.")';
				$error_description[$error_code][2] = 3;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {	

		if ( $kurz_plain ne ''
			 and (  ($kurz_plain =~ /[0-9] n\./ and index ($kurz_plain, ' n. Chr.') == -1)
				  or($kurz_plain =~ /[0-9] v\./ and index ($kurz_plain, ' v. Chr.') == -1)
			)) {
				fehlermeldung($error_code, $title, $kurz_plain);
				#print $error_code."\t".$title."\t".'<nowiki>'. $kurz_plain.'</nowiki>'."\n";
		}
	}
}




sub F247_Ort_mit_Link_vor_Wort{
	my $error_code = 247;
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
		$error_description[$error_code][0] = 'Geburts- oder Sterbeort mit Link vor Wort';
		$error_description[$error_code][1] = 'Es wurde ein Datenfeld gefunden, dass einen Link direkt vor einem Wort hat, ohne einem Leerzeichen dazwischen (z.B. "[[Köln]]Porz"). Das deutet meist auf einen Tippfehler hin.' ;
		$error_description[$error_code][2] = 3;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {	
		F247_Ort_mit_Link_vor_Wort2($error_code, $geburtsort);
		F247_Ort_mit_Link_vor_Wort2($error_code, $sterbeort);
	}
		
}

sub F247_Ort_mit_Link_vor_Wort2{
	my $error_code = $_[0];
	my $test = $_[1];

	if ( $test ne ''
	     and $test =~ /([ ]+)?\[\[[[:alnum:]]+\]\][[:upper:]]+([ ]+)?/
		){
		fehlermeldung($error_code, $title, '<nowiki>'.$test.'</nowiki>');
		#print $error_code."\t".$title."\t".'<nowiki>'. $test.'</nowiki>'."\n";
		
	}
}


sub F248_Alternativname_falsch_sortiert {
	my $error_code = 248;
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Alternativnamen falsch sortiert';
				$error_description[$error_code][1] = 'Das erste Wort vor dem Komma aus dem Datenfeld NAME wurde im Datenfeld ALTERNATIVNAMEN vor einem Semikolon, einer Klammer oder dem Ende gefunden. Beispiel: Artikel "Arthur Meier" enthält "NAME=Meier, Arthur" und im "ALTERNATIVNAMEN=Hans Arthur Meier; H. Arthur Meier". Das Feld ALTERNATIVNAMEN sollte genauso sortiert werden wie im Datenfeld NAME, damit bei einer Auflistung aller Name diese korrekt sortiert werden können. Bei Sonderfällen kann in Klammern hinter dem entsprechenden Alternativnamen darauf hingewiesen werden z.B. : (Spitzname), (wirklicher Name), (bürgerlicher Name) etc.';
				$error_description[$error_code][2] = 3;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {	

		if ( $aname ne ''
		     and index ($name, ',') > -1 ) {
			#NAME=Meier, Arthur
			#ALTERNATIVNAMEN=Hans Arthur Meier; H. Arthur Meier

	 		 # Familienname von NAME --> Meier
			 my $test_name_1 = substr($name,0, index ($name, ','));
			 $test_name_1 =~ s/^[ ]+//;
			 #print "\t".$test_name_1."\n";

			 # Suche nach 
			 # Meier ;
   			 # Meier $
			 # Meier (	( Klammer herausgenommen, da z.B. bei "Anthony Hope" --> Hawkins, Anthony Hope (wirklicher Name)  )
			 if ( # old 	$aname =~ / $test_name_1([ ]+)?(;|$|\()/
			      $aname =~ / $test_name_1([ ]+)?(;|$)/
				){
				fehlermeldung($error_code, $title, $aname);
				#print $error_code."\t".$title."\t".'<nowiki>'. $aname.'</nowiki>'."\n";
			}
		}
	}
}

sub F249_Zeitlos_mit_Jahrhundert {
	my $error_code = 249;
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
						$error_description[$error_code][0] = 'Zeitlose Person mit Jahrhundert';
						$error_description[$error_code][1] = 'Bei diesen Personen konnte die Lebenszeit nicht über die Kategorien oder die Personendaten ermittelt werden. Hier sollten die Kategorien (Geboren/Gestorben) und die Personendaten ergänzt werden. Im Text wurde eine Angabe wie z.B. "4. Jahrhundert" gefunden.';
						$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {	
		if ( #$parameter{'modus_scan'}  eq "live" and 
			 $all_categories ne '' ) {
			 my $test = $all_categories;
			if (   $test =~ /(G|g)eboren [1-9]/ 
				or $test =~ /(G|g)eboren im/
				or $test =~ /(G|g)estorben [1-9]/
				or $test =~ /(G|g)estorben im/
				or $test =~ /Jahrhundert/
				){
				#ok
			} else {
				my $test2 = $geburtsdatum;
				$test2 =~ s/\t//g;
				$test2 =~ s/ //g;
				my $test3 = $sterbedatum;
				$test2 =~ s/\t//g;
				$test2 =~ s/ //g;
				if ($test2 eq '' 
					and $test3 eq '') {
					
					if ($text =~ '[0-9]\. Jahrhundert') {
						fehlermeldung($error_code, $title, $kurz_plain);
						#print $error_code."\t".$title."\t".'<nowiki>'.$kurz_plain.'</nowiki>'."\n";
					}
				}
			}
		}
	}
}

sub F250_Zeitlos_mit_Jahr {
	my $error_code = 250;
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
						$error_description[$error_code][0] = 'Zeitlose Person mit Jahr';
						$error_description[$error_code][1] = 'Bei diesen Personen konnte die Lebenszeit nicht über die Kategorien oder die Personendaten ermittelt werden. Hier sollten die Kategorien (Geboren/Gestorben) und die Personendaten ergänzt werden. Im Text wurde eine Angabe wie z.B. " 1912" gefunden.';
						$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		if ( #$parameter{'modus_scan'}  eq "live" and 
			 $all_categories ne '' ) {
			 my $test = $all_categories;
			if (   $test =~ /(G|g)eboren [1-9]/ 
				or $test =~ /(G|g)eboren im/
				or $test =~ /(G|g)estorben [1-9]/
				or $test =~ /(G|g)estorben im/
				or $test =~ /Jahrhundert/
				){
				#ok
			} else {
				my $test2 = $geburtsdatum;
				$test2 =~ s/\t//g;
				$test2 =~ s/ //g;
				my $test3 = $sterbedatum;
				$test2 =~ s/\t//g;
				$test2 =~ s/ //g;
				if ($test2 eq '' 
					and $test3 eq '') {
					
					if ($text =~ ' (\[\[)?[1-2][0-9][0-9][0-9](\]\])?') {
						fehlermeldung($error_code, $title, $kurz_plain);
						#print $error_code."\t".$title."\t".'<nowiki>'.$kurz_plain.'</nowiki>'."\n";
					}
				}
			}
		}
	}
}

sub F251_Kurzbeschreibung_ohne_Nationalitaet_aber_im_Text {
	my $error_code = 251;
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
						$error_description[$error_code][0] = 'Kurzbeschreibung ohne Nationalität, aber im Text ';
						$error_description[$error_code][1] = 'Das Datenfeld Kurzbeschreibung enthält keine Nationalität am Anfang (z.B. "Erfinder des World Wide Web"). Es werden hier alle Personen ausgegeben, deren Kurzbeschreibung mit einem Großbuchstaben anfängt (außer "US-amerikanische ..." und "Schweizer") und deren Geburtsjahr nach 1900 bzw. das Sterbejahr nach 1975 liegt. Diesen Personen sollten alle einer oder mehrere Nationalitäten zugeordnet werden können. (Siehe auch <a href="http://de.wikipedia.org/wiki/Wikipedia:Namenskonventionen/Staaten">Namenskonvention/Staaten</a>)';
						$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		if ( $kurz_plain ne '' ) {

			if ( 
				 $kurz_plain =~ /^([ ]+)?[[:upper:]]/ 
				 and index ($kurz_plain,  'US-a' ) == -1
				 and index ($kurz_plain,  'Schweizer' ) == -1
				 and index ($kurz_plain,  'Liechtensteiner' ) == -1
			){
			
				my $ok = 'false';
				if ($geburtsjahr ne ''){
					$ok = 'true' if ( $geburtsjahr > 1900);
				}
				if ($sterbejahr ne ''){
					$ok = 'true' if ( $sterbejahr > 1975);
				}	
			
				if ($ok eq 'true') 
				 {
					# no nationalität
					
					# get first word of kurz
					my $pos = index ($kurz_plain, ' ');
					my $first_word = $kurz_plain;
					if ($pos > -1) {
						$first_word = substr($kurz_plain, 0, $pos);
					}
					$first_word =~ s/^[ ]//g;
					$first_word =~ s/[ ]$//g;
					
					$pos = -1;
					$pos = index( $text, 'ischer '.$first_word) if ($pos == -1);
					$pos = index( $text, 'ischer]] '.$first_word) if ($pos == -1);
					$pos = index( $text, 'ischer]] [['.$first_word) if ($pos == -1);
					$pos = index( $text, 'ische '.$first_word) if ($pos == -1);
					$pos = index( $text, 'ische]] '.$first_word) if ($pos == -1);
					$pos = index( $text, 'ische]] [['.$first_word) if ($pos == -1);
					
					if ($pos > -1 ) {
						my $word_after = substr($text, $pos);
						my $word_before = substr($text, 0, $pos);
						
						#word before
						$word_before =~ s/\n/ /g;
						my $pos = rindex($word_before, ' ');
						#print length($word_before)."\n";
						#print $pos."\n";
						$word_before = substr( $word_before, $pos, length($word_before)-$pos);
						#print $word_before."\n";
						
						#isch
						$word_after =~ s/\n/ /g;
						$pos = index($word_after, ' ');
						$word_after = substr($word_after, 0, $pos);
						
						
						my $notice = $word_before.$word_after.' '.$first_word."\n";
						$notice =~ s/^ //g;
						$notice =~ s/\[//g;
						$notice =~ s/\]//g;
						$pos = index($notice ,'|');
						$notice = substr ($notice, $pos+1) if ($pos > -1);
						
				
						fehlermeldung($error_code, $title, $notice);
						#print $error_code."\t".$title."\t".'<nowiki>'. $notice.'</nowiki>'."\n";
					}
				}
			}
		}
	}
}



sub F252_Name_mit_fehlendem_Buchstaben {
	my $error_code = 252;
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
			$error_description[$error_code][0] = 'Name mit fehlenden Buchstaben';
			$error_description[$error_code][1] = 'Alle Buchstaben des Artikeltitel müssen im Datenfeld Name auftauchen. Dazu wurde überprüft, ob die Buchstaben im Titel (nur vor der möglichen Klammer) auch im Datenfeld Name enthalten sind. Es wird angezeigt, welcher Buchstabe vermisst wird.';
			$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		my $kurz_title = $title;
		
		$kurz_title = $kurz_title.' (';
		$kurz_title = substr ($kurz_title, 0, index($kurz_title,'('));		# Titel bis zur Klammer kürzen
		$kurz_title = $kurz_title.' ,';
		$kurz_title = substr ($kurz_title, 0, index($kurz_title,','));		# Titel bis zur Komma
		
		#Senior Junior
		$kurz_title =~ s/[ ]senior$//i;
		$kurz_title =~ s/[ ]junior$//i;
		$kurz_title =~ s/[ ]jr\.$//i;

		my $lower_name = $name;
		$lower_name	= lc($lower_name);
		$kurz_title = lc($kurz_title);
		$kurz_title =~ s/ //g;
		$kurz_title =~ s/\.//g;
		
		# Überprüfen ob jeder Buchstabe aus dem Titel im Feld Name vorkommt.
		my $found = '';
		#print 'title='.$kurz_title."\n";
		#print 'name= '.$lower_name."\n";
		for(my $i = 0; $i <= length($kurz_title)-1; $i ++) {
			my $search_character = substr($kurz_title, $i,1);
			#print $search_character."\n";
			if (index ($lower_name, $search_character) == -1) {
				if ($found eq ''
					and $search_character ne '.'	# bei Namensabkürzungen
					){
					$found = $search_character; 
				}
			}
		}

		if ($found ne '') {
			my $notice = '"'.$found.'" - '.$name;
			fehlermeldung($error_code, $title, $notice);
			#print "\t".$error_code."\t".$title."\t".'<nowiki>'. $notice.'</nowiki>'."\n";
		}
	}

}

sub F253_Kurzbeschreibung_ohne_Nationalitaet_aber_mit_Kategorie{
	my $error_code = 253;
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
					$error_description[$error_code][0] = 'Kurzbeschreibung ohne Nationalität, aber mit Kategorie';
					$error_description[$error_code][1] = 'Im Feld Kurzbeschreibung sollte die Nationaltität angegeben werden. In den folgenden Artikel wurde ein Großbuchstabe am Artikelanfang gefunden, aber es ist keine "US-amerikanische" oder "Schweizer" Person. In den Kategorien wurde eine Kategorie mit Nationalität gefunden (z.B. Kategorie:Franzose). Alle hier aufgelisteten Personen sind nach 1900 geboren oder nach 1975 gestorben, dass heißt sie sollten einer Nationalität zuzuordnen sein.';
					$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		if ($all_categories ne ''){
			# ist es ein Mensch der Neuzeit?
			if ( 
				 $kurz_plain =~ /^([ ]+)?[[:upper:]]/ 
				 and index ($kurz_plain,  'US-a' ) == -1
				 and index ($kurz_plain,  'Schweizer' ) == -1
				 and index ($kurz_plain,  'Liechtensteiner' ) == -1
			){
				my $ok = 'false';
				if ($geburtsjahr ne ''){
					$ok = 'true' if ( $geburtsjahr > 1900);
				}
				if ($sterbejahr ne ''){
					$ok = 'true' if ( $sterbejahr > 1975);
				}

				my $found = '';
				if ($ok eq 'true') {
					
					$found = 'Afghane' 			if (index($all_categories, ':Afghane') > -1);
					$found = 'Ägypter' 			if (index($all_categories, ':Ägypter') > -1);
					$found = 'Äquatorialguineer'if (index($all_categories, ':Äquatorialguineer') > -1);
					$found = 'Äthiopier' 		if (index($all_categories, ':Äthiopier') > -1);
					$found = 'Albaner' 			if (index($all_categories, ':Albaner') > -1);
					$found = 'Argentinier' 		if (index($all_categories, ':Argentinier') > -1);
					$found = 'Australier' 		if (index($all_categories, ':Australier') > -1);
					$found = 'Brasilianer'		if (index($all_categories, ':Brasilianer') > -1);
					$found = 'Belgier'			if (index($all_categories, ':Belgier') > -1);
					$found = 'Brite' 			if (index($all_categories, ':Brite') > -1);
					$found = 'Brite' 			if (index($all_categories, ':Waliser') > -1);
					$found = 'Brite' 			if (index($all_categories, ':Schotte') > -1);
					$found = 'Bulgare'			if (index($all_categories, ':Bulgare') > -1);
					$found = 'Chinese' 			if (index($all_categories, ':Chinese') > -1);
					$found = 'Däne' 			if (index($all_categories, ':Däne') > -1);
					$found = 'Deutscher' 		if (index($all_categories, ':Deutscher') > -1);
					$found = 'Este' 			if (index($all_categories, ':Este') > -1);
					$found = 'Finne' 			if (index($all_categories, ':Finne') > -1);
					$found = 'Franzose' 		if (index($all_categories, ':Franzose') > -1);
					$found = 'Grieche' 			if (index($all_categories, ':Grieche') > -1);
					$found = 'Inder' 			if (index($all_categories, ':Inder') > -1);
					$found = 'Ire' 				if (index($all_categories, ':Ire') > -1);
					$found = 'Isländer' 		if (index($all_categories, ':Isländer') > -1);
					$found = 'Italiener' 		if (index($all_categories, ':Italiener') > -1);
					$found = 'Japaner' 			if (index($all_categories, ':Japaner') > -1);
					$found = 'Kasache' 			if (index($all_categories, ':Japaner') > -1);
					$found = 'Lette' 			if (index($all_categories, ':Lette') > -1);
					$found = 'Litauer' 			if (index($all_categories, ':Litauer') > -1);
					$found = 'Mongole'			if (index($all_categories, ':Mongole') > -1);
					$found = 'Niederländer'		if (index($all_categories, ':Niederländer') > -1);
					$found = 'Norweger' 		if (index($all_categories, ':Norweger') > -1);
					$found = 'Österreicher'		if (index($all_categories, ':Österreicher') > -1);
					$found = 'Pole' 			if (index($all_categories, ':Pole') > -1);
					$found = 'Portugiese' 		if (index($all_categories, ':Portugiese') > -1);
					$found = 'Rumäne' 			if (index($all_categories, ':Rumäne') > -1);
					$found = 'Russe' 			if (index($all_categories, ':Russe') > -1);
					$found = 'Schwede' 			if (index($all_categories, ':Schwede') > -1);
					$found = 'Schweizer' 		if (index($all_categories, ':Schweizer') > -1);
					$found = 'Spanier' 			if (index($all_categories, ':Spanier') > -1);
					$found = 'Usbeke'			if (index($all_categories, ':Usbeke') > -1);
					$found = 'Ungar'			if (index($all_categories, ':Ungar') > -1);
					$found = 'US-Amerikaner'	if (index($all_categories, ':US-Amerikaner') > -1);
					$found = 'Weißrusse' 		if (index($all_categories, ':Weißrusse') > -1);
					
					$found = 'Afghane' 			if (index($all_categories, ':Afghane') > -1);
					$found = 'Ägypter' 			if (index($all_categories, ':Ägypter') > -1);
					$found = 'Äquatorialguineer'if (index($all_categories, ':Äquatorialguineer') > -1);
					$found = 'Äthiopier' 		if (index($all_categories, ':Äthiopier') > -1);
					$found = 'Albaner' 			if (index($all_categories, ':Albaner') > -1);
					$found = 'Algerier'     	if (index($all_categories, ':Algerier') > -1);
					$found = 'Andorraner'		if (index($all_categories, ':Andorraner') > -1);
					$found = 'Angolaner'		if (index($all_categories, ':Angolaner') > -1);
					$found = 'Antiguaner'		if (index($all_categories, ':Antiguaner') > -1);
					$found = 'Armenier'			if (index($all_categories, ':Armenier') > -1);
					$found = 'Aserbaidschaner'	if (index($all_categories, ':Aserbaidschaner') > -1);
					$found = 'Australier' 		if (index($all_categories, ':Australier') > -1);
					$found = 'Bahamaer' 		if (index($all_categories, ':Bahamaer') > -1);
					$found = 'Bahrainer' 		if (index($all_categories, ':Bahrainer') > -1);
					$found = 'Bangladescher' 	if (index($all_categories, ':Bangladescher') > -1);
					$found = 'Barbadier' 		if (index($all_categories, ':Barbadier') > -1);
					$found = 'Belgier' 			if (index($all_categories, ':Belgier') > -1);
					$found = 'Belizer' 			if (index($all_categories, ':Belizer') > -1);
					$found = 'Beniner' 			if (index($all_categories, ':Beniner') > -1);
					$found = 'Bhutaner' 		if (index($all_categories, ':Bhutaner') > -1);
					$found = 'Bolivianer' 		if (index($all_categories, ':Bolivianer') > -1);
					$found = 'Bosnier' 			if (index($all_categories, ':Bosnier') > -1);
					$found = 'Botsuaner' 		if (index($all_categories, ':Botsuaner') > -1);
					$found = 'Brasilianer' 		if (index($all_categories, ':Brasilianer') > -1);
					$found = 'Brite' 			if (index($all_categories, ':Brite') > -1);
					$found = 'Bulgare' 			if (index($all_categories, ':Bulgare') > -1);
					$found = 'Burkiner' 		if (index($all_categories, ':Burkiner') > -1);
					$found = 'Burundier' 		if (index($all_categories, ':Burundier') > -1);
					$found = 'Chilene' 			if (index($all_categories, ':Chilene') > -1);
					$found = 'Chinese' 			if (index($all_categories, ':Chinese') > -1);
					$found = 'Costa-Ricaner' 	if (index($all_categories, ':Costa-Ricaner') > -1);
					$found = 'Däne' 			if (index($all_categories, ':Däne') > -1);
					$found = 'Deutscher' 		if (index($all_categories, ':Deutscher') > -1);
					$found = 'Dominicaner' 		if (index($all_categories, ':Dominicaner') > -1);
					$found = 'Dschibutier' 		if (index($all_categories, ':Dschibutier') > -1);
					$found = 'Ecuadorianer' 	if (index($all_categories, ':Ecuadorianer') > -1);
					$found = 'Eritreer' 		if (index($all_categories, ':Eritreer') > -1);
					$found = 'Este' 			if (index($all_categories, ':Este') > -1);
					$found = 'Färinger' 		if (index($all_categories, ':Färinger') > -1);
					$found = 'Fidschianer' 		if (index($all_categories, ':Fidschianer') > -1);
					$found = 'Finne' 			if (index($all_categories, ':Finne') > -1);
					$found = 'Franzose' 		if (index($all_categories, ':Franzose') > -1);
					$found = 'Gabuner' 			if (index($all_categories, ':Gabuner') > -1);
					$found = 'Gambier' 			if (index($all_categories, ':Gambier') > -1);
					$found = 'Georgier' 		if (index($all_categories, ':Georgier') > -1);
					$found = 'Ghanaer' 			if (index($all_categories, ':Ghanaer') > -1);
					$found = 'Grönländer' 		if (index($all_categories, ':Grönländer') > -1);
					$found = 'Grenader' 		if (index($all_categories, ':Grenader') > -1);
					$found = 'Grieche' 			if (index($all_categories, ':Grieche') > -1);
					$found = 'Guatemalteke' 	if (index($all_categories, ':Guatemalteke') > -1);
					$found = 'Guinea-Bissauer' 	if (index($all_categories, ':Guinea-Bissauer') > -1);
					$found = 'Guineer' 			if (index($all_categories, ':Guineer') > -1);
					$found = 'Guyaner' 			if (index($all_categories, ':Guyaner') > -1);
					$found = 'Haitianer' 		if (index($all_categories, ':Haitianer') > -1);
					$found = 'Honduraner' 		if (index($all_categories, ':Honduraner') > -1);
					$found = 'Inder' 			if (index($all_categories, ':Inder') > -1);
					$found = 'Indonesier' 		if (index($all_categories, ':Indonesier') > -1);
					$found = 'Iraker' 			if (index($all_categories, ':Iraker') > -1);
					$found = 'Iraner' 			if (index($all_categories, ':Iraner') > -1);
					$found = 'Ire' 				if (index($all_categories, ':Ire') > -1);
					$found = 'Isländer' 		if (index($all_categories, ':Isländer') > -1);
					$found = 'Israeli' 			if (index($all_categories, ':Israeli') > -1);
					$found = 'Italiener' 		if (index($all_categories, ':Italiener') > -1);
					$found = 'Ivorer' 			if (index($all_categories, ':Ivorer') > -1);
					$found = 'Jamaikaner' 		if (index($all_categories, ':Jamaikaner') > -1);
					$found = 'Japaner' 			if (index($all_categories, ':Japaner') > -1);
					$found = 'Jemenit' 			if (index($all_categories, ':Jemenit') > -1);
					$found = 'Jordanier' 		if (index($all_categories, ':Jordanier') > -1);
					#$found = 'Jugoslawe' 		if (index($all_categories, ':Jugoslawe') > -1);
					$found = 'Kambodschaner' 	if (index($all_categories, ':Kambodschaner') > -1);
					$found = 'Kameruner' 		if (index($all_categories, ':Kameruner') > -1);
					$found = 'Kanadier' 		if (index($all_categories, ':Kanadier') > -1);
					$found = 'Kap-Verdier' 		if (index($all_categories, ':Kap-Verdier') > -1);
					$found = 'Kasache' 			if (index($all_categories, ':Kasache') > -1);
					$found = 'Katarer' 			if (index($all_categories, ':Katarer') > -1);
					$found = 'Kenianer' 		if (index($all_categories, ':Kenianer') > -1);
					$found = 'Kirgise' 			if (index($all_categories, ':Kirgise') > -1);
					$found = 'Kiribatier' 		if (index($all_categories, ':Kiribatier') > -1);
					$found = 'Kolumbianer' 		if (index($all_categories, ':Kolumbianer') > -1);
					$found = 'Komorer' 			if (index($all_categories, ':Komorer') > -1);
					$found = 'Kongolese' 		if (index($all_categories, ':Kongolese') > -1);
					$found = 'Kongolese' 		if (index($all_categories, ':Kongolese') > -1);
					$found = 'Kongolese' 		if (index($all_categories, ':Kongolese') > -1);
					$found = 'Koreaner' 		if (index($all_categories, ':Koreaner') > -1);
					$found = 'Kosovare' 		if (index($all_categories, ':Kosovare') > -1);
					$found = 'Kroate' 			if (index($all_categories, ':Kroate') > -1);
					$found = 'Kubaner' 			if (index($all_categories, ':Kubaner') > -1);
					$found = 'Kuwaiter' 		if (index($all_categories, ':Kuwaiter') > -1);
					$found = 'Laote' 			if (index($all_categories, ':Laote') > -1);
					$found = 'Lesother' 		if (index($all_categories, ':Lesother') > -1);
					$found = 'Lette' 			if (index($all_categories, ':Lette') > -1);
					$found = 'Libanese' 		if (index($all_categories, ':Libanese') > -1);
					$found = 'Liberianer' 		if (index($all_categories, ':Liberianer') > -1);
					$found = 'Libyer' 			if (index($all_categories, ':Libyer') > -1);
					$found = 'Liechtensteiner' 	if (index($all_categories, ':Liechtensteiner') > -1);
					$found = 'Litauer' 			if (index($all_categories, ':Litauer') > -1);
					$found = 'Lucianer' 		if (index($all_categories, ':Lucianer') > -1);
					$found = 'Luxemburger' 		if (index($all_categories, ':Luxemburger') > -1);
					$found = 'Madagasse' 		if (index($all_categories, ':Madagasse') > -1);
					$found = 'Malawier' 		if (index($all_categories, ':Malawier') > -1);
					$found = 'Malaysier' 		if (index($all_categories, ':Malaysier') > -1);
					$found = 'Malediver' 		if (index($all_categories, ':Malediver') > -1);
					$found = 'Malier' 			if (index($all_categories, ':Malier') > -1);
					$found = 'Malteser' 		if (index($all_categories, ':Malteser') > -1);
					$found = 'Marokkaner' 		if (index($all_categories, ':Marokkaner') > -1);
					$found = 'Marshaller' 		if (index($all_categories, ':Marshaller') > -1);
					$found = 'Mauretanier' 		if (index($all_categories, ':Mauretanier') > -1);
					$found = 'Mauritier' 		if (index($all_categories, ':Mauritier') > -1);
					$found = 'Mazedonier' 		if (index($all_categories, ':Mazedonier') > -1);
					$found = 'Mexikaner' 		if (index($all_categories, ':Mexikaner') > -1);
					$found = 'Mikronesier' 		if (index($all_categories, ':Mikronesier') > -1);
					$found = 'Moldawier' 		if (index($all_categories, ':Moldawier') > -1);
					$found = 'Monegasse' 		if (index($all_categories, ':Monegasse') > -1);
					$found = 'Mongole' 			if (index($all_categories, ':Mongole') > -1);
					$found = 'Montenegriner' 	if (index($all_categories, ':Montenegriner') > -1);
					$found = 'Mosambikaner' 	if (index($all_categories, ':Mosambikaner') > -1);
					$found = 'Myanmare' 		if (index($all_categories, ':Myanmare') > -1);
					$found = 'Namibier' 		if (index($all_categories, ':Namibier') > -1);
					$found = 'Nauruer' 			if (index($all_categories, ':Nauruer') > -1);
					$found = 'Nepalese' 		if (index($all_categories, ':Nepalese') > -1);
					$found = 'Neuseeländer' 	if (index($all_categories, ':Neuseeländer') > -1);
					$found = 'Nicaraguaner' 	if (index($all_categories, ':Nicaraguaner') > -1);
					$found = 'Niederländer' 	if (index($all_categories, ':Niederländer') > -1);
					$found = 'Nigerianer' 		if (index($all_categories, ':Nigerianer') > -1);
					$found = 'Nigrer' 			if (index($all_categories, ':Nigrer') > -1);
					$found = 'Niueaner' 		if (index($all_categories, ':Niueaner') > -1);
					$found = 'Norweger' 		if (index($all_categories, ':Norweger') > -1);
					$found = 'Omaner' 			if (index($all_categories, ':Omaner') > -1);
					$found = 'Pakistaner' 		if (index($all_categories, ':Pakistaner') > -1);
					$found = 'Palauer' 			if (index($all_categories, ':Palauer') > -1);
					$found = 'Panamaer' 		if (index($all_categories, ':Panamaer') > -1);
					$found = 'Papua-Neuguineer' if (index($all_categories, ':Papua-Neuguineer') > -1);
					$found = 'Paraguayer' 		if (index($all_categories, ':Paraguayer') > -1);
					$found = 'Perser' 			if (index($all_categories, ':Perser') > -1);
					$found = 'Peruaner' 		if (index($all_categories, ':Peruaner') > -1);
					$found = 'Philippiner' 		if (index($all_categories, ':Philippiner') > -1);
					$found = 'Pole' 			if (index($all_categories, ':Pole') > -1);
					$found = 'Portugiese' 		if (index($all_categories, ':Portugiese') > -1);
					$found = 'Puertoricaner' 	if (index($all_categories, ':Puertoricaner') > -1);
					$found = 'Ruander' 			if (index($all_categories, ':Ruander') > -1);
					$found = 'Rumäne' 			if (index($all_categories, ':Rumäne') > -1);
					$found = 'Russe' 			if (index($all_categories, ':Russe') > -1);
					$found = 'São-Toméer' 		if (index($all_categories, ':São-Toméer') > -1);
					$found = 'Südafrikaner' 	if (index($all_categories, ':Südafrikaner') > -1);
					$found = 'Sahraui' 			if (index($all_categories, ':Sahraui') > -1);
					$found = 'Salomoner' 		if (index($all_categories, ':Salomoner') > -1);
					$found = 'Salvadorianer' 	if (index($all_categories, ':Salvadorianer') > -1);
					$found = 'Sambier' 			if (index($all_categories, ':Sambier') > -1);
					$found = 'Samoaner' 		if (index($all_categories, ':Samoaner') > -1);
					$found = 'San-Marinese' 	if (index($all_categories, ':San-Marinese') > -1);
					$found = 'Saudi-Araber' 	if (index($all_categories, ':Saudi-Araber') > -1);
					$found = 'Schwede' 			if (index($all_categories, ':Schwede') > -1);
					$found = 'Schweizer' 		if (index($all_categories, ':Schweizer') > -1);
					$found = 'Senegalese' 		if (index($all_categories, ':Senegalese') > -1);
					$found = 'Serbe' 			if (index($all_categories, ':Serbe') > -1);
					$found = 'Seycheller' 		if (index($all_categories, ':Seycheller') > -1);
					$found = 'Sierra-Leoner' 	if (index($all_categories, ':Sierra-Leoner') > -1);
					$found = 'Simbabwer' 		if (index($all_categories, ':Simbabwer') > -1);
					$found = 'Singapurer' 		if (index($all_categories, ':Singapurer') > -1);
					$found = 'Slowake' 			if (index($all_categories, ':Slowake') > -1);
					$found = 'Slowene' 			if (index($all_categories, ':Slowene') > -1);
					$found = 'Somalier' 		if (index($all_categories, ':Somalier') > -1);
					$found = 'Spanier' 			if (index($all_categories, ':Spanier') > -1);
					$found = 'Sri-Lanker' 		if (index($all_categories, ':Sri-Lanker') > -1);
					$found = 'Sudanese' 		if (index($all_categories, ':Sudanese') > -1);
					$found = 'Surinamer' 		if (index($all_categories, ':Surinamer') > -1);
					$found = 'Swasi' 			if (index($all_categories, ':Swasi') > -1);
					$found = 'Syrer' 			if (index($all_categories, ':Syrer') > -1);
					$found = 'Türke' 			if (index($all_categories, ':Türke') > -1);
					$found = 'Tadschike' 		if (index($all_categories, ':Tadschike') > -1);
					$found = 'Tansanier' 		if (index($all_categories, ':Tansanier') > -1);
					$found = 'Thailänder' 		if (index($all_categories, ':Thailänder') > -1);
					$found = 'Timorese' 		if (index($all_categories, ':Timorese') > -1);
					$found = 'Togoer' 			if (index($all_categories, ':Togoer') > -1);
					$found = 'Tongaer' 			if (index($all_categories, ':Tongaer') > -1);
					$found = 'Tschader' 		if (index($all_categories, ':Tschader') > -1);
					$found = 'Tscheche' 		if (index($all_categories, ':Tscheche') > -1);
					$found = 'Tschechoslowake' 	if (index($all_categories, ':Tschechoslowake') > -1);
					$found = 'Tunesier' 		if (index($all_categories, ':Tunesier') > -1);
					$found = 'Turkmene' 		if (index($all_categories, ':Turkmene') > -1);
					$found = 'Tuvaluer' 		if (index($all_categories, ':Tuvaluer') > -1);
					$found = 'US-Amerikaner' 	if (index($all_categories, ':US-Amerikaner') > -1);
					$found = 'Ugander' 			if (index($all_categories, ':Ugander') > -1);
					$found = 'Ukrainer' 		if (index($all_categories, ':Ukrainer') > -1);
					$found = 'Ungar' 			if (index($all_categories, ':Ungar') > -1);
					$found = 'Uruguayer' 		if (index($all_categories, ':Uruguayer') > -1);
					$found = 'Usbeke' 			if (index($all_categories, ':Usbeke') > -1);
					$found = 'Vanuatuer' 		if (index($all_categories, ':Vanuatuer') > -1);
					$found = 'Venezolaner' 		if (index($all_categories, ':Venezolaner') > -1);
					$found = 'Vietnamese' 		if (index($all_categories, ':Vietnamese') > -1);
					$found = 'Vincenter' 		if (index($all_categories, ':Vincenter') > -1);
					$found = 'Weißrusse' 		if (index($all_categories, ':Weißrusse') > -1);
					$found = 'Zentralafrikaner' if (index($all_categories, ':Zentralafrikaner') > -1);
					$found = 'Zyprer' 			if (index($all_categories, ':Zyprer') > -1);

				}
				
				if ($found ne '') {
					my $notice = $found.' - '.$kurz_plain;
					fehlermeldung($error_code, $title, $notice);
					#print "\t".$error_code."\t".$title."\t".'<nowiki>'. $notice.'</nowiki>'."\n";			
				
				}
			}
		}
	}
}


sub F254_Kurzbeschreibung_mit_Abkuerzung_am_Anfang {
	my $error_code = 254;
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Kurzbeschreibung mit Abkürzung';
				$error_description[$error_code][1] = 'Das Datenfeld KURZBESCHREIBUNG enthält am Anfang eine Abkürzung (z.B. "franz. Pilot"). Diese Abkürzung sollte besser ausgeschrieben werden.")';
				$error_description[$error_code][2] = 0;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		if ( $kurz_plain ne ''
			 and $kurz_plain =~ /^[^ ]+\./ 
			 and not ($kurz_plain =~ /[0-9]\./)
			) {
				fehlermeldung($error_code, $title, $kurz_plain);
				#print $error_code."\t".$title."\t".'<nowiki>'. $kurz_plain.'</nowiki>'."\n";
		}
	}
}

sub F255_Alternativnamen_besser_mit_wirklicher_Name {
	my $error_code = 255;
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Alternativnamen besser mit wirklicher Name';
				$error_description[$error_code][1] = 'Im Datenfeld Alternativname steht ein Name mit dem Zusatz "eigentlicher Name", "richtiger Name", "bürgerlicher Name" oder ähnliches, der besser als "wirklicher Name" beschrieben werden sollte (Siehe Empfehlung von <a href="http://de.wikipedia.org/wiki/Hilfe:Personendaten#Alternativnamen">Wikipedia:Personendaten</a>). Ersetzte bitte in den Klammern die Beschreibung durch "wirklicher Name".';
				$error_description[$error_code][2] = 3;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		if ( $aname ne ''
			 and (
				lc($aname) =~ /eigentlicher name/ 
			 or lc($aname) =~ /eigentlicher name/
			 or lc($aname) =~ /richtiger name/
			 or lc($aname) =~ /bürgerlicher name/
			 or lc($aname) =~ /bürg. Name/
			 or lc($aname) =~ /eigentlich/
			 or lc($aname) =~ /tatsächlicher name/
			 or $aname =~ /Wirklicher/				# Großschreibung (zusätzlich)
			 )
			) {
				fehlermeldung($error_code, $title, $aname);
				#print $error_code."\t".$title."\t".'<nowiki>'. $aname.'</nowiki>'."\n";
		}
	}
}

sub F256_Alternativnamen_besser_mit_vollstaendiger_Name {
	my $error_code = 256;
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Alternativnamen besser mit vollständiger Name';
				$error_description[$error_code][1] = 'Im Datenfeld Alternativname steht ein Name mit dem Zusatz "voller Name", der besser als "vollständiger Name" beschrieben werden sollte (Siehe Empfehlung von <a href="http://de.wikipedia.org/wiki/Hilfe:Personendaten#Alternativnamen">Wikipedia:Personendaten</a>). Ersetzte bitte in den Klammern die Beschreibung durch "vollständiger Name".';
				$error_description[$error_code][2] = 3;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		if ( $aname ne ''
			 and (
				lc($aname) =~ /voller name/ 
				or $aname =~ /Vollständig/	#Großschreibung (zusätzlich)
			 )
			) {
				fehlermeldung($error_code, $title, $aname);
				#print $error_code."\t".$title."\t".'<nowiki>'. $aname.'</nowiki>'."\n";
		}
	}
}

sub F257_Kategorie_Geboren_im_falschen_Jahrhundert {
	my $error_code = 257;
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
							$error_description[$error_code][0] = 'Kategorie:Geboren im falschen Jahrhundert';
							$error_description[$error_code][1] = 'Die Kategorie:Geboren im X. Jahrhundert passt nicht zum Datenfeld GEBURTSDATUM, wo ein Wert wie "um 1878" steht. Das 19. Jahrhundert z.B. begann 1800 und geht bis 1899, demnach ist eine Person, die "um 1878" geboren wurde, der "Kategorie:Geboren im 19. Jahrhundert" zuzuordnen.';
							$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		if ($all_categories ne ''){
			if ($geburtsdatum =~ /([ ]+)?um [0-9]+( v\. Chr\.)?([ ]+)?$/) {
				my ($test) = $all_categories;
				if ($test =~ /Geboren im [0-9]+\. Jahrhundert/) {
					
					#
					my($test) = $text =~ m/(Geboren im [0-9]+\. Jahrhundert)/;		#holt genau die Jahrhundert Kategorie raus
					my $notice = $test;
					#print $test."\n";
					$test =~ s/^Geboren im //;
					$test =~ s/. Jahrhundert$//;
					
					# Jahreszahl aus Geburtsdatum ermitteln
					my $jh = $geburtsdatum;
					$jh =~ s/^.*um //;				# old $jh =~ s/([ ]+)?um //;
					$jh =~ s/([ ]+)?$//;
					$jh =~ s/ v\. Chr\.//;
					
					if ($jh =~ /^[0-9]+/){				#nur wenn komplette aus Zahlen besteht
						$jh = (($jh - ($jh % 100)) / 100)+1;
						#print $geburtsdatum.' - '.$jh."\n";
						
						#print abs($test - $jh);
						if (abs($test - $jh) > 0 ) {
							$notice = $geburtsdatum.' - '.$notice;
							fehlermeldung($error_code, $title, $notice);
							#print $error_code."\t".$title."\t".'<nowiki>'. $notice.'</nowiki>'."\n";
						}
					}
					
				}
			}
		}
	}
}

sub F258_Kategorie_Gestorben_im_falschen_Jahrhundert {
	my $error_code = 258;
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
							$error_description[$error_code][0] = 'Kategorie:Gestorben im falschen Jahrhundert';
							$error_description[$error_code][1] = 'Die Kategorie:Gestorben im X. Jahrhundert passt nicht zum Datenfeld STERBEDATUM, wo ein Wert wie "um 1878" steht. Das 19. Jahrhundert z.B. begann 1800 und geht bis 1899, demnach ist eine Person, die "um 1878" gestorben ist, der "Kategorie:Gestorben im 19. Jahrhundert" zuzuordnen.';
							$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		if ($all_categories ne ''){
			if ($sterbedatum =~ /([ ]+)?um [0-9]+( v\. Chr\.)?([ ]+)?$/) {
				my ($test) = $all_categories;
				if ($test =~ /Gestorben im [0-9]+\. Jahrhundert/) {
					
					#
					my($test) = $text =~ m/(Gestorben im [0-9]+\. Jahrhundert)/;		#holt genau die Jahrhundert Kategorie raus
					my $notice = $test;
					#print $test."\n";
					$test =~ s/^Gestorben im //;
					$test =~ s/. Jahrhundert$//;
					
					# Jahreszahl aus Geburtsdatum ermitteln
					my $jh = $sterbedatum;
					$jh =~ s/^.*um //;			#$jh =~ s/([ ]+)?um //;
					$jh =~ s/([ ]+)?$//;
					$jh =~ s/ v\. Chr\.//;
					
					if ($jh =~ /^[0-9]+/){				#nur wenn komplette aus Zahlen besteht
						$jh = (($jh - ($jh % 100)) / 100)+1;
						#print $geburtsdatum.' - '.$jh."\n";
						
						#print abs($test - $jh);
						if (abs($test - $jh) > 0 ) {
							$notice = $sterbedatum.' - '.$notice;
							fehlermeldung($error_code, $title, $notice);
							#print $error_code."\t".$title."\t".'<nowiki>'. $notice.'</nowiki>'."\n";
						}
					}
					
				}
			}
		}
	}
}


sub F259_Name_und_Titel_abweichend{
	my $error_code = 259;
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Name und Titel abweichend';
				$error_description[$error_code][1] = 'Das Datenfeld NAME ist abweichend vom Titel des Artikels. (Beispiel: Artikel "Arthur Meier" enthält "NAME=Meier, Hans Arthur"). Das Feld Name sollte nur die Worte des Titels enthalten. Alle Namensvariationen oder zusätzliche Vornamen gehören in das Feld ALTERNATIVNAMEN.';
				$error_description[$error_code][2] = 1;
	}

	if ($title ne 'Naqyrjinsan...'
		and $title ne 'Ka-nacht...') {
	
	
		if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {


			my $notice = '';

			# 1. Reason (word)
			my $name_1 = $name;
			$name_1 =~ s/,//g;
			my @name_split_lc    = split(/ /, lc($name_1));
			my @name_split_no_lc = split(/ /, $name_1);
			my $counter = -1;
			foreach (@name_split_lc) {
				my $word = $_ ;
				$counter ++;
				if (index (lc($title), $word) == -1) {
					# don't found the part in title
					$notice = 'Fand kein: '.$name_split_no_lc[$counter]; #.'-check '.$word ;
				}
			}

			# 2. Reason	(length)
			my $name_2 = $name;
			$name_2 =~ s/ //g;
			$name_2 =~ s/,//g;
	
			my $title_2 = $title;
			$title_2 =~ s/ //g;
			if (length($name_2) > length( $title_2)) {
				#and (index ($title, '&') == -1 ) 			# Soap&Skin
				$notice = 'Name länger als Titel: '.$name if ($notice ne '');
			}

			# 3. Reason #   Artikel "Arthur Meier" enthält "NAME=Meier, Hans Arthur".

			if ($name =~ '[^ ]+, [^ ]+ [^ ]+' and $title =~ '^[^ ]+ [^ ]+$') {
				$notice = 'Check: '.$name if ($notice ne '');
			}


			if ($notice ne '') {
				fehlermeldung($error_code, $title, $notice);
				#print $error_code."\t".$title."\t".'<nowiki>'. $name.'</nowiki>'."\n";
			}
		}
	}
}



# nur ab und zu
sub F260_Ort_mit_Zahl{
	my $error_code = 260;
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
			$error_description[$error_code][0] = 'Geburts- oder Sterbeort mit Zahl';
			$error_description[$error_code][1] = 'Das Feld GEBURTSORT oder STERBEORT enthält eine Zahl. Eigentlich sollte das nicht notwendig sein. Diesen Fehlerart muss man eher als Hinweis sehen. Die darin enthaltenen Artikel sind meistens korrekt, aber einige sind halt wirkliche Fehler. Bsp. "Geburtsort=6. Jh. v. Chr." - Bekannte Ausnahmen sind: Kara Spears Hultgreen, Alexandra Freund, Auguste de Montferrand, Thomas Parr, Manabendra Nath Roy, Cristian Maidana, Ralph Roese, Hermann Günther, Anna Mae Aquash.';
			$error_description[$error_code][2] = 3;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		F260_Ort_mit_Zahl2($error_code, $geburtsort_plain);
		F260_Ort_mit_Zahl2($error_code, $sterbeort_plain);
	}
}

sub F260_Ort_mit_Zahl2{
	my $error_code = $_[0];
	my $test_ort = $_[1];
	if (   ( $test_ort  ne '' 
	         and $test_ort =~ /[0-9]/g 
			 and index ($test_ort, 'Street')== -1)
		and $title ne 'Kara Spears Hultgreen'
		and $title ne 'Alexandra Freund'
		and $title ne 'Auguste de Montferrand'
		and $title ne 'Thomas Parr'
		and $title ne 'Manabendra Nath Roy'
		and $title ne 'Cristian Maidana'
		and $title ne 'Ralph Roese'
		and $title ne 'Hermann Günther'
		and $title ne 'Anna Mae Aquash'
		and $title ne 'Friedensreich Hundertwasser'
		and index ($test_ort, 'Bundesautobahn')== -1
		and index ($test_ort, 'Bundesstraße')== -1
		and index ($test_ort, 'K2')== -1
		and index ($test_ort, 'Speziallager Nr.')== -1
		and index ($test_ort, 'Speziallager Nr.')== -1
		and index ($test_ort, 'Stadtgefängnis Nr.')== -1
		and index ($test_ort, 'Lufthansa-Flug LH 588')== -1
		and index ($test_ort, 'Mila 23')== -1
		and index ($test_ort, 'Sojus 11')== -1
		and index ($test_ort, 'Moravian 47')== -1
		and index ($test_ort, 'BB-39')== -1
		and index ($test_ort, 'CL-51')== -1
		and index ($test_ort, 'Autobahn')== -1
		and index ($test_ort, 'Ashigram-5')== -1
		and index ($test_ort, 'Queen Elizabeth 2')== -1
		and index ($test_ort, 'Krasnojarsk-26')== -1
		) 
	{
		fehlermeldung($error_code, $title, $test_ort);
		#print "\t".$title."\n";
		#print "\t".$test_ort."\n";
		#die;
	}
}

sub F261_Kategorie_geboren_nicht_in_Geburtsdatum {
	my $error_code = 261; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Kategorie:Geboren nicht in Geburtsdatum';
				$error_description[$error_code][1] = 'Das Feld GEBURTSDATUM enthält nicht die Jahreszahl aus der "Kategorie:Geboren 1234". Meist handelt es sich um Tippfehler oder Copy-and-Paste-Fehler.';
				$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		if ( #$parameter{'modus_scan'}  eq "live" and 
			 $all_categories ne '' and 
			 $geburtsdatum ne '') {
			
			my $test = $all_categories;
			$test =~ m/Kategorie:Geboren ([0-9]+)/;
			my $test2 = '';
			$test2 = $1 if ($1);
		
			if ( index ($geburtsdatum, $test2) == -1){
				
				fehlermeldung($error_code, $title, $geburtsdatum.' vs. '. $test2);
				#print "\t".$title."\n";
				#print "\t".'Geburtsdatum '.$geburtsdatum.' vs. '. $test2."\n";
				#print "\t\t\t".$all_categories."\n";
				
				#die;
			}
			#}
			
		}
	}
}

sub F262_Kategorie_gestorben_nicht_in_Sterbedatum {
	my $error_code = 262; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Kategorie:Gestorben nicht in Sterbedatum';
				$error_description[$error_code][1] = 'Das Feld STERBEDATUM enthält nicht die Jahreszahl aus der "Kategorie:Gestorben 1234". Meist handelt es sich um Tippfehler oder Copy-and-Paste-Fehler.';
				$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		if ( #$parameter{'modus_scan'}  eq "live" and 
			 $all_categories ne '' and 
			 $sterbejahr ne '') {
			
			my $test = $all_categories;
			$test =~ m/Kategorie:Gestorben ([0-9]+)/;
			my $test2 = '';
			$test2 = $1 if ($1);
			
			if ( index ($sterbedatum, $test2) == -1){
				
				#my $pos = index (lc ($all_categories), 'kategorie:gestorben');
				fehlermeldung($error_code, $title, $sterbedatum.' vs. '. $test2);
				#print "\t".$title."\n";
				#print "\t".'Sterbedatum '.$sterbedatum.' vs. '. $test2."\n";
				#print "\t\t\t".$all_categories."\n";
				#die;
			}
			
		}
	}
}

sub F263_Kategorie_geboren_im_Jahrhundert_fehlerhaft {
	my $error_code = 263; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Kategorie:Geboren im Jahrhundert fehlerhaft';
				$error_description[$error_code][1] = 'Das Feld GEBURTSDATUM enthält eine Jahrhundert, das nicht mit der vorhandenen "Kategorie:Geboren" übereinstimmt. Meist handelt es sich um Tippfehler oder Copy-and-Paste-Fehler.';
				$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		if ( #$parameter{'modus_scan'}  eq "live" and 
			 $all_categories ne ''
			 and index($geburtsdatum, '. Jahrhundert') > -1
			 and index($geburtsdatum, ' oder ') == -1
			 and index($geburtsdatum, 'unsicher: ') == -1
			 and index($geburtsdatum, 'zwischen ') == -1
			 and index($geburtsdatum, 'vor ') == -1
			 and index($geburtsdatum, 'um ') == -1
			 and index($geburtsdatum, 'nach ') == -1
			) {
			
			#print 'drin'."\n";
			#print $all_categories."\n";
			#print "\t".'Geburtsdatum: '.$geburtsdatum_plain."\n";
			#die;

			# get cat geboren
			my $test = $all_categories;
			$test =~ m/Kategorie:Geboren ([^\]]*)/;
			my $test2 = '';
			$test2 = $1 if ($1);
			$test2 =~ s/^im //;					# Kategorie:Geboren im 20. Jahrhundert
			#print "\t".'Kat:'.$test2."\n"; 	# 20. Jahrhundert, 1. Jahrhundert oder 2. Jahrhundert, 1. Jahrtausend v. Chr. oder 1. Jahrtausend
			

			# get year from birthday
			# replace days # 20. April 20. Jahrhundert
			my $test_3 = $geburtsdatum_plain;		
			$test_3 =~ m/([0-9][0-9]?\. Jahrhundert( v\. Chr\.)?)/;
			my $test4 = '';
			$test4 = $1 if ($1);
			#print "\t".'PD :'.$test4."\n";
			
			my $check = 'false';

			
			$check = 'true' if ($test2 eq $test4);	# Kategorie:Geboren im 20. Jahrhundert	 Geburtsdatum=15. August 20. Jahrhundert

			if (index ($test2, 'Jahrtausend') > -1){
				$check = 'true' if ($test2 eq '1. Jahrtausend' and $test4 eq '1. Jahrhundert');
				$check = 'true' if ($test2 eq '1. Jahrtausend' and $test4 eq '2. Jahrhundert');
				$check = 'true' if ($test2 eq '1. Jahrtausend' and $test4 eq '3. Jahrhundert');
				$check = 'true' if ($test2 eq '1. Jahrtausend' and $test4 eq '4. Jahrhundert');
				$check = 'true' if ($test2 eq '1. Jahrtausend' and $test4 eq '5. Jahrhundert');
				$check = 'true' if ($test2 eq '1. Jahrtausend' and $test4 eq '6. Jahrhundert');
				$check = 'true' if ($test2 eq '1. Jahrtausend' and $test4 eq '7. Jahrhundert');
				$check = 'true' if ($test2 eq '1. Jahrtausend' and $test4 eq '8. Jahrhundert');
				$check = 'true' if ($test2 eq '1. Jahrtausend' and $test4 eq '9. Jahrhundert');
				$check = 'true' if ($test2 eq '1. Jahrtausend' and $test4 eq '10. Jahrhundert');

				$check = 'true' if ($test2 eq '2. Jahrtausend' and $test4 eq '11. Jahrhundert');
				$check = 'true' if ($test2 eq '2. Jahrtausend' and $test4 eq '12. Jahrhundert');
				$check = 'true' if ($test2 eq '2. Jahrtausend' and $test4 eq '13. Jahrhundert');
				$check = 'true' if ($test2 eq '2. Jahrtausend' and $test4 eq '14. Jahrhundert');
				$check = 'true' if ($test2 eq '2. Jahrtausend' and $test4 eq '15. Jahrhundert');
				$check = 'true' if ($test2 eq '2. Jahrtausend' and $test4 eq '16. Jahrhundert');
				$check = 'true' if ($test2 eq '2. Jahrtausend' and $test4 eq '17. Jahrhundert');
				$check = 'true' if ($test2 eq '2. Jahrtausend' and $test4 eq '18. Jahrhundert');
				$check = 'true' if ($test2 eq '2. Jahrtausend' and $test4 eq '19. Jahrhundert');
				$check = 'true' if ($test2 eq '2. Jahrtausend' and $test4 eq '20. Jahrhundert');
				$check = 'true' if ($test2 eq '2. Jahrtausend' and $test4 eq '20. Jahrhundert');

				$check = 'true' if ($test2 eq '1. Jahrtausend v. Chr.' and $test4 eq '1. Jahrhundert v. Chr.');
				$check = 'true' if ($test2 eq '1. Jahrtausend v. Chr.' and $test4 eq '2. Jahrhundert v. Chr.');
				$check = 'true' if ($test2 eq '1. Jahrtausend v. Chr.' and $test4 eq '3. Jahrhundert v. Chr.');
				$check = 'true' if ($test2 eq '1. Jahrtausend v. Chr.' and $test4 eq '4. Jahrhundert v. Chr.');
				$check = 'true' if ($test2 eq '1. Jahrtausend v. Chr.' and $test4 eq '5. Jahrhundert v. Chr.');
				$check = 'true' if ($test2 eq '1. Jahrtausend v. Chr.' and $test4 eq '6. Jahrhundert v. Chr.');
				$check = 'true' if ($test2 eq '1. Jahrtausend v. Chr.' and $test4 eq '7. Jahrhundert v. Chr.');
				$check = 'true' if ($test2 eq '1. Jahrtausend v. Chr.' and $test4 eq '8. Jahrhundert v. Chr.');
				$check = 'true' if ($test2 eq '1. Jahrtausend v. Chr.' and $test4 eq '9. Jahrhundert v. Chr.');
				$check = 'true' if ($test2 eq '1. Jahrtausend v. Chr.' and $test4 eq '10. Jahrhundert v. Chr.');

				$check = 'true' if ($test2 eq '2. Jahrtausend v. Chr.' and $test4 eq '11. Jahrhundert v. Chr.');
				$check = 'true' if ($test2 eq '2. Jahrtausend v. Chr.' and $test4 eq '12. Jahrhundert v. Chr.');
				$check = 'true' if ($test2 eq '2. Jahrtausend v. Chr.' and $test4 eq '13. Jahrhundert v. Chr.');
				$check = 'true' if ($test2 eq '2. Jahrtausend v. Chr.' and $test4 eq '14. Jahrhundert v. Chr.');
				$check = 'true' if ($test2 eq '2. Jahrtausend v. Chr.' and $test4 eq '15. Jahrhundert v. Chr.');
				$check = 'true' if ($test2 eq '2. Jahrtausend v. Chr.' and $test4 eq '16. Jahrhundert v. Chr.');
				$check = 'true' if ($test2 eq '2. Jahrtausend v. Chr.' and $test4 eq '17. Jahrhundert v. Chr.');
				$check = 'true' if ($test2 eq '2. Jahrtausend v. Chr.' and $test4 eq '18. Jahrhundert v. Chr.');
				$check = 'true' if ($test2 eq '2. Jahrtausend v. Chr.' and $test4 eq '19. Jahrhundert v. Chr.');
				$check = 'true' if ($test2 eq '2. Jahrtausend v. Chr.' and $test4 eq '20. Jahrhundert v. Chr.');

				$check = 'true' if ($test2 eq '3. Jahrtausend v. Chr.' and $test4 eq '21. Jahrhundert v. Chr.');
				$check = 'true' if ($test2 eq '3. Jahrtausend v. Chr.' and $test4 eq '22. Jahrhundert v. Chr.');
				$check = 'true' if ($test2 eq '3. Jahrtausend v. Chr.' and $test4 eq '23. Jahrhundert v. Chr.');
				$check = 'true' if ($test2 eq '3. Jahrtausend v. Chr.' and $test4 eq '24. Jahrhundert v. Chr.');
				$check = 'true' if ($test2 eq '3. Jahrtausend v. Chr.' and $test4 eq '25. Jahrhundert v. Chr.');
				$check = 'true' if ($test2 eq '3. Jahrtausend v. Chr.' and $test4 eq '26. Jahrhundert v. Chr.');
				$check = 'true' if ($test2 eq '3. Jahrtausend v. Chr.' and $test4 eq '27. Jahrhundert v. Chr.');
				$check = 'true' if ($test2 eq '3. Jahrtausend v. Chr.' and $test4 eq '28. Jahrhundert v. Chr.');
				$check = 'true' if ($test2 eq '3. Jahrtausend v. Chr.' and $test4 eq '29. Jahrhundert v. Chr.');
				$check = 'true' if ($test2 eq '3. Jahrtausend v. Chr.' and $test4 eq '30. Jahrhundert v. Chr.');

				$check = 'true' if ($test2 eq '4. Jahrtausend v. Chr.' and $test4 eq '31. Jahrhundert v. Chr.');
				$check = 'true' if ($test2 eq '4. Jahrtausend v. Chr.' and $test4 eq '32. Jahrhundert v. Chr.');
				$check = 'true' if ($test2 eq '4. Jahrtausend v. Chr.' and $test4 eq '33. Jahrhundert v. Chr.');
				$check = 'true' if ($test2 eq '4. Jahrtausend v. Chr.' and $test4 eq '34. Jahrhundert v. Chr.');
				$check = 'true' if ($test2 eq '4. Jahrtausend v. Chr.' and $test4 eq '35. Jahrhundert v. Chr.');
				$check = 'true' if ($test2 eq '4. Jahrtausend v. Chr.' and $test4 eq '36. Jahrhundert v. Chr.');
				$check = 'true' if ($test2 eq '4. Jahrtausend v. Chr.' and $test4 eq '37. Jahrhundert v. Chr.');
				$check = 'true' if ($test2 eq '4. Jahrtausend v. Chr.' and $test4 eq '38. Jahrhundert v. Chr.');
				$check = 'true' if ($test2 eq '4. Jahrtausend v. Chr.' and $test4 eq '39. Jahrhundert v. Chr.');
				$check = 'true' if ($test2 eq '4. Jahrtausend v. Chr.' and $test4 eq '40. Jahrhundert v. Chr.');


				$check = 'true' if ($test2 eq '1. Jahrtausend v. Chr. oder 1. Jahrtausend' and $test4 eq '1. Jahrhundert');
				$check = 'true' if ($test2 eq '1. Jahrtausend v. Chr. oder 1. Jahrtausend' and $test4 eq '1. Jahrhundert v. Chr.');
			}
			



			if ( $check eq 'false' ){
				#print $search_text."\n";
				my $pos = index (lc ($all_categories), 'kategorie:geboren');
				my $pos2 = index (lc ($all_categories), ']',$pos);
				my $notice = $geburtsdatum.' vs. '.substr($all_categories, $pos , $pos2 - $pos);
				fehlermeldung($error_code, $title, $notice);
				#print "\t"."\t".$title."\n";
				#print "\t"."\t".$notice."\n";
				#print "\t\t\t".$all_categories."\n";
				#die;
			}
			
		}
	}
}


sub F264_Kategorie_gestorben_im_Jahrhundert_fehlerhaft {
	my $error_code = 264; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Kategorie:Gestorben im Jahrhundert fehlerhaft';
				$error_description[$error_code][1] = 'Das Feld STERBEDATUM enthält eine Jahrhundert, das nicht mit der vorhandenen "Kategorie:Gestorben" übereinstimmt. Meist handelt es sich um Tippfehler oder Copy-and-Paste-Fehler.';
				$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		if ( #$parameter{'modus_scan'}  eq "live" and 
			 $all_categories ne '' 
			 and  index($sterbedatum, '. Jahrhundert') > -1
			 and index($sterbedatum, ' oder ') == -1
			 and index($sterbedatum, 'unsicher: ') == -1
			 and index($sterbedatum, 'zwischen ') == -1
			 and index($sterbedatum, 'vor ') == -1
			 and index($sterbedatum, 'um ') == -1
			 and index($sterbedatum, 'nach ') == -1
			) {
			
			# get cat gestorben
			my $test = $all_categories;
			$test =~ m/Kategorie:Gestorben ([^\]]*)/;
			my $test2 = '';
			$test2 = $1 if ($1);
			$test2 =~ s/^im //;					# Kategorie:Geboren im 20. Jahrhundert
			#print "\t".'Kat:'.$test2."\n"; 	# 20. Jahrhundert, 1. Jahrhundert oder 2. Jahrhundert, 1. Jahrtausend v. Chr. oder 1. Jahrtausend
			

			# get year from birthday
			# replace days # 20. April 20. Jahrhundert
			my $test_3 = $sterbedatum_plain;		
			$test_3 =~ m/([0-9][0-9]?\. Jahrhundert( v\. Chr\.)?)/;
			my $test4 = '';
			$test4 = $1 if ($1);
			#print "\t".'PD :'.$test4."\n";
			
			my $check = 'false';

			
			$check = 'true' if ($test2 eq $test4);	# Kategorie:Geboren im 20. Jahrhundert	 Geburtsdatum=15. August 20. Jahrhundert

			if (index ($test2, 'Jahrtausend') > -1){
				$check = 'true' if ($test2 eq '1. Jahrtausend' and $test4 eq '1. Jahrhundert');
				$check = 'true' if ($test2 eq '1. Jahrtausend' and $test4 eq '2. Jahrhundert');
				$check = 'true' if ($test2 eq '1. Jahrtausend' and $test4 eq '3. Jahrhundert');
				$check = 'true' if ($test2 eq '1. Jahrtausend' and $test4 eq '4. Jahrhundert');
				$check = 'true' if ($test2 eq '1. Jahrtausend' and $test4 eq '5. Jahrhundert');
				$check = 'true' if ($test2 eq '1. Jahrtausend' and $test4 eq '6. Jahrhundert');
				$check = 'true' if ($test2 eq '1. Jahrtausend' and $test4 eq '7. Jahrhundert');
				$check = 'true' if ($test2 eq '1. Jahrtausend' and $test4 eq '8. Jahrhundert');
				$check = 'true' if ($test2 eq '1. Jahrtausend' and $test4 eq '9. Jahrhundert');
				$check = 'true' if ($test2 eq '1. Jahrtausend' and $test4 eq '10. Jahrhundert');
				$check = 'true' if ($test2 eq '2. Jahrtausend' and $test4 eq '11. Jahrhundert');
				$check = 'true' if ($test2 eq '2. Jahrtausend' and $test4 eq '12. Jahrhundert');
				$check = 'true' if ($test2 eq '2. Jahrtausend' and $test4 eq '13. Jahrhundert');
				$check = 'true' if ($test2 eq '2. Jahrtausend' and $test4 eq '14. Jahrhundert');
				$check = 'true' if ($test2 eq '2. Jahrtausend' and $test4 eq '15. Jahrhundert');
				$check = 'true' if ($test2 eq '2. Jahrtausend' and $test4 eq '16. Jahrhundert');
				$check = 'true' if ($test2 eq '2. Jahrtausend' and $test4 eq '17. Jahrhundert');
				$check = 'true' if ($test2 eq '2. Jahrtausend' and $test4 eq '18. Jahrhundert');
				$check = 'true' if ($test2 eq '2. Jahrtausend' and $test4 eq '19. Jahrhundert');
				$check = 'true' if ($test2 eq '2. Jahrtausend' and $test4 eq '20. Jahrhundert');
				$check = 'true' if ($test2 eq '2. Jahrtausend' and $test4 eq '20. Jahrhundert');

				$check = 'true' if ($test2 eq '1. Jahrtausend v. Chr.' and $test4 eq '1. Jahrhundert v. Chr.');
				$check = 'true' if ($test2 eq '1. Jahrtausend v. Chr.' and $test4 eq '2. Jahrhundert v. Chr.');
				$check = 'true' if ($test2 eq '1. Jahrtausend v. Chr.' and $test4 eq '3. Jahrhundert v. Chr.');
				$check = 'true' if ($test2 eq '1. Jahrtausend v. Chr.' and $test4 eq '4. Jahrhundert v. Chr.');
				$check = 'true' if ($test2 eq '1. Jahrtausend v. Chr.' and $test4 eq '5. Jahrhundert v. Chr.');
				$check = 'true' if ($test2 eq '1. Jahrtausend v. Chr.' and $test4 eq '6. Jahrhundert v. Chr.');
				$check = 'true' if ($test2 eq '1. Jahrtausend v. Chr.' and $test4 eq '7. Jahrhundert v. Chr.');
				$check = 'true' if ($test2 eq '1. Jahrtausend v. Chr.' and $test4 eq '8. Jahrhundert v. Chr.');
				$check = 'true' if ($test2 eq '1. Jahrtausend v. Chr.' and $test4 eq '9. Jahrhundert v. Chr.');
				$check = 'true' if ($test2 eq '1. Jahrtausend v. Chr.' and $test4 eq '10. Jahrhundert v. Chr.');
				$check = 'true' if ($test2 eq '2. Jahrtausend v. Chr.' and $test4 eq '11. Jahrhundert v. Chr.');
				$check = 'true' if ($test2 eq '2. Jahrtausend v. Chr.' and $test4 eq '12. Jahrhundert v. Chr.');
				$check = 'true' if ($test2 eq '2. Jahrtausend v. Chr.' and $test4 eq '13. Jahrhundert v. Chr.');
				$check = 'true' if ($test2 eq '2. Jahrtausend v. Chr.' and $test4 eq '14. Jahrhundert v. Chr.');
				$check = 'true' if ($test2 eq '2. Jahrtausend v. Chr.' and $test4 eq '15. Jahrhundert v. Chr.');
				$check = 'true' if ($test2 eq '2. Jahrtausend v. Chr.' and $test4 eq '16. Jahrhundert v. Chr.');
				$check = 'true' if ($test2 eq '2. Jahrtausend v. Chr.' and $test4 eq '17. Jahrhundert v. Chr.');
				$check = 'true' if ($test2 eq '2. Jahrtausend v. Chr.' and $test4 eq '18. Jahrhundert v. Chr.');
				$check = 'true' if ($test2 eq '2. Jahrtausend v. Chr.' and $test4 eq '19. Jahrhundert v. Chr.');
				$check = 'true' if ($test2 eq '2. Jahrtausend v. Chr.' and $test4 eq '20. Jahrhundert v. Chr.');
				$check = 'true' if ($test2 eq '2. Jahrtausend v. Chr.' and $test4 eq '20. Jahrhundert v. Chr.');

				$check = 'true' if ($test2 eq '1. Jahrtausend v. Chr. oder 1. Jahrtausend' and $test4 eq '1. Jahrhundert');
				$check = 'true' if ($test2 eq '1. Jahrtausend v. Chr. oder 1. Jahrtausend' and $test4 eq '1. Jahrhundert v. Chr.');
			}
			



			if ( $check eq 'false' ){
				#print $search_text."\n";
				my $pos = index (lc ($all_categories), 'kategorie:gestorben');
				my $pos2 = index (lc ($all_categories), ']',$pos);
				my $notice = $sterbedatum.' vs. '.substr($all_categories, $pos , $pos2 - $pos);
				fehlermeldung($error_code, $title, $notice);
				#print "\t".$title."\n";
				#print "\t".$notice."\n";
				#print "\t\t\t".$all_categories."\n";
				#die;
			}
			
		}
	}
}

sub F265_Titelwort_mehrfach_im_Alternativnamen {
	my $error_code = 265; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Alternativnamen enthält mehrfach Wort aus Titel';
				$error_description[$error_code][1] = 'Das Feld ALTERNATIVNAMEN enthält kein Semikolon, aber mehrfach einen Wortteil aus dem Titel. Meist fehlt hier nur das Semikolon um die unterschiedlichen Alternativnamen voneinander zu trennen';
				$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		if ( #$parameter{'modus_scan'}  eq "live" and 
			 $aname ne '' 
			 and index($aname, ';') == -1
			) {
			
			my $test = $title;
			$test =~ s/\(*//g;
			#print 'test='.$test."\n";
			my @wordparts = split( / /,$test);
			
			my $notice = '';
			foreach(@wordparts) {
				my $current_word = $_;
				#print $current_word."\n";
				if (length($current_word) >1) {
					if ( substr($current_word, 2,1) ne '.'
						 and $current_word =~ /^[[:upper:]]/)
						{
						
						if ( 
								( #Vorname z.B: "Hans Suess, Hans Süß von Kulmbach"
								  index( $aname, $current_word ) == 0
									and index( $aname, ', '.$current_word ) > -1
								)
								or
								( #Nachname z.B: "Hans Meier, Max Hans Meier"
								  substr( $aname, length($aname) - length($current_word) ) eq $current_word
								  and index( $aname, $current_word.', ' ) > -1
								)
							){
							#print $current_word."\n";
							$notice = $current_word.' - '.$aname;
						}
					}
				}
				
			}

			if ( $notice ne ''	){
				fehlermeldung($error_code, $title, $notice);
				#print "\t".$title."\n";
				#print "\t".$notice."\n";
				
			}
			
		}
	}
}


sub F266_Geburtsdatum_nicht_leer {
	my $error_code = 266; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Geburtsdatum leer';
				$error_description[$error_code][1] = 'Das Feld GEBURTSDATUM ist leer. Jede Person sollte irgendwann geboren sein. Die Zeit lässt sich zumindestens auf Jahrtausend eingrenzen. Es gibt also kein Grund das Feld leer zu lassen. Mögliche Einträge könnten zum Beispiel sein: "1987" oder "1. Jahrhundert" oder "1. Jahrhundert oder 2. Jahrhundert" oder "1. Jahrtausend" oder "1. Jahrtausend oder 2. Jahrtausend"';
				$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		if ($geburtsdatum eq ''
			or $geburtsdatum eq ' '
			or $geburtsdatum eq '  ') {
				my $notice = '';
				fehlermeldung($error_code, $title, $notice);
				#print $error_code."\t".$title."\t".'<nowiki>'. $name.'</nowiki>'."\n";
		}
				
	}
}


sub F267_Alternativname_leer_Titel_nicht_im_Text{
	my $error_code = 267; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Alternativname leer, aber Titel nicht im Text';
				$error_description[$error_code][1] = 'Das Feld ALTERNATIVNAMEN ist leer und es konnte im Text kein fett geschriebenen Text gefunden werden, der die ersten Worte des Lemmas enthält. Generell sollte der erste fettgeschriebenen Text im Artikel den Titel komplett enthalten. Es sollte hier ergänzend der Alternativnamen in den Personendaten eingetragen werden. Bei einem Lemma mit Klammeranhang (z.B. "Al-Muwaffaq (Abbaside)") wurde der Klammerteil abgeschnitten. Ebenso wurden bei einem Lemma mit "von" z.B. "Agathokles von Syrakus" alle Lemmabestandteile nach "von" entfernt, bevor nach dem Titel im Text gesucht wurde.

Sollte der Titel in der korrekten Schreibweise enthalten sein, dann kann es möglicherweise durch das Fehlen von drei Hochkommatas (zum fett schreiben) in einem Text/Template davor dazu führen, dass das Skript den Titel nicht korrekt erkennt.';
				$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		if ($aname eq ''
		    and $is_redirect == 0
		    and $title ne 'Naqyrjinsan...'
			and $title ne 'Ka-nacht...'
			) {


			my @title_variant ;
			push (@title_variant, $title);

			#get title without ()
			my $text_title = $title;
			$text_title =~ s/ \(.*$//;
			push (@title_variant, $text_title);
			#print 'Title1: '.$text_title."\n";

			# Agathokles von Syrakus --> alles nach "von" weg
			$text_title =~ s/ (aus|von|de|der|die|genannt) .*//g;
			push (@title_variant, $text_title);
			#print 'Title2: '.$text_title."\n";

			# Alphonso, Earl of Chester --> alles nach "," weg
			$text_title =~ s/, .*//g;
			push (@title_variant, $text_title);
			#print 'Title3: '.$text_title."\n";

			# Johann Pölzer senior --> alles nach "junior/senior" weg
			$text_title =~ s/( |,)(junior|senior).*//gi;
			push (@title_variant, $text_title);
			#print 'Title4: '.$text_title."\n";

			# Constans II. --> Römische Zahlen weg
			#$text_title =~ s/ (I|II|III|IV|V|VI|VII|VIII|IX|X)(\.)?( *|$)/ /g;		# split, long first, short last
			$text_title =~ s/ (VIII)(\.)?( *|$)/ /g;
			$text_title =~ s/ (VII|III)(\.)?( *|$)/ /g;
			$text_title =~ s/ (II|IV|VI|IX|XI)(\.)?( *|$)/ /g;
			$text_title =~ s/ (I|V|X)(\.)?( *|$)/ /g;
			$text_title =~ s/ $//;
			push (@title_variant, $text_title);
			#print 'Title5: '.$text_title."\n";

			#problem arabic names 
			# Ag, Al, An, As
			my $text_title_2 = $text_title;
			$text_title_2 =~ s/(^| )Ag( |\-)/$1ag$2/;
			push (@title_variant, $text_title_2 );

			$text_title_2 = $text_title;
			$text_title_2 =~ s/(^| )Al( |\-)/$1al$2/;
			push (@title_variant, $text_title_2 );

			$text_title_2 = $text_title;
			$text_title_2 =~ s/(^| )An( |\-)/$1an$2/;
			push (@title_variant, $text_title_2 );

			$text_title_2 = $text_title;
			$text_title_2 =~ s/(^| )As( |\-)/$1as$2/;
			push (@title_variant, $text_title_2 );

			# El
			$text_title_2 = $text_title;
			$text_title_2 =~ s/(^| )El( |\-)/$1el$2/;
			push (@title_variant, $text_title_2 );

			#$text_title =~ s/\'\'\'//;				# first '''
			#my $pos= index($text_title, "'''");
			#$text_title = substr($text_title, 0 , $pos);	


			# loop to get all fat words
			my $notice = '';
			my $found = 'false';
			my $regex = "'''";
			#print $text."\n";

			my $counter = 0;	#nur jedes zweite Vorkommen interessiert
			my $pos1 = 0;
			my $pos2 = 0;
			my $found_fat = '';
			
			while($text =~ /$regex/g) {
				my $found_text = $&;
				
				$counter = $counter +1;
				if ($counter == 1){ 		# first
					$pos1 = pos($text) - length($found_text) 
				}
				if ($counter == 2) {		# secound
					$pos2 = pos($text);
					$found_fat = substr ($text, $pos1, $pos2 - $pos1);
					$counter = 0;
					#print 'Found: '.$found_fat."\n";

					##remove Space like ''' Mister Bond '''
					$found_fat =~ s/'''\s+//;
					$found_fat =~ s/\s+'''$//;


					foreach (@title_variant) {
						#print 'Check: '.$_."\n";
						if (index($found_fat, $_) > -1) {
							$found = 'true'; 
							#print '"'.$found_fat.'" has "'.$text_title.'" inside'."\n\n";
							last;	# end while
						} 
					}	
				}
			}
			
		
			#die if ($title eq 'Ahmed Mohamed Ag Hamani');
			#die if ($found eq 'false');	

			if ($found eq 'false') {
				$notice = 'fand kein: '.$text_title."\n"; 
				foreach (@title_variant){
						if ( index ($notice, $_) == -1) { 	# no double
							$notice .= 'fand kein: '.$_."\n";
						}			
				}
				fehlermeldung($error_code, $title, $notice);
				#print $error_code."\t".$title."\t".'<nowiki>'. $notice.'</nowiki>'."\n";
				#die;
			}

		}
	}
}

sub F268_Kategorie_mit_Nationalitaet_fehlt{
	my $error_code = 268; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Kategorie mit Nationalität fehlt';
				$error_description[$error_code][1] = 'Im Feld KURZBESCHREIBUNG steht eine Nationalität am Anfang ("deutsch", "französisch") und das Geburtsjahr liegt nach 1900, aber es fehlt eine Kategorie mit der Nationalität ("Kategorie:Deutscher" oder "Kategorie:Fußballspieler (Deutschland)"). ';
				$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		if ($kurz_plain ne '') {

			my $ok = 'false';
			if ($geburtsjahr ne ''){
				$ok = 'true' if ( $geburtsjahr > 1900);
			}
			
			if ($ok eq 'true') {
				# erstes Wort von Kurzbeschreibung
				#print $kurz_plain."\n";
				my $pos = index ($kurz_plain, ' ');
				if ($pos > -1) {
					# more then one word
					my $first = '';
					$first = substr($kurz_plain,0,$pos);
					$first = lc($first);



					my @array_land;
					my $i = -1;
					# zuerst das Suchwort für die Kurzbeschreibung, dann alle Wörter die möglicherweise in den Kategorien auftauchen



					$i++;$array_land[$i] = 'afghanisch, Afghane, Afghanistan';
					$i++;$array_land[$i] = 'ägyptisch, Ägypter, Ägypten, Ägyptisch';	# Ägyptisch und  Äthiopisch, wegen Problemem
					$i++;$array_land[$i] = 'albanisch, Albaner, Albanien';
					$i++;$array_land[$i] = 'algerisch, Algerier, Algerien';
					$i++;$array_land[$i] = 'andorranisch, Andorraner, Andorra';
					$i++;$array_land[$i] = 'angolanisch, Angolaner, Angola';
					$i++;$array_land[$i] = 'antiguanisch, Antiguaner, Antigua';
					$i++;$array_land[$i] = 'äquatorialguineisch, Äquatorialguineer, Äquatorialguinea';
					$i++;$array_land[$i] = 'argentinisch, Argentinier, Argentinien';
					$i++;$array_land[$i] = 'armenisch, Armenier, Armenien';
					$i++;$array_land[$i] = 'aserbaidschanisch, Aserbaidschaner, Aserbaidschan';
					$i++;$array_land[$i] = 'äthiopisch, Äthiopier, Äthiopien, Äthiopisch';   # Ägyptisch und  Äthiopisch, wegen Problemem
					$i++;$array_land[$i] = 'australisch, Australier, Australien';
					$i++;$array_land[$i] = 'bahamaisch, Bahamaer, Bahamas';
					$i++;$array_land[$i] = 'bahrainisch, Bahrainer, Bahrain';
					$i++;$array_land[$i] = 'bangladeschisch, Bangladescher, Bangladesch';
					$i++;$array_land[$i] = 'barbadisch, Barbadier, Barbados';
					$i++;$array_land[$i] = 'belarussisch, Weißrusse, Weißrussland';
					$i++;$array_land[$i] = 'weißrussisch, Weißrusse, Weißrussland';
					$i++;$array_land[$i] = 'belgisch, Belgier, Belgien';
					$i++;$array_land[$i] = 'belizisch, Belizer, Belize';
					$i++;$array_land[$i] = 'beninisch, Beniner, Benin';
					$i++;$array_land[$i] = 'bhutanisch, Bhutaner, Bhutan';
					$i++;$array_land[$i] = 'bolivianisch, Bolivianer, Bolivien';
					$i++;$array_land[$i] = 'bosnisch, Bosnier, Bosnien';
					$i++;$array_land[$i] = 'bosnisch-herzegowinisch, Bosnier, Bosnien';
					$i++;$array_land[$i] = 'botsuanisch, Botsuaner, Botsuana';	# englische Schreibweise Botswana raus
					$i++;$array_land[$i] = 'brasilianisch, Brasilianer, Brasilien';
					$i++;$array_land[$i] = 'bruneiisch, Bruneier, Brunei';
					$i++;$array_land[$i] = 'bulgarisch, Bulgare, Bulgarien';
					$i++;$array_land[$i] = 'burkinisch, Burkiner, Burkina';
					$i++;$array_land[$i] = 'burundisch, Burundier, Burundi';
					$i++;$array_land[$i] = 'chilenisch, Chilene, Chile';
					$i++;$array_land[$i] = 'chinesisch, Chinese, China, Hongkong, Macao, Macau';
					$i++;$array_land[$i] = 'costa-ricanisch, Costa-Ricaner, Costa Rica';
					$i++;$array_land[$i] = 'dänisch, Däne, Dänin, Dänemark';
					$i++;$array_land[$i] = 'deutsch, Deutscher, Deutschland, DDR, BRD';
					$i++;$array_land[$i] = 'dominicanisch, Dominicaner, Dominica';
					$i++;$array_land[$i] = 'dschibutisch, Dschibutier, Dschibuti';
					$i++;$array_land[$i] = 'ecuadorianisch, Ecuadorianer, Ecuador';
					$i++;$array_land[$i] = 'ivorisch, Ivorer, Elfenbeinküste';
					$i++;$array_land[$i] = 'salvadorianisch, Salvadorianer, Salvador';
					$i++;$array_land[$i] = 'eritreisch, Eritreer, Eritrea';
					$i++;$array_land[$i] = 'estnisch, Este, Estland';
					$i++;$array_land[$i] = 'fidschianisch, Fidschianer, Fidschi';
					$i++;$array_land[$i] = 'finnisch, Finne, Finnland';
					$i++;$array_land[$i] = 'französisch, Franzose, Frankreich';
					$i++;$array_land[$i] = 'gabunisch, Gabuner, Gabun';
					$i++;$array_land[$i] = 'gambisch, Gambier, Gambia';
					$i++;$array_land[$i] = 'georgisch, Georgier, Georgien';
					$i++;$array_land[$i] = 'ghanaisch, Ghanaer, Ghana';
					$i++;$array_land[$i] = 'grenadisch, Grenader, Grenada';
					$i++;$array_land[$i] = 'griechisch, Grieche, Griechenland';
					$i++;$array_land[$i] = 'guatemaltekisch, Guatemalteke, Guatemala';
					$i++;$array_land[$i] = 'guineisch, Guineer, Guinea';
					$i++;$array_land[$i] = 'guinea-bissauisch, Guinea-Bissauer, Guinea-Bissau';
					$i++;$array_land[$i] = 'guyanisch, Guyaner, Guyana';
					$i++;$array_land[$i] = 'haitianisch, Haitianer, Haiti';
					$i++;$array_land[$i] = 'honduranisch, Honduraner, Honduras';
					$i++;$array_land[$i] = 'indisch, Inder, Indien';
					$i++;$array_land[$i] = 'indonesisch, Indonesier, Indonesien';
					$i++;$array_land[$i] = 'irakisch, Iraker, Irak';
					$i++;$array_land[$i] = 'iranisch, Iraner, Iran';
					$i++;$array_land[$i] = 'iranisch, Iraner, Iran, persisch, Perser, Persien';
					$i++;$array_land[$i] = 'irisch, Ire, Irland';
					$i++;$array_land[$i] = 'isländisch, Isländer, Island';
					$i++;$array_land[$i] = 'israelisch, Israeli, Israel';
					$i++;$array_land[$i] = 'italienisch, Italiener, Italien';
					$i++;$array_land[$i] = 'jamaikanisch, Jamaikaner, Jamaika';
					$i++;$array_land[$i] = 'japanisch, Japaner, Japan';
					$i++;$array_land[$i] = 'jemenitisch, Jemenit, Jemen';
					$i++;$array_land[$i] = 'jordanisch, Jordanier, Jordanien';
					$i++;$array_land[$i] = 'kambodschanisch, Kambodschaner, Kambodscha';
					$i++;$array_land[$i] = 'kamerunisch, Kameruner, Kamerun';
					$i++;$array_land[$i] = 'kanadisch, Kanadier, Kanada';
					$i++;$array_land[$i] = 'kap-verdisch, Kap-Verdier, Kap Verde';
					$i++;$array_land[$i] = 'kasachisch, Kasache, Kasachstan';
					$i++;$array_land[$i] = 'katarisch, Katarer, Katar';
					$i++;$array_land[$i] = 'kenianisch, Kenianer, Kenia';
					$i++;$array_land[$i] = 'kirgisisch, kirgisisch, Kirgise, Kirgisien, Kirgisistan';
					$i++;$array_land[$i] = 'kiribatisch, Kiribatier, Kiribati';
					$i++;$array_land[$i] = 'kolumbianisch, Kolumbianer, Kolumbien';
					$i++;$array_land[$i] = 'komorisch, Komorer, Komoren';
					$i++;$array_land[$i] = 'kongolesisch, Kongolese, Kongo';
					$i++;$array_land[$i] = 'koreanisch, Koreaner, Nordkoreaner, Südkoreaner, Nordkorea, Südkorea, Korea';
					$i++;$array_land[$i] = 'nordkoreanisch, Koreaner, Nordkoreaner, Korea, Nordkorea';
					$i++;$array_land[$i] = 'südkoreanisch, Koreaner, Nordkoreaner, Korea, Südkorea';
					$i++;$array_land[$i] = 'nord-koreanisch, Koreaner, Nordkoreaner, Korea, Nordkorea';
					$i++;$array_land[$i] = 'süd-koreanisch, Koreaner, Nordkoreaner, Korea, Südkorea';
					$i++;$array_land[$i] = 'kosovarisch, Kosovare, Kosovo';
					$i++;$array_land[$i] = 'kroatisch, Kroate, Kroatien';
					$i++;$array_land[$i] = 'kubanisch, Kubaner, Kuba';
					$i++;$array_land[$i] = 'kuwaitisch, Kuwaiter, Kuwait';
					$i++;$array_land[$i] = 'laotisch, Laote, Laos';
					$i++;$array_land[$i] = 'lesothisch, Lesother, Lesotho';
					$i++;$array_land[$i] = 'lettisch, Lette, Lettland';
					$i++;$array_land[$i] = 'libanesisch, Libanese, Libanon';
					$i++;$array_land[$i] = 'liberianisch, Liberianer, Liberia';
					$i++;$array_land[$i] = 'libysch, Libyer, Libyen';
					$i++;$array_land[$i] = 'liechtensteinisch, Liechtensteiner, Liechtenstein';
					$i++;$array_land[$i] = 'Liechtensteiner, Liechtenstein';
					$i++;$array_land[$i] = 'litauisch, Litauer, Litauen';
					$i++;$array_land[$i] = 'luxemburgisch, Luxemburger, Luxemburg';
					$i++;$array_land[$i] = 'Luxemburger, Luxemburger, Luxemburg';
					$i++;$array_land[$i] = 'madagassisch, Madagasse, Madagaskar';
					$i++;$array_land[$i] = 'malawisch, Malawier, Malawi';
					$i++;$array_land[$i] = 'malaysisch, Malaysier, Malaysia';
					$i++;$array_land[$i] = 'maledivisch, Malediver, Malediven';
					$i++;$array_land[$i] = 'malisch, Malier, Mali';
					$i++;$array_land[$i] = 'maltesisch, Malteser, Malta';
					$i++;$array_land[$i] = 'marokkanisch, Marokkaner, Marokko';
					$i++;$array_land[$i] = 'marshallisch, Marshaller, Marshall';
					$i++;$array_land[$i] = 'mauretanisch, Mauretanier, Mauretanien';
					$i++;$array_land[$i] = 'mauritisch, Mauritier, Mauritius';
					$i++;$array_land[$i] = 'mazedonisch, Mazedonier, Mazedonien';
					$i++;$array_land[$i] = 'mexikanisch, Mexikaner, Mexiko';
					$i++;$array_land[$i] = 'mikronesisch, Mikronesier, Mikronesien';
					$i++;$array_land[$i] = 'moldauisch, Moldawier, Moldawien';
					$i++;$array_land[$i] = 'moldawisch, Moldawier, Moldawien';
					$i++;$array_land[$i] = 'monegassisch, Monegasse, Monaco';
					$i++;$array_land[$i] = 'mongolisch, Mongole, Mongolei';
					$i++;$array_land[$i] = 'montenegrinisch, Montenegriner, Montenegro';
					$i++;$array_land[$i] = 'mosambikanisch, Mosambikaner, Mosambik';
					$i++;$array_land[$i] = 'myanmarisch, Myanmare, Myanmar, Birma, Burma';
					$i++;$array_land[$i] = 'birmanisch, Myanmare, Myanmar, Birma, Burma';
					$i++;$array_land[$i] = 'burmesisch, Myanmare, Myanmar, Birma, Burma';
					$i++;$array_land[$i] = 'namibisch, Namibier, Namibia';
					$i++;$array_land[$i] = 'nauruisch, Nauruer, Nauru';
					$i++;$array_land[$i] = 'nepalesisch, Nepalese, Nepal';
					$i++;$array_land[$i] = 'neuseeländisch, Neuseeländer, Neuseeland';
					$i++;$array_land[$i] = 'nicaraguanisch, Nicaraguaner, Nicaragua';
					$i++;$array_land[$i] = 'niederländisch, Niederländer, Niederlande';
					$i++;$array_land[$i] = 'nigrisch, Nigrer, Niger';
					$i++;$array_land[$i] = 'nigerianisch, Nigerianer, Nigeria';
					$i++;$array_land[$i] = 'niueanisch, Niueaner, Niue';
					$i++;$array_land[$i] = 'norwegisch, Norweger, Norwegen';
					$i++;$array_land[$i] = 'omanisch, Omaner, Oman';
					$i++;$array_land[$i] = 'österreichisch, Österreicher, Österreich';
					$i++;$array_land[$i] = 'osttimoresisch, Osttimorese, Osttimor';
					$i++;$array_land[$i] = 'pakistanisch, Pakistaner, Pakistan';
					$i++;$array_land[$i] = 'palauisch, Palauer, Palau';
					$i++;$array_land[$i] = 'panamaisch, Panamaer, Panama';
					$i++;$array_land[$i] = 'papua-neuguineisch, Papua-Neuguineer, Papua-Neuguinea';
					$i++;$array_land[$i] = 'paraguayisch, Paraguayer, Paraguay';
					$i++;$array_land[$i] = 'peruanisch, Peruaner, Peru';
					$i++;$array_land[$i] = 'philippinisch, Philippiner, Philippinen';
					$i++;$array_land[$i] = 'polnisch, Pole, Polen';
					$i++;$array_land[$i] = 'portugiesisch, Portugiese, Portugal';
					$i++;$array_land[$i] = 'ruandisch, Ruander, Ruanda';
					$i++;$array_land[$i] = 'rumänisch, Rumäne, Rumänien';
					$i++;$array_land[$i] = 'russisch, Russe, Russland, sowjetisch, Sowjetbürger, Sowjetunion';
					$i++;$array_land[$i] = 'salomonisch, Salomoner, Salomonen';
					$i++;$array_land[$i] = 'sambisch, Sambier, Sambia';
					$i++;$array_land[$i] = 'samoanisch, amoaner, Samoa';
					$i++;$array_land[$i] = 'san-marinesisch, San-Marinese, San Marino';
					$i++;$array_land[$i] = 'são-toméisch, São-Toméer, São Tomé';
					$i++;$array_land[$i] = 'saudi-arabisch, Saudi-Araber, Saudi-Arabien';
					$i++;$array_land[$i] = 'schwedisch, Schwede, Schweden';
					$i++;$array_land[$i] = 'Schweizer, Schweiz, schweizerisch';
					$i++;$array_land[$i] = 'schweizerisch, Schweizer, Schweiz';
					$i++;$array_land[$i] = 'senegalesisch, Senegalese, Senegal';
					$i++;$array_land[$i] = 'serbisch, Serbe, Serbien';
					$i++;$array_land[$i] = 'seychellisch, Seycheller, Seychellen';
					$i++;$array_land[$i] = 'sierra-leonisch, Sierra-Leoner, Sierra Leone';
					$i++;$array_land[$i] = 'simbabwisch, Simbabwer, Simbabwe';
					$i++;$array_land[$i] = 'singapurisch, ingapurer, Singapur';
					$i++;$array_land[$i] = 'slowakisch, Slowake, Slowakei, tschechoslowakisch, tschechoslowakisch, Tschechoslowakei';
					$i++;$array_land[$i] = 'slowenisch, Slowene, Slowenien';
					$i++;$array_land[$i] = 'somalisch, Somalier, Somalia';
					$i++;$array_land[$i] = 'spanisch, Spanier, Spanien';
					$i++;$array_land[$i] = 'sri-lankisch, Sri-Lanker, Sri Lanka';
					$i++;$array_land[$i] = 'lucianisch, Lucianer, Lucia';
					$i++;$array_land[$i] = 'vincentisch, Vincenter, Vincent';
					$i++;$array_land[$i] = 'südafrikanisch, Südafrikaner, Südafrika';
					$i++;$array_land[$i] = 'sudanesisch, Sudanese, Sudan';
					$i++;$array_land[$i] = 'surinamisch, Surinamer, Surinam';
					$i++;$array_land[$i] = 'swasiländisch, Swasi, Swasiland';
					$i++;$array_land[$i] = 'syrisch, Syrer, Syrien';
					$i++;$array_land[$i] = 'tadschikisch, Tadschike, Tadschikistan';
					$i++;$array_land[$i] = 'taiwanisch, Taiwaner, Taiwan';
					$i++;$array_land[$i] = 'tansanisch, Tansanier, Tansania';
					$i++;$array_land[$i] = 'thailändisch, Thailänder, Thailand';
					$i++;$array_land[$i] = 'togoisch, Togoer, Togo';
					$i++;$array_land[$i] = 'tongaisch, Tongaer, Tonga';
					$i++;$array_land[$i] = 'tschadisch, Tschader, Tschad';
					$i++;$array_land[$i] = 'tschechisch, Tscheche, Tschechien';
					$i++;$array_land[$i] = 'tunesisch, Tunesier, Tunesien';
					$i++;$array_land[$i] = 'türkisch, Türke, Türkei';
					$i++;$array_land[$i] = 'turkmenisch, Turkmenistan, Turkmene';
					$i++;$array_land[$i] = 'tuvaluisch, Tuvaluer, Tuvalu';
					$i++;$array_land[$i] = 'ugandisch, Ugander, Uganda';
					$i++;$array_land[$i] = 'ukrainisch, Ukrainer, Ukraine';
					$i++;$array_land[$i] = 'ungarisch, Ungar, Ungarn';
					$i++;$array_land[$i] = 'uruguayisch, Uruguayer, Uruguay';
					$i++;$array_land[$i] = 'usbekisch, Usbeke, Usbekistan';
					$i++;$array_land[$i] = 'vanuatuisch, Vanuatuer, Vanuatu';
					$i++;$array_land[$i] = 'vatikanisch, Vatikan';
					$i++;$array_land[$i] = 'venezolanisch, Venezolaner, Venezuela';
					$i++;$array_land[$i] = 'US-amerikanisch, Amerikaner, US-amerikanisch, Amerika, US-Amerikaner, Vereinigte Staaten, USA';
					$i++;$array_land[$i] = 'amerikanisch, Amerikaner, US-amerikanisch, Amerika, US-Amerikaner, Vereinigte Staaten, USA';
					$i++;$array_land[$i] = 'vietnamesisch, Vietnamese, Vietnam';
					$i++;$array_land[$i] = 'saharauisch, Sahraui, Sahara, Westsahara';
					$i++;$array_land[$i] = 'zentralafrikanisch, Zentralafrikaner, Zentralafrikanische';
					$i++;$array_land[$i] = 'zyprisch, Zyprer, Zypern';
					$i++;$array_land[$i] = 'zypriotisch, Zyprer, Zypern';
					$i++;$array_land[$i] = 'amerikanisch, Amerikaner, US-amerikanisch, Amerika, US-Amerikaner';
					$i++;$array_land[$i] = 'tschechisch, Tscheche, Tschechien, tschechoslowakisch, tschechoslowakisch, Tschechoslowakei';
					$i++;$array_land[$i] = 'tschechoslowakisch, tschechoslowakisch, Tschechoslowakei, tschechisch, Tscheche, Tschechien';
					$i++;$array_land[$i] = 'altägyptisch, ägyptisch, Ägypter, Ägypten';
					$i++;$array_land[$i] = 'römisch, Römer, Rom';
					$i++;$array_land[$i] = 'grönländisch, Grönländer, Grönland';
					$i++;$array_land[$i] = 'sowjetisch, sowjetisch, Sowjetbürger, Sowjetunion, russisch, Russe, Russland';
					$i++;$array_land[$i] = 'jugoslawisch, Jugoslawe, Jugoslawien';
					$i++;$array_land[$i] = 'persisch, Perser, Persien, Iran, iranisch';
					$i++;$array_land[$i] = 'holländisch, Holländer, Holland, Niederlande, Niederländer, niederländisch';
					


					#$i++;$array_land[$i] = 'englisch, Engländer, England, Vereinigtes Königreich, britisch, Brite, Großbritannien';
					#$i++;$array_land[$i] = 'britisch, Brite, Großbritannien, England, Britannien, Vereinigtes Königreich';
					#$i++;$array_land[$i] = 'schottisch,Schotte, Schottland, Vereinigtes Königreich, britisch, Brite, Großbritannien';
					#$i++;$array_land[$i] = 'nordirisch, Nordire, Nordirland, Vereinigtes Königreich, britisch, Brite, Großbritannien,';

					$i++;$array_land[$i] = 'britisch, britisch, Brite, Großbritannien, englisch, Engländer, England, Britannien, Vereinigtes Königreich, schottisch, Schotte, Schottland, nordirisch, Nordire, Nordirland, walisisch, Waliser, Wales';
					 
	
					my $check1 = 'false';	# Nationalität in Kurzbeschreibung
					my $check2 = 'false';	# Nationalität in Kategorie			
					foreach (@array_land) {
						my $test = $_;
						
						my @search = split (/, /, $test);
						# print $search[0]."\n";


						if (index ($first,lc($search[0])) > -1) {
							#print "\t".'found: '.$search[0]."\n";
							#shift(@search);
							$check1 = 'true';


							foreach	(@search) {
								my $search2 = $_;
								$search2 = lc($search2);
								#print "\t".'check: '.$search2."\n";
								$check2 = 'true' if ( index (lc($all_categories), $search2 ) > -1 );
							}					
						}


					}


	
					my $notice = $first.' - '.$all_categories;
					if ($check1 eq 'true' and $check2 eq 'false' ) {
						fehlermeldung($error_code, $title, $notice);
						#print $error_code."\t".$title."\t".'<nowiki>'. $notice.'</nowiki>'."\n";
						

					}
				}
			}

		}
	}
}


sub F269_Kategorienanzahl_zu_niedrig {
	my $error_code = 269; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Kategorienanzahl zu niedrig';
				$error_description[$error_code][1] = 'Bei einem Artikel mit Personendaten es muss neben der Geschlechts- (Mann, Frau, Intersexueller), Nationalität- (Hauptkategorien von Kategorie:Person nach Staatsangehörigkeit, Kategorie:Person eines historischen Volkes, Kategorie:Person nach Ethnie und Kategorie:Historische Person nach Region</a>), Geburts- und Sterbekategorie mindestens eine weitere Kategorie vorhanden sein, die die Tätigkeit bezeichnet. Bei lebenden Personen müssten also demnach 4 Kategorien, bei verstorbenen Personen 5 Kategorien zu finden sein. Siehe auch <a href="http://de.wikipedia.org/wiki/Wikipedia:Formatvorlage Biografie">Wikipedia:Formatvorlage Biografie</a>.';
				$error_description[$error_code][2] = 1;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {

		
		
		my @kategorien = split ( /\t/, $all_categories);
		
		#my $anzahl= @kategorien;
		#$anzahl = $anzahl -1;
		
		my $rest_cat = '';
				
		foreach (@kategorien) {
			my $current_categorie = $_;
			$current_categorie =~ s/\[\[Kategorie://g;
			$current_categorie =~ s/\|.+//g;
			$current_categorie =~ s/\]\]//g;
			#print $current_categorie ."\n";
			
			
			# Geschlecht
			my $kat_geschlecht_found = 'false';
			$kat_geschlecht_found = 'true' if ($current_categorie eq 'Mann' );
			$kat_geschlecht_found = 'true' if ($current_categorie eq 'Frau' );
			$kat_geschlecht_found = 'true' if ($current_categorie eq 'Intersexueller' );
			$kat_geschlecht_found = 'true' if ($current_categorie eq 'Geschlecht unbekannt' );
			
			# Geboren
			my $kat_geboren_found = 'false';
			$kat_geboren_found = 'true' if (index ($current_categorie, 'Geboren') == 0 );

			# Gestorben
			my $kat_gestorben_found = 'false';
			$kat_gestorben_found = 'true' if (index ($current_categorie, 'Gestorben')  > -1 );		

			# Nationalität
			my $kat_nat_found = 'false';
			$kat_nat_found = 'true' if ($current_categorie eq 'Afghane');
			$kat_nat_found = 'true' if ($current_categorie eq 'Ägypter');
			$kat_nat_found = 'true' if ($current_categorie eq 'Albaner');
			$kat_nat_found = 'true' if ($current_categorie eq 'Algerier');
			$kat_nat_found = 'true' if ($current_categorie eq 'Andorraner');
			$kat_nat_found = 'true' if ($current_categorie eq 'Angolaner');
			$kat_nat_found = 'true' if ($current_categorie eq 'Antiguaner');
			$kat_nat_found = 'true' if ($current_categorie eq 'Äquatorialguineer');
			$kat_nat_found = 'true' if ($current_categorie eq 'Argentinier');
			$kat_nat_found = 'true' if ($current_categorie eq 'Armenier');
			$kat_nat_found = 'true' if ($current_categorie eq 'Aserbaidschaner');
			$kat_nat_found = 'true' if ($current_categorie eq 'Äthiopier');
			$kat_nat_found = 'true' if ($current_categorie eq 'Australier');
			$kat_nat_found = 'true' if ($current_categorie eq 'Bahamaer');
			$kat_nat_found = 'true' if ($current_categorie eq 'Bahrainer');
			$kat_nat_found = 'true' if ($current_categorie eq 'Bangladescher');
			$kat_nat_found = 'true' if ($current_categorie eq 'Barbadier');
			$kat_nat_found = 'true' if ($current_categorie eq 'Belgier');
			$kat_nat_found = 'true' if ($current_categorie eq 'Belizer');
			$kat_nat_found = 'true' if ($current_categorie eq 'Beniner');
			$kat_nat_found = 'true' if ($current_categorie eq 'Bhutaner');
			$kat_nat_found = 'true' if ($current_categorie eq 'Bolivianer');
			$kat_nat_found = 'true' if ($current_categorie eq 'Bosnier');
			$kat_nat_found = 'true' if ($current_categorie eq 'Botsuaner');
			$kat_nat_found = 'true' if ($current_categorie eq 'Brasilianer');
			$kat_nat_found = 'true' if ($current_categorie eq 'Brite');
			$kat_nat_found = 'true' if ($current_categorie eq 'Bruneier');
			$kat_nat_found = 'true' if ($current_categorie eq 'Bulgare');
			$kat_nat_found = 'true' if ($current_categorie eq 'Burkiner');
			$kat_nat_found = 'true' if ($current_categorie eq 'Burundier');
			$kat_nat_found = 'true' if ($current_categorie eq 'Chilene');
			$kat_nat_found = 'true' if ($current_categorie eq 'Chinese');
			$kat_nat_found = 'true' if ($current_categorie eq 'Costa-Ricaner');
			$kat_nat_found = 'true' if ($current_categorie eq 'Däne');
			$kat_nat_found = 'true' if ($current_categorie eq 'Danziger');
			$kat_nat_found = 'true' if ($current_categorie eq 'Person (Demokratische Republik Kongo)');
			$kat_nat_found = 'true' if ($current_categorie eq 'Deutscher');
			$kat_nat_found = 'true' if ($current_categorie eq 'Dominicaner');
			$kat_nat_found = 'true' if ($current_categorie eq 'Person (Dominikanische Republik)');
			$kat_nat_found = 'true' if ($current_categorie eq 'Dschibutier');
			$kat_nat_found = 'true' if ($current_categorie eq 'Ecuadorianer');
			$kat_nat_found = 'true' if ($current_categorie eq 'Eritreer');
			$kat_nat_found = 'true' if ($current_categorie eq 'Este');
			$kat_nat_found = 'true' if ($current_categorie eq 'Färinger');
			$kat_nat_found = 'true' if ($current_categorie eq 'Fidschianer');
			$kat_nat_found = 'true' if ($current_categorie eq 'Finne');
			$kat_nat_found = 'true' if ($current_categorie eq 'Franzose');
			$kat_nat_found = 'true' if ($current_categorie eq 'Gabuner');
			$kat_nat_found = 'true' if ($current_categorie eq 'Gambier');
			$kat_nat_found = 'true' if ($current_categorie eq 'Georgier');
			$kat_nat_found = 'true' if ($current_categorie eq 'Ghanaer');
			$kat_nat_found = 'true' if ($current_categorie eq 'Grenader');
			$kat_nat_found = 'true' if ($current_categorie eq 'Griechischer Staatsbürger');
			$kat_nat_found = 'true' if ($current_categorie eq 'Grönländer');
			$kat_nat_found = 'true' if ($current_categorie eq 'Guatemalteke');
			$kat_nat_found = 'true' if ($current_categorie eq 'Guinea-Bissauer');
			$kat_nat_found = 'true' if ($current_categorie eq 'Guineer');
			$kat_nat_found = 'true' if ($current_categorie eq 'Guyaner');
			$kat_nat_found = 'true' if ($current_categorie eq 'Haitianer');
			$kat_nat_found = 'true' if ($current_categorie eq 'Honduraner');
			$kat_nat_found = 'true' if ($current_categorie eq 'Inder');
			$kat_nat_found = 'true' if ($current_categorie eq 'Indonesier');
			$kat_nat_found = 'true' if ($current_categorie eq 'Iraker');
			$kat_nat_found = 'true' if ($current_categorie eq 'Iraner');
			$kat_nat_found = 'true' if ($current_categorie eq 'Ire');
			$kat_nat_found = 'true' if ($current_categorie eq 'Isländer');
			$kat_nat_found = 'true' if ($current_categorie eq 'Israeli');
			$kat_nat_found = 'true' if ($current_categorie eq 'Italiener');
			$kat_nat_found = 'true' if ($current_categorie eq 'Ivorer');
			$kat_nat_found = 'true' if ($current_categorie eq 'Jamaikaner');
			$kat_nat_found = 'true' if ($current_categorie eq 'Japaner');
			$kat_nat_found = 'true' if ($current_categorie eq 'Jemenit');
			$kat_nat_found = 'true' if ($current_categorie eq 'Jordanier');
			$kat_nat_found = 'true' if ($current_categorie eq 'Jugoslawe');
			$kat_nat_found = 'true' if ($current_categorie eq 'Kambodschaner');
			$kat_nat_found = 'true' if ($current_categorie eq 'Kameruner');
			$kat_nat_found = 'true' if ($current_categorie eq 'Kanadier');
			$kat_nat_found = 'true' if ($current_categorie eq 'Kap-Verdier');
			$kat_nat_found = 'true' if ($current_categorie eq 'Kasache');
			$kat_nat_found = 'true' if ($current_categorie eq 'Katarer');
			$kat_nat_found = 'true' if ($current_categorie eq 'Kenianer');
			$kat_nat_found = 'true' if ($current_categorie eq 'Kirgise');
			$kat_nat_found = 'true' if ($current_categorie eq 'Kiribatier');
			$kat_nat_found = 'true' if ($current_categorie eq 'Kolumbianer');
			$kat_nat_found = 'true' if ($current_categorie eq 'Komorer');
			$kat_nat_found = 'true' if ($current_categorie eq 'Koreaner');
			$kat_nat_found = 'true' if ($current_categorie eq 'Nordkoreaner');
			$kat_nat_found = 'true' if ($current_categorie eq 'Südkoreaner');
			$kat_nat_found = 'true' if ($current_categorie eq 'Kosovare');
			$kat_nat_found = 'true' if ($current_categorie eq 'Kroate');
			$kat_nat_found = 'true' if ($current_categorie eq 'Kubaner');
			$kat_nat_found = 'true' if ($current_categorie eq 'Kuwaiter');
			$kat_nat_found = 'true' if ($current_categorie eq 'Laote');
			$kat_nat_found = 'true' if ($current_categorie eq 'Lesother');
			$kat_nat_found = 'true' if ($current_categorie eq 'Lette');
			$kat_nat_found = 'true' if ($current_categorie eq 'Libanese');
			$kat_nat_found = 'true' if ($current_categorie eq 'Liberianer');
			$kat_nat_found = 'true' if ($current_categorie eq 'Libyer');
			$kat_nat_found = 'true' if ($current_categorie eq 'Liechtensteiner');
			$kat_nat_found = 'true' if ($current_categorie eq 'Litauer');
			$kat_nat_found = 'true' if ($current_categorie eq 'Lucianer');
			$kat_nat_found = 'true' if ($current_categorie eq 'Luxemburger');
			$kat_nat_found = 'true' if ($current_categorie eq 'Madagasse');
			$kat_nat_found = 'true' if ($current_categorie eq 'Malawier');
			$kat_nat_found = 'true' if ($current_categorie eq 'Malaysier');
			$kat_nat_found = 'true' if ($current_categorie eq 'Malediver');
			$kat_nat_found = 'true' if ($current_categorie eq 'Malier');
			$kat_nat_found = 'true' if ($current_categorie eq 'Malteser');
			$kat_nat_found = 'true' if ($current_categorie eq 'Marokkaner');
			$kat_nat_found = 'true' if ($current_categorie eq 'Marshaller');
			$kat_nat_found = 'true' if ($current_categorie eq 'Mauretanier');
			$kat_nat_found = 'true' if ($current_categorie eq 'Mauritier');
			$kat_nat_found = 'true' if ($current_categorie eq 'Mazedonier');
			$kat_nat_found = 'true' if ($current_categorie eq 'Mexikaner');
			$kat_nat_found = 'true' if ($current_categorie eq 'Mikronesier');
			$kat_nat_found = 'true' if ($current_categorie eq 'Moldawier');
			$kat_nat_found = 'true' if ($current_categorie eq 'Monegasse');
			$kat_nat_found = 'true' if ($current_categorie eq 'Mongole');
			$kat_nat_found = 'true' if ($current_categorie eq 'Montenegriner');
			$kat_nat_found = 'true' if ($current_categorie eq 'Mosambikaner');
			$kat_nat_found = 'true' if ($current_categorie eq 'Myanmare');
			$kat_nat_found = 'true' if ($current_categorie eq 'Namibier');
			$kat_nat_found = 'true' if ($current_categorie eq 'Nauruer');
			$kat_nat_found = 'true' if ($current_categorie eq 'Nepalese');
			$kat_nat_found = 'true' if ($current_categorie eq 'Neuseeländer');
			$kat_nat_found = 'true' if ($current_categorie eq 'Nicaraguaner');
			$kat_nat_found = 'true' if ($current_categorie eq 'Niederländer');
			$kat_nat_found = 'true' if ($current_categorie eq 'Nigerianer');
			$kat_nat_found = 'true' if ($current_categorie eq 'Nigrer');
			$kat_nat_found = 'true' if ($current_categorie eq 'Niueaner');
			$kat_nat_found = 'true' if ($current_categorie eq 'Norweger');
			$kat_nat_found = 'true' if ($current_categorie eq 'Omaner');
			$kat_nat_found = 'true' if ($current_categorie eq 'Österreicher');
			$kat_nat_found = 'true' if ($current_categorie eq 'Osttimorese');
			$kat_nat_found = 'true' if ($current_categorie eq 'Pakistaner');
			$kat_nat_found = 'true' if ($current_categorie eq 'Palauer');
			$kat_nat_found = 'true' if ($current_categorie eq 'Panamaer');
			$kat_nat_found = 'true' if ($current_categorie eq 'Papua-Neuguineer');
			$kat_nat_found = 'true' if ($current_categorie eq 'Paraguayer');
			$kat_nat_found = 'true' if ($current_categorie eq 'Peruaner');
			$kat_nat_found = 'true' if ($current_categorie eq 'Philippiner');
			$kat_nat_found = 'true' if ($current_categorie eq 'Pole');
			$kat_nat_found = 'true' if ($current_categorie eq 'Portugiese');
			$kat_nat_found = 'true' if ($current_categorie eq 'Puertoricaner');
			$kat_nat_found = 'true' if ($current_categorie eq 'Person (Republik Kongo)');
			$kat_nat_found = 'true' if ($current_categorie eq 'Ruander');
			$kat_nat_found = 'true' if ($current_categorie eq 'Rumäne');
			$kat_nat_found = 'true' if ($current_categorie eq 'Russe');
			$kat_nat_found = 'true' if ($current_categorie eq 'Salomoner');
			$kat_nat_found = 'true' if ($current_categorie eq 'Salvadorianer');
			$kat_nat_found = 'true' if ($current_categorie eq 'Sambier');
			$kat_nat_found = 'true' if ($current_categorie eq 'Samoaner');
			$kat_nat_found = 'true' if ($current_categorie eq 'San-Marinese');
			$kat_nat_found = 'true' if ($current_categorie eq 'São-Toméer');
			$kat_nat_found = 'true' if ($current_categorie eq 'Saudi-Araber');
			$kat_nat_found = 'true' if ($current_categorie eq 'Schwede');
			$kat_nat_found = 'true' if ($current_categorie eq 'Schweizer');
			$kat_nat_found = 'true' if ($current_categorie eq 'Senegalese');
			$kat_nat_found = 'true' if ($current_categorie eq 'Serbe');
			$kat_nat_found = 'true' if ($current_categorie eq 'Seycheller');
			$kat_nat_found = 'true' if ($current_categorie eq 'Sierra-Leoner');
			$kat_nat_found = 'true' if ($current_categorie eq 'Simbabwer');
			$kat_nat_found = 'true' if ($current_categorie eq 'Singapurer');
			$kat_nat_found = 'true' if ($current_categorie eq 'Slowake');
			$kat_nat_found = 'true' if ($current_categorie eq 'Slowene');
			$kat_nat_found = 'true' if ($current_categorie eq 'Somalier');
			$kat_nat_found = 'true' if ($current_categorie eq 'Sowjetbürger');
			$kat_nat_found = 'true' if ($current_categorie eq 'Spanier');
			$kat_nat_found = 'true' if ($current_categorie eq 'Sri-Lanker');
			$kat_nat_found = 'true' if ($current_categorie eq 'Person (St. Kitts und Nevis)');
			$kat_nat_found = 'true' if ($current_categorie eq 'Südafrikaner');
			$kat_nat_found = 'true' if ($current_categorie eq 'Sudanese');
			$kat_nat_found = 'true' if ($current_categorie eq 'Surinamer');
			$kat_nat_found = 'true' if ($current_categorie eq 'Swasi');
			$kat_nat_found = 'true' if ($current_categorie eq 'Syrer');
			$kat_nat_found = 'true' if ($current_categorie eq 'Tadschike');
			$kat_nat_found = 'true' if ($current_categorie eq 'Tansanier');
			$kat_nat_found = 'true' if ($current_categorie eq 'Thailänder');
			$kat_nat_found = 'true' if ($current_categorie eq 'Togoer');
			$kat_nat_found = 'true' if ($current_categorie eq 'Tongaer');
			$kat_nat_found = 'true' if ($current_categorie eq 'Person (Trinidad und Tobago)');
			$kat_nat_found = 'true' if ($current_categorie eq 'Tschader');
			$kat_nat_found = 'true' if ($current_categorie eq 'Tscheche');
			$kat_nat_found = 'true' if ($current_categorie eq 'Tschechoslowake');
			$kat_nat_found = 'true' if ($current_categorie eq 'Tunesier');
			$kat_nat_found = 'true' if ($current_categorie eq 'Türke');
			$kat_nat_found = 'true' if ($current_categorie eq 'Turkmene');
			$kat_nat_found = 'true' if ($current_categorie eq 'Tuvaluer');
			$kat_nat_found = 'true' if ($current_categorie eq 'Sahraui');
			$kat_nat_found = 'true' if ($current_categorie eq 'Zentralafrikaner');
			$kat_nat_found = 'true' if ($current_categorie eq 'Zyprer');
			$kat_nat_found = 'true' if ($current_categorie eq 'Ugander');
			$kat_nat_found = 'true' if ($current_categorie eq 'Ukrainer');
			$kat_nat_found = 'true' if ($current_categorie eq 'Ungar');
			$kat_nat_found = 'true' if ($current_categorie eq 'Uruguayer');
			$kat_nat_found = 'true' if ($current_categorie eq 'US-Amerikaner');
			$kat_nat_found = 'true' if ($current_categorie eq 'Usbeke');
			$kat_nat_found = 'true' if ($current_categorie eq 'Vanuatuer');
			$kat_nat_found = 'true' if ($current_categorie eq 'Venezolaner');
			$kat_nat_found = 'true' if ($current_categorie eq 'Person (Vereinigte Arabische Emirate)');
			$kat_nat_found = 'true' if ($current_categorie eq 'Vietnamese');
			$kat_nat_found = 'true' if ($current_categorie eq 'Vincenter');
			$kat_nat_found = 'true' if ($current_categorie eq 'Weißrusse');
			$kat_nat_found = 'true' if ($current_categorie eq 'Altägypter');
			$kat_nat_found = 'true' if ($current_categorie eq 'Angelsachse');
			$kat_nat_found = 'true' if ($current_categorie eq 'Araber');
			$kat_nat_found = 'true' if ($current_categorie eq 'Assyrer');
			$kat_nat_found = 'true' if ($current_categorie eq 'Azteke');
			$kat_nat_found = 'true' if ($current_categorie eq 'Babylonier');
			$kat_nat_found = 'true' if ($current_categorie eq 'Burgunder');
			$kat_nat_found = 'true' if ($current_categorie eq 'Byzantiner');
			$kat_nat_found = 'true' if ($current_categorie eq 'Etrusker');
			$kat_nat_found = 'true' if ($current_categorie eq 'Gallier');
			$kat_nat_found = 'true' if ($current_categorie eq 'Germane');
			$kat_nat_found = 'true' if ($current_categorie eq 'Grieche (Antike)');
			$kat_nat_found = 'true' if ($current_categorie eq 'Hawaiianer');
			$kat_nat_found = 'true' if ($current_categorie eq 'Hunne');
			$kat_nat_found = 'true' if ($current_categorie eq 'Person (Inka-Reich)');
			$kat_nat_found = 'true' if ($current_categorie eq 'Person (Inuit)');
			$kat_nat_found = 'true' if ($current_categorie eq 'Karthager');
			$kat_nat_found = 'true' if ($current_categorie eq 'Kelte');
			$kat_nat_found = 'true' if ($current_categorie eq 'Makedone');
			$kat_nat_found = 'true' if ($current_categorie eq 'Meder');
			$kat_nat_found = 'true' if ($current_categorie eq 'Normanne');
			$kat_nat_found = 'true' if ($current_categorie eq 'Perser');
			$kat_nat_found = 'true' if ($current_categorie eq 'Sassanide');
			$kat_nat_found = 'true' if ($current_categorie eq 'Skythe');
			$kat_nat_found = 'true' if ($current_categorie eq 'Wikinger');
			$kat_nat_found = 'true' if ($current_categorie eq 'Aramäer');
			$kat_nat_found = 'true' if ($current_categorie eq 'Bengale');
			$kat_nat_found = 'true' if ($current_categorie eq 'Deutsch-Balte');
			$kat_nat_found = 'true' if ($current_categorie eq 'Friese');
			$kat_nat_found = 'true' if ($current_categorie eq 'Grieche');
			$kat_nat_found = 'true' if ($current_categorie eq 'Indianer');
			$kat_nat_found = 'true' if ($current_categorie eq 'Person (Inka-Reich)');
			$kat_nat_found = 'true' if ($current_categorie eq 'Person (Inuit)');
			$kat_nat_found = 'true' if ($current_categorie eq 'Kaschube');
			$kat_nat_found = 'true' if ($current_categorie eq 'Kurde');
			$kat_nat_found = 'true' if ($current_categorie eq 'Ladiner');
			$kat_nat_found = 'true' if ($current_categorie eq 'Maya-Persönlichkeit');
			$kat_nat_found = 'true' if ($current_categorie eq 'Muisca-Persönlichkeit');
			$kat_nat_found = 'true' if ($current_categorie eq 'Maori (Person)');
			$kat_nat_found = 'true' if ($current_categorie eq 'Paschtune');
			$kat_nat_found = 'true' if ($current_categorie eq 'Quechua-Persönlichkeit');
			$kat_nat_found = 'true' if ($current_categorie eq 'Roma (Person)');
			$kat_nat_found = 'true' if ($current_categorie eq 'Russlanddeutscher');
			$kat_nat_found = 'true' if ($current_categorie eq 'Sinto');
			$kat_nat_found = 'true' if ($current_categorie eq 'Sorbe');
			$kat_nat_found = 'true' if ($current_categorie eq 'Tamile');
			$kat_nat_found = 'true' if ($current_categorie eq 'Tibeter');
			$kat_nat_found = 'true' if ($current_categorie eq 'Tuwiner');
			$kat_nat_found = 'true' if ($current_categorie eq 'Historische Person (Baden-Württemberg)');
			$kat_nat_found = 'true' if ($current_categorie eq 'Historische Person (Italien)');
			$kat_nat_found = 'true' if ($current_categorie eq 'Person (Osmanisches Reich)');
			$kat_nat_found = 'true' if ($current_categorie eq 'Person (Polen-Litauen)');
			$kat_nat_found = 'true' if ($current_categorie eq 'Person (Königlich-Preußen)');
			$kat_nat_found = 'true' if ($current_categorie eq 'Römer');
			$kat_nat_found = 'true' if ($current_categorie eq 'Historische Person (Südliches Tirol)');
			$kat_nat_found = 'true' if ($current_categorie eq 'Historische Person (Südosteuropa)');
			
			$kat_nat_found = 'true' if ($current_categorie eq 'Armenier (Osmanisches Reich)');
			$kat_nat_found = 'true' if ($current_categorie eq 'Chinese (Hongkong)');
			$kat_nat_found = 'true' if ($current_categorie eq 'Taiwaner');
			$kat_nat_found = 'true' if ($current_categorie eq 'Moldauer');
			$kat_nat_found = 'true' if ($current_categorie eq 'Alamanne');
			$kat_nat_found = 'true' if ($current_categorie eq 'Cherusker');
			$kat_nat_found = 'true' if ($current_categorie eq 'Franke');
			$kat_nat_found = 'true' if ($current_categorie eq 'Gote');
			$kat_nat_found = 'true' if ($current_categorie eq 'Langobarde');
			$kat_nat_found = 'true' if ($current_categorie eq 'Vandale');
			$kat_nat_found = 'true' if ($current_categorie eq 'Spartaner');
			$kat_nat_found = 'true' if ($current_categorie eq 'Perser der Antike');
			$kat_nat_found = 'true' if ($current_categorie eq 'Achämeniden');
			$kat_nat_found = 'true' if ($current_categorie eq 'Nordfriese');
			$kat_nat_found = 'true' if ($current_categorie eq 'Ostfriese');
			$kat_nat_found = 'true' if ($current_categorie eq 'Grieche (Rhodos)');
			$kat_nat_found = 'true' if ($current_categorie eq 'Apache');
			$kat_nat_found = 'true' if ($current_categorie eq 'Shawnee-Indianer');
			$kat_nat_found = 'true' if ($current_categorie eq 'Burgenland-Roma (Person)');
			$kat_nat_found = 'true' if ($current_categorie eq 'Person (Baden)');
			$kat_nat_found = 'true' if ($current_categorie eq 'Person (Hohenzollernsche Lande)');
			$kat_nat_found = 'true' if ($current_categorie eq 'Person (Württemberg)');
			$kat_nat_found = 'true' if ($current_categorie eq 'Person (Altwürttemberg)');
			$kat_nat_found = 'true' if ($current_categorie eq 'Osmane');
			$kat_nat_found = 'true' if ($current_categorie eq 'Person in der Konföderation von Targowica');
			$kat_nat_found = 'true' if ($current_categorie eq 'Historische Person (Bulgarisches Reich)');



						
			#print 'Geschlecht found = '.$kat_geschlecht_found ."\n";
			#print 'Geboren found = '.$kat_geboren_found ."\n";
			#print 'Gestorben found = '.$kat_gestorben_found ."\n";
			#print 'Nationalität found = '.$kat_nat_found ."\n";
			
			
			my $is_rest_cat = 'true';
			
			$is_rest_cat = 'false' if ($kat_geschlecht_found eq 'true'); 
			$is_rest_cat = 'false' if ($kat_geboren_found eq 'true');
			$is_rest_cat = 'false' if ($kat_gestorben_found eq 'true');
			$is_rest_cat = 'false' if ($kat_nat_found eq 'true');
			
			if ($is_rest_cat eq 'true') {
				$rest_cat = $rest_cat.', '.$current_categorie ;
			}
		
		}

		
		
		if ($rest_cat eq '' ){
			my $notice = '';
			foreach (@kategorien) {
				my $current_categorie = $_;
				$current_categorie =~ s/\[\[Kategorie://g;
				$current_categorie =~ s/\|.+//g;
				$current_categorie =~ s/\]\]//g;
				#print $current_categorie ."\n";
				$notice = $notice.', '.$current_categorie;
			}
			$notice =~ s/^, //;
			fehlermeldung($error_code, $title, $notice);
			#print $error_code."\t".$title."\t".'<nowiki>'. $name.'</nowiki>'."\n";
			#print $notice."\n";
		}
				
	}
}


sub	F270_Kategorie_mit_Leerzeichen{
	my $error_code = 270; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Kategorienanzahl mit Leerzeichen';
				$error_description[$error_code][1] = 'Es gibt in dem Artikel mindestens eine Kategorie, die ein Leerzeichen am Anfang oder am Ende aufweist. Diese Leerzeichen sind zu vermeiden. Siehe auch <a href="http://de.wikipedia.org/wiki/Hilfe:Kategorien">Hilfe:Kategorien</a>)';
				$error_description[$error_code][2] = 3;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
		my @kategorien = split ( /\t/, $all_categories);
		my $rest_cat = '';

		my $found_cat = '';			
		foreach (@kategorien) {
			my $current_categorie = $_;
			$current_categorie =~ s/\[\[Kategorie://g;
			$current_categorie =~ s/\|.+//g;
			$current_categorie =~ s/\]\]//g;
			if ($current_categorie =~ /^ / or $current_categorie =~ / $/) {
				$found_cat = $current_categorie;
			}
		}
		if ($found_cat ne '') {
			fehlermeldung($error_code, $title, $found_cat);
			#print $error_code."\t".$title."\t".'<nowiki>'. $found_cat.'</nowiki>'."\n";
			#print $notice."\n";
		}
	}
}

sub F271_Kategorie_geboren_oder_gestorben_ungenau {
	my $error_code = 271; 
	my $attribut = $_[0];
	if ($attribut eq 'get_description') {
				$error_description[$error_code][0] = 'Kategorie geboren oder gestorben ungenau';
				$error_description[$error_code][1] = 'Wenn im Datenfeld GEBURTSDATUM oder STERBEDATUM ein ungenaues Jahr steht (z.B. "um 1234"), dann sollte die entsprechende Kategorie nicht "Geboren 1234" sondern "Geboren im 13. Jahrhundert" heißen.';
				$error_description[$error_code][2] = 2;
	}
	if ($attribut eq 'check' and $error_description[$error_code][2] != 0 ) {
	
		my $found = F271_Kategorie_geboren_oder_gestorben_ungenau_2($error_code, 'geb', $geburtsdatum_plain);
		if ($found ne 'true') {
		            F271_Kategorie_geboren_oder_gestorben_ungenau_2($error_code, 'gest', $sterbedatum_plain);
		}
	}
}

sub F271_Kategorie_geboren_oder_gestorben_ungenau_2{
	my $error_code = shift;	
	my $test_was = shift;
	my $test_datum = shift;


	my @kategorien = split ( /\t/, $all_categories);
	my $rest_cat = '';

	# get categorie ( 'Geboren 1871' or 'Gestorben 1821' )
	my $found_cat = '';			
	foreach (@kategorien) {
		my $current_categorie = $_;
		$current_categorie =~ s/\[\[Kategorie://g;
		$current_categorie =~ s/\|.+//g;
		$current_categorie =~ s/\]\]//g;
		
		if ($test_was eq 'geb' and $current_categorie =~ /^Geboren [^i]/ ){
			$found_cat = $current_categorie;
		}

		if ($test_was eq 'gest' and $current_categorie =~ /^Gestorben [^i]/	){
			$found_cat = $current_categorie;
		}
	}

	my $notice = '';

	# ungenaues Datum
	# um 1981 vs. Kategorie 1981
	# um 1000
	# unsicher: um 550 v. Chr.
	# unsicher: um 1603 

	if ($test_datum =~ /um /
		and not $test_datum =~/(Januar|Februar|März|April|Mai|Juni|Juli|August|September|Oktober|November|Dezember)/g 
		and $found_cat ne '') {
		$notice = 'Kategorie:'.$found_cat.' → '.$test_datum;
	}

	#zwischen und / oder
	#zwischen 1604 und 1610
	#zwischen 100 v. Chr. und 350
	#zwischen 20. Dezember 1459 und 21. März 1460
	#zwischen Dezember 1459 und März 1460
	#zwischen 1. Jahrhundert v. Chr. und 3. Jahrhundert
	#zwischen 2. Jahrhundert v. Chr. und 1. Jahrhundert
	#1766 oder um 1768
	#10. Januar 1811 oder 1812
	#10. März 1988 oder 15. März 1989
	#1092 oder 1093
	if ($test_datum =~ /(zwischen|oder) /
		and $found_cat ne '') {
		$test_datum =~ /[0-9]+( Jahrhundert)?( v\. Chr.)? (und|oder)/ ;
		my $first_year = $&;
		$first_year =~ s/ (und|oder)//;
		$test_datum =~ /[0-9]+( Jahrhundert)?( v\. Chr.)?$/ ;
		my $last_year = $&;
		#print $test_datum."\n";
		#print $first_year."\n";
		#print $last_year."\n";
		if ($first_year ne $last_year) {
			$notice = 'Kategorie:'.$found_cat.' → '.$test_datum;
		}
	}

	if ($notice ne '') {
		fehlermeldung($error_code, $title, $notice);
		#print $error_code."\t".$title."\t".'<nowiki>'. $notice.'</nowiki>'."\n";
		#die;
		#print $notice."\n";
		return ('true');
	}
	return('false');

}
###################################################################################################################################################
###################################################################################################################################################
###################################################################################################################################################
###################################################################################################################################################


sub fehlermeldung {
	my $error_code = shift;
	my $article    = shift;
	my $notice     = shift;
	#print "\t". $error_code."\t".$article."\t".$notice."\n";
	
	if ($article ne 'Mann' and $article ne 'Frau') { 
		$this_article_is_with_errors = $this_article_is_with_errors + 1;
		$fehler_anzahl = $fehler_anzahl +1;
		$fehlerarticle[$fehler_anzahl][0] = $error_code;
		$fehlerarticle[$fehler_anzahl][1] = $article;
		$fehlerarticle[$fehler_anzahl][2] = $notice;
		$this_article_all_errors = $this_article_all_errors .', '.$error_code;
		$this_article_all_errors =~ s/^, //g;
		
		if ($parameter{'modus_nodb'}  ne 'nodb') {
			insert_pd_error_into_db($fehler_anzahl, $article, $error_code, $notice);
		}
	}
}	


sub insert_pd_error_into_db{
	my $error_id = shift;
	my $article  = shift;
	my $code     = shift;
	my $notice   = shift;
	$notice = substr($notice, 0, 3999) if (length($notice) > 3999);
	$notice =~ s/'/\\'/g;
	$article =~ s/'/\\'/g;
	
	my $revid = '';
	$revid = $page{'revid'} if ($page{'revid'});
	#print 'Notice:'.$notice."\n";

	# search for this error
	my $sql_text = "select 1 from pd_error where page_id = ".$page_id." and error_id = ".$code.";";
	my $sth = $dbh->prepare( $sql_text )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
	$sth->execute or die $sth->errstr; # hier geschieht die Anfrage an die DB
	my $found_error = 0;
	$found_error = 1 if ($sth->rows() > 0);	
	
	if ($found_error == 1) {

		# update old error
		my $sql_text = "update pd_error set notice = '".$notice."', done = 0, revid='".$revid."' where page_id = ".$page_id." and error_id = ".$code.";";
		#print $sql_text."\n"; 
		my $sth = $dbh->prepare( $sql_text )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
		$sth->execute or die $sth->errstr; # hier geschieht die Anfrage an die DB
	
	} else {
	
		# testen ob Leertitel gescannt wurde.
		if ($page_id ne '0') {	

			# insert new error
			my $sql_text = "insert into pd_error (project,page_id,title,error_id,notice,done,found,revid) values ( 'dewiki', ".$page_id.", '".$article."', ".$code.", '".$notice."', 0, now(), '".$revid."' );";
			print $sql_text."\n\n";# if ($page_id == 212635); 
			my $sth = $dbh->prepare( $sql_text )  ||  die "Kann Statement nicht vorbereiten: $DBI::errstr\n";
			$sth->execute or die $sth->errstr; # hier geschieht die Anfrage an die DB
		}
	}

}


############################################################
# Infotext Fuccy-Logic
############################################################

sub regelbasierter_hinweistext{
	my $erklaerung = $_[0];
	$erklaerung = $erklaerung."\n\n".'Da diese Fehlerrubrik auf unscharfen Regeln basiert (Stichwort Vagheit, Fuzzy-Logik), kann es sein, dass hier auch Artikel enthalten sind, die eigentlich nicht falsch sind. Die Fehlerliste kann deshalb eventuell nicht vollständig abgearbeitet werden.';
	return($erklaerung);
}

sub testphase_hinweistext{
	my $erklaerung = $_[0];
	$erklaerung = $erklaerung."\n\n".'<span style="color:#e80000;">Diese Fehlerrubrik befindet sich noch in der Testphase. Bitte auffälligen Fehler auf der Diskussionsseite melden.</span>';
	return($erklaerung);
}

sub Text_verkuerzen{
	my $input = $_[0];
	my $number = $_[1];
	my $output = '';
	#print $input."\n";
	if ( length($input) > $number) {
		# text verkürzen
		my $pos = index($input, ' ', $number);
		$output = substr($input, 0, $pos);
		
	} else {
		$output = $input;
	}
	#print $output."\n";
	return($output);
}

sub export_pd {
open(OUTPUT, ">>$output_pd_export_filename");
	print OUTPUT $title."\t";
	print OUTPUT $page_id."\t";
	print OUTPUT $name."\t";
	print OUTPUT $aname."\t";
	print OUTPUT $kurz."\t";
	print OUTPUT $kurz_plain."\t";
	print OUTPUT $geburtsdatum."\t";
	print OUTPUT $geburtsdatum_plain."\t";
	print OUTPUT $geburtstag."\t";
	print OUTPUT $geburtsmonat."\t";
	print OUTPUT $geburtsjahr."\t";
	print OUTPUT $geburtsort."\t";
	print OUTPUT $geburtsort_plain."\t";
	print OUTPUT $sterbedatum."\t";
	print OUTPUT $sterbedatum_plain."\t";
	print OUTPUT $sterbetag."\t";
	print OUTPUT $sterbemonat."\t";
	print OUTPUT $sterbejahr."\t";
	print OUTPUT $sterbeort."\t";
	print OUTPUT $sterbeort_plain."\t";
	
	my $output_cat = $all_categories;
	$output_cat =~ s/\t/@/g;
	print OUTPUT $output_cat."\t";
	print OUTPUT $is_redirect."\t";
	print OUTPUT $is_a_person."\t";
	print OUTPUT $is_a_noperson."\t";
	print OUTPUT $has_defaultsort."\t";
	print OUTPUT $defaultsort."\t";
	print OUTPUT $this_article_all_errors."\t";
	print OUTPUT $this_article_is_with_errors;
	print OUTPUT "\n";

close(OUTPUT);	

#make zip from txt
#my $zipfile = $output_pd_export_filename;
#$zipfile =~ s/csv\.txt/csv\.zip/g;
#my $ZipCommand = qx(zip $zipfile $output_pd_export_filename);
#print "$ZipCommand";
	
}


sub save_pd_in_database {
	# save the current scanned article with persondata in the database
	#TODO more data pur_text
	
	my %input;
	$input{'title'}  = $title;
	$input{'name'}  = $name;
	$input{'aname'}  = $aname;
	$input{'short'}  = $kurz;
	$input{'date_birth'}  = $geburtsdatum;
	$input{'place_birth'}  = $geburtsort;
	$input{'date_death'}  = $sterbedatum;
	$input{'place_death'}  = $sterbeort;
		

	# replace ' in text with \' then no problem with insert
	foreach my $key(keys %input){
		my $text = $input{$key};
		$text =~ s/'/\\'/g;
		$input{$key} = $text;
	}


	my $sql_text = "INSERT INTO pd 
	(dbname, page_id, title, revid, page_timestamp, redirect, persondata, name, aname, short, date_birth, place_birth, date_death, place_death, has_error) 
	VALUES 
	('dewiki', ".$page_id.", '".$input{'title'}."', '0', '0', '".$is_redirect."', '".$is_a_person."'
, '".$input{'name'}."', '".$input{'aname'}."', '".$input{'short'}."'
, '".$input{'date_birth'}."', '".$input{'place_birth'}."'
, '".$input{'date_death'}."', '".$input{'place_death'}."', ".$this_article_is_with_errors.") 
	ON DUPLICATE KEY UPDATE    
	title = VALUES(title), 
	name  = VALUES(name),
	aname  = VALUES(aname),
	short  = VALUES(short),
	date_birth  = VALUES(date_birth),
	place_birth  = VALUES(place_birth),
	date_death  = VALUES(date_death),
	place_death  = VALUES(place_death),
	has_error  = VALUES(has_error)
";

	
	# print $sql_text."\n";
	
	my $sth = $dbh->do( $sql_text );

	return;
}

#FINISH



