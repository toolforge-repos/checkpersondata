﻿#!/usr/bin/perl -w

my $error_log_path="../../data/error_logfile.txt";
my $protocol = 0;				#0=no protocol, 1=protocol at commandline




sub modulprint {
	#print path of moduls
	foreach (@INC) {
	 #print "$_\n";
	}
	print "\n";
	#print "Modul wikipedia.pm available \n";
	#print "modulprint2";
}

sub get_meta_from_dumpfilename {
	print "Extraxt meta\n" if ($protocol == 1);
	my $dump_filename = $_[0];
	my @meta;
	my $lenght = -1;
	if ( index($dump_filename,'/') > -1 ){
		@meta = split(/\//,$dump_filename);
		$lenght = @meta;
		#print "Length = $lenght \n";
		$dump_filename =$meta[$lenght-1];
	}
	if (index($dump_filename,'-') > -1) {
		@meta = split(/-/,$dump_filename);
		$lenght = @meta;
		#print join(", ", @meta);
	}
	else {
		die "No correkt filename (dewiki-20060803-pages-articles.xml)";
	}

	# split language (enwiki)
	#$meta[0] = substr($meta[0],0,2);
	$meta[0] =~ s/wiki//g;
	
	# split date (20060803)
	my $day = substr($meta[1],6,2);
	my $month = substr($meta[1],4,2);
	my $year = substr($meta[1],0,4);
	$meta[1] = $year."-".$month."-".$day;
	
	return @meta;
		#0 language (de,en)
		#1 2006-03-18
}

sub load_meta_from_dump {
	#this subroutine load the header with metadata from the dump
	print "Load keywords from dump\n" if ($protocol == 1);
	my $dump_filename = $_[0];
	#open dump
	open(DUMP, $dump_filename);
	my $line = "";
	my $meta_text ="";
	
	my @results ;
	
	do {
		$line = <DUMP>;
		$meta_text = $meta_text.$line;
		#print $line;
	}
	until( index ($line,'</siteinfo>' ) > -1) ;	

	#print $meta_text;
	#die;
	my @siteinfo = contents_of_first_xmltag ($meta_text, "siteinfo");
	my @base =  contents_of_first_xmltag ($siteinfo[1], "base");
	$results[0] = $base[1]; 
	#print "$results[0]\n";
	
	#load all keywords
	my $i = 0;
	my @namespace;
	do {
		$i = $i + 1;
		@namespace =  contents_of_first_xmltag ($siteinfo[1], "namespace");
		my $value = get_value_from_xml_attributes( $namespace[5],"key");
		#$results[$i] = "$value\t$namespace[1]";
		#$results[$i] = "";
		$results[$i] = "$namespace[1]";
		$siteinfo[1] = substr($siteinfo[1],$namespace[4] + length($namespace[2]) );
	}
	until ($namespace[6] == 0);
	close (DUMP);
	return @results;
	
}

sub get_value_from_xml_attributes {
	my $text = $_[0];
	my $attributname = $_[1];
	my $result = "";
	if ($text =~ /$attributname\b/) {
		#print "$attributname enthalten\n";
		my $pos1 = index ( $text, $attributname);
		$text = substr ($text,$pos1);
		#print $text."\n";
		my $pos2 = index ( $text, '"');
		$text = substr ($text,$pos2+1);
		#print $text."\n";
		my $pos3 = index ( $text, '"');
		$result =  substr ($text,0, $pos3);
	}
	#print $result."\n";
	return $result;
}


sub contents_of_first_xmltag {
	my $text = $_[0];
	my $tagname = $_[1];
	
	my $starttag = "";
	my $inside = ""; #inside the tags
	my $endtag = "";
	
	my $pos_starttag = -1;
	my $pos_endtag = -1;

	my $attributes_text="0";
	my $error = "";#error message;
	my $more_results = 0;
	
	my $start_eq_endtag = 0; #<test />
	
	my @result;
	
	
	#$tagname = "title";
	#$text = "xyz".'< html test="w"><head><     title      xxyz  =  "23"  y="26" z="asd" >Testausgabe</title ></head><body><h1>&Uuml;berschrift</h1></body></html>';
	#print "Original:$tagname \n$text\n\n" if ($tagname eq "namespace");
	
	#Filter tag
	if ($text =~ /([<][ ]*($tagname\b)[[ ]*|[ ]*>])/) {
		#tag is inside the text
		
		#cut all text before tag
		my $searchstring =$1;
		$pos_starttag = index ($text, $searchstring);
		$text = substr($text, $pos_starttag);
		#print "Start weg:\n$text\n\n";
		
		#search > from Starttag
		my $pos_starttag_end = index ($text, ">");
		if ($pos_starttag_end == -1) {
			error_message($language, "Syntax", "1", $page_id, $title, $revision_id, $revision_time, "no end of tag: $searchstring", "",""); 
		
			#print $result3."\n";
		}
		else{	
			#extract complett starttag
			$starttag = substr($text, 0, $pos_starttag_end + 1);
			#print "Starttag:\n$starttag\n\n";
			$start_eq_endtag = 1 if  ($starttag =~ /([\/][ ]*[>])/);
			#print $start_eq_endtag."\n";

			#test inside starttag for character "<"
			my$test= substr($starttag,1, length($starttag)-2);
			if (index($test, "<") > -1){
				error_message($language, "Syntax", "1", $page_id, $title, $revision_id, $revision_time, "found in starttag $searchstring the character < ", "",""); 
			}	
			else{
				#starttag is ok
				
				#extact attributes from starttag
				$attributes_text = substr($text, length($searchstring), $pos_starttag_end-length($searchstring) );
				$attributes_text =~ s/^[ ]*//g;
				$attributes_text =~ s/[ ]*$//g;
				
				

				#find endtag
		  #old: if ($text =~ /([<][\/][ ]*($tagname\b)[ ]*[>])/) {
				if ($start_eq_endtag == 0) {
					if ($text =~ /([<][\/][ ]*($tagname\b)[ ]*[>])/) {
						#endtag found
						$endtag = $1;
						#print "Endtag:\n$endtag\n\n";
						
						#extract inside between $starttag and $endtag
						$pos_endtag = index($text,$endtag);
						$inside = substr($text,length($starttag), $pos_endtag-length($starttag));
						#print "insid:\n$result2\n\n";
					}				
					else {
						#no endtag
						error_message($language, "Syntax", "1", $page_id, $title, $revision_id, $revision_time, 'starttag "'.$starttag.'" has no endtag "</'.$tagname.'>"', "",""); 
					}
				}
				else{
					#starttag == endtag
					$endtag = $starttag;
					$pos_endtag = $pos_starttag;
					$inside = "";
				}
			}			
		}

		#test for more results
		$text = substr($text,$pos_endtag + length($endtag) );
		if ($text =~ /([<][ ]*($tagname\b)[[ ]*|[ ]*>])/) {
			$more_results=1;
		}
		

	}
	
	#print $result3;

	$pos_endtag = $pos_starttag + $pos_endtag;
	
	$result[0] = $starttag;
	$result[1] = $inside; 
	$result[2] = $endtag;
	$result[3] = $pos_starttag;
	$result[4] = $pos_endtag;
	$result[5] = $attributes_text;
	$result[6] = $more_results;
	$result[7] = $error;

	return @result;
}


sub load_keywords {
	print "Load sprecial keywords\n" if ($protocol == 1);
	
	my $keyword_path = $_[0];
	my $language = $_[1];
	my $filename = $keyword_path.'/'.$language.'_input_keywords.txt';
	my  @keywords;
	my @test;
	#print $filename."\n";

	if (-e "$filename") {
		#print "$dump_filename existiert!\n";
	} else {
		die "$filename don't exist!\n";
	}
	open(KEYWORDS, $filename);
	
	#Redirect
	$keywords[0]= <KEYWORDS>;
	$keywords[0] =~ s/\n//;
	@test = split(/\t/,$keywords[0]);
	$keywords[0] = $test[1];
	
	#Disambig
	$keywords[1]= <KEYWORDS>;
	$keywords[1] =~ s/\n//;
	@test = split(/\t/,$keywords[1]);
	$keywords[1] = $test[1];
	
	#die;
	close(KEYWORDS);
	return @keywords;
}



sub cut_out_all_square_brackets_with_content {
        my $text = $_[0];

        $text =~ s/(\[(.|\n)+?\])//g;

        return($text);
}


sub cut_out_comment {
        my $text = $_[0];

        $text =~ s/(<!--(.|\n)+?-->)//g;
        $text =~ s/(&lt;!--(.|\n)+?--&gt;)//g;

        return($text);
}

sub cut_out_ref {
        my $text = $_[0];

        #$text =~ s/(<ref>.+?<\/ref>|&lt;ref&gt;.+?&lt;\/ref&gt;|&lt;ref&gt;.+?&lt;&#47;ref&gt;)//g;
        $text =~ s/(<ref.+?<\/ref>|&lt;ref.+?&lt;\/ref&gt;|&lt;ref.+?&lt;&#47;ref&gt;)//g;

        return($text);
}

sub cut_out_vorlage_dieser_artikel {
        my $text = $_[0];

        $text =~ s/({{Dieser Artikel(.|\n)+?}})//g;
        $text =~ s/(&#123;&#123;Dieser Artikel(.|\n)+?&#125;&#125;)//g;

        return($text);
}


sub scan_article_for_categorys {
	#filter category from article
	print "Scan Article for categorys\n" if ($protocol == 1);
	
	my $page_id		  = $_[0];	#id of page
	my $title 		  = $_[1];	#title from article
	my $revision_id   = $_[2];
	my $revision_time = $_[3];
	my $test_text  	  = $_[4];	#text from article
	my $searchstring  = $_[5];    #category
	my $language	  = $_[6]; 	#language of the wiki 
	

	my $tagstart="[[";
	#my $searchstring = 
	my $tagend="]]";
	
	my $categories ="";
	my $category ="";

	#print $searchstring."\n";
	#die;
	
	$test_text = cut_out_comment($test_text);
	$test_text =~ s/\[\[ /\[\[/g;
	$test_text =~ s/\]\] /\]\]/g;	
	$test_text =~ s/: /:/g;	
	$test_text =~ s/ :/:/g;		
	#Error_Message
	
	#read all categories
	
	my @categories = find_all_tags($page_id,$title, $revision_id, $revision_time, $test_text, "[[", $searchstring, ":", "]]" ,$language);
	
	#test also for lower case 
	my $searchstring_lc = lc($searchstring);
	my @categories_lc = find_all_tags($page_id, $title, $revision_id, $revision_time, $test_text, "[[", $searchstring_lc, ":", "]]",$language);
	my $length = @categories_lc;
	if ($length > 0) {
		foreach(@categories_lc) {
			$_ =~ s/$searchstring_lc/$searchstring/g;
			push(@categories,$_);
			error_message($language, "Writing", "3", $page_id, $title, $revision_id, $revision_time, "Found upper character $searchstring_lc for $searchstring ", "",""); 
		}
		#print "$title \n";
		#print "@categories \n";
		#die;
	}
	
	foreach(@categories) {
		if ($_ =~ s/\n//g){
			#ENTER inside interwiki, endtag not found
			my $trash = $_ ;
			my $length = length($trash);
			$trash = substr($trash, 0, $length,"\n");
			error_message($language, "Category", 1,  $page_id, $title, $revision_id, $revision_time, "Problem with $trash", "",""); 
			$_ = "";
		}
		if ($_ =~ s/\t//g){
			#Tabulator inside interwiki, endtag not found
			my $trash = $_ ;
			my $length = length($trash);
			$trash = substr($trash, 0, $length,"\t");
			error_message($language, "Category", 1,  $page_id, $title, $revision_id, $revision_time, "Tabulator behind $trash", "",""); 
			$_ = "";
		}
	}	
	
	
	foreach(@categories) {
		$_ =~ s/\n//g;
		$_ =~ s/\t//g;
	#	$_ =~ s/\[//;
	#	$_ =~ s/$searchstring:/cat:/i;
	#	$_ =~ s/\]//;
	}
	#delete linebreak in categories
	#my $length = @categories;
	#print "$length categories in $title\n";
	#for ($i = 0; $i <= $categories[0]; $i++){ 
	#	$categories[$i+2] =~ s/\t/ /g;
	#	$categories[$i+2] =~ s/\n/ /g;
	#	#Fehler muss gemeldet werden
	#}
	
	return @categories;
	#0 Number of categories
	#1 All categories
	#2.. one category 

}




sub scan_article_for_interwikis {
	#filter interwikis from article
	print "Scan article for interwikis\n" if ($protocol == 1);
	
	my $page_id		  = $_[0];	#id of page
	my $title 		  = $_[1];	#title from article
	my $revision_id   = $_[2];
	my $revision_time = $_[3];
	my $test_text  	  = $_[4];	#text from article
	my $language	  = $_[5]; 	#language of the wiki 
	my $searchstring  = $_[6]; 	#Interwiki en,de,fi ...
	

	my @interwikis = find_all_tags($page_id, $title, $revision_id, $revision_time, $test_text, "[[", $searchstring, ":", "]]",$language);
	

	foreach(@interwikis) {
		if ($_ =~ s/\n//g){
			#ENTER inside interwiki, endtag not found
			my $trash = $_ ;
			my $length = length($trash);
			$trash = substr($trash, 0, $length,"\n");
			error_message($language, "Interwiki", 1,  $page_id, $title, $revision_id, $revision_time, "Problem with $trash", "",""); 
			@interwikis = ("");
		}
	}
	
	my $number_of_interwikis_in_one_language = @interwikis;
	
	foreach(@interwikis) {
		$_ =~ s/\[\[//g;
		$_ =~ s/^$searchstring://;
		$_ =~ s/\]\]//g;
	}
	
	if ($number_of_interwikis_in_one_language <= 1){
		if ($number_of_interwikis_in_one_language == 1){
			return ($interwikis[0])
		}else{
			return ("")
		}
	}
	else {
		#print "more then one interwiki in\n";
		#print "$title\n";
		#print "$revision_time\n";
		#print "$searchstring\n";
		#foreach( @interwikis) {
		#	print $_,"\n";
		#}
		error_message($language, "Interwiki", 1,  $page_id, $title, $revision_id, $revision_time, "More than one interwiki to $searchstring", "",""); 

		return ($interwikis[0]);
	}

}





















sub find_all_tags{
	#get the contents of all tags in the article
	#for categories
	my $page_id		= $_[0];	#id of page
	my $title 		= $_[1];	#title of article
	my $revision_id   = $_[2];
	my $revision_time = $_[3];
	my $test_text 	= $_[4];	#text of article
	my $tagstart	= $_[5];	#{{
	my $tagname1 	= $_[6];	#category
	my $tagname2 	= $_[7];	#:
	my $tagend  	= $_[8];	#}}
	my $language	= $_[9]; 	#language of the wiki 
	
	
	my $tagbegin = $tagstart.$tagname1.$tagname2;
	
	my @result_find_all_tags;
	#$result_find_all_tags[0] = 0;
	
	do {
		@test = contents_of_a_tag($page_id, $title, $revision_id, $revision_time, $test_text, $tagstart, $tagname1, $tagname2, $tagend, $language);
		if ($test[0] ne "") { 
			#Cut out the text after tag
			$test_text = substr($test_text, $test[2] + length($tagend) );
			
			my $length = @result_find_all_tags;
			#print "Tag number: $length $test[0]\n";
			$result_find_all_tags[$length] = $test[0];
			#print "$result_find_all_tags[$length]\n";
		}
		else {
			#no content in tag
			
			#no endtag
			if ($test[1]>-1 and $test[2] == -1) {
				$test_text = substr($test_text, $test[1] + length($tagstart) );
			}	
			
			
		}
	}	
	while ( index($test_text, $tagbegin ) != -1 );

	return @result_find_all_tags;
}



sub contents_of_a_tag{
	#get the contents of the first tag in the article

	my $page_id		= $_[0];	#id of page
	my $title 		= $_[1];	#title of article
	my $revision_id   = $_[2];
	my $revision_time = $_[3];
	my $test_text 	= $_[4];	#text of article
	my $tagstart	= $_[5];	#[[
	my $tagname1 	= $_[6];	#category
	my $tagname2 	= $_[7];	#:
	my $tagend  	= $_[8];	#]]
	my $language	= $_[9]; 	#language of the wiki 
	
	
	#print $tagname1,"\n" if ($title eq "Krakau");
	
	my $position1 = -1;
	my $position2 = -1;
	
	#here we need a script for errors like "[[ Category:" or "[[Category :"
	#$test_text
	#$test_text =~ s/\[ /$tagname1/\[/$tagname1/g;
	#if ($test_text =~ s/\[ /$tagname1/\[/$tagname1/g) {
	#	error_message($language, $page_id, $title, "Space before $tagname1", "","");
	#}
	#$test_text =~ s/$tagname1 :/$tagname1:/g;
	#if ($test_text =~ s/$tagname1: /$tagname1:/g) {
	#	error_message($language, $page_id, $title, "Space behind $tagstart$tagname1$tagname2", "","");
	#}

	
	#find tag
	my $tagbegin = $tagstart.$tagname1.$tagname2;
	my $tag ="";
	
	
	#print "$title \n";
	if (  index($test_text, $tagbegin) > -1){
			$position1 = -1;
			$position1 = index($test_text, $tagbegin); 
			#print "$title\n";
			#die;
			if 	(index($test_text, $tagend, $position1)>0 )  {
				#found tagend behind the tagstart
				$position2 = -1;
				$position2 = index($test_text, $tagend, $position1);
				#print "$Test1 , $Test2\n";
				$tag = "";
				$tag = substr($test_text,$position1, $position2 + length($tagend) - $position1 );
				#print "x$tag\n";
				#die;
			}
			else{
				#error: found no tagend behind the tagstart
				
				#get text to next space
				$position2 = -1;
				$position2 = index($test_text, " ", $position1);
				$tag = substr($test_text,$position1, $position2 + length($tagend) - $position1 );
				
				#get rest of text
				$tag = $test_text if ($tag eq "");				
				
				#cut at linebreak
				my $test = index ($tag, "\n");
				$tag = substr ($tag,0,$test) if ($test > -1) ;
				
				error_message($language, "Sourcecode", "1",  $page_id, $title, $revision_id, $revision_time, "Found no end behind $tag ", "",""); 
				$position2 = -1;
				$tag = "";
			}
	}	
	#print "result: $title, $tag, $position1, $position2\n";
	my @result_contents_of_a_tag;
	$result_contents_of_a_tag[0] = $tag;
	$result_contents_of_a_tag[1] = $position1; #Start
	$result_contents_of_a_tag[2] = $position2; #End
	#print "result: $title, $result_contents_of_a_tag[0], $result_contents_of_a_tag[1], $result_contents_of_a_tag[2]\n";
	
	 
	#print "@result_contents_of_a_tag\n" if ($title eq "Aussagenlogik");
	#print "@result_contents_of_a_tag\n" if ($title eq "Anarchism");
	return @result_contents_of_a_tag;

}


sub delete_tag{
	#delet all comments or other tags
	#displace all with displace_character
	
	my $page_id			= $_[0];	#id of page
	my $title 			= $_[1];	#title of article
	my $revision_id   	= $_[2];
	my $revision_time 	= $_[3];
	my $text 			= $_[4];	#text of article
	my $language 		= $_[5];	#text of article
	my $tagstart		= $_[6];	#<!--
	my $tagend	 		= $_[7];	#-->
	my $displace_character = $_[8];

	#print "$title \t $tagstart \t $tagend \n";
	
	my $test1 = index($text, $tagstart); 
	
	if ($test1 > -1){
		#search for all tags
		do {
			#next tagend
			$test1 = index($text, $tagstart); 
			my $position_end = index($text, $tagend, $test1) ;
			my $position_start = -1;
			if ($position_end>-1 and $position_end <= length($text)){
				#Suche für Endzeichen entsprechende letztes Anfangszeichen
				do{
					if ($position_start == -1){
						$position_start = index($text, $tagstart); 				
					}
					else{
						$position_start = index($text, $tagstart,$position_start+1); 
					}
					$test2 = substr($text, $position_start+1,$position_end-$position_start-1); 
					#print "$test2\n" if ($Titel eq "Ar (Flächenmaß)");
				}
				until (index($test2,$tagstart) == -1);
				
				if ($position_start == -1){
					error_message($language, "Tag", "1",  $page_id, $title, $revision_id, $revision_time, "Found no starttag $tagstart but $tagend", "",""); 
		
					#Fehler_hinzufuegen($Titel,"Tag geschlossen aber nicht geöffnet, $tagstart $tagend",$position_end ,"");
				}
				else{ 
					#Text vor Tag
					$test1 = substr($text, 0, $position_start); 
					#Text nach Tag
					my $test2 = substr($text, $position_end+length($tagend));
					
					
					#Ersetze Tag durch entsprechenden Ersetzungsbuchstaben
					my $test3 = substr($text, $position_start, $position_end-$position_start+length($tagend));
					#print "Test3 = $test3\n";
					$test3 =~ s/./$displace_character/g;
					#print "Test3 = $test3\n";
					#die;
					$text = $test1.$test3.$test2;
					#print "$text\n";
					#die;
				}
			}
			else{
				my $S1= $tagstart;
				my $S2= $tagend;
				$S1 =~ s/&lt;/</g;
				$S1 =~ s/&gt;/>/g;
				$S2 =~ s/&lt;/</g;
				$S2 =~ s/&gt;/>/g;
				#Fehler_hinzufuegen($Titel,"Tag geöffnet aber nicht geschlossen $S1 $S2",$test1 ,"");
				error_message($language, "Tag", "1",  $page_id, $title, $revision_id, $revision_time, "Found starttag $S1 but no $S2", "",""); 

				}
			$test1 = index($text, $tagstart);
		}
		until (  $test1 == -1 or index($text, $tagend,$test1) == -1  );
	
		#print $text;
		#die;
	}
	return($text);
}




sub scan_article_for_first_image{
	#not need because there is a newer sub "scan_article_for_multimedia"
	print "Scan Article for first image\n" if ($protocol == 1);
	
	my $first_image = "";
	my $page_id		  = $_[0];	#id of page
	my $title 		  = $_[1];	#title from article
	my $revision_id   = $_[2];
	my $revision_time = $_[3];
	my $test_text  	  = $_[4];	#text from article
	my $language	  = $_[5]; 	#language of the wiki 
	my $searchstring  = $_[6];  #bild: 
	my $searchstring_en  = $_[7];  # image: 
	
	my $searchstring_lc  	= lc($searchstring);
	my $searchstring_en_lc  = lc($searchstring_en);
	
	my $test1 = -1;
	my $test2 = -1;
	my $test3 = -1;
	my $test4 = -1;
	$test1 = index($test_text, $searchstring);			
	$test2 = index($test_text, $searchstring_en); 
	$test3 = index($test_text, $searchstring_lc);
	$test4 = index($test_text, $searchstring_en_lc); 	
	
	if ($test1 > -1 or $test2 > -1 or $test3 > -1 or $test4 > -1){
		#found image
		#print "found image in $title\n";
		#print $searchstring," ",$test1,"\n";
		#print $searchstring_en," ",$test2,"\n";
		#print $searchstring_lc," ",$test3,"\n";
		#print $searchstring_en_lc," ",$test4,"\n";
		
		my $search_pos = -1;
		$search_pos = $test1 if ($test1 > -1);
		
		if ( ($search_pos > -1 and $test2 > -1 and $test2 < $search_pos) or $search_pos == -1 ){
			$search_pos = $test2;
			$searchstring =  $searchstring_en;
		}
		if ( ($search_pos > -1 and $test3 > -1 and $test3 < $search_pos) or $search_pos == -1){
			$search_pos = $test3;
			$searchstring =  $searchstring_lc;
		}
		if ( ($search_pos > -1 and $test4 > -1 and $test4 < $search_pos) or $search_pos == -1){
			$search_pos = $test4;
			$searchstring =  $searchstring_en_lc;
		}
		
		#print $searchstring," ",$search_pos,"\n";
		$test_text = substr ($test_text, $search_pos);
		
		#found end of line
		my $length = -1;
		$length = length($test_text);
		my $next_return = -1;
		$next_return = index ($test_text, "\n");
		
		if ($next_return > -1) {
			$length = $next_return if ($next_return < $length);
		}
		$test_text = substr ($test_text, 0, $length);
		#print $test_text;
		
		#cut after image
		$length = -1;
		$length = index ($test_text, "]");
		$test_text = substr ($test_text, 0, $length) if ( $length > -1) ;

		$length = -1;
		$length = index ($test_text, "|");
		$test_text = substr ($test_text, 0, $length) if ( $length > -1) ;		
		
		#cut before :
		$length = -1;
		$length = index ($test_text, ":");
		$length = $length + 1;
		$test_text = substr($test_text, $length);
		
		$first_image = $test_text;
		$first_image =~ s/^ //g;
		$first_image =~ s/ $//g;
		$first_image =~ s/ /_/g;
		
		#print $first_image;
	}
	return($first_image);
}


sub scan_article_for_multimedia{
	print "Scan Article for first JPG image\n" if ($protocol == 1);
	
	my $page_id		  = $_[0];	#id of page
	my $title 		  = $_[1];	#title from article
	my $revision_id   = $_[2];
	my $revision_time = $_[3];
	my $test_text  	  = $_[4];	#text from article
	my $language	  = $_[5]; 	#language of the wiki 
	my $searchstring  = $_[6];  #bild: 
	my $searchstring_en  = $_[7];  # image: 
	
	my $searchstring_lc  	= lc($searchstring);
	my $searchstring_en_lc  = lc($searchstring_en);

	
	my $test1 = -1;
	my $test2 = -1;
	my $test3 = -1;
	my $test4 = -1;
	$test1 = index($test_text, $searchstring);			
	$test2 = index($test_text, $searchstring_en); 
	$test3 = index($test_text, $searchstring_lc);
	$test4 = index($test_text, $searchstring_en_lc); 	
	
	my $output = "";
	my $all_media = "";
	my $first_image = "";
	my $first_jpg = "";
	my $first_gif = "";
	my $first_png = "";
	my $first_svg = "";
	my $first_ogg = "";
	my $first_avi = "";
	my $first_mpg = "";
	
	
	
	if ($test1 > -1 or $test2 > -1 or $test3 > -1 or $test4 > -1){
		#found image
		#print "found image in $title\n\n";
		#print $searchstring," ",$test1,"\n";
		#print $searchstring_en," ",$test2,"\n";
		#print $searchstring_lc," ",$test3,"\n";
		#print $searchstring_en_lc," ",$test4,"\n";
		
		#delet all linebreaks
		$test_text =~ s/\n/\ /g;
		#make linebreak before every Media (Image,Bild,...);
		$test_text =~ s/$searchstring/\n$searchstring/g;
		$test_text =~ s/$searchstring_en/\n$searchstring_en/g;
		$test_text =~ s/$searchstring_lc/\n$searchstring_lc/g;
		$test_text =~ s/$searchstring_en_lc/\n$searchstring_en_lc/g;
		
		#http://commons.wikimedia.org/wiki/Commons:Dateitypen
		my @media = ("jpg","jpeg","gif","png","svg","ogg", "mid");
		my $number_media = @media;
		#print $number_media."\n";
		# DjVu für gescannte Dokumente
		# Einige OpenOffice.org-Formate: SXW (Text), SXI (Präsentation), SXC (Tabellen), SXD (Vektorgrafiken)
		
		#make linebreak after every media
		for (my $i=0; $i < $number_media; $i++) {
			$test_text =~ s/(\.$media[$i])/$1\n/gi;
		}
		
		#split text by linebreakes
		my @text_split = split(/\n/, $test_text);
		my $number_split =  @text_split;
		$test_text = "";
		#print $number_split."\n";
		
		#seach for lines with searchstring at begin
		for (my $i=0; $i < $number_split; $i++) {
			#print $i."\n";
			
			#some media has no currect ending [[Image:Test]] [[Image:Test.jpg]] 
			my $media_normal = 0;
			for (my $j=0; $j < $number_media; $j++) {
				my $test = $text_split[$i];
				 if ($test =~ /(\.$media[$j])/i) {
					$media_normal = 1;
					#print $media[$j]."\n" if ($title eq "Wittmar");
				}
			}
			
			
			#print "Normal:".$media_normal."\n" if ($title eq "Wittmar");
			if ($media_normal == 1) {		
				$test_text = $test_text.$text_split[$i] if ($text_split[$i] =~ s/^$searchstring/\n/g);
				$test_text = $test_text.$text_split[$i] if ($text_split[$i] =~ s/^$searchstring_en/\n/g);
				$test_text = $test_text.$text_split[$i] if ($text_split[$i] =~ s/^$searchstring_lc/\n/g);
				$test_text = $test_text.$text_split[$i] if ($text_split[$i] =~ s/^$searchstring_en_lc/\n/g);
			}
		}
		

		
		#now only mediafiles in test_text
		#print "y".$test_text."y" if ($title eq "Wittmar");
		$test_text =~ s/ /_/g;
		#die if ($title eq "Shanghai");

		
		@text_split = split(/\n/, $test_text);
		$number_split =  @text_split;
		
		for (my $i=0; $i < $number_split; $i++) {
			my $test = $text_split[$i];
			if ($test =~ /\.jpg$/i or $test =~ /\.jpeg$/i ) {
				#print $text_split[$i]."\n";
				$first_image = $text_split[$i] if ( $first_image eq "");
				$first_jpg = $text_split[$i] if ( $first_jpg eq "");
				$all_media .= "|".$text_split[$i];
				$text_split[$i] = "";

			}
			$test = $text_split[$i];
			if ($test =~ /\.gif$/i) {
				$first_image = $text_split[$i] if ( $first_image eq "");
				$first_gif = $text_split[$i] if ( $first_gif eq "");
				$all_media .= "|".$text_split[$i];
				$text_split[$i] = "";
			}			
			$test = $text_split[$i];
			if ($test =~ /\.png$/i) {
				$first_image = $text_split[$i] if ( $first_image eq "");
				$first_png = $text_split[$i] if ( $first_png eq "");
				$all_media .= "|".$text_split[$i];
				$text_split[$i] = "";
			}
			$test = $text_split[$i];
			if ($test =~ /\.svg$/i) {
				$first_image = $text_split[$i] if ( $first_image eq "");
				$first_svg = $text_split[$i] if ( $first_svg eq "");
				$all_media .= "|".$text_split[$i];
				$text_split[$i] = "";
			}		

			#if ($text_split[$i] =~ /\.ogg$/i) {
			#	print "\t\t\t".$title." ".$text_split[$i]."\n";
			#}

			#if ($text_split[$i] =~ /\.mpg$/i) {
		    #	print "\t\t\t".$title." ".$text_split[$i]."\n";
			#}

			#if ($text_split[$i] =~ /\.avi$/i) {
			#	print "\t\t\t".$title." ".$text_split[$i]."\n";
			#}

			#if ($text_split[$i] =~ /\.mp3$/i) {
			#	print "\t\t\t".$title." ".$text_split[$i]."\n";
			#}			
			
			#if ($text_split[$i] ne "") {
				#a media was forgot
				#print "\t\t\t".$title."\n";
				#print $text_split[$i]."\n";
				#die;
			#}

		}
		
		#print "\n\n";
		#print "first_image ".$first_image."\n";
		#print "first_jpg ".$first_jpg."\n";
		#print "first_gif ".$first_gif."\n";
		#print "first_png ".$first_png."\n";
		#print "first_svg ".$first_svg."\n";
		#print "\n";
		#print "\n";

		$all_media =~s /\n/\|/g;
		$all_media =~s /^\|//g;		
		
		if ($all_media ne "") {
			$output = $all_media;
			$output .= "\t".$first_image;
			$output .= "\t".$first_jpg;
			$output .= "\t".$first_gif;
			$output .= "\t".$first_png;
			$output .= "\t".$first_svg;
		}
		#print $output;
		#print $first_image." ".$first_jpg."\n";
		#print "\n";
		#print "\n";
		#die;

		
		#$first_image = $test_text;
		#$first_image =~ s/^ //g;
		#$first_image =~ s/ $//g;
		#$first_image =~ s/ /_/g;
		
		#print $first_image;
		
		#if ($title eq "Wittmar") {
		#	print "Output".$output."x\n";
		#	print $all_media."x\n";
		#	print $first_image."x\n";
		#	print $first_jpg."x\n";
			#die;
		#}
		
		
		
		
	}
	return($output);
}




sub error_message {
	my $language	 	= $_[0]; 	#language of the wiki 
	my $error_type	 	= $_[1]; 	#language of the wiki 
	my $priority	 	= $_[2]; 	#1 = low, 3 = high
	my $page_id		 	= $_[3];	#id of page
	my $title 		 	= $_[4];	#title of article
	my $revision_id  	= $_[5];
	my $revision_time 	= $_[6];
	my $errormessage 	= $_[7];	#message
	my $position1 	 	= $_[8];	#position
	
	my $time = localtime(time);
	
	if ($priority < 2) {
		open(ERROR_LOGFILE, ">>$error_log_path");

		print ERROR_LOGFILE $language,"\t";
		print ERROR_LOGFILE $time,"\t";
		print ERROR_LOGFILE $priority,"\t";
		print ERROR_LOGFILE $error_type,"\t";
		print ERROR_LOGFILE $page_id,"\t";
		print ERROR_LOGFILE $title,"\t";
		print ERROR_LOGFILE $revision_id,"\t";	
		print ERROR_LOGFILE $revision_time,"\t";	
		print ERROR_LOGFILE $errormessage,"\t";
		print ERROR_LOGFILE $position1,"\n";	
		close (ERROR_LOGFILE);
	}
	#print $page_id, $title, $errormessage, $position1; 
	
}

1;
