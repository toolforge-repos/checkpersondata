#!/usr/bin/perl


##############################################
# Skript holt per Web-API die zuletzt 
# geänderten Artikeltitel verschiedenen 
# Sprachen von Wikipedia
##############################################

use strict;


use URI::Escape;		# URL-Zeichen-Umwandlung
use LWP::UserAgent;		# Webzugriff
use DBI;				# Datenbankzugriff
use Encode;				# Zeichensatz-Anpassung
use HTML::Entities;

print "\n";
print "new article - Version 0.1\n";
print "\n";	


# define variables
our %parameter;
our $dbh;
our $format = "%30s\t%-30s\n";

# define paths
my $output_path = 'data';



# my @project_to_get = ('dewiki','enwiki','commons','pdcwiki');
my @project_to_get = ('dewiki'); # for testing
our $current_lang = -1;


my $number_of_languages = scalar @project_to_get;
print $number_of_languages. ' languages to load'."\n";



read_configfile();


############################
# Login to database
############################
my $dbh = db_login();


############################
# Loop to every project
############################


foreach my $project (@project_to_get){
	load_recentchanges_via_api($dbh, $project);
}


############################
# Loout from database
############################
db_logout($dbh);



print 'finish'."\n\n";
	
############################
# Finish
############################
	



#################################################################	
# Subroutines
#################################################################	


sub read_configfile{
	# read_configfile for global settings (DB,Schemata,Password,dumppath...)

	print 'read configfile'."\n";

	# open configfile
	my $config_file = 'config.txt';
	open(CONFIG, "<$config_file") or die "cannot open < $config_file\n$!";


	# read configfile line by line
	do {
		my $line = <CONFIG>;

		
		if ($line =~ /=/ ) {
			$line =~ s/\n//g;  
			$line =~ s/\t/ /g; #tabulator to space
			my $key = substr($line, 0, index($line,'=')-1);
			my $value = substr($line, index($line,'=')+1);

			$key   =~ s/^\s+|\s+$//g;	# remove space at begin and end
			$value =~ s/^\s+|\s+$//g;

			if ($key ne ''){
				$parameter{$key} = $value;	# save in hash parameter
			}
			
		}
	}
	while (eof(CONFIG) != 1);

	# close configfile
	close(CONFIG);

	# print all parameter
	foreach my $name(sort keys %parameter){
		 printf ("%15s = %s\n", $name ,$parameter{$name}) ; 
	}
	#print "\n";
	#die;
}

	
	

##########################################
# load recentchanges from web via API
##########################################


sub load_recentchanges_via_api{	
	#print 'load new article from web'."\n";
	my $dbh = shift;				# Database-Handler
	my $project_dbname = shift;		# Project_dbname (dewiki,commons,enwiki,dewiktionary)

	print "\n".'Project:'.$project_dbname."\n";	

	


	#################################
	# check for timestamp of last run
	#################################
	my $parameter_last_recentchange = '';
	my $sql = "select value from parameter where dbname = '".$project_dbname."' and name ='recentchange'";
	#print $sql."\n";
	my $sth = db_abfrage($dbh, $sql);
	if ($sth->rows() > 0) {
		while (my ($value) = $sth->fetchrow_array()) {
			$parameter_last_recentchange = $value;
			#print 'last_recentchange:        '.$parameter_last_recentchange."\n";
			$parameter_last_recentchange =~ s/[-:TZ ]//g;
			printf $format, 'last_recentchange for url',$parameter_last_recentchange;

		}		
	} else {
		print 'Found no timestamp for last_recentchange on table parameter for '.$project_dbname."\n";
	}


	#################################
	# get url from project
	#################################	
	my $sql = "select replace(url,'http://','') from project where dbname = '".$project_dbname."'";
	#print $sql."\n";
	my $sth = db_abfrage($dbh, $sql);
	my $project_url = '';
	if ($sth->rows() > 0) {
		while (my ($value) = $sth->fetchrow_array()) {
			$project_url = $value;
		}		
	} else {
		print 'Found no URL for '.$project_dbname."\n";
	}
	printf $format, 'URL', $project_url;
	

	#################################
	# get recentchange via API
	#################################

	# API for Recentchange
	# zeigt von zeitstempel an die vorhergehenden Änderungen
	# durch rccountinue kann es weiter gehen

	
	# https://www.mediawiki.org/wiki/API:Recentchanges
	# rcstart = begin 
	# rcend   = end
	# &user   = only user edits
	# &rcnamespace=0   only articles
	# rcprop
	#   time
	# &continue = show next page rcstart
	#
	#http://de.wikipedia.org/w/api.php?
	#action=query&list=recentchanges&rcprop=timestamp|ids|title&user&rclimit=5

	#http://de.wikipedia.org/w/api.php?
	#action=query
	#&list=recentchanges
	#&rcprop=timestamp|ids|title
	#&rclimit=5
	#&rcstart=20141206181357
	#&rcnamespace=0
	#&rcdir=newer
	#rccontinue=20141206181354|151245166


	#  http://de.wikipedia.org/w/api.php?action=query&list=recentchanges&rcprop=title|flags&user&rclimit=5000
	#  http://de.wikipedia.org/w/index.php?title=Special:NewPages&limit=500&action=raw



#http://de.wikipedia.org/w/api.php?action=query&list=recentchanges&rcprop=timestamp|ids|title&rclimit=50&rcnamespace=0&rctype=edit&format=xml&continue&rcstart=20141116135827

	# for logging
	my $first_timestamp;
	my $last_timestamp;
	my $counter;
	my $rccontinue = '';
	my $first_rccontinue = '';

	#if ($parameter_last_recentchange ne '')
	#$rcend = $parameter_last_recentchange
	
	if ($project_url ne '') {
		#project is known and url for API not null

		do {

	
			my $url = '';
			if ($project_dbname eq 'commons') {
				$url = 'https://'.$project_url.'/w/api.php?action=query&list=recentchanges&rcprop=timestamp|ids|title&rclimit=500&rcnamespace=0|6&rctype=edit&continue&format=xml';
			} else {
				$url = 'https://'.$project_url.'/w/api.php?action=query&list=recentchanges&rcprop=timestamp|ids|title&rclimit=500&rcnamespace=0&rctype=edit&continue&format=xml';
			}

			#printf "%s %s %s\n",'old rccontinue=', $rccontinue, 'x';			
			if ($rccontinue ne '') {
				$url .= '&rcstart='.$rccontinue;
				
			}

			if ($parameter_last_recentchange ne ''){
				# end recentchange at time of last newest change from database
				$url .= '&rcend='.$parameter_last_recentchange;
			}
		

			#print $url."\n";

			my $text = "";
			$text = get_text_from_webapi($url);
			$text =~ s/<rc type=/\n<rc type=/g;
			#print $text;


	
			$rccontinue = substr(get_attribut ($text,'rccontinue'),0,14);
			
			#printf "%s %s %s\n",'new rccontinue=', $rccontinue, 'x';	

			my @text_split = split( /\n/, $text);
	
	
	
			################################################################################
			# get data from raw XML (title, pageid, revid, timestamp)
			################################################################################
			my $current_line ='';


			foreach	(@text_split) {
				$current_line = $_;
				$current_line =~ s/\n//g;
				if ( $current_line =~ /^<rc/) {
					#print "\nA:".$current_line."\n"; 
					if (index ($current_line, '" title="') >-1) {
						#print "B:".$current_line."\n";
				
						#$current_line = substr($current_line, index ($current_line,'" title="') + length('" title="'));
						my $title 		= get_attribut ($current_line,'title');
						my $pageid		= get_attribut ($current_line,'pageid');
						my $revid		= get_attribut ($current_line,'revid');
						my $timestamp	= get_attribut ($current_line,'timestamp');

						print 'PageID    = '.$pageid."\n";				
						print 'Title     = "'.$title.'"'."\n";
						print 'Timestamp = '.$timestamp."\n";
						print 'RevID     = '.$revid."\n";
						$timestamp =~ s/T/ /;
                                                $timestamp =~ s/Z//;
                                                print 'Timestamp = '.$timestamp."\n";

						# printf "%s %s %8s %10s %s\n",$project[$current_lang], $timestamp, $pageid, $revid, $title;

						my $sql = '';
						$sql .= "insert into recentchange(dbname, page_id, title, revid, page_timestamp) values ('".$project_dbname."', ".$pageid.", '".$title."', ".$revid.", '".$timestamp."');"."\n";

						#print $sql."\n";
						#die;
						#$sql .= "commit;\n" if ( ($pageid %= 5)==0);
						db_do($dbh, $sql);

						#for logging
						$first_timestamp = $timestamp if ($first_timestamp eq '');
						$last_timestamp  = $timestamp if ($last_timestamp  eq '');
						$first_timestamp = $timestamp if ($first_timestamp lt $timestamp);
						$last_timestamp  = $timestamp if ($last_timestamp  gt $timestamp);
						$counter ++;
					}
				}
			}
			#print "\n";
			printf "from: %s to: %s %s\n", $first_timestamp, $last_timestamp, $counter if ($first_timestamp ne '');	

		} until ($counter > 20000000 or $rccontinue eq '');


		# logging into database
		if ($first_timestamp eq '') {

			# no internet
			print 'get no first_timestamp'."\n";
			print 'Maybe no internet'."\n";

		} else {

			# get internet


			# save first_timestamp in database for next load of recentchange;
			# delete from parameter where name='recentchange' and dbname = 'dewiki';
			my $sql = "delete from parameter where name='recentchange' and dbname = '".
		$project_dbname."'";
			db_do($dbh, $sql);	


			# insert into parameter(dbname, name, value) values ('dewiki','recentchange','2015-02-05T08:09:01Z');
			$sql = "insert into parameter(dbname, name, value) values ('".$project_dbname."', 'recentchange', '".$first_timestamp."');"."\n";
			#print $sql."\n";
			db_do($dbh, $sql);

		}
	}

}



####################################################################


sub get_attribut{
	# get from XML attribut; like title="Dresden" --> Dresden
	my $line 	 = shift;
	my $attribut = shift;
	my $result;
	my $regex = ''.$attribut.'="(.*?)"';
	if ( $line =~ /$regex/) {
		$result = $1;
		#nicht gierig bei --> titel="Snowpiercer"
		#if ($attribut eq 'title'){
		#	print "Dieses Muster passt auf '$&'.\n";
		#	print "Davor= ($`)\n";
		#	print "Treffer= ($&)\n";
		#	print "Danach = ($')\n";
		#	print "Inhalt = ($1)\n";
		#}
	}
	return ($result);
}



###########################################################################

sub get_text_from_webapi {
	my $url = $_[0];
	
	$url =~ s/&amp;/%26/g;			# Problem with & in title
	$url =~ s/&#039;/'/g;			# Problem with apostroph in title

	
	my $response2 ;
	uri_escape($url);
	my $ua2 = LWP::UserAgent->new;
	$response2 = $ua2->get( $url );
	
	my $content2 = $response2->content;
	my  $result2  = '';
	$result2 = $content2 if ($content2) ;
	
	return($result2);
}


###############################################################################

######################################################################################
######################################
# DB- Anmeldung
sub db_login{
	print "\n".'open database: '.$parameter{'db_database'}."\n";
	my $dbh = DBI->connect( "dbi:mysql:$parameter{'db_database'};host=$parameter{'host'}",
				$parameter{'db_user'},
				$parameter{'db_password'} , 
	                      ) || die "Database connection not made: $DBI::errstr";
	$dbh->{'mysql_enable_utf8'}=1;  # sicherstellen das UTF-8 aus der DB kommt
	#$dbh->do("SET lc_time_names = 'de_DE'");	# Wochentage auf Deutsch	
	#$dbh->do("SET NAMES utf8");
	$dbh->do("SET CHARSET utf8");
	return ($dbh);
}

######################################
# DB- Abmeldung
sub db_logout{
	my $dbh = shift;	
	$dbh->disconnect();
}
######################################
# DB-Abfrage

sub db_abfrage{
	my $dbh = shift;
	my $sql = shift;
	my $sth = $dbh->prepare($sql) or die ($dbh->errstr);
	$sth->execute or die $sth->errstr;
	return ($sth);
}

######################################
# DB-Do (insert, update, ...)
######################################
sub db_do{
	my $dbh = shift;
	my $sql = shift;
	my $sth = $dbh->prepare($sql) or die ($dbh->errstr);
	$sth = $dbh->do($sql) or die ($dbh->errstr);
	return ($sth);
}








