--######################################## 
--# SQL-Statements for Personendaten (dewiki)
--########################################



--######################################## 
--# Create database and table
--########################################

mysql -u sk -p

alter 

create database u_sk_pd_p
DEFAULT CHARACTER SET utf8
DEFAULT COLLATE utf8_general_ci;

use u_sk_pd_p;

create table pd_overview_errors
(project varchar(100),
error_id int(8),
error_sum bigint(20),
done_sum bigint(20),
name varchar(4000),
prio int(8)
);


create table pd_error
(project varchar(100),
page_id bigint(20),
title varchar(4000),
error_id int(8),
notice varchar(4000),
done int(1),
found datetime
);

create table pd_error_desc
(error_id int(8),
prio int(8),
name varchar(4000),
notice varchar(4000)
);


create table parameter
(
project varchar(200),
name varchar(200),
value varchar(200)
);

create table pd_new
(
project varchar (100),
title varchar(4000),
daytime datetime,
scan_live tinyint(1)
);


create table pd_change
(
project varchar (100),
title varchar(4000),
daytime datetime,
scan_live tinyint(1)
);






create index pd_error_done on pd_error (done);
create index pd_error_error_id on pd_error (error_id);
create index pd_error_page_id on pd_error (page_id);
create index pd_error_error_id_done on pd_error (error_id, done);

SHOW INDEX FROM

update parameter set value = '2008-04-02 05:59:32' where project = 'dewiki' and name ='daytime_last_article_from_new';
update parameter set value = '2008-04-02 05:59:32' where project = 'dewiki' and name ='daytime_last_article_from_change';


insert into parameter value ('dewiki', 'daytime_last_article_from_new',    '2008-04-02 05:59:32');
insert into parameter value ('dewiki', 'daytime_last_article_from_change', '2008-04-02 05:59:32');



-- erst nach dem zweiten live-scan sind die Fehlerprioritäten in der Datenbank drin !!! Warum?


--########################################
--# some SQL-Statements
--########################################


-- Artikel hinzufügen (zu Testzwecken)
insert into pd_error values ('dewiki', 123, 'Albert Einstein',  200, 'test', 0, NULL);
insert into pd_error values ('dewiki', 123, 'Albert Einstein',  201, 'test', 0, NULL);
insert into pd_error values ('dewiki', 123, 'Albert Einstein',  202, 'test', 0, NULL);
insert into pd_error values ('dewiki', 123, 'Albert Einstein',  203, 'test', 0, NULL);
insert into pd_error values ('dewiki', 123, 'Albert Einstein',  204, 'test', 0, NULL);
insert into pd_error values ('dewiki', 456, 'Eduard Imhof',  200, 'test', 0, NULL);
insert into pd_error values ('dewiki', 456, 'Eduard Imhof',  200, 'test2', 0, NULL);
insert into pd_error values ('dewiki', 456, 'Eduard Imhof',  200, 'test3', 0, NULL);


-- Anzahl der Artikel
select count(distinct title) from pd_error;

-- Anzahl der Fehler
select count(*) from pd_error;
		 

-- Auswahl erste Seite (Zahlen für jede Prio)
  
select a1.anzahl, a1.prio, IFNULL(b1.anzahl,0) erledigt  
from (  select count(*) anzahl, b.prio prio 
	from pd_error a 
	join pd_error_desc b 
	on ( a.error_id = b.error_id) 
	where a.done=0 
	group by b.prio
) a1
left outer join (
	select count(*) anzahl, b.prio prio 
	from pd_error a 
	join pd_error_desc b 
	on ( a.error_id = b.error_id) 
	where a.done=1 
	group by b.prio
) b1
on (a1.prio = b1.prio) order by a1.prio;
 

 

-- Top 25 (Artikel mit meisten Fehlern)
select count(*), page_id, title 
from pd_error 
group by title 
order by count(*) desc limit 25;

-- Top 25 (Artikel mit meisten Fehlern, prio gewichtet)
select title, count(b.prio) 
from pd_error a 
join pd_error_desc b 
on ( a.error_id = b.error_id) 
where a.done = 0 
group by a.title 
order by count(b.prio) desc limit 25;
 

-- Detailansicht - Alle Fehler eines Artikels über die ID oder Titel
 
select b.name, a.notice, a.error_id 
from pd_error a 
join pd_error_desc b 
on ( a.error_id = b.error_id) 
where a.title in (
select distinct title 
from pd_error 
where page_id =29269
or title = 'Albert Einstein' );


 

 
-- Alle Fehler
select error_id, prio, name from pd_error_desc where prio != 0;


-- Alle Fehler + Häufigkeit
select count(*) ||  b.name
from pd_error a 
join pd_error_desc b 
on ( a.error_id = b.error_id) 
where b.prio = 1 
group by b.error_id 
order by b.name;

		 
-- Alle Artikel eines Fehlers
select title, notice, error_id 
from pd_error 
where error_id=123
order by title;
 
 
-- "fast" zufällige Auswahl von 25 Artikeln eines Fehlers
select a.title from (
select title, 
replace(replace (REVERSE (title),')',''),'.','') Test
from pd_error
where error_id = 202
order by Test
limit 25)a order by title;
 
 
 


 
 


