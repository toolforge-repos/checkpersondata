#!/bin/sh

# scan last change from de.wikipedia.org


wget https://ftp.acc.umu.se/mirror/wikimedia.org/dumps/dewiki/20220820/dewiki-20220820-all-titles.gz
#    https://ftp.acc.umu.se/mirror/wikimedia.org/dumps/dewiki/20220120/dewiki-20220120-all-titles.gz

gzip -d dewiki*.gz
# rm dewiki*.gz

sed 's/^[0-9]*\t//' dewiki-*-all-titles > dumpTitles.txt
rm dewiki-*-all-titles
shuf dumpTitles.txt > tempDump.txt
mv tempDump.txt dumpTitles.txt

currentLine=1

lines=`wc -l < dumpTitles.txt`

echo "Total Number of Entries: $lines"

chunkSize=100

while [ $currentLine -le $lines ]
do
	echo "Running ( $currentLine / $lines )" >> /data/project/checkpersondata/dumpScan.log 2>> /data/project/checkpersondata/dumpScan.log
	echo "Currentline: $currentLine \t| Chunksize: $chunkSize" >> /data/project/checkpersondata/dumpScan.log 2>> /data/project/checkpersondata/dumpScan.log

	echo "QuickMATH: $(($currentLine+$chunkSize))" >> /data/project/checkpersondata/dumpScan.log 2>> /data/project/checkpersondata/dumpScan.log
	toLine=$(($currentLine+$chunkSize))
	sed -n "${currentLine},${toLine}p" dumpTitles.txt > tempDump.txt

        # sed -n '$currentLine,$(($currentLine+$chunkSize))p' dumpTitles.txt > tempDump.txt

	shuf tempDump.txt > var/testTidoni.txt
	perl bin/personendaten.pl -file >> /data/project/checkpersondata/dumpScan.log 2>> /data/project/checkpersondata/dumpScan.log

	currentLine=$(($currentLine+$chunkSize))
done



